package ch.vknws.vierkant.backend.command;

import java.io.InputStream;
import java.io.OutputStream;
import java.text.MessageFormat;

import org.apache.logging.log4j.Level;

import ch.bwe.faa.v1.core.service.ServiceRegistry;

/**
 * Environment for a command.
 * 
 * @author benjamin
 */
public abstract class CommandContext {

  /**
   * Logs with the FATAL level.
   * 
   * @param message   the log message
   * @param arguments the message format arguments
   */
  public final void fatal(String message, Object... arguments) {
    log(Level.FATAL, message, arguments);
  }

  /**
   * Logs with the FATAL level.
   * 
   * @param message   the log message
   * @param problem   the problem that occurred.
   * @param arguments the message format arguments
   */
  public final void fatal(String message, Throwable problem, Object... arguments) {
    log(Level.FATAL, message, problem, arguments);
  }

  /**
   * Logs with the ERROR level.
   * 
   * @param message   the log message
   * @param arguments the message format arguments
   */
  public final void error(String message, Object... arguments) {
    log(Level.ERROR, message, arguments);
  }

  /**
   * Logs with the ERROR level.
   * 
   * @param message   the log message
   * @param problem   the problem that occurred.
   * @param arguments the message format arguments
   */
  public final void error(String message, Throwable problem, Object... arguments) {
    log(Level.ERROR, message, problem, arguments);
  }

  /**
   * Logs with the WARN level.
   * 
   * @param message   the log message
   * @param arguments the message format arguments
   */
  public final void warn(String message, Object... arguments) {
    log(Level.WARN, message, arguments);
  }

  /**
   * Logs with the WARN level.
   * 
   * @param message   the log message
   * @param problem   the problem that occurred.
   * @param arguments the message format arguments
   */
  public final void warn(String message, Throwable problem, Object... arguments) {
    log(Level.WARN, message, problem, arguments);
  }

  /**
   * Logs with the INFO level.
   * 
   * @param message   the log message
   * @param arguments the message format arguments
   */
  public final void info(String message, Object... arguments) {
    log(Level.INFO, message, arguments);
  }

  /**
   * Logs with the DEBUG level.
   * 
   * @param message   the log message
   * @param arguments the message format arguments
   */
  public final void debug(String message, Object... arguments) {
    log(Level.DEBUG, message, arguments);
  }

  /**
   * Logs with the TRACE level.
   * 
   * @param message   the log message
   * @param arguments the message format arguments
   */
  public final void trace(String message, Object... arguments) {
    log(Level.TRACE, message, arguments);
  }

  /**
   * Logs a message.
   * 
   * @param level     the log level
   * @param message   the log message
   * @param arguments the message format arguments
   */
  public final void log(Level level, String message, Object... arguments) {
    log(level, message, null, arguments);
  }

  /**
   * Logs a message.
   * 
   * @param level     the log level
   * @param message   the log message
   * @param problem   the problem that occurred.
   * @param arguments the message format arguments
   */
  public abstract void log(Level level, String message, Throwable problem, Object... arguments);

  /**
   * Prints a message.
   * 
   * @param message   the message
   * @param arguments the message format arguments
   */
  public abstract void print(String message, Object... arguments);

  /**
   * @return the input stream to act on if necessary.
   */
  public abstract InputStream getInputStream();

  /**
   * @return the output stream to act on if necessary.
   */
  public abstract OutputStream getOutputStream();

  /**
   * Default command context.
   * 
   * @author benjamin
   */
  public static final class DefaultCommandContext extends CommandContext {

    @Override
    public void log(Level level, String message, Throwable problem, Object... arguments) {
      ServiceRegistry.getLogProxy().log(level, this, message, problem, arguments);
    }

    @Override
    public void print(String message, Object... arguments) {
      System.out.println(MessageFormat.format(message, arguments));
    }

    @Override
    public InputStream getInputStream() {
      return System.in;
    }

    @Override
    public OutputStream getOutputStream() {
      return System.out;
    }
  }
}
