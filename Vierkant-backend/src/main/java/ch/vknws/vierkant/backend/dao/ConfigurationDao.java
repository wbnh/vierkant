package ch.vknws.vierkant.backend.dao;

import java.util.List;

import ch.bwe.fac.v1.type.SaveException;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.ConjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.vknws.vierkant.type.ConfigurationModel;
import ch.vknws.vierkant.type.impl.ConfigurationPersistable;

/**
 * DAO for the LicenseModel.
 *
 * @author Benjamin Weber
 */
public class ConfigurationDao extends AbstractVierkantDao<ConfigurationModel> {

  /**
   * The business key of a configuration.
   * 
   * @author benjamin
   */
  public static class ConfigurationKey {
    private String userName;
    private String context;
    private String keyName;

    /**
     * Constructor handling initialization of mandatory fields.
     * 
     * @param userName the user for which the configuration applies
     * @param context  the context in which the configuration applies
     * @param keyName  the key of the configuration
     */
    public ConfigurationKey(String userName, String context, String keyName) {
      this.userName = userName;
      this.context = context;
      this.keyName = keyName;
    }

    /**
     * Returns the user for which the configuration applies.
     * 
     * @return the user.
     */
    public String getUserName() {
      return userName;
    }

    /**
     * Returns the context in which the configuration applies.
     * 
     * @return the context.
     */
    public String getContext() {
      return context;
    }

    /**
     * Returns the key of the configuration.
     * 
     * @return the key.
     */
    public String getKeyName() {
      return keyName;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((context == null) ? 0 : context.hashCode());
      result = prime * result + ((keyName == null) ? 0 : keyName.hashCode());
      result = prime * result + ((userName == null) ? 0 : userName.hashCode());
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) { return true; }
      if (obj == null) { return false; }
      if (!(obj instanceof ConfigurationKey)) { return false; }
      ConfigurationKey other = (ConfigurationKey) obj;
      if (context == null) {
        if (other.context != null) { return false; }
      } else if (!context.equals(other.context)) { return false; }
      if (keyName == null) {
        if (other.keyName != null) { return false; }
      } else if (!keyName.equals(other.keyName)) { return false; }
      if (userName == null) {
        if (other.userName != null) { return false; }
      } else if (!userName.equals(other.userName)) { return false; }
      return true;
    }
  }

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public ConfigurationDao() {
    super(ConfigurationPersistable.class);
  }

  /**
   * Loads the configuration with the passed business key.
   * 
   * @param businessKey the key of the configuration to load
   * @return the loaded configuration or null if it is not found.
   */
  public ConfigurationModel loadConfiguration(ConfigurationKey businessKey) {
    return loadConfiguration(businessKey.getUserName(), businessKey.getContext(), businessKey.getKeyName());
  }

  private ConfigurationModel loadConfiguration(String user, String context, String key) {
    AbstractRestriction restriction = new EqualityRestriction(ConfigurationModel.PropertyId.USER_NAME, user);
    restriction = new ConjunctionRestriction(restriction,
        new EqualityRestriction(ConfigurationModel.PropertyId.CONTEXT, context));
    restriction = new ConjunctionRestriction(restriction,
        new EqualityRestriction(ConfigurationModel.PropertyId.KEY_NAME, key));

    List<? extends ConfigurationModel> found = find(restriction, 0, 1);
    if (found.isEmpty()) return null;
    return found.get(0);
  }

  /**
   * Sets the value of a configuration.
   * 
   * @param businessKey the business key of the configuration to set
   * @param value       the value to set the configuration to
   * @param modifier    the person who performs the modification
   * @throws SaveException if we cannot save
   */
  public void setConfiguration(ConfigurationKey businessKey, String value, Integer modifier) throws SaveException {
    setConfiguration(businessKey.getUserName(), businessKey.getContext(), businessKey.getKeyName(), value, modifier);
  }

  private void setConfiguration(String user, String context, String key, String value, Integer modifier)
      throws SaveException {
    ConfigurationModel persistable = loadConfiguration(user, context, key);

    if (persistable == null) {
      persistable = new ConfigurationPersistable();
      persistable.setUserName(user);
      persistable.setContext(context);
      persistable.setKeyName(key);
    }
    persistable.setValue(value);

    save(persistable, modifier);
  }
}
