package ch.vknws.vierkant.backend.command;

/**
 * The helptext of a command.
 * 
 * @author benjamin
 */
public interface Helptext {

  /**
   * a general description of the command, describing what it does.
   */
  String GENERAL_DESCRIPTION = "generalDescription";

  /**
   * documentation of the parameters/flags and arguments.
   */
  String PARAMETERS = "parameters";

  /**
   * more information about the command, e.g., where to find more help.
   */
  String MORE_INFORMATION = "moreInformation";
}
