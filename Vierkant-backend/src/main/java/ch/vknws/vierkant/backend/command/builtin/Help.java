package ch.vknws.vierkant.backend.command.builtin;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ch.vknws.vierkant.backend.command.Command;
import ch.vknws.vierkant.backend.command.CommandContext;
import ch.vknws.vierkant.backend.command.CommandExecutionResult;
import ch.vknws.vierkant.backend.command.CommandExecutionResult.Level;
import ch.vknws.vierkant.backend.command.CommandExecutor;
import ch.vknws.vierkant.backend.command.CommandName;

/**
 * @author benjamin
 */
@CommandName("help")
public class Help implements Command {
  static {
    Class<?> clazz = Help.class;
    String name = clazz.getAnnotation(CommandName.class).value();
    CommandExecutor.registerCommand(name, clazz);
  }

  @Override
  public List<CommandExecutionResult> execute(Map<String, String> parameters, List<String> arguments,
      CommandContext context) {
    List<CommandExecutionResult> result = new LinkedList<>();
    if (arguments.size() < 1) {
      result.add(new CommandExecutionResult(Level.ERROR, "Must specify a command to display help about."));
      return result;
    }

    String commandForHelp = arguments.get(0);
    Command command = CommandExecutor.findCommand(commandForHelp);
    if (command == null) {
      result.add(new CommandExecutionResult(Level.ERROR, "No command found with name: " + commandForHelp));
      return result;
    }

    context.print(command.getHelptext());

    return result;
  }

}
