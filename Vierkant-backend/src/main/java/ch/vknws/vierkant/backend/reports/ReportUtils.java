package ch.vknws.vierkant.backend.reports;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;

import ch.vknws.vierkant.backend.configuration.ConfigurationUtils;
import ch.vknws.vierkant.backend.configuration.OrganisationConfigurations;
import ch.vknws.vierkant.backend.configuration.VierkantConfigurations;
import ch.vknws.vierkant.backend.dao.ConfigurationDao.ConfigurationKey;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.backend.dao.PersonDao;
import ch.vknws.vierkant.reports.ReportFactory;
import ch.vknws.vierkant.type.MemberCategoryType;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.SexType;
import net.sf.jasperreports.engine.JRException;

/**
 * Utility for reports.
 *
 * @author Benjamin Weber
 */
public class ReportUtils {

  private static Path tempFileLocation = Paths
      .get(ConfigurationUtils.getConfiguration(VierkantConfigurations.FILE_BASE_DIRECTORY))
      .resolve(ConfigurationUtils.getConfiguration(VierkantConfigurations.FILE_TEMP_DIRECTORY));
  private static int eligibilityAge = Integer
      .parseInt(ConfigurationUtils.getConfiguration(OrganisationConfigurations.ELIGIBILITY_AGE));
  private static String eligibilityHeight = ConfigurationUtils
      .getConfiguration(OrganisationConfigurations.ELIGIBILITY_HEIGHT);

  /**
   * Creates a PDF document containing a letter informing the recipient of a
   * mutation about themselves.
   * 
   * @param sender                  the person who sends/writes the letter
   * @param recipient               the person that was mutated and receives the
   *                                letter
   * @param formerCategory          the member category before the mutation
   * @param currentCategory         the member category after the mutation
   * @param boardOfDirectorsMeeting the date of the relevant meeting of the board
   *                                of directors
   * @param generalAssamblyDate     the date of the relevant general assembly
   * @return the path to the (temporarily) generated letter
   * @throws JRException if we could not generate the letter
   * @throws IOException if we could not save the letter
   */
  public static Path createMutationLetter(PersonModel sender, PersonModel recipient, MemberCategoryType formerCategory,
      MemberCategoryType currentCategory, LocalDate boardOfDirectorsMeeting, LocalDate generalAssamblyDate)
      throws JRException, IOException {
    final String mutationType = getMutationType(formerCategory, currentCategory, recipient);

    Path letter = tempFileLocation.resolve(getFileName(recipient, mutationType));
    PersonDao dao = DaoFactory.getInstance().getPersonDao();

    ReportFactory.createLetter(sender, recipient, getContentTitle(recipient, mutationType), getContentText(recipient,
        mutationType, boardOfDirectorsMeeting, dao.getAspirantApplicationDate(recipient.getRecordId()),
        dao.getActiveApplicationDate(recipient.getRecordId()), dao.getPassiveApplicationDate(recipient.getRecordId()),
        dao.getVkBrevetDate(recipient.getRecordId()), generalAssamblyDate),
        getGreetingForPerson(recipient, mutationType), getSignature(recipient, mutationType), letter);

    return letter;
  }

  private static String getMutationType(MemberCategoryType source, MemberCategoryType target, PersonModel mutated) {
    StringBuilder builder = new StringBuilder();

    builder.append("-");
    if (source == null) {
      builder.append("NULL");
    } else {
      builder.append(source.name());
    }
    builder.append("-");
    if (target == null) {
      builder.append("NULL");
    } else {
      builder.append(target.name());
    }
    builder.append("-");

    if (source == null && MemberCategoryType.INTERESTED.equals(target)) {
      SexType sex = mutated.getSex();
      LocalDate birthdate = mutated.getBirthdate();

      if (SexType.NON_HUMAN.equals(sex)) {
        builder.append("institution");
      } else {
        if (birthdate.isAfter(LocalDate.now().minusYears(18))) {
          builder.append("underage_");
          if (birthdate.isBefore(LocalDate.now().minusYears(eligibilityAge))) {
            builder.append("eligible");
          } else {
            builder.append("ineligible");
          }
        } else {
          builder.append("of_age");
        }
      }

      builder.append("-");
    }

    return builder.toString();
  }

  private static String getFileName(PersonModel mutated, String mutationType) {
    StringBuilder builder = new StringBuilder();
    builder.append("Mutation");
    builder.append(mutationType.toLowerCase());
    builder.append(mutated.getGivenName());
    builder.append(mutated.getFamilyName());
    builder.append(".pdf");

    return builder.toString();
  }

  private static int getSexIndex(PersonModel person) {
    SexType sex = person.getSex();

    if (sex != null) {
      switch (sex) {
      case MALE:
        return 0;
      case FEMALE:
        return 1;
      case NON_HUMAN:
        return 2;
      }
    }

    return 2;
  }

  private static String getContentText(PersonModel mutated, String mutationType, LocalDate boardOfDirectorsMeeting,
      LocalDate aspirantApplication, LocalDate activeApplication, LocalDate passiveApplication, LocalDate vkBrevet,
      LocalDate generalAssembly) {
    String key = "mutation" + mutationType + "Text";

    // Text Parameters
    // {1} Date of meeting of board of directors
    // {2} Date of application as aspirant
    // {3} Date of application as active
    // {4} Date of application as passive
    // {5} Date of brevet to VK
    // {6} Date of GV
    // {7} Age of eligibility
    // {8} Height of eligibility
    return ConfigurationUtils.getConfiguration(new ConfigurationKey("sys", "letter", key), getSexIndex(mutated),
        boardOfDirectorsMeeting == null ? null : VierkantConfigurations.DATE_FORMAT.format(boardOfDirectorsMeeting),
        aspirantApplication == null ? null : VierkantConfigurations.DATE_FORMAT.format(aspirantApplication),
        activeApplication == null ? null : VierkantConfigurations.DATE_FORMAT.format(activeApplication),
        passiveApplication == null ? null : VierkantConfigurations.DATE_FORMAT.format(passiveApplication),
        vkBrevet == null ? null : VierkantConfigurations.DATE_FORMAT.format(vkBrevet),
        generalAssembly == null ? null : VierkantConfigurations.DATE_FORMAT.format(generalAssembly), eligibilityAge,
        eligibilityHeight);
  }

  private static String getContentTitle(PersonModel mutated, String mutationType) {
    String key = "mutation" + mutationType + "Header";

    return ConfigurationUtils.getConfiguration(new ConfigurationKey("sys", "letter", key), getSexIndex(mutated));
  }

  private static String getSignature(PersonModel mutated, String mutationType) {
    String key = "mutation" + mutationType + "Signature";

    return ConfigurationUtils.getConfiguration(new ConfigurationKey("sys", "letter", key), getSexIndex(mutated));
  }

  private static String getGreetingForPerson(PersonModel person, String mutationType) {
    String key = "mutation" + mutationType + "Greeting";

    return ConfigurationUtils.getConfiguration(new ConfigurationKey("sys", "letter", key), getSexIndex(person),
        person.getGivenName(), person.getFamilyName());
  }
}
