package ch.vknws.vierkant.backend.dao;

import java.util.HashMap;
import java.util.Map;

import ch.bwe.fac.v1.database.AbstractDatabaseDAO;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.vknws.vierkant.type.AargauLicenseModel;
import ch.vknws.vierkant.type.AddressModel;
import ch.vknws.vierkant.type.AuthorizationKeyModel;
import ch.vknws.vierkant.type.ConfigurationModel;
import ch.vknws.vierkant.type.ContactPersonModel;
import ch.vknws.vierkant.type.FunctionGroupModel;
import ch.vknws.vierkant.type.MeetingModel;
import ch.vknws.vierkant.type.MeetingTypeModel;
import ch.vknws.vierkant.type.MemberGroupModel;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.PersonFunctionModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.RankModel;
import ch.vknws.vierkant.type.TrainingCourseInstanceModel;
import ch.vknws.vierkant.type.TrainingCourseModel;

/**
 * @author Benjamin Weber
 */
public class DaoFactory {

  private static DaoFactory instance = new DaoFactory();

  /**
   * Returns the singleton instance.
   * 
   * @return the singleton instance.
   */
  public static DaoFactory getInstance() {
    return instance;
  }

  @SuppressWarnings("rawtypes")
  private Map<Class, AbstractDatabaseDAO> daos = new HashMap<>();

  /**
   * Constructor handling initialization of mandatory fields.
   */
  private DaoFactory() {
  }

  /**
   * Finds the associated dao for the passed type.
   * 
   * @param modelClass the type of dao to find
   * @return the dao or <code>null</code> if none could be found.
   */
  public AbstractDatabaseDAO<?> findDaoForType(Class<? extends DatabaseModel> modelClass) {
    return daos.get(modelClass);
  }

  /**
   * @return the DAO for persons.
   */
  public PersonDao getPersonDao() {
    if (!daos.containsKey(PersonModel.class)) {
      daos.put(PersonModel.class, new PersonDao());
    }

    return (PersonDao) daos.get(PersonModel.class);
  }

  /**
   * @return the DAO for ranks.
   */
  public RankDao getRankDao() {
    if (!daos.containsKey(RankModel.class)) {
      daos.put(RankModel.class, new RankDao());
    }

    return (RankDao) daos.get(RankModel.class);
  }

  /**
   * @return the addressDao
   */
  public AddressDao getAddressDao() {
    if (!daos.containsKey(AddressModel.class)) {
      daos.put(AddressModel.class, new AddressDao());
    }

    return (AddressDao) daos.get(AddressModel.class);
  }

  /**
   * @return the aargauLicenseDao
   */
  public AargauLicenseDao getAargauLicenseDao() {
    if (!daos.containsKey(AargauLicenseModel.class)) {
      daos.put(AargauLicenseModel.class, new AargauLicenseDao());
    }

    return (AargauLicenseDao) daos.get(AargauLicenseModel.class);
  }

  /**
   * @return the memberCategoryDao
   */
  public MemberCategoryDao getMemberCategoryDao() {
    if (!daos.containsKey(MemberGroupModel.class)) {
      daos.put(MemberGroupModel.class, new MemberCategoryDao());
    }

    return (MemberCategoryDao) daos.get(MemberGroupModel.class);
  }

  /**
   * @return the contactPersonDao
   */
  public ContactPersonDao getContactPersonDao() {
    if (!daos.containsKey(ContactPersonModel.class)) {
      daos.put(ContactPersonModel.class, new ContactPersonDao());
    }

    return (ContactPersonDao) daos.get(ContactPersonModel.class);
  }

  /**
   * @return the organizationFunctionDao
   */
  public OrganizationFunctionDao getOrganizationFunctionDao() {
    if (!daos.containsKey(OrganizationFunctionModel.class)) {
      daos.put(OrganizationFunctionModel.class, new OrganizationFunctionDao());
    }

    return (OrganizationFunctionDao) daos.get(OrganizationFunctionModel.class);
  }

  /**
   * @return the configurationDao
   */
  public ConfigurationDao getConfigurationDao() {
    if (!daos.containsKey(ConfigurationModel.class)) {
      daos.put(ConfigurationModel.class, new ConfigurationDao());
    }

    return (ConfigurationDao) daos.get(ConfigurationModel.class);
  }

  /**
   * @return the trainingCourseDao
   */
  public TrainingCourseDao getTrainingCourseDao() {
    if (!daos.containsKey(TrainingCourseModel.class)) {
      daos.put(TrainingCourseModel.class, new TrainingCourseDao());
    }

    return (TrainingCourseDao) daos.get(TrainingCourseModel.class);
  }

  /**
   * @return the trainingCourseInstanceDao
   */
  public TrainingCourseInstanceDao getTrainingCourseInstanceDao() {
    if (!daos.containsKey(TrainingCourseInstanceModel.class)) {
      daos.put(TrainingCourseInstanceModel.class, new TrainingCourseInstanceDao());
    }

    return (TrainingCourseInstanceDao) daos.get(TrainingCourseInstanceModel.class);
  }

  /**
   * @return the functionGroupDao
   */
  public FunctionGroupDao getFunctionGroupDao() {
    if (!daos.containsKey(FunctionGroupModel.class)) {
      daos.put(FunctionGroupModel.class, new FunctionGroupDao());
    }

    return (FunctionGroupDao) daos.get(FunctionGroupModel.class);
  }

  /**
   * @return the personFunctionDao
   */
  public PersonFunctionDao getPersonFunctionDao() {
    if (!daos.containsKey(PersonFunctionModel.class)) {
      daos.put(PersonFunctionModel.class, new PersonFunctionDao());
    }

    return (PersonFunctionDao) daos.get(PersonFunctionModel.class);
  }

  /**
   * @return the authorizationKeyDao
   */
  public AuthorizationKeyDao getAuthorizationKeyDao() {
    if (!daos.containsKey(AuthorizationKeyModel.class)) {
      daos.put(AuthorizationKeyModel.class, new AuthorizationKeyDao());
    }

    return (AuthorizationKeyDao) daos.get(AuthorizationKeyModel.class);
  }

  /**
   * @return the meetingTypeDao
   */
  public MeetingTypeDao getMeetingTypeDao() {
    if (!daos.containsKey(MeetingTypeModel.class)) {
      daos.put(MeetingTypeModel.class, new MeetingTypeDao());
    }

    return (MeetingTypeDao) daos.get(MeetingTypeModel.class);
  }

  /**
   * @return the meetingDao
   */
  public MeetingDao getMeetingDao() {
    if (!daos.containsKey(MeetingModel.class)) {
      daos.put(MeetingModel.class, new MeetingDao());
    }

    return (MeetingDao) daos.get(MeetingModel.class);
  }
}
