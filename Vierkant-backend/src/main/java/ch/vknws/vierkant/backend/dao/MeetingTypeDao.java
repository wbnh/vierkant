package ch.vknws.vierkant.backend.dao;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ch.vknws.vierkant.type.MeetingTypeModel;
import ch.vknws.vierkant.type.impl.MeetingPersistable;
import ch.vknws.vierkant.type.impl.MeetingTypePersistable;

/**
 * DAO for the AddressModel.
 *
 * @author Benjamin Weber
 */
public class MeetingTypeDao extends AbstractVierkantDao<MeetingTypeModel> {

  /**
   * Constructor handling initialization of mandatory fields.
   */
  protected MeetingTypeDao() {
    super(MeetingTypePersistable.class);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isDeletable(Integer recordId) {
    EntityManager entityManager = getEntityManagerFactory().createEntityManager();

    try {

      CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      CriteriaQuery<Long> query = builder.createQuery(Long.class);

      Root<MeetingPersistable> personRoot = query.from(MeetingPersistable.class);
      query.select(builder.count(personRoot));
      query.where(builder.equal(personRoot.get(MeetingPersistable.PropertyId.REF_MEETING_TYPE), recordId));

      Long count = entityManager.createQuery(query).getSingleResult();

      return count == 0L;

    } finally {
      entityManager.close();
    }
  }

}
