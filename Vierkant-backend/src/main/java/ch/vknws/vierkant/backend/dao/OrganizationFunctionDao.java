package ch.vknws.vierkant.backend.dao;

import java.util.List;

import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.vknws.vierkant.backend.configuration.VierkantConfigurations;
import ch.vknws.vierkant.type.ConfigurationModel;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.impl.OrganizationFunctionPersistable;

/**
 * DAO for the LicenseModel.
 *
 * @author Benjamin Weber
 */
public class OrganizationFunctionDao extends AbstractVierkantDao<OrganizationFunctionModel> {

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public OrganizationFunctionDao() {
    super(OrganizationFunctionPersistable.class);
  }

  /**
   * Loads the function by name.
   * 
   * @param name the function name
   * @return the function
   */
  public OrganizationFunctionModel loadByName(String name) {
    AbstractRestriction restriction = new EqualityRestriction(OrganizationFunctionModel.PropertyId.FUNCTION_NAME, name);
    List<OrganizationFunctionModel> found = find(restriction, 0, 1);
    if (found == null || found.isEmpty()) { return null; }
    return found.get(0);
  }

  /**
   * @return the secretary function
   */
  public OrganizationFunctionModel loadSecretary() {
    ConfigurationModel configuration = DaoFactory.getInstance().getConfigurationDao()
        .loadConfiguration(VierkantConfigurations.SECRETARY_ID);
    Integer id = Integer.valueOf(configuration.getValue());

    return load(id);
  }

}
