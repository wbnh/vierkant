package ch.vknws.vierkant.backend.command;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.util.AssertUtils;

/**
 * Executes commands.
 * 
 * @author benjamin
 */
public class CommandExecutor {

  /**
   * The registered commands.
   */
  private static Map<String, Class<?>> registeredCommands = new HashMap<>();

  /**
   * The context.
   */
  private static CommandContext context = new CommandContext.DefaultCommandContext();

  /**
   * Registers a command on the executor.
   * 
   * @param name  the command name
   * @param clazz the command class
   */
  public static void registerCommand(String name, Class<?> clazz) {
    if (registeredCommands.containsKey(
        name)) { throw new IllegalArgumentException("A command with the name '" + name + "' is already registered."); }
    registeredCommands.put(name, clazz);
  }

  /**
   * Removes a command from the executor.
   * 
   * @param name the command name
   */
  public static void unregisterCommand(String name) {
    registeredCommands.remove(name);
  }

  /**
   * Executes the passed command.
   * 
   * @param rawLine the raw line to process
   * @return the result.
   */
  public static List<CommandExecutionResult> executeCommand(String rawLine) {
    throw new NotImplementedException();
  }

  /**
   * Executes the passed command.
   * 
   * @param command    the command to execute
   * @param parameters the parameters to the command
   * @param arguments  the arguments
   * @return the result.
   */
  public static List<CommandExecutionResult> executeCommand(Command command, Map<String, String> parameters,
      List<String> arguments) {
    return executeCommand(command, parameters, arguments, context);
  }

  /**
   * Executes the passed command.
   * 
   * @param command    the command to execute
   * @param parameters the parameters to the command
   * @param arguments  the arguments
   * @param context    the command context
   * @return the result.
   */
  public static List<CommandExecutionResult> executeCommand(Command command, Map<String, String> parameters,
      List<String> arguments, CommandContext context) {
    AssertUtils.notNull(command);
    AssertUtils.notNull(parameters);
    AssertUtils.notNull(arguments);
    AssertUtils.notNull(context);

    return command.execute(parameters, arguments, context);
  }

  /**
   * Attempts to find the command with the passed name.
   * 
   * @param name the name of the command
   * @return the found command or <code>null</code> if it could not be found.
   */
  public static Command findCommand(String name) {
    Class<?> clazz = registeredCommands.get(name);
    try {
      return (Command) clazz.getConstructor().newInstance();
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
        | NoSuchMethodException | SecurityException e) {
      ServiceRegistry.getLogProxy().error(CommandExecutor.class, "Could not initialise command", e);
      return null;
    }
  }
}
