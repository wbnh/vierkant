package ch.vknws.vierkant.backend.dao;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import ch.bwe.faa.v1.core.util.AssertUtils;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.vknws.vierkant.rules.module.secretary.RankModificationUtil;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.PersonStatus;
import ch.vknws.vierkant.type.impl.PersonPersistable;

/**
 * DAO for PersonModel.
 *
 * @author Benjamin Weber
 */
public class PersonDao extends AbstractVierkantDao<PersonModel> {

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public PersonDao() {
    super(PersonPersistable.class);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isDeletable(Integer recordId) {
    PersonModel person = load(recordId);
    return person != null && person.getMemberCategory() == null;
  }

  /**
   * Looks for a person with the passed shorthand.
   * 
   * @param shorthand the person's shorthand
   * @return the person or null if it was not found.
   */
  public PersonModel loadByShorthand(String shorthand) {
    AbstractRestriction restriction = new EqualityRestriction("shorthand", shorthand);

    List<? extends PersonModel> found = find(restriction, 0, 1);
    if (found == null || found.isEmpty()) { return null; }
    return found.get(0);
  }

  /**
   * Loads persons by their primary function.
   * 
   * @param function the function the persons should have
   * @return the persons with the passed primary function
   */
  public List<PersonModel> loadByPrimaryFunction(OrganizationFunctionModel function) {
    AbstractRestriction restriction = new EqualityRestriction(PersonModel.PropertyId.REF_PRIMARY_FUNCTION, function);

    return find(restriction, 0, Integer.MAX_VALUE);
  }

  /**
   * Returns all the members of the group the passed person is also in (including
   * the person themselves).
   * 
   * @param member the member to determine the group
   * @return all the group members, but never null.
   */
  public List<PersonModel> getMemberGroupMembers(PersonModel member) {
    AssertUtils.notNull(member);
    AssertUtils.notNull(member.getRefMemberGroup());

    AbstractRestriction restriction = new EqualityRestriction("refMemberGroup",
        member.getRefMemberGroup().getRecordId());

    List<? extends PersonModel> found = find(restriction, 0, Integer.MAX_VALUE);
    List<PersonModel> result = new LinkedList<>(found);
    return result;
  }

  /**
   * Gets the date of application for aspirant for the passed person.
   * 
   * @param recordId the ID of the person to check
   * @return the application date or <code>null</code> if no application has
   *         happened
   */
  public LocalDate getAspirantApplicationDate(Integer recordId) {
    Set<PersonStatus> set = new HashSet<>();
    set.add(PersonStatus.APPLIED_FOR_ASPIRANT);

    return findDateForAnyStatus(recordId, set);
  }

  /**
   * Gets the date of application for VK for the passed person.
   * 
   * @param recordId the ID of the person to check
   * @return the application date or <code>null</code> if no application has
   *         happened
   */
  public LocalDate getVkApplicationDate(Integer recordId) {
    Set<PersonStatus> set = new HashSet<>();
    set.add(PersonStatus.APPLIED_FOR_VK);

    return findDateForAnyStatus(recordId, set);
  }

  /**
   * Gets the date of application for non-commissioned officer for the passed
   * person.
   * 
   * @param recordId the ID of the person to check
   * @return the application date or <code>null</code> if no application has
   *         happened
   */
  public LocalDate getNcOfApplicationDate(Integer recordId) {
    Set<PersonStatus> set = new HashSet<>();
    set.add(PersonStatus.APPLIED_FOR_NC_OF);

    return findDateForAnyStatus(recordId, set);
  }

  /**
   * Gets the date of application for becoming an active member for the passed
   * person.
   * 
   * @param recordId the ID of the person to check
   * @return the application date or <code>null</code> if no application has
   *         happened
   */
  public LocalDate getActiveApplicationDate(Integer recordId) {
    Set<PersonStatus> set = new HashSet<>();
    set.add(PersonStatus.APPLIED_FOR_ASPIRANT);
    set.add(PersonStatus.APPLIED_FOR_VK);
    set.add(PersonStatus.APPLIED_FOR_NC_OF);
    set.add(PersonStatus.APPLIED_FOR_OF);

    return findDateForAnyStatus(recordId, set);
  }

  /**
   * Gets the date of application for becoming a passive member for the passed
   * person.
   * 
   * @param recordId the ID of the person to check
   * @return the application date or <code>null</code> if no application has
   *         happened
   */
  public LocalDate getPassiveApplicationDate(Integer recordId) {
    Set<PersonStatus> set = new HashSet<>();
    set.add(PersonStatus.APPLIED_FOR_PASSIVE);

    return findDateForAnyStatus(recordId, set);
  }

  /**
   * Gets the date of application for officer for the passed person.
   * 
   * @param recordId the ID of the person to check
   * @return the application date or <code>null</code> if no application has
   *         happened
   */
  public LocalDate getOfApplicationDate(Integer recordId) {
    Set<PersonStatus> set = new HashSet<>();
    set.add(PersonStatus.APPLIED_FOR_OF);

    return findDateForAnyStatus(recordId, set);
  }

  /**
   * Gets the date of VK brevet for the passed person.
   * 
   * @param recordId the ID of the person to check
   * @return the application date or <code>null</code> if no application has
   *         happened
   */
  public LocalDate getVkBrevetDate(Integer recordId) {
    List<Integer> revisionIds = loadEnversRevisionIds(recordId);

    // searching backwards in the history to only consider the latest application
    Collections.reverse(revisionIds);

    boolean lastVkState = false;
    for (Integer revision : revisionIds) {
      PersonModel historyModel = loadHistoryEnvers(recordId, revision);
      if (historyModel == null) {
        continue;
      }
      boolean currentVkState = historyModel.getRefRank() != null
          && RankModificationUtil.VK.equals(historyModel.getRefRank().getAbbreviation());

      if (lastVkState && !currentVkState) { return historyModel.getModificationDateTime().toLocalDate(); }

      lastVkState = currentVkState;
    }

    return null;
  }

  private LocalDate findDateForAnyStatus(Integer recordId, Set<PersonStatus> status) {
    List<Integer> revisionIds = loadEnversRevisionIds(recordId);

    // searching backwards in the history to only consider the latest application
    Collections.reverse(revisionIds);

    boolean lastApplicationState = false;
    for (Integer revision : revisionIds) {
      PersonModel historyModel = loadHistoryEnvers(recordId, revision);
      if (historyModel == null) {
        continue;
      }
      boolean currentApplicationState = status.contains(historyModel.getStatus());

      if (lastApplicationState
          && !currentApplicationState) { return historyModel.getModificationDateTime().toLocalDate(); }

      lastApplicationState = currentApplicationState;
    }
    return null;
  }

}
