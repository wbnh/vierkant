package ch.vknws.vierkant.backend.dao;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.AbstractReferencedDatabaseDAO;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.vknws.vierkant.backend.configuration.EnvironmentConfigurations;
import ch.vknws.vierkant.rules.RuleRegistry;
import ch.vknws.vierkant.rules.ValidationMessage;
import ch.vknws.vierkant.rules.ValidationResult;

/**
 * DAO for the ContactPerson.
 *
 * @author Benjamin Weber
 * @param <T> the class in which the model is referenced
 * @param <U> the class of the model
 */
public class AbstractVierkantReferencedDao<T extends DatabaseModel, U extends DatabaseModel>
    extends AbstractReferencedDatabaseDAO<T, U> {

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param rootClass          the class in which the model is referenced
   * @param referenceAttribute the attribute referencing the model
   * @param persistableClass   the class of the model
   */
  protected AbstractVierkantReferencedDao(Class<? extends T> rootClass, String referenceAttribute,
      Class<? extends U> persistableClass) {
    super(rootClass, referenceAttribute, persistableClass, EnvironmentConfigurations.getPersistenceUnit());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected boolean isValid(U persistable) {
    ValidationResult result = RuleRegistry.validateModel(persistable);
    List<ValidationMessage> messages = result.getMessages();
    for (Iterator<ValidationMessage> iterator = messages.iterator(); iterator.hasNext();) {
      ValidationMessage message = iterator.next();
      if (DatabaseModel.PropertyId.CREATOR.equals(message.getField())) {
        iterator.remove();
      }
    }

    if (!messages.isEmpty()) {
      List<String> problems = new LinkedList<>();
      for (var message : messages) {
        problems.add(message.getField() + ":" + message.getType().name());
      }
      ServiceRegistry.getLogProxy().info(this, "Validation problems: {0}", problems);
    }
    return messages.isEmpty();
  }

}
