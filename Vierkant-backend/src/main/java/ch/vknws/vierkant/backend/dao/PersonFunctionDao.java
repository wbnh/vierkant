package ch.vknws.vierkant.backend.dao;

import ch.vknws.vierkant.type.PersonFunctionModel;
import ch.vknws.vierkant.type.impl.PersonFunctionPersistable;

/**
 * DAO for the AddressModel.
 *
 * @author Benjamin Weber
 */
public class PersonFunctionDao extends AbstractVierkantDao<PersonFunctionModel> {

  /**
   * Constructor handling initialization of mandatory fields.
   */
  protected PersonFunctionDao() {
    super(PersonFunctionPersistable.class);
  }
}
