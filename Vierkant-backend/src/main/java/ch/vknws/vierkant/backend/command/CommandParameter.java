package ch.vknws.vierkant.backend.command;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A parameter to a command.
 * 
 * @author benjamin
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(CommandParameters.class)
public @interface CommandParameter {
  /**
   * @return the name of the parameter
   */
  String value();

  /**
   * @return whether it is a flag (i. e., has no value)
   */
  boolean flag() default true;

  /**
   * @return whether the parameter is optional
   */
  boolean optional() default true;

  /**
   * @return the alias names
   */
  String[] aliases() default {};
}
