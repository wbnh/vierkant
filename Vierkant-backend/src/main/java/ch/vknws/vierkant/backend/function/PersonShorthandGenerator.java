package ch.vknws.vierkant.backend.function;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.List;

import ch.bwe.fac.v1.type.restriction.ConjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.PersonModel;

/**
 * Generator for shortcuts for people.
 *
 * @author Benjamin Weber
 */
public class PersonShorthandGenerator {

  static List<String> testTabooShorthands;

  /**
   * Generates the default shorthand for the passed person.
   * 
   * @param person the person
   * @return the new shorthand
   */
  public static String generate(PersonModel person) {
    if (person == null) { throw new IllegalArgumentException("Person must not be null"); }
    if (person.getGivenName() == null || person.getFamilyName() == null) { return ""; }

    String shorthand;
    int iteration = 0;
    do {
      shorthand = generate0(person, iteration);

      iteration++;
    } while (shorthandExists(shorthand));

    return shorthand;
  }

  private static String normalise(String string) {
    string = Normalizer.normalize(string, Form.NFD);
    string = string.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");

    return string;
  }

  private static String generate0(PersonModel person, int iteration) {
    StringBuilder builder = new StringBuilder();

    String givenName = normalise(person.getGivenName());
    String familyName = normalise(person.getFamilyName());
    if (familyName.contains(" ")) {
      String[] split = familyName.split("\\s");
      familyName = split[split.length - 1];
    }

    builder.append(givenName.substring(0, 1));

    int index = 1 + iteration / 2;

    if (iteration == 0 || iteration % 2 == 0) {
      builder.append(givenName.substring(1, 2));
    } else {
      builder.append(givenName.substring(index, index + 1));
    }

    builder.append(familyName.substring(0, 1));

    if (iteration == 0 || iteration % 2 != 0) {
      builder.append(familyName.substring(1, 2));
    } else {
      builder.append(familyName.substring(index, index + 1));
    }

    return builder.toString();
  }

  private static boolean shorthandExists(String shorthand) {
    if (testTabooShorthands != null) { return testTabooShorthands.contains(shorthand); }

    ConjunctionRestriction restriction = new ConjunctionRestriction(new EqualityRestriction("shorthand", shorthand),
        new EqualityRestriction("deletedFlag", 0));

    List<? extends PersonModel> found = DaoFactory.getInstance().getPersonDao().find(restriction, 0, Integer.MAX_VALUE);

    return !found.isEmpty();
  }

  /**
   * Set a shorthand if it is possible and none is set yet.
   * 
   * @param person the person to generate for.
   */
  public static void maybeGenerateShorthand(PersonModel person) {
    if (person != null && person.getGivenName() != null && person.getFamilyName() != null
        && (person.getShorthand() == null || person.getShorthand().isEmpty())) {

      String shorthand = PersonShorthandGenerator.generate(person);
      person.setShorthand(shorthand);
    }
  }
}
