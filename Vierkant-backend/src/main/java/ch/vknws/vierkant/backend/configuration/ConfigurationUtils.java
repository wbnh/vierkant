package ch.vknws.vierkant.backend.configuration;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import ch.bwe.fac.v1.type.DeleteException;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.backend.dao.ConfigurationDao.ConfigurationKey;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.ConfigurationModel;

/**
 * Util to load configurations.
 *
 * @author Benjamin Weber
 */
public class ConfigurationUtils {

  private static Map<ConfigurationKey, String> cache = new HashMap<>();
  private static ReentrantReadWriteLock mutex = new ReentrantReadWriteLock();

  /**
   * Gets a configuration from the cache or database.
   * 
   * @param key       the key of the config
   * @param arguments the arguments to format the value with
   * @return the formatted value
   */
  public static String getConfiguration(ConfigurationKey key, Object... arguments) {
    boolean contained;
    String value = null;

    ReadLock lock = mutex.readLock();
    lock.lock();
    try {
      contained = cache.containsKey(key);
      if (contained) {
        value = cache.get(key);
      }
    } finally {
      lock.unlock();
    }

    if (contained) { return format(value, arguments); }
    return loadConfiguration(key, arguments);
  }

  /**
   * Loads a configuration, forces a load from the database.
   * 
   * @param key       the key to identify
   * @param arguments the values to format with
   * @return the formatted configuration value
   */
  public static String loadConfiguration(ConfigurationKey key, Object... arguments) {
    ConfigurationModel configuration = DaoFactory.getInstance().getConfigurationDao().loadConfiguration(key);
    String value = null;
    if (configuration != null) {
      value = configuration.getValue();
    }

    WriteLock writeLock = mutex.writeLock();
    writeLock.lock();
    try {
      if (configuration != null) {
        cache.put(key, value);
      } else {
        cache.put(key, null);
      }
    } finally {
      writeLock.unlock();
    }

    return format(value, arguments);
  }

  /**
   * Deletes a configuration from the database.
   * 
   * @param key the key to identify
   * @throws DeleteException if we could not delete
   */
  public static void removeConfiguration(ConfigurationKey key) throws DeleteException {
    WriteLock writeLock = mutex.writeLock();

    writeLock.lock();
    try {
      ConfigurationModel configuration = DaoFactory.getInstance().getConfigurationDao().loadConfiguration(key);
      if (configuration != null) {
        DaoFactory.getInstance().getConfigurationDao().remove(configuration.getRecordId());
      }
      cache.remove(key);
    } finally {
      writeLock.unlock();
    }
  }

  /**
   * Updates the value of a configuration.
   * 
   * @param key      the key to identify
   * @param value    the new value
   * @param modifier the user who modified
   * @throws SaveException if we could not persist the change
   */
  public static void setConfiguration(ConfigurationKey key, String value, Integer modifier) throws SaveException {
    WriteLock writeLock = mutex.writeLock();

    writeLock.lock();
    try {
      DaoFactory.getInstance().getConfigurationDao().setConfiguration(key, value, modifier);
      cache.put(key, value);
    } finally {
      writeLock.unlock();
    }
  }

  /**
   * Removes all values from the cache and forces subsequent reloads.
   */
  public static void clearCache() {
    WriteLock writeLock = mutex.writeLock();

    writeLock.lock();
    try {
      cache.clear();
    } finally {
      writeLock.unlock();
    }
  }

  private static String format(String value, Object... arguments) {
    if (value == null) return null;
    return MessageFormat.format(value, arguments);
  }
}
