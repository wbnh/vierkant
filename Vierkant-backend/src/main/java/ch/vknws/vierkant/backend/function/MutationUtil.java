package ch.vknws.vierkant.backend.function;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.util.Pair;
import ch.vknws.vierkant.rules.module.secretary.MutationAction;
import ch.vknws.vierkant.type.MemberCategoryType;

/**
 * Utility class to help with member group modification.
 *
 * @author Benjamin Weber
 */
public class MutationUtil {

  /**
   * The possible mutations.
   */
  public static final Map<MemberCategoryType, Set<MemberCategoryType>> MUTATIONS;
  static {
    Map<MemberCategoryType, Set<MemberCategoryType>> values = new HashMap<>();
    Set<MemberCategoryType> interested = new LinkedHashSet<>();
    interested.add(MemberCategoryType.ASPIRANT);
    interested.add(MemberCategoryType.ACTIVE);
    interested.add(MemberCategoryType.PASSIVE_SINGLE);
    interested.add(MemberCategoryType.PASSIVE_COLLECTIVE);
    interested.add(MemberCategoryType.PASSIVE_FAMILY);
    values.put(MemberCategoryType.INTERESTED, Collections.unmodifiableSet(interested));

    Set<MemberCategoryType> aspirant = new LinkedHashSet<>();
    aspirant.add(MemberCategoryType.ACTIVE);
    aspirant.add(MemberCategoryType.PASSIVE_SINGLE);
    aspirant.add(MemberCategoryType.PASSIVE_COLLECTIVE);
    aspirant.add(MemberCategoryType.PASSIVE_FAMILY);
    values.put(MemberCategoryType.ASPIRANT, Collections.unmodifiableSet(aspirant));

    Set<MemberCategoryType> active = new LinkedHashSet<>();
    active.add(MemberCategoryType.PASSIVE_SINGLE);
    active.add(MemberCategoryType.PASSIVE_COLLECTIVE);
    active.add(MemberCategoryType.PASSIVE_FAMILY);
    active.add(MemberCategoryType.HONORARY_ACTIVE);
    values.put(MemberCategoryType.ACTIVE, Collections.unmodifiableSet(active));

    Set<MemberCategoryType> passiveSingle = new LinkedHashSet<>();
    passiveSingle.add(MemberCategoryType.ASPIRANT);
    passiveSingle.add(MemberCategoryType.ACTIVE);
    passiveSingle.add(MemberCategoryType.PASSIVE_COLLECTIVE);
    passiveSingle.add(MemberCategoryType.PASSIVE_FAMILY);
    passiveSingle.add(MemberCategoryType.HONORARY_PASSIVE);
    values.put(MemberCategoryType.PASSIVE_SINGLE, Collections.unmodifiableSet(passiveSingle));

    Set<MemberCategoryType> passiveCollective = new LinkedHashSet<>();
    passiveCollective.add(MemberCategoryType.ASPIRANT);
    passiveCollective.add(MemberCategoryType.ACTIVE);
    passiveCollective.add(MemberCategoryType.PASSIVE_SINGLE);
    passiveCollective.add(MemberCategoryType.PASSIVE_FAMILY);
    passiveCollective.add(MemberCategoryType.HONORARY_PASSIVE);
    values.put(MemberCategoryType.PASSIVE_COLLECTIVE, Collections.unmodifiableSet(passiveCollective));

    Set<MemberCategoryType> passiveFamily = new LinkedHashSet<>();
    passiveFamily.add(MemberCategoryType.ASPIRANT);
    passiveFamily.add(MemberCategoryType.ACTIVE);
    passiveFamily.add(MemberCategoryType.PASSIVE_SINGLE);
    passiveFamily.add(MemberCategoryType.PASSIVE_COLLECTIVE);
    passiveFamily.add(MemberCategoryType.HONORARY_PASSIVE);
    values.put(MemberCategoryType.PASSIVE_FAMILY, Collections.unmodifiableSet(passiveFamily));

    Set<MemberCategoryType> honoraryActive = new LinkedHashSet<>();
    honoraryActive.add(MemberCategoryType.HONORARY_PASSIVE);
    values.put(MemberCategoryType.HONORARY_ACTIVE, Collections.unmodifiableSet(honoraryActive));

    Set<MemberCategoryType> honoraryPassive = new LinkedHashSet<>();
    honoraryPassive.add(MemberCategoryType.HONORARY_ACTIVE);
    values.put(MemberCategoryType.HONORARY_PASSIVE, Collections.unmodifiableSet(honoraryPassive));

    MUTATIONS = Collections.unmodifiableMap(values);
  }

  /**
   * The actions associated with a mutation.
   */
  public static final Map<Pair<MemberCategoryType, MemberCategoryType>, Set<MutationAction>> MUTATION_ACTIONS;
  static {
    Map<Pair<MemberCategoryType, MemberCategoryType>, Set<MutationAction>> values = new HashMap<>();

    //
    // Interested
    //

    Set<MutationAction> set = new LinkedHashSet<>();
    set.add(MutationAction.GENERATE_SHORTHAND);
    set.add(MutationAction.SEND_NEWSLETTER);
    set.add(MutationAction.APPOINT_ASPIRANT);
    values.put(new Pair<>(MemberCategoryType.INTERESTED, MemberCategoryType.ASPIRANT),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.GENERATE_SHORTHAND);
    set.add(MutationAction.SEND_NEWSLETTER);
    set.add(MutationAction.APPOINT_RANK);
    values.put(new Pair<>(MemberCategoryType.INTERESTED, MemberCategoryType.ACTIVE), Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.GENERATE_SHORTHAND);
    set.add(MutationAction.SEND_NEWSLETTER);
    values.put(new Pair<>(MemberCategoryType.INTERESTED, MemberCategoryType.PASSIVE_SINGLE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.GENERATE_SHORTHAND);
    set.add(MutationAction.SEND_NEWSLETTER);
    values.put(new Pair<>(MemberCategoryType.INTERESTED, MemberCategoryType.PASSIVE_COLLECTIVE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.GENERATE_SHORTHAND);
    set.add(MutationAction.SEND_NEWSLETTER);
    values.put(new Pair<>(MemberCategoryType.INTERESTED, MemberCategoryType.PASSIVE_FAMILY),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    values.put(new Pair<>(MemberCategoryType.INTERESTED, null), Collections.unmodifiableSet(set));

    //
    // Aspirant
    //

    set = new LinkedHashSet<>();
    set.add(MutationAction.BREVET);
    values.put(new Pair<>(MemberCategoryType.ASPIRANT, MemberCategoryType.ACTIVE), Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.REMOVE_RANK);
    values.put(new Pair<>(MemberCategoryType.ASPIRANT, MemberCategoryType.PASSIVE_SINGLE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.REMOVE_RANK);
    values.put(new Pair<>(MemberCategoryType.ASPIRANT, MemberCategoryType.PASSIVE_COLLECTIVE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.REMOVE_RANK);
    values.put(new Pair<>(MemberCategoryType.ASPIRANT, MemberCategoryType.PASSIVE_FAMILY),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.REMOVE_RANK);
    set.add(MutationAction.REVOKE_NEWSLETTER);
    set.add(MutationAction.REVOKE_SHORTHAND);
    values.put(new Pair<>(MemberCategoryType.ASPIRANT, null), Collections.unmodifiableSet(set));

    //
    // Active
    //

    set = new LinkedHashSet<>();
    set.add(MutationAction.REMOVE_RANK);
    values.put(new Pair<>(MemberCategoryType.ACTIVE, MemberCategoryType.PASSIVE_SINGLE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.REMOVE_RANK);
    values.put(new Pair<>(MemberCategoryType.ACTIVE, MemberCategoryType.PASSIVE_COLLECTIVE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.REMOVE_RANK);
    values.put(new Pair<>(MemberCategoryType.ACTIVE, MemberCategoryType.PASSIVE_FAMILY),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    values.put(new Pair<>(MemberCategoryType.ACTIVE, MemberCategoryType.HONORARY_ACTIVE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.REMOVE_RANK);
    set.add(MutationAction.REVOKE_NEWSLETTER);
    set.add(MutationAction.REVOKE_SHORTHAND);
    values.put(new Pair<>(MemberCategoryType.ACTIVE, null), Collections.unmodifiableSet(set));

    //
    // Passive-Single
    //

    set = new LinkedHashSet<>();
    set.add(MutationAction.APPOINT_ASPIRANT);
    values.put(new Pair<>(MemberCategoryType.PASSIVE_SINGLE, MemberCategoryType.ASPIRANT),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.APPOINT_RANK);
    values.put(new Pair<>(MemberCategoryType.PASSIVE_SINGLE, MemberCategoryType.ACTIVE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    values.put(new Pair<>(MemberCategoryType.PASSIVE_SINGLE, MemberCategoryType.PASSIVE_COLLECTIVE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    values.put(new Pair<>(MemberCategoryType.PASSIVE_SINGLE, MemberCategoryType.PASSIVE_FAMILY),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    values.put(new Pair<>(MemberCategoryType.PASSIVE_SINGLE, MemberCategoryType.HONORARY_PASSIVE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.REVOKE_NEWSLETTER);
    set.add(MutationAction.REVOKE_SHORTHAND);
    values.put(new Pair<>(MemberCategoryType.PASSIVE_SINGLE, null), Collections.unmodifiableSet(set));

    //
    // Passive-Collective
    //

    set = new LinkedHashSet<>();
    set.add(MutationAction.APPOINT_ASPIRANT);
    values.put(new Pair<>(MemberCategoryType.PASSIVE_COLLECTIVE, MemberCategoryType.ASPIRANT),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.APPOINT_RANK);
    values.put(new Pair<>(MemberCategoryType.PASSIVE_COLLECTIVE, MemberCategoryType.ACTIVE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    values.put(new Pair<>(MemberCategoryType.PASSIVE_COLLECTIVE, MemberCategoryType.PASSIVE_SINGLE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    values.put(new Pair<>(MemberCategoryType.PASSIVE_COLLECTIVE, MemberCategoryType.PASSIVE_FAMILY),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    values.put(new Pair<>(MemberCategoryType.PASSIVE_COLLECTIVE, MemberCategoryType.HONORARY_PASSIVE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.REVOKE_NEWSLETTER);
    set.add(MutationAction.REVOKE_SHORTHAND);
    values.put(new Pair<>(MemberCategoryType.PASSIVE_COLLECTIVE, null), Collections.unmodifiableSet(set));

    //
    // Passive-Family
    //

    set = new LinkedHashSet<>();
    set.add(MutationAction.APPOINT_ASPIRANT);
    values.put(new Pair<>(MemberCategoryType.PASSIVE_FAMILY, MemberCategoryType.ASPIRANT),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.APPOINT_RANK);
    values.put(new Pair<>(MemberCategoryType.PASSIVE_FAMILY, MemberCategoryType.ACTIVE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    values.put(new Pair<>(MemberCategoryType.PASSIVE_FAMILY, MemberCategoryType.PASSIVE_SINGLE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    values.put(new Pair<>(MemberCategoryType.PASSIVE_FAMILY, MemberCategoryType.PASSIVE_COLLECTIVE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    values.put(new Pair<>(MemberCategoryType.PASSIVE_FAMILY, MemberCategoryType.HONORARY_PASSIVE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.REVOKE_NEWSLETTER);
    set.add(MutationAction.REVOKE_SHORTHAND);
    values.put(new Pair<>(MemberCategoryType.PASSIVE_FAMILY, null), Collections.unmodifiableSet(set));

    //
    // Honorary Active
    //

    set = new LinkedHashSet<>();
    set.add(MutationAction.REMOVE_RANK);
    values.put(new Pair<>(MemberCategoryType.HONORARY_ACTIVE, MemberCategoryType.HONORARY_PASSIVE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.REMOVE_RANK);
    set.add(MutationAction.REVOKE_NEWSLETTER);
    set.add(MutationAction.REVOKE_SHORTHAND);
    values.put(new Pair<>(MemberCategoryType.HONORARY_ACTIVE, null), Collections.unmodifiableSet(set));

    //
    // Honorary Passive
    //

    set = new LinkedHashSet<>();
    set.add(MutationAction.APPOINT_RANK);
    values.put(new Pair<>(MemberCategoryType.HONORARY_PASSIVE, MemberCategoryType.HONORARY_ACTIVE),
        Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.REVOKE_NEWSLETTER);
    set.add(MutationAction.REVOKE_SHORTHAND);
    values.put(new Pair<>(MemberCategoryType.HONORARY_PASSIVE, null), Collections.unmodifiableSet(set));

    //
    // null
    //

    set = new LinkedHashSet<>();
    values.put(new Pair<>(null, MemberCategoryType.INTERESTED), Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.GENERATE_SHORTHAND);
    set.add(MutationAction.SEND_NEWSLETTER);
    set.add(MutationAction.APPOINT_ASPIRANT);
    values.put(new Pair<>(null, MemberCategoryType.ASPIRANT), Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.GENERATE_SHORTHAND);
    set.add(MutationAction.SEND_NEWSLETTER);
    set.add(MutationAction.APPOINT_RANK);
    values.put(new Pair<>(null, MemberCategoryType.ACTIVE), Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.GENERATE_SHORTHAND);
    set.add(MutationAction.SEND_NEWSLETTER);
    values.put(new Pair<>(null, MemberCategoryType.PASSIVE_SINGLE), Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.GENERATE_SHORTHAND);
    set.add(MutationAction.SEND_NEWSLETTER);
    values.put(new Pair<>(null, MemberCategoryType.PASSIVE_COLLECTIVE), Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.GENERATE_SHORTHAND);
    set.add(MutationAction.SEND_NEWSLETTER);
    values.put(new Pair<>(null, MemberCategoryType.PASSIVE_FAMILY), Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.GENERATE_SHORTHAND);
    set.add(MutationAction.SEND_NEWSLETTER);
    set.add(MutationAction.APPOINT_RANK);
    values.put(new Pair<>(null, MemberCategoryType.HONORARY_ACTIVE), Collections.unmodifiableSet(set));

    set = new LinkedHashSet<>();
    set.add(MutationAction.GENERATE_SHORTHAND);
    set.add(MutationAction.SEND_NEWSLETTER);
    values.put(new Pair<>(null, MemberCategoryType.HONORARY_PASSIVE), Collections.unmodifiableSet(set));

    MUTATION_ACTIONS = Collections.unmodifiableMap(values);
  }

}
