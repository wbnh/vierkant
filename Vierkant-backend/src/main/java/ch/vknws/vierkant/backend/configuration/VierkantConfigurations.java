package ch.vknws.vierkant.backend.configuration;

import java.time.format.DateTimeFormatter;

import ch.vknws.vierkant.backend.dao.ConfigurationDao.ConfigurationKey;

/**
 * Keys for configurations.
 *
 * @author Benjamin Weber
 */
public class VierkantConfigurations {

  /**
   * Pattern for date format.
   */
  public static final String DATE_FORMAT_PATTERN = "dd.MM.yyyy";

  /**
   * Pattern for date time format.
   */
  public static final String DATE_TIME_FORMAT_PATTERN = "dd.MM.yyyy HH:mm:ss";

  /**
   * Pattern for date time format.
   */
  public static final String DATE_TIME_MINUTE_FORMAT_PATTERN = "dd.MM.yyyy HH:mm";

  /**
   * The format to use for dates.
   */
  public static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern(DATE_FORMAT_PATTERN);

  /**
   * The format to use for dates with time.
   */
  public static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_PATTERN);

  /**
   * The ID of the board of directors function group.
   */
  public static final ConfigurationKey BOARD_OF_DIRECTORS_ID = new ConfigurationKey("sys", "functionGroups",
      "boardOfDirectorsGroup");
  /**
   * The ID of the Aspirant platoon function group.
   */
  public static final ConfigurationKey ASP_PLATOON_ID = new ConfigurationKey("sys", "functionGroups",
      "aspPlatoonGroup");
  /**
   * The ID of the VK platoon function group.
   */
  public static final ConfigurationKey VK_PLATOON_ID = new ConfigurationKey("sys", "functionGroups", "vkPlatoonGroup");
  /**
   * The ID of the 4G function group.
   */
  public static final ConfigurationKey FOUR_G_ID = new ConfigurationKey("sys", "functionGroups", "fourGGroup");

  /**
   * The ID of the secretary function.
   */
  public static final ConfigurationKey SECRETARY_ID = new ConfigurationKey("sys", "functions", "secretary");

  /**
   * The ID of the secretary function.
   */
  public static final ConfigurationKey GENERAL_ASSEMBLY_MEETING_TYPE_ID = new ConfigurationKey("sys", "meetingTypes",
      "generalAssembly");

  /**
   * The ID of the secretary function.
   */
  public static final ConfigurationKey BOARD_OF_DIRECTORS_MEETING_TYPE_ID = new ConfigurationKey("sys", "meetingTypes",
      "boardOfDirectorsMeeting");

  /**
   * The message the admin has set for everyone to see.
   */
  public static final ConfigurationKey ADMIN_USER_MESSAGE = new ConfigurationKey("all", "admin", "userMessage");
  /**
   * The date the admin message expires.
   */
  public static final ConfigurationKey ADMIN_USER_MESSAGE_EXPIRATION = new ConfigurationKey("all", "admin",
      "userMessageExpiration");

  /**
   * The shorthand of the person who maintains the system.
   */
  public static final ConfigurationKey MAINTAINER_SHORTHAND = new ConfigurationKey("sys", "maintainer", "shorthand");

  /**
   * The default password for new users.
   */
  public static final ConfigurationKey DEFAULT_PASSWORD = new ConfigurationKey("sys", "authentication",
      "defaultPassword");

  /**
   * The hostname of the LDAP server.
   */
  public static final ConfigurationKey LDAP_HOST = new ConfigurationKey("sys", "ldap", "host");
  /**
   * The port of the LDAP server.
   */
  public static final ConfigurationKey LDAP_PORT = new ConfigurationKey("sys", "ldap", "port");
  /**
   * The username to login with on the LDAP.
   */
  public static final ConfigurationKey LDAP_USERNAME = new ConfigurationKey("sys", "ldap", "username");
  /**
   * The password for login on the LDAP.
   */
  public static final ConfigurationKey LDAP_PASSWORD = new ConfigurationKey("sys", "ldap", "password");

  /**
   * The address to use as sender for email.
   */
  public static final ConfigurationKey EMAIL_SENDER_ADDRESS = new ConfigurationKey("sys", "mail", "sender");
  /**
   * The subject to use for interested people.
   */
  public static final ConfigurationKey EMAIL_INTERESTED_SUBJECT = new ConfigurationKey("sys", "mail",
      "interestedSubject");
  /**
   * The message to use for interested people.
   */
  public static final ConfigurationKey EMAIL_INTERESTED_MESSAGE = new ConfigurationKey("sys", "mail",
      "interestedMessage");
  /**
   * The subject to use to reset the password.
   */
  public static final ConfigurationKey EMAIL_RESET_PASSWORD_SUBJECT = new ConfigurationKey("sys", "mail",
      "resetPasswordSubject");
  /**
   * The message to use for password reset.
   */
  public static final ConfigurationKey EMAIL_RESET_PASSWORD_MESSAGE = new ConfigurationKey("sys", "mail",
      "resetPasswordMessage");
  /**
   * The subject to use for error reports.
   */
  public static final ConfigurationKey EMAIL_ERROR_REPORT_SUBJECT = new ConfigurationKey("sys", "mail",
      "errorReportSubject");
  /**
   * The message to use for error reports.
   */
  public static final ConfigurationKey EMAIL_ERROR_REPORT_MESSAGE = new ConfigurationKey("sys", "mail",
      "errorReportMessage");
  /**
   * The IMAP hostname.
   */
  public static final ConfigurationKey EMAIL_IMAP_HOST = new ConfigurationKey("sys", "mail", "mailImapHost");
  /**
   * The IMAP port.
   */
  public static final ConfigurationKey EMAIL_IMAP_PORT = new ConfigurationKey("sys", "mail", "mailImapPort");
  /**
   * Whether to use an encrypted connection to IMAP.
   */
  public static final ConfigurationKey EMAIL_IMAP_SSL = new ConfigurationKey("sys", "mail", "mailImapSsl");
  /**
   * The username to use on the IMAP.
   */
  public static final ConfigurationKey EMAIL_IMAP_USER = new ConfigurationKey("sys", "mail", "mailImapUser");
  /**
   * The password to use on the IMAP.
   */
  public static final ConfigurationKey EMAIL_IMAP_PASSWORD = new ConfigurationKey("sys", "mail", "mailImapPwd");
  /**
   * The SMTP hostname.
   */
  public static final ConfigurationKey EMAIL_SMTP_HOST = new ConfigurationKey("sys", "mail", "mailSmtpHost");
  /**
   * The SMTP port.
   */
  public static final ConfigurationKey EMAIL_SMTP_PORT = new ConfigurationKey("sys", "mail", "mailSmtpPort");
  /**
   * Whether to authenticate on SMTP.
   */
  public static final ConfigurationKey EMAIL_SMTP_AUTH = new ConfigurationKey("sys", "mail", "mailSmtpAuth");
  /**
   * The username to use on SMTP.
   */
  public static final ConfigurationKey EMAIL_SMTP_USER = new ConfigurationKey("sys", "mail", "mailSmtpUser");
  /**
   * The password to use on SMTP.
   */
  public static final ConfigurationKey EMAIL_SMTP_PASSWORD = new ConfigurationKey("sys", "mail", "mailSmtpPwd");

  /**
   * The base directory to use for FS operations.
   */
  public static final ConfigurationKey FILE_BASE_DIRECTORY = new ConfigurationKey("sys", "file", "baseDirectory");
  /**
   * The subdirectory to use for temporary files.
   */
  public static final ConfigurationKey FILE_TEMP_DIRECTORY = new ConfigurationKey("sys", "file", "tempDirectory");
  /**
   * The subdirectory to use for users' personal directories.
   */
  public static final ConfigurationKey FILE_USERS_DIRECTORY = new ConfigurationKey("sys", "file", "userDirectories");

}
