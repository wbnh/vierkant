package ch.vknws.vierkant.backend.dao;

import ch.vknws.vierkant.type.ContactPersonModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.impl.ContactPersonPersistable;
import ch.vknws.vierkant.type.impl.PersonPersistable;

/**
 * DAO for the ContactPerson.
 *
 * @author Benjamin Weber
 */
public class ContactPersonDao extends AbstractVierkantReferencedDao<PersonModel, ContactPersonModel> {

  /**
   * Constructor handling initialization of mandatory fields.
   */
  protected ContactPersonDao() {
    super(PersonPersistable.class, PersonModel.PropertyId.REF_CONTACT_PERSONS, ContactPersonPersistable.class);
  }

}
