package ch.vknws.vierkant.backend.function;

import java.util.Properties;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fad.v1.model.MailAccount;
import ch.bwe.fad.v1.model.MailMessage;
import ch.bwe.fad.v1.model.MailMessageBuilder;
import ch.vknws.vierkant.backend.configuration.ConfigurationUtils;
import ch.vknws.vierkant.backend.configuration.VierkantConfigurations;
import ch.vknws.vierkant.backend.dao.ConfigurationDao.ConfigurationKey;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.PersonModel;

/**
 * Utility for emails.
 *
 * @author Benjamin Weber
 */
public class EmailFunctions {

  private static final Properties imapConfiguration = loadImapConfig();
  private static final Properties smtpConfiguration = loadSmtpConfig();

  private static String senderAddress() {
    return ConfigurationUtils.getConfiguration(VierkantConfigurations.EMAIL_SENDER_ADDRESS);
  }

  private static String interestedSubject() {
    return ConfigurationUtils.getConfiguration(VierkantConfigurations.EMAIL_INTERESTED_SUBJECT);
  }

  private static String interestedMessage(String name, Integer referenceNumber) {
    return ConfigurationUtils.getConfiguration(VierkantConfigurations.EMAIL_INTERESTED_MESSAGE, name, referenceNumber);
  }

  private static String resetPasswordSubject() {
    return ConfigurationUtils.getConfiguration(VierkantConfigurations.EMAIL_RESET_PASSWORD_SUBJECT);
  }

  private static String resetPasswordMessage(String name, String link) {
    return ConfigurationUtils.getConfiguration(VierkantConfigurations.EMAIL_RESET_PASSWORD_MESSAGE, name, link);
  }

  private static String errorReportSubject() {
    return ConfigurationUtils.getConfiguration(VierkantConfigurations.EMAIL_ERROR_REPORT_SUBJECT);
  }

  private static String errorReportMessage(String userName, Integer userId, String userEmail, String errorReport) {
    return ConfigurationUtils.getConfiguration(VierkantConfigurations.EMAIL_ERROR_REPORT_MESSAGE, userName, userId,
        userEmail, errorReport);
  }

  private static PersonModel getMaintainer() {
    String shorthand = ConfigurationUtils.getConfiguration(VierkantConfigurations.MAINTAINER_SHORTHAND);
    return DaoFactory.getInstance().getPersonDao().loadByShorthand(shorthand);
  }

  private static Properties loadImapConfig() {
    Properties imap = new Properties();

    ConfigurationKey key;
    String configuration;

    key = VierkantConfigurations.EMAIL_IMAP_HOST;
    configuration = ConfigurationUtils.getConfiguration(key);
    imap.setProperty(key.getKeyName(), configuration);

    key = VierkantConfigurations.EMAIL_IMAP_PORT;
    configuration = ConfigurationUtils.getConfiguration(key);
    imap.setProperty(key.getKeyName(), configuration);

    key = VierkantConfigurations.EMAIL_IMAP_SSL;
    configuration = ConfigurationUtils.getConfiguration(key);
    imap.setProperty(key.getKeyName(), configuration);

    key = VierkantConfigurations.EMAIL_IMAP_USER;
    configuration = ConfigurationUtils.getConfiguration(key);
    imap.setProperty(key.getKeyName(), configuration);

    key = VierkantConfigurations.EMAIL_IMAP_PASSWORD;
    configuration = ConfigurationUtils.getConfiguration(key);
    imap.setProperty(key.getKeyName(), configuration);

    return imap;
  }

  private static Properties loadSmtpConfig() {
    Properties smtp = new Properties();

    ConfigurationKey key;
    String configuration;

    key = VierkantConfigurations.EMAIL_SMTP_HOST;
    configuration = ConfigurationUtils.getConfiguration(key);
    smtp.setProperty(key.getKeyName(), configuration);

    key = VierkantConfigurations.EMAIL_SMTP_PORT;
    configuration = ConfigurationUtils.getConfiguration(key);
    smtp.setProperty(key.getKeyName(), configuration);

    key = VierkantConfigurations.EMAIL_SMTP_AUTH;
    configuration = ConfigurationUtils.getConfiguration(key);
    smtp.setProperty(key.getKeyName(), configuration);

    key = VierkantConfigurations.EMAIL_SMTP_USER;
    configuration = ConfigurationUtils.getConfiguration(key);
    smtp.setProperty(key.getKeyName(), configuration);

    key = VierkantConfigurations.EMAIL_SMTP_PASSWORD;
    configuration = ConfigurationUtils.getConfiguration(key);
    smtp.setProperty(key.getKeyName(), configuration);

    return smtp;
  }

  /**
   * Send email to a new interested person.
   * 
   * @param name             The name of the person
   * @param referenceNumber  the PersonModel recordId
   * @param recipientAddress the address to send the message to.
   */
  public static void sendInterestedMail(String name, Integer referenceNumber, String recipientAddress) {

    MailAccount account = new MailAccount();
    account.connect(imapConfiguration, smtpConfiguration);
    try {
      MailMessageBuilder builder = new MailMessageBuilder();
      builder.sender(senderAddress());
      builder.subject(interestedSubject());
      builder.message(interestedMessage(name, referenceNumber));
      builder.recipient(recipientAddress);

      account.sendMail(builder.build(), 5, 5000);
    } finally {
      account.disconnect();
    }
  }

  /**
   * Sets a password reset link to the specified address.
   * 
   * @param name             the name of the person
   * @param link             the link for resetting
   * @param recipientAddress the destination address
   */
  public static void sendResetPasswordMail(String name, String link, String recipientAddress) {

    MailAccount account = new MailAccount();
    account.connect(imapConfiguration, smtpConfiguration);
    try {
      MailMessageBuilder builder = new MailMessageBuilder();
      builder.sender(senderAddress());
      builder.subject(resetPasswordSubject());
      builder.message(resetPasswordMessage(name, link));
      builder.recipient(recipientAddress);

      MailMessage message = builder.build();

      ServiceRegistry.getLogProxy().info(EmailFunctions.class, "Sending password reset mail for {0} to {1}", name,
          recipientAddress);

      account.sendMail(message, 5, 5000);
    } finally {
      account.disconnect();
    }
  }

  /**
   * Send an error report to the maintainer.
   * 
   * @param reporter the person who reported the issue
   * @param problem  the problem that occurred
   */
  public static void setErrorReportMail(PersonModel reporter, Throwable problem) {
    MailAccount account = new MailAccount();
    account.connect(imapConfiguration, smtpConfiguration);
    try {
      MailMessageBuilder builder = new MailMessageBuilder();
      builder.sender(senderAddress());
      builder.subject(errorReportSubject());
      builder.message(errorReportMessage(reporter.toString(), reporter.getRecordId(), reporter.getEmailOrganization(),
          extractStacktrace(problem)));
      builder.recipient(getMaintainer().getEmailOrganization());

      MailMessage message = builder.build();

      ServiceRegistry.getLogProxy().info(EmailFunctions.class, "Sending error report from {0}", reporter.toString());

      account.sendMail(message, 5, 5000);
    } finally {
      account.disconnect();
    }
  }

  private static String extractStacktrace(Throwable problem) {
    if (problem == null) { return ""; }
    StringBuilder stacktrace = new StringBuilder();

    while (problem != null) {
      if (stacktrace.length() > 0) {
        stacktrace.append("Caused by: ");
      }
      stacktrace.append(problem.getClass().getName());
      stacktrace.append(": ");
      stacktrace.append(problem.getLocalizedMessage());

      for (var element : problem.getStackTrace()) {
        stacktrace.append(element);
        stacktrace.append("\n");
      }
      problem = problem.getCause();
    }

    return stacktrace.toString();
  }

}
