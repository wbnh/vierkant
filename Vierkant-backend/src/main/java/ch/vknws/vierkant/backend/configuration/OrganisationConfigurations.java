package ch.vknws.vierkant.backend.configuration;

import ch.vknws.vierkant.backend.dao.ConfigurationDao.ConfigurationKey;

/**
 * Keys for configurations.
 *
 * @author Benjamin Weber
 */
public class OrganisationConfigurations {

  /**
   * The age at which a person is eligible for active membership.
   */
  public static final ConfigurationKey ELIGIBILITY_AGE = new ConfigurationKey("sys", "org", "eligibilityAge");
  /**
   * The minimum height required for active membership.
   */
  public static final ConfigurationKey ELIGIBILITY_HEIGHT = new ConfigurationKey("sys", "org", "eligibilityHeight");

  /**
   * The name of the organisation the application serves.
   */
  public static final ConfigurationKey ORGANISATION_NAME = new ConfigurationKey("sys", "org", "organisationName");
  /**
   * The short name of the organisation the application serves.
   */
  public static final ConfigurationKey ORGANISATION_NAME_SHORT = new ConfigurationKey("sys", "org",
      "organisationNameShort");
}
