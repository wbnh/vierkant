package ch.vknws.vierkant.backend.ldap;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.apache.directory.api.ldap.model.exception.LdapException;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.type.DeleteException;
import ch.bwe.fac.v1.type.SaveException;
import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.bwe.fae.type.LdapConnection;
import ch.vknws.vierkant.ApplicationConstants;
import ch.vknws.vierkant.backend.configuration.ConfigurationUtils;
import ch.vknws.vierkant.backend.configuration.VierkantConfigurations;
import ch.vknws.vierkant.backend.dao.ConfigurationDao.ConfigurationKey;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.PersonModel;

/**
 * Utility to authenticate a user against the backend.
 *
 * @author Benjamin Weber
 */
public class AuthenticationUtil {
  /**
   * @return the LDAP hostname.
   */
  public static String getLdapHost() {
    return ConfigurationUtils.getConfiguration(VierkantConfigurations.LDAP_HOST);
  }

  /**
   * @return the LDAP port.
   */
  public static int getLdapPort() {
    return Integer.valueOf(ConfigurationUtils.getConfiguration(VierkantConfigurations.LDAP_PORT));
  }

  /**
   * @return the LDAP username for auth.
   */
  public static String getLdapUsername() {
    return ConfigurationUtils.getConfiguration(VierkantConfigurations.LDAP_USERNAME);
  }

  /**
   * @return the LDAP password for auth.
   */
  public static String getLdapPassword() {
    return ConfigurationUtils.getConfiguration(VierkantConfigurations.LDAP_PASSWORD);
  }

  /**
   * @param userId the user to check for
   * @return whether the user must change their password
   */
  public static boolean isMustChangePassword(String userId) {
    String config = ConfigurationUtils
        .getConfiguration(new ConfigurationKey(userId.toLowerCase(), "authentication", "mustChangePwd"));
    if (config == null) { return false; }
    Boolean value = Boolean.valueOf(config);
    return value;
  }

  /**
   * @param userId   the user to set for
   * @param flag     the flag to set
   * @param modifier the person to make the change
   * @throws SaveException if we cannot save the change
   */
  public static void setMustChangePassword(String userId, boolean flag, Integer modifier) throws SaveException {
    ConfigurationUtils.setConfiguration(new ConfigurationKey(userId.toLowerCase(), "authentication", "mustChangePwd"),
        Boolean.toString(flag), modifier);
  }

  /**
   * @param userId the user to get the code for
   * @return the password reset code to verify the user's identity
   */
  public static String getPasswordResetCode(String userId) {
    return ConfigurationUtils
        .getConfiguration(new ConfigurationKey(userId.toLowerCase(), "authentication", "resetPasswordCode"));
  }

  /**
   * Removes the reset code for the passed user.
   * 
   * @param userId the user to remove the code for
   * @throws DeleteException if we cannot remove it
   */
  public static void removePasswordResetCode(String userId) throws DeleteException {
    ConfigurationUtils
        .removeConfiguration(new ConfigurationKey(userId.toLowerCase(), "authentication", "resetPasswordCode"));
  }

  /**
   * @param userId   the concerned user
   * @param code     the new code
   * @param modifier the person to make the change
   * @throws SaveException if we cannot save the change
   */
  public static void setPasswordResetCode(String userId, String code, Integer modifier) throws SaveException {
    ConfigurationUtils.setConfiguration(
        new ConfigurationKey(userId.toLowerCase(), "authentication", "resetPasswordCode"), code, modifier);
  }

  /**
   * Authenticates the user on the system.
   * 
   * @param userId   the user to authenticate
   * @param password the user's password to verify
   * @return the person associated with the login if auth was successful, and
   *         <code>null</code> if it was not.
   */
  public static PersonModel authenticate(String userId, String password) {
    try (LdapConnection connection = new LdapConnection(getLdapHost(), getLdapPort(), getLdapUsername(),
        getLdapPassword(), ApplicationConstants.APPLICATION_NAME)) {
      boolean authenticated = connection.authenticatePerson(userId, password);

      authenticated &= !connection.isUserInGroup(userId, LdapGroup.VK_NOLOGIN.toString());

      if (!authenticated) { return null; }

      EqualityRestriction restriction = new EqualityRestriction("shorthand", userId);

      List<? extends PersonModel> found = DaoFactory.getInstance().getPersonDao().find(restriction, 0, 1);
      if (found.isEmpty()) { return null; }
      return found.get(0);
    } catch (IOException | LdapException e) {
      ServiceRegistry.getLogProxy().error(AuthenticationUtil.class, "Could not authenticate user {0}", e, userId);
      return null;
    }
  }

  /**
   * Check if the user is an admin.
   * 
   * @param userId the user to check
   * @return <code>true</code> if they are, otherwise <code>false</code>
   */
  public static boolean isAdministrator(String userId) {
    try (LdapConnection connection = new LdapConnection(getLdapHost(), getLdapPort(), getLdapUsername(),
        getLdapPassword(), ApplicationConstants.APPLICATION_NAME)) {

      return connection.isUserInGroup(userId, LdapGroup.VK_ADMIN.toString());

    } catch (IOException | LdapException e) {
      ServiceRegistry.getLogProxy().error(AuthenticationUtil.class, "Could not check user {0}", e, userId);
      return false;
    }

  }

  /**
   * Create a new login for the user.
   * 
   * @param userId   the user's shorthand
   * @param password the new password
   * @throws LdapException if we cannot create the login
   * @throws SaveException if we cannot force the user to change their password
   * @throws IOException   if we cannot open a connection to LDAP
   */
  public static void createLogin(String userId, String password) throws LdapException, SaveException, IOException {
    userId = userId.toLowerCase();
    try (LdapConnection connection = new LdapConnection(getLdapHost(), getLdapPort(), getLdapUsername(),
        getLdapPassword(), ApplicationConstants.APPLICATION_NAME)) {
      connection.addPerson(userId, password);
      setMustChangePassword(userId, true, 0);
    }
  }

  /**
   * Adds a person to an LDAP group.
   * 
   * @param userId the user to add
   * @param groups the groups to add to
   * @throws IOException   if we cannot establish a connection to LDAP
   * @throws LdapException if we cannot add the user
   */
  public static void addToGroup(String userId, String... groups) throws IOException, LdapException {
    userId = userId.toLowerCase();
    try (LdapConnection connection = new LdapConnection(getLdapHost(), getLdapPort(), getLdapUsername(),
        getLdapPassword(), ApplicationConstants.APPLICATION_NAME)) {
      for (String group : groups) {
        connection.addPersonToGroup(userId, group);
      }
    }
  }

  /**
   * Revokes the user's login.
   * 
   * @param userId the user to revoke the login of
   * @throws LdapException if we cannot perform the operation on LDAP
   * @throws IOException   if we cannot establish a connection
   */
  public static void revokeLogin(String userId) throws LdapException, IOException {
    userId = userId.toLowerCase();
    try (LdapConnection connection = new LdapConnection(getLdapHost(), getLdapPort(), getLdapUsername(),
        getLdapPassword(), ApplicationConstants.APPLICATION_NAME)) {
      for (LdapGroup group : LdapGroup.values()) {
        if (connection.isUserInGroup(userId, group.toString())) {
          connection.removePersonFromGroup(userId, group.toString());
        }
      }
      connection.removePerson(userId);
    }
  }

  /**
   * Changes a user's password after verifying the current one.
   * 
   * @param userId      the user's login name
   * @param oldPassword the old password to verify
   * @param newPassword the new password to set
   * @return whether the verification and subsequent update was successful
   */
  public static boolean changeVerifiedUserPassword(String userId, String oldPassword, String newPassword) {
    userId = userId.toLowerCase();
    try (LdapConnection connection = new LdapConnection(getLdapHost(), getLdapPort(), getLdapUsername(),
        getLdapPassword(), ApplicationConstants.APPLICATION_NAME)) {

      if (oldPassword != null) {
        boolean match = connection.authenticatePerson(userId, oldPassword);
        if (!match) { return false; }
      }

      connection.changePassword(userId, newPassword);

      setMustChangePassword(userId, false, 0);
      removePasswordResetCode(userId);

      return connection.authenticatePerson(userId, newPassword);
    } catch (IOException | LdapException | SaveException | DeleteException e) {
      ServiceRegistry.getLogProxy().error(AuthenticationUtil.class, "Could not verify password", e);
      return false;
    }
  }

  /**
   * Changes a user's password without verification. Used for password resets.
   * 
   * @param userId      the user's login name
   * @param newPassword the new password to set
   * @return whether the change was successful
   */
  public static boolean changeUserPassword(String userId, String newPassword) {
    return changeVerifiedUserPassword(userId, null, newPassword);
  }

  /**
   * Create a new code for the user to reset their password with.
   * 
   * @param userId the user to reset for
   * @return the code
   */
  public static String createPasswordResetCode(String userId) {
    UUID code = UUID.randomUUID();
    try {
      setPasswordResetCode(userId, code.toString(), 0);
    } catch (SaveException e) {
      ServiceRegistry.getLogProxy().error(AuthenticationUtil.class, "Could not create reset code", e);
      return null;
    }

    return code.toString();
  }
}
