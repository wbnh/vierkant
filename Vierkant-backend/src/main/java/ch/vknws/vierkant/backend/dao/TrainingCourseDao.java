package ch.vknws.vierkant.backend.dao;

import ch.vknws.vierkant.type.TrainingCourseModel;
import ch.vknws.vierkant.type.impl.TrainingCoursePersistable;

/**
 * DAO for the MemberCategoryModel.
 *
 * @author Benjamin Weber
 */
public class TrainingCourseDao extends AbstractVierkantDao<TrainingCourseModel> {

  /**
   * Constructor handling initialization of mandatory fields.
   */
  protected TrainingCourseDao() {
    super(TrainingCoursePersistable.class);
  }

}
