package ch.vknws.vierkant.backend.dao;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.AbstractDatabaseDAO;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.vknws.vierkant.backend.configuration.EnvironmentConfigurations;
import ch.vknws.vierkant.rules.RuleRegistry;
import ch.vknws.vierkant.rules.ValidationMessage;
import ch.vknws.vierkant.rules.ValidationResult;

/**
 * DAO for PersonModel.
 *
 * @author Benjamin Weber
 * @param <T> the type to represent
 */
public class AbstractVierkantDao<T extends DatabaseModel> extends AbstractDatabaseDAO<T> {

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param clazz the type of model to represent
   */
  public AbstractVierkantDao(Class<? extends T> clazz) {
    super(clazz, EnvironmentConfigurations.getPersistenceUnit());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected boolean isValid(T persistable) {
    ValidationResult result = RuleRegistry.validateModel(persistable);
    List<ValidationMessage> messages = result.getMessages();
    for (Iterator<ValidationMessage> iterator = messages.iterator(); iterator.hasNext();) {
      ValidationMessage message = iterator.next();
      if (DatabaseModel.PropertyId.CREATOR.equals(message.getField())
          || DatabaseModel.PropertyId.AUDIT_COMMENT.equals(message.getField())) {
        iterator.remove();
      }
    }

    if (!messages.isEmpty()) {
      List<String> problems = new LinkedList<>();
      for (var message : messages) {
        problems.add(message.getField() + ":" + message.getType().name());
      }
      ServiceRegistry.getLogProxy().info(this, "Validation problems: {0}", problems);
    }

    return messages.isEmpty();
  }

}
