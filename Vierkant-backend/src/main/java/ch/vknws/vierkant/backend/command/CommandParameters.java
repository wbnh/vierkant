package ch.vknws.vierkant.backend.command;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Contains multiple command parameters.
 * 
 * @author benjamin
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CommandParameters {
  /**
   * @return the parameters
   */
  CommandParameter[] value();
}
