package ch.vknws.vierkant.backend.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.vknws.vierkant.rules.module.secretary.RankLoader;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.RankModel;
import ch.vknws.vierkant.type.impl.PersonPersistable;
import ch.vknws.vierkant.type.impl.RankPersistable;

/**
 * DAO for RankModel.
 *
 * @author Benjamin Weber
 */
public class RankDao extends AbstractVierkantDao<RankModel> implements RankLoader {

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public RankDao() {
    super(RankPersistable.class);
  }

  @Override
  public RankModel loadByAbbreviation(String abbreviation) {
    AbstractRestriction restriction = new EqualityRestriction("abbreviation", abbreviation);

    List<? extends RankModel> found = find(restriction, 0, 1);
    return found.get(0);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isDeletable(Integer recordId) {
    EntityManager entityManager = getEntityManagerFactory().createEntityManager();

    try {

      CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      CriteriaQuery<Long> query = builder.createQuery(Long.class);

      Root<PersonPersistable> personRoot = query.from(PersonPersistable.class);
      query.select(builder.count(personRoot));
      query.where(builder.equal(personRoot.get(PersonModel.PropertyId.REF_RANK), recordId));

      Long count = entityManager.createQuery(query).getSingleResult();

      return count == 0L;

    } finally {
      entityManager.close();
    }
  }

}
