package ch.vknws.vierkant.backend.dao;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ch.vknws.vierkant.type.AargauLicenseModel;
import ch.vknws.vierkant.type.AargauPersonLicenseModel;
import ch.vknws.vierkant.type.impl.AargauLicensePersistable;
import ch.vknws.vierkant.type.impl.AargauPersonLicensePersistable;

/**
 * DAO for the AddressModel.
 *
 * @author Benjamin Weber
 */
public class AargauLicenseDao extends AbstractVierkantDao<AargauLicenseModel> {

  /**
   * Constructor handling initialization of mandatory fields.
   */
  protected AargauLicenseDao() {
    super(AargauLicensePersistable.class);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isDeletable(Integer recordId) {
    EntityManager entityManager = getEntityManagerFactory().createEntityManager();

    try {

      CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      CriteriaQuery<Long> query = builder.createQuery(Long.class);

      Root<AargauPersonLicensePersistable> root = query.from(AargauPersonLicensePersistable.class);
      query.select(builder.count(root));
      query.where(builder.equal(root.get(AargauPersonLicenseModel.PropertyId.REF_LICENSE), recordId));

      Long count = entityManager.createQuery(query).getSingleResult();

      return count == 0L;

    } finally {
      entityManager.close();
    }
  }

}
