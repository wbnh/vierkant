package ch.vknws.vierkant.backend.dao;

import java.util.List;

import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.vknws.vierkant.type.AuthorizationKeyModel;
import ch.vknws.vierkant.type.impl.AuthorizationKeyPersistable;

/**
 * DAO for the AddressModel.
 *
 * @author Benjamin Weber
 */
public class AuthorizationKeyDao extends AbstractVierkantDao<AuthorizationKeyModel> {

  /**
   * Constructor handling initialization of mandatory fields.
   */
  protected AuthorizationKeyDao() {
    super(AuthorizationKeyPersistable.class);
  }

  /**
   * Loads an authorisation key by name.
   * 
   * @param keyName the name of the key to load
   * @return the authorisation key or null if no such key exists
   */
  public AuthorizationKeyModel loadByKeyName(String keyName) {
    AbstractRestriction restriction = new EqualityRestriction(AuthorizationKeyModel.PropertyId.KEY_NAME, keyName);
    List<AuthorizationKeyModel> found = find(restriction, 0, 1);
    if (found == null || found.isEmpty()) { return null; }
    return found.get(0);
  }

}
