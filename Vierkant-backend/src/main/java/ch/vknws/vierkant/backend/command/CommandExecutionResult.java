package ch.vknws.vierkant.backend.command;

/**
 * Result of the execution.
 * 
 * @author benjamin
 */
public class CommandExecutionResult {
  /**
   * The level of the result.
   * 
   * @author benjamin
   */
  public static enum Level {
    /**
     * An error.
     */
    ERROR,

    /**
     * A warning.
     */
    WARN,

    /**
     * Information.
     */
    INFO;
  }

  /**
   * The level of the result.
   */
  private Level level;

  /**
   * The message.
   */
  private String message;

  /**
   * @param level   the level of the result
   * @param message the message
   */
  public CommandExecutionResult(Level level, String message) {
    this.level = level;
    this.message = message;
  }

  /**
   * @return the level
   */
  public Level getLevel() {
    return level;
  }

  /**
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append(level);
    builder.append(": ");
    builder.append(message);

    return builder.toString();
  }

}
