package ch.vknws.vierkant.backend.function;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.directory.api.ldap.model.exception.LdapException;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.backend.configuration.ConfigurationUtils;
import ch.vknws.vierkant.backend.configuration.VierkantConfigurations;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.backend.ldap.AuthenticationUtil;
import ch.vknws.vierkant.backend.ldap.LdapGroup;
import ch.vknws.vierkant.rules.RuleRegistry;
import ch.vknws.vierkant.rules.module.secretary.MutationAction;
import ch.vknws.vierkant.type.MemberCategoryType;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.VKA;

/**
 * Utility for mutations.
 *
 * @author Benjamin Weber
 */
public class MutationFunctions {

  /**
   * @return the configured default password for new logins.
   */
  public static String getDefaultPassword() {
    return ConfigurationUtils.getConfiguration(VierkantConfigurations.DEFAULT_PASSWORD);
  }

  /**
   * Mutates a person to the passed category. The mutation must be valid.
   * 
   * @param person      the person to mutate
   * @param newCategory the new category
   * @return the actions to perform to complete the mutation.
   */
  public static Set<MutationAction> mutate(PersonModel person, MemberCategoryType newCategory) {
    if (person == null) { return new HashSet<>(); }

    Set<MutationAction> mutationActions = RuleRegistry.mutatePerson(person, newCategory,
        DaoFactory.getInstance().getRankDao(), PersonShorthandGenerator.generate(person));

    person.setMemberCategory(newCategory);

    return mutationActions;
  }

  /**
   * Performs the passed mutation actions on the passed person.
   * 
   * @param person          the person to mutate
   * @param mutationActions the actions to perform
   */
  public static void performMutationActions(PersonModel person, Set<MutationAction> mutationActions) {

    RuleRegistry.mutatePerson(person, mutationActions, DaoFactory.getInstance().getRankDao(),
        PersonShorthandGenerator.generate(person));
  }

  /**
   * Creates a new login for the passed person.
   * 
   * @param person the person to create the login for
   * @throws LdapException if there was a problem
   */
  public static void createLogin(PersonModel person) throws LdapException {
    if (person == null || person.getShorthand() == null) { return; }

    try {

      LdapGroup group = LdapGroup.VK_INTERNAL;

      AuthenticationUtil.createLogin(person.getShorthand(), getDefaultPassword());

      if (MemberCategoryType.INTERESTED.equals(person.getMemberCategory())) {
        group = LdapGroup.VK_INTERESTED;
      } else if (!VKA.NWS.equals(person.getVka())) {
        group = LdapGroup.VK_EXTERNAL;
      }

      AuthenticationUtil.addToGroup(person.getShorthand(), group.toString());
    } catch (IOException | LdapException | SaveException e) {
      ServiceRegistry.getLogProxy().error(MutationFunctions.class, "Could not modify login", e);
      throw new LdapException(e);
    }
  }

  /**
   * Deletes the login for a person.
   * 
   * @param shorthand the person's shorthand, the login name
   * @throws LdapException if there was a problem
   */
  public static void revokeLogin(String shorthand) throws LdapException {
    if (shorthand == null) { return; }

    try {
      AuthenticationUtil.revokeLogin(shorthand);
    } catch (IOException | LdapException e) {
      ServiceRegistry.getLogProxy().error(MutationFunctions.class, "Could not modify login", e);
      throw new LdapException(e);
    }
  }
}
