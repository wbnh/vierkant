package ch.vknws.vierkant.backend.dao;

import ch.vknws.vierkant.type.MemberGroupModel;
import ch.vknws.vierkant.type.impl.MemberGroupPersistable;

/**
 * DAO for the MemberCategoryModel.
 *
 * @author Benjamin Weber
 */
public class MemberCategoryDao extends AbstractVierkantDao<MemberGroupModel> {

  /**
   * Constructor handling initialization of mandatory fields.
   */
  protected MemberCategoryDao() {
    super(MemberGroupPersistable.class);
  }

}
