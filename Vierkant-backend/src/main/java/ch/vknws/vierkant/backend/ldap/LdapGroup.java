package ch.vknws.vierkant.backend.ldap;

/**
 * Groups in the LDAP.
 *
 * @author Benjamin Weber
 */
public enum LdapGroup {

  /**
   * Group Lv 1, no login allowed.
   */
  VK_NOLOGIN,

  /**
   * Group Lv 1, login allowed.
   */
  VK_LOGIN,

  /**
   * Group Lv 2, external persons, non-members. Login allowed.
   */
  VK_EXTERNAL,

  /**
   * Group Lv 2, non-members that are interested in becoming members. Login
   * allowed.
   */
  VK_INTERESTED,

  /**
   * Group Lv 2, members. Login allowed.
   */
  VK_INTERNAL,

  /**
   * Group Lv 2, administrators. Login allowed.
   */
  VK_ADMIN;

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return name().toLowerCase();
  }
}
