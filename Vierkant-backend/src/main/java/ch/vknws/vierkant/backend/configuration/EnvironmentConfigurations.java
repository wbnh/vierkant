package ch.vknws.vierkant.backend.configuration;

import ch.bwe.faa.v1.core.service.ServiceRegistry;

/**
 * Configurations about the environment.
 * 
 * @author benjamin
 */
public class EnvironmentConfigurations {

  /**
   * The name of the persistence unit for DEV.
   */
  public static final String PERSISTENCE_UNIT_DEV = "vierkant_dev";

  /**
   * The name of the persistence unit for INT.
   */
  public static final String PERSISTENCE_UNIT_INT = "vierkant_int";

  /**
   * @return the persistence unit for the current environment
   */
  public static String getPersistenceUnit() {
    switch (ServiceRegistry.getEnvironment()) {
    case DEV:
      return PERSISTENCE_UNIT_DEV;
    case INT:
      return PERSISTENCE_UNIT_INT;
    case TST:
    case PRD:
    default:
      return null;
    }
  }
}
