package ch.vknws.vierkant.backend.dao;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ch.vknws.vierkant.type.AddressModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.impl.AddressPersistable;
import ch.vknws.vierkant.type.impl.PersonPersistable;

/**
 * DAO for the AddressModel.
 *
 * @author Benjamin Weber
 */
public class AddressDao extends AbstractVierkantDao<AddressModel> {

  /**
   * Constructor handling initialization of mandatory fields.
   */
  protected AddressDao() {
    super(AddressPersistable.class);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isDeletable(Integer recordId) {
    EntityManager entityManager = getEntityManagerFactory().createEntityManager();

    try {

      CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      CriteriaQuery<Long> query = builder.createQuery(Long.class);

      Root<PersonPersistable> personRoot = query.from(PersonPersistable.class);
      query.select(builder.count(personRoot));
      query.where(builder.equal(personRoot.get(PersonModel.PropertyId.REF_HOME_ADDRESS), recordId));

      Long count = entityManager.createQuery(query).getSingleResult();

      return count == 0L;

    } finally {
      entityManager.close();
    }
  }

}
