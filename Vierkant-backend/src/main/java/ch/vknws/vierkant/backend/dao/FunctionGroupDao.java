package ch.vknws.vierkant.backend.dao;

import java.util.List;

import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.vknws.vierkant.backend.configuration.VierkantConfigurations;
import ch.vknws.vierkant.type.ConfigurationModel;
import ch.vknws.vierkant.type.FunctionGroupModel;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.impl.FunctionGroupPersistable;

/**
 * DAO for the LicenseModel.
 *
 * @author Benjamin Weber
 */
public class FunctionGroupDao extends AbstractVierkantDao<FunctionGroupModel> {

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public FunctionGroupDao() {
    super(FunctionGroupPersistable.class);
  }

  /**
   * Loads the group for the board of directors.
   * 
   * @return the group.
   */
  public FunctionGroupModel loadBoardOfDirectors() {
    ConfigurationModel configuration = DaoFactory.getInstance().getConfigurationDao()
        .loadConfiguration(VierkantConfigurations.BOARD_OF_DIRECTORS_ID);
    Integer id = Integer.valueOf(configuration.getValue());

    return load(id);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isDeletable(Integer recordId) {
    FunctionGroupModel persistable = load(recordId);

    return persistable.getRefGroupMembers() == null || persistable.getRefGroupMembers().isEmpty();
  }

  /**
   * Searches for the group with the passed details.
   * 
   * @param details the details of the group
   * @return the group or null if it was not found.
   */
  public FunctionGroupModel findFunctionGroup(OrganizationFunctionModel details) {

    AbstractRestriction restriction = new EqualityRestriction(FunctionGroupModel.PropertyId.REF_DETAILS,
        details.getRecordId());

    List<? extends FunctionGroupModel> found = find(restriction, 0, 1);

    if (found.isEmpty()) { return null; }
    return found.get(0);

  }

}
