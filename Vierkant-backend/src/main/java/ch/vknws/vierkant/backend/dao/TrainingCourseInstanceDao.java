package ch.vknws.vierkant.backend.dao;

import ch.vknws.vierkant.type.TrainingCourseInstanceModel;
import ch.vknws.vierkant.type.impl.TrainingCourseInstancePersistable;

/**
 * DAO for the MemberCategoryModel.
 *
 * @author Benjamin Weber
 */
public class TrainingCourseInstanceDao extends AbstractVierkantDao<TrainingCourseInstanceModel> {

  /**
   * Constructor handling initialization of mandatory fields.
   */
  protected TrainingCourseInstanceDao() {
    super(TrainingCourseInstancePersistable.class);
  }

}
