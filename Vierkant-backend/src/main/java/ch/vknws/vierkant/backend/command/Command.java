package ch.vknws.vierkant.backend.command;

import java.util.List;
import java.util.Map;

import com.google.common.base.Strings;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.Configuration;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;

/**
 * A command to be executed.
 * 
 * @author benjamin
 */
@FunctionalInterface
public interface Command {

  /**
   * Executes the command.
   * 
   * @param parameters The parameters/flags to the command.
   * @param arguments  The arguments (nameless parameters) to the command.
   * @param context    the context for the command to execute in.
   * @return the results.
   */
  List<CommandExecutionResult> execute(Map<String, String> parameters, List<String> arguments, CommandContext context);

  /**
   * @return the command's call name.
   */
  default String getName() {
    if (getClass().isAnnotationPresent(CommandName.class)) {
      String value = getClass().getAnnotation(CommandName.class).value();
      if (value != null) {
        value = value.toLowerCase();
      }
      return value;
    }

    return null;
  }

  /**
   * @return the help text for the command.
   */
  default String getHelptext() {
    Configuration configuration = ServiceRegistry.getConfigurationProxy()
        .getConfiguration(new ConfigurationProperties<>(getClass()));
    StringBuilder builder = new StringBuilder();

    builder.append(getName());
    builder.append("\n\n");
    builder.append(configuration.getStringValue(Helptext.GENERAL_DESCRIPTION));
    String parameters = configuration.getStringValue(Helptext.PARAMETERS);
    if (!Strings.isNullOrEmpty(parameters)) {
      builder.append("\n\nParameters\n");
      builder.append(parameters);
    }
    String moreInformation = configuration.getStringValue(Helptext.MORE_INFORMATION);
    if (!Strings.isNullOrEmpty(moreInformation)) {
      builder.append("\n\nMore Information\n");
      builder.append(moreInformation);
    }

    return builder.toString();
  }

}
