package ch.vknws.vierkant.backend.rules;

import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.rules.rights.UserRules;
import ch.vknws.vierkant.type.FunctionGroupModel;
import ch.vknws.vierkant.type.PersonModel;

/**
 * Utility for working with rules.
 *
 * @author Benjamin Weber
 */
public class UserRuleUtils {

  /**
   * Checks if a person is on the board of directors.
   * 
   * @param person the person to check
   * @return whether the person is a member of the board
   */
  public static boolean isOnBoardOfDirectors(PersonModel person) {
    FunctionGroupModel boardOfDirectorsGroup = DaoFactory.getInstance().getFunctionGroupDao().loadBoardOfDirectors();

    return UserRules.getInstance().isInFunctionGroup(person, boardOfDirectorsGroup);
  }

}
