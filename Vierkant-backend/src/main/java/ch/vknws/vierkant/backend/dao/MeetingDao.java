package ch.vknws.vierkant.backend.dao;

import java.util.LinkedList;
import java.util.List;

import ch.bwe.faa.v1.core.util.Pair;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.vknws.vierkant.backend.configuration.ConfigurationUtils;
import ch.vknws.vierkant.backend.configuration.VierkantConfigurations;
import ch.vknws.vierkant.type.MeetingModel;
import ch.vknws.vierkant.type.MeetingTypeModel;
import ch.vknws.vierkant.type.impl.MeetingPersistable;

/**
 * DAO for the AddressModel.
 *
 * @author Benjamin Weber
 */
public class MeetingDao extends AbstractVierkantDao<MeetingModel> {

  /**
   * Constructor handling initialization of mandatory fields.
   */
  protected MeetingDao() {
    super(MeetingPersistable.class);
  }

  /**
   * @return the latest general assembly meeting
   */
  public MeetingModel findLatestGeneralAssembly() {
    String typeId = ConfigurationUtils.getConfiguration(VierkantConfigurations.GENERAL_ASSEMBLY_MEETING_TYPE_ID);
    MeetingTypeModel meetingType = DaoFactory.getInstance().getMeetingTypeDao().load(Integer.valueOf(typeId));

    AbstractRestriction restriction = new EqualityRestriction(MeetingModel.PropertyId.REF_MEETING_TYPE, meetingType);

    List<Pair<String, Boolean>> orders = new LinkedList<>();
    orders.add(new Pair<String, Boolean>(MeetingModel.PropertyId.END, false));

    List<MeetingModel> foundModels = find(restriction, 0, 1, orders);

    if (foundModels == null || foundModels.isEmpty()) { return null; }
    return foundModels.get(0);
  }

  /**
   * @return the latest board of directors meeting
   */
  public MeetingModel findLatestBoardMeeting() {
    String typeId = ConfigurationUtils.getConfiguration(VierkantConfigurations.BOARD_OF_DIRECTORS_MEETING_TYPE_ID);
    MeetingTypeModel meetingType = DaoFactory.getInstance().getMeetingTypeDao().load(Integer.valueOf(typeId));

    AbstractRestriction restriction = new EqualityRestriction(MeetingModel.PropertyId.REF_MEETING_TYPE, meetingType);

    List<Pair<String, Boolean>> orders = new LinkedList<>();
    orders.add(new Pair<String, Boolean>(MeetingModel.PropertyId.END, false));

    List<MeetingModel> foundModels = find(restriction, 0, 1, orders);

    if (foundModels == null || foundModels.isEmpty()) { return null; }
    return foundModels.get(0);
  }

}
