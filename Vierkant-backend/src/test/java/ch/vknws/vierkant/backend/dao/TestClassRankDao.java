package ch.vknws.vierkant.backend.dao;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.RankCategory;
import ch.vknws.vierkant.type.RankModel;
import ch.vknws.vierkant.type.impl.RankPersistable;

/**
 * Test class for the RankDao.
 *
 * @author Benjamin Weber
 */
public class TestClassRankDao {

  private RankDao dao;

  /**
   * Sets the test up.
   */
  @Before
  public void setUp() {
    dao = DaoFactory.getInstance().getRankDao();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testIds() throws Exception {
    List<Integer> ids = dao.getAllIds();
    Assert.assertNotNull(ids);
    Assert.assertFalse(ids.isEmpty());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCount() throws Exception {
    Long count = dao.count();
    Assert.assertNotNull(count);
    Assert.assertEquals(17L, count.longValue());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testExists() throws Exception {
    boolean containsId = dao.containsId(1);
    Assert.assertTrue(containsId);
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testHistory() throws Exception {
    RankModel rank = new RankPersistable();
    rank.setAbbreviation("TST");
    rank.setRankName("TestRank");
    rank.setRankCategory(RankCategory.ENLISTED);
    rank.setDefaultFunction("Test");
    rank.setHierarchyIndex(2);
    rank.setMemberCountGoal(15);

    rank = dao.save(rank, 0);
    Integer revisionId = rank.getRevisionId();

    try {
      rank.setMemberCountGoal(16);

      dao.save(rank, 0);

      rank = dao.load(rank.getRecordId());

      Assert.assertEquals(Integer.valueOf(16), rank.getMemberCountGoal());

      rank = dao.loadHistory(rank.getRecordId(), revisionId);

      Assert.assertEquals(Integer.valueOf(15), rank.getMemberCountGoal());
    } finally {
      dao.remove(rank.getRecordId());
    }
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAttributes() throws Exception {
    LocalDateTime startTime = LocalDateTime.now();

    String abbreviation = "Abb";
    String auditComment = "audit comment";
    String defaultFunction = "default function";
    boolean deletedFlag = false;
    Integer hierarchyIndex = 3;
    Integer memberCountGoal = 10;
    RankCategory rankCategory = RankCategory.ENLISTED;
    String rankName = "rank name";
    Path shoulderStrapPicture = null;

    RankModel rank = new RankPersistable();
    rank.setAbbreviation(abbreviation);
    rank.setAuditComment(auditComment);
    rank.setDefaultFunction(defaultFunction);
    rank.setDeletedFlag(deletedFlag);
    rank.setHierarchyIndex(hierarchyIndex);
    rank.setMemberCountGoal(memberCountGoal);
    rank.setRankCategory(rankCategory);
    rank.setRankName(rankName);
    rank.setShoulderStrapPicture(shoulderStrapPicture);

    try {

      rank = dao.save(rank, 0);

      Assert.assertEquals(abbreviation, rank.getAbbreviation());
      Assert.assertEquals(auditComment, rank.getAuditComment());
      Assert.assertEquals(defaultFunction, rank.getDefaultFunction());
      Assert.assertEquals(deletedFlag, rank.getDeletedFlag());
      Assert.assertEquals(hierarchyIndex, rank.getHierarchyIndex());
      Assert.assertEquals(memberCountGoal, rank.getMemberCountGoal());
      Assert.assertEquals(rankCategory, rank.getRankCategory());
      Assert.assertEquals(rankName, rank.getRankName());
      Assert.assertEquals(shoulderStrapPicture, rank.getShoulderStrapPicture());
      Assert.assertEquals(Integer.valueOf(0), rank.getCreator());
      Assert.assertEquals(Integer.valueOf(0), rank.getModifier());
      Assert.assertTrue(startTime.isBefore(rank.getCreationDateTime()));
      Assert.assertTrue(startTime.isBefore(rank.getModificationDateTime()));

    } finally {
      if (rank.getRecordId() != null) dao.remove(rank.getRecordId());
    }
  }
}
