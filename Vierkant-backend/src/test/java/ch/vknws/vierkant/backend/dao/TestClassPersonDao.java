package ch.vknws.vierkant.backend.dao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.MemberCategoryType;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.PersonStatus;
import ch.vknws.vierkant.type.SexType;
import ch.vknws.vierkant.type.VKA;
import ch.vknws.vierkant.type.impl.PersonPersistable;

/**
 * Test class for the RankDao.
 *
 * @author Benjamin Weber
 */
public class TestClassPersonDao {

  private PersonDao dao;

  /**
   * Sets the test up.
   */
  @Before
  public void setUp() {
    dao = DaoFactory.getInstance().getPersonDao();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testIds() throws Exception {
    List<Integer> ids = dao.getAllIds();
    Assert.assertNotNull(ids);
    Assert.assertFalse(ids.isEmpty());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAttributes() throws Exception {
    LocalDateTime startTime = LocalDateTime.now();

    String auditComment = "audit comment";
    boolean deletedFlag = false;
    String givenName = "givenname";
    String familyName = "familyname";
    String title = "title";
    String telephone = "061 111 11 11";
    String mobile = "079 111 11 11";
    String emailPrivate = "private@example.com";
    String emailOrganization = "organization@example.com";
    LocalDate birthdate = LocalDate.now();
    String secretaryComment = "secretary comment";
    boolean vkNews = true;
    LocalDate lastUpdateCheck = LocalDate.now();
    String shorthand = "shrt";
    VKA vka = VKA.NWS;
    MemberCategoryType memberCategory = MemberCategoryType.PASSIVE_SINGLE;
    SexType sex = SexType.MALE;
    PersonStatus status = PersonStatus.NORMAL;

    PersonModel model = new PersonPersistable();
    model.setAuditComment(auditComment);
    model.setDeletedFlag(deletedFlag);
    model.setGivenName(givenName);
    model.setFamilyName(familyName);
    model.setTitle(title);
    model.setTelephone(telephone);
    model.setMobile(mobile);
    model.setEmailPrivate(emailPrivate);
    model.setEmailOrganization(emailOrganization);
    model.setBirthdate(birthdate);
    model.setSecretaryComment(secretaryComment);
    model.setVkNews(vkNews);
    model.setLastUpdateCheck(lastUpdateCheck);
    model.setShorthand(shorthand);
    model.setVka(vka);
    model.setMemberCategory(memberCategory);
    model.setSex(sex);
    model.setStatus(status);

    try {

      model = dao.save(model, 0);

      Assert.assertEquals(givenName, model.getGivenName());
      Assert.assertEquals(familyName, model.getFamilyName());
      Assert.assertEquals(title, model.getTitle());
      Assert.assertEquals(telephone, model.getTelephone());
      Assert.assertEquals(mobile, model.getMobile());
      Assert.assertEquals(emailPrivate, model.getEmailPrivate());
      Assert.assertEquals(emailOrganization, model.getEmailOrganization());
      Assert.assertEquals(birthdate, model.getBirthdate());
      Assert.assertEquals(secretaryComment, model.getSecretaryComment());
      Assert.assertEquals(vkNews, model.getVkNews());
      Assert.assertEquals(lastUpdateCheck, model.getLastUpdateCheck());
      Assert.assertEquals(shorthand, model.getShorthand());
      Assert.assertEquals(vka, model.getVka());
      Assert.assertEquals(memberCategory, model.getMemberCategory());
      Assert.assertEquals(sex, model.getSex());
      Assert.assertEquals(status, model.getStatus());
      Assert.assertEquals(auditComment, model.getAuditComment());
      Assert.assertEquals(deletedFlag, model.getDeletedFlag());
      Assert.assertEquals(Integer.valueOf(0), model.getCreator());
      Assert.assertEquals(Integer.valueOf(0), model.getModifier());
      Assert.assertTrue(startTime.isBefore(model.getCreationDateTime()));
      Assert.assertTrue(startTime.isBefore(model.getModificationDateTime()));

    } finally {
      if (model.getRecordId() != null) {
        model.setVka(null);
        model.setMemberCategory(null);
        model = dao.save(model, 0);
        dao.remove(model.getRecordId());
      }
    }
  }
}
