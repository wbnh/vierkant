package ch.vknws.vierkant.backend.dao;

import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.ContactPersonModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.SexType;
import ch.vknws.vierkant.type.impl.ContactPersonPersistable;
import ch.vknws.vierkant.type.impl.PersonPersistable;

/**
 * Test class for the RankDao.
 *
 * @author Benjamin Weber
 */
public class TestClassContactPersonDao {

  private ContactPersonDao dao;

  /**
   * Sets the test up.
   */
  @Before
  public void setUp() {
    dao = DaoFactory.getInstance().getContactPersonDao();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAttributes() throws Exception {
    LocalDateTime startTime = LocalDateTime.now();

    String auditComment = "audit comment";
    boolean deletedFlag = false;
    String relationship = "relationship";

    PersonModel person = new PersonPersistable();
    person.setFamilyName("familyname");
    person.setSex(SexType.FEMALE);

    ContactPersonModel model = new ContactPersonPersistable();
    model.setAuditComment(auditComment);
    model.setDeletedFlag(deletedFlag);
    model.setRelationship(relationship);
    model.setRefPerson(person);

    try {

      model = dao.save(model, 0);

      Assert.assertEquals(relationship, model.getRelationship());
      Assert.assertEquals(auditComment, model.getAuditComment());
      Assert.assertEquals(deletedFlag, model.getDeletedFlag());
      Assert.assertEquals(Integer.valueOf(0), model.getCreator());
      Assert.assertEquals(Integer.valueOf(0), model.getModifier());
      Assert.assertTrue(startTime.isBefore(model.getCreationDateTime()));
      Assert.assertTrue(startTime.isBefore(model.getModificationDateTime()));

    } finally {
      if (model.getRecordId() != null) dao.remove(model.getRecordId());
    }
  }
}
