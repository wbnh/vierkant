package ch.vknws.vierkant.backend.command.builtin;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import ch.vknws.vierkant.backend.command.Command;
import ch.vknws.vierkant.backend.command.CommandExecutionResult;
import ch.vknws.vierkant.backend.command.CommandExecutor;

/**
 * Test Class.
 * 
 * @author benjamin
 */
public class TestClassHelpCommand {

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRun() throws Exception {
    Command command = new Help();
    Map<String, String> parameters = new HashMap<>();
    List<String> arguments = new LinkedList<>();

    arguments.add("help");

    List<CommandExecutionResult> result = CommandExecutor.executeCommand(command, parameters, arguments);

    Assert.assertEquals(0, result.size());
  }
}
