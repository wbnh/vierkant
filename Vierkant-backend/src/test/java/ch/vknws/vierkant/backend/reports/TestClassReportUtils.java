package ch.vknws.vierkant.backend.reports;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.MemberCategoryType;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.PersonFunctionModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.PersonStatus;
import ch.vknws.vierkant.type.SexType;
import ch.vknws.vierkant.type.impl.AddressPersistable;
import ch.vknws.vierkant.type.impl.PersonFunctionPersistable;
import ch.vknws.vierkant.type.impl.PersonPersistable;

/**
 * TODO: Add Type Comment
 *
 * @author Benjamin Weber
 */
public class TestClassReportUtils {

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationNullInterested() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), null, MemberCategoryType.INTERESTED,
        LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationInterestedAspirant() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.INTERESTED,
        MemberCategoryType.ASPIRANT, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationInterestedActive() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.INTERESTED,
        MemberCategoryType.ACTIVE, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationInterestedPassiveSingle() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.INTERESTED,
        MemberCategoryType.PASSIVE_SINGLE, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationInterestedPassiveFamily() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.INTERESTED,
        MemberCategoryType.PASSIVE_FAMILY, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationInterestedPassiveCollective() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.INTERESTED,
        MemberCategoryType.PASSIVE_COLLECTIVE, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationAspirantActive() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.ASPIRANT,
        MemberCategoryType.ACTIVE, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationAspirantPassiveSingle() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.ASPIRANT,
        MemberCategoryType.PASSIVE_SINGLE, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationAspirantPassiveFamily() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.ASPIRANT,
        MemberCategoryType.PASSIVE_FAMILY, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationActivePassiveSingle() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.ACTIVE,
        MemberCategoryType.PASSIVE_SINGLE, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationActivePassiveFamily() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.ACTIVE,
        MemberCategoryType.PASSIVE_FAMILY, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationActiveHonoraryActive() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.ACTIVE,
        MemberCategoryType.HONORARY_ACTIVE, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationActiveHonoraryPassive() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.ACTIVE,
        MemberCategoryType.HONORARY_PASSIVE, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationPassiveSingleAspirant() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.PASSIVE_SINGLE,
        MemberCategoryType.ASPIRANT, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationPassiveSingleActive() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.PASSIVE_SINGLE,
        MemberCategoryType.ACTIVE, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationPassiveSingleHonoraryActive() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.PASSIVE_SINGLE,
        MemberCategoryType.HONORARY_ACTIVE, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationPassiveSingleHonoraryPassive() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.PASSIVE_SINGLE,
        MemberCategoryType.HONORARY_PASSIVE, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationPassiveFamilyAspirant() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.PASSIVE_FAMILY,
        MemberCategoryType.ASPIRANT, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationPassiveFamilyActive() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.PASSIVE_FAMILY,
        MemberCategoryType.ACTIVE, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationPassiveFamilyHonoraryActive() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.PASSIVE_FAMILY,
        MemberCategoryType.HONORARY_ACTIVE, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationPassiveFamilyHonoraryPassive() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.PASSIVE_FAMILY,
        MemberCategoryType.HONORARY_PASSIVE, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationHonoraryActiveHonoraryPassive() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.HONORARY_ACTIVE,
        MemberCategoryType.HONORARY_PASSIVE, LocalDate.now(), LocalDate.now());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMutationHonoraryPassiveHonoraryActive() throws Exception {
    ReportUtils.createMutationLetter(createLetterSender(), createLetterRecipient(), MemberCategoryType.HONORARY_PASSIVE,
        MemberCategoryType.HONORARY_ACTIVE, LocalDate.now(), LocalDate.now());
  }

  private PersonPersistable createLetterSender() throws SaveException {
    PersonPersistable person = new PersonPersistable();

    person.setGivenName("Matthijs");
    person.setFamilyName("de Leeuw");
    person.setShorthand("MaLe");
    person.setMobile("079 111 11 11");
    person.setSex(SexType.MALE);

    PersonFunctionPersistable personFunction = new PersonFunctionPersistable();
    OrganizationFunctionModel function = DaoFactory.getInstance().getOrganizationFunctionDao().load(8);
    personFunction.setRefFunction(function);
    Set<PersonFunctionModel> functions = new HashSet<>();
    functions.add(personFunction);
    person.setRefFunctions(functions);
    person.setRefPrimaryFunction(function);

    DaoFactory.getInstance().getPersonDao().save(person, 0);

    return person;
  }

  private PersonPersistable createLetterRecipient() throws SaveException {
    PersonModel person = new PersonPersistable();

    person.setTitle("Herr");
    person.setGivenName("Max");
    person.setFamilyName("Mustermann");
    person.setSex(SexType.MALE);
    person.setBirthdate(LocalDate.of(1995, 8, 4));

    AddressPersistable address = createAddress("Musterstrasse 1", null, "1234", "Musterdorf", "BL", "CH");
    person.setRefHomeAddress(address);

    person = DaoFactory.getInstance().getPersonDao().save(person, 0);

    person.setStatus(PersonStatus.APPLIED_FOR_ASPIRANT);
    person = DaoFactory.getInstance().getPersonDao().save(person, 0);

    person.setStatus(PersonStatus.NORMAL);
    person = DaoFactory.getInstance().getPersonDao().save(person, 0);

    person.setStatus(PersonStatus.APPLIED_FOR_VK);
    person = DaoFactory.getInstance().getPersonDao().save(person, 0);

    person.setStatus(PersonStatus.NORMAL);
    person = DaoFactory.getInstance().getPersonDao().save(person, 0);

    person.setStatus(PersonStatus.APPLIED_FOR_PASSIVE);
    person = DaoFactory.getInstance().getPersonDao().save(person, 0);

    person.setStatus(PersonStatus.NORMAL);
    person = DaoFactory.getInstance().getPersonDao().save(person, 0);

    person.setStatus(PersonStatus.APPLIED_FOR_NC_OF);
    person = DaoFactory.getInstance().getPersonDao().save(person, 0);

    person.setStatus(PersonStatus.NORMAL);
    person = DaoFactory.getInstance().getPersonDao().save(person, 0);

    person.setStatus(PersonStatus.APPLIED_FOR_OF);
    person = DaoFactory.getInstance().getPersonDao().save(person, 0);

    person.setStatus(PersonStatus.NORMAL);
    person = DaoFactory.getInstance().getPersonDao().save(person, 0);

    return (PersonPersistable) person;
  }

  private AddressPersistable createAddress(String line1, String line2, String zip, String city, String canton,
      String country) throws SaveException {
    AddressPersistable address = new AddressPersistable();

    address.setLine1(line1);
    address.setLine2(line2);
    address.setZipCode(zip);
    address.setCity(city);
    address.setCanton(canton);
    address.setCountry(country);

    // DaoFactory.getInstance().getAddressDao().save(address, 0);

    return address;
  }

}
