package ch.vknws.vierkant.backend.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.FunctionType;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.impl.OrganizationFunctionPersistable;

/**
 * Test class for the RankDao.
 *
 * @author Benjamin Weber
 */
public class TestClassOrganizationFunctionDao {

  private OrganizationFunctionDao dao;

  /**
   * Sets the test up.
   */
  @Before
  public void setUp() {
    dao = DaoFactory.getInstance().getOrganizationFunctionDao();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testIds() throws Exception {
    List<Integer> ids = dao.getAllIds();
    Assert.assertNotNull(ids);
    Assert.assertFalse(ids.isEmpty());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAttributes() throws Exception {
    LocalDateTime startTime = LocalDateTime.now();

    String auditComment = "audit comment";
    boolean deletedFlag = false;
    String functionName = "function name";
    String email = "email@example.com";
    Integer maxPersonCount = 3;
    Integer minPersonCount = 2;
    FunctionType functionType = FunctionType.NORMAL;

    OrganizationFunctionModel model = new OrganizationFunctionPersistable();
    model.setAuditComment(auditComment);
    model.setDeletedFlag(deletedFlag);
    model.setFunctionName(functionName);
    model.setEmail(email);
    model.setMaxPersonCount(maxPersonCount);
    model.setMinPersonCount(minPersonCount);
    model.setType(functionType);

    try {

      model = dao.save(model, 0);

      Assert.assertEquals(functionName, model.getFunctionName());
      Assert.assertEquals(email, model.getEmail());
      Assert.assertEquals(maxPersonCount, model.getMaxPersonCount());
      Assert.assertEquals(minPersonCount, model.getMinPersonCount());
      Assert.assertEquals(functionType, model.getType());
      Assert.assertEquals(auditComment, model.getAuditComment());
      Assert.assertEquals(deletedFlag, model.getDeletedFlag());
      Assert.assertEquals(Integer.valueOf(0), model.getCreator());
      Assert.assertEquals(Integer.valueOf(0), model.getModifier());
      Assert.assertTrue(startTime.isBefore(model.getCreationDateTime()));
      Assert.assertTrue(startTime.isBefore(model.getModificationDateTime()));

    } finally {
      if (model.getRecordId() != null) dao.remove(model.getRecordId());
    }
  }
}
