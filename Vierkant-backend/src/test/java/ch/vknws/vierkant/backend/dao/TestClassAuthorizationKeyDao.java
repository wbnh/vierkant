package ch.vknws.vierkant.backend.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.AuthorizationKeyMode;
import ch.vknws.vierkant.type.AuthorizationKeyModel;
import ch.vknws.vierkant.type.AuthorizationKeyType;
import ch.vknws.vierkant.type.impl.AuthorizationKeyPersistable;

/**
 * Test class for the RankDao.
 *
 * @author Benjamin Weber
 */
public class TestClassAuthorizationKeyDao {

  private AuthorizationKeyDao dao;

  /**
   * Sets the test up.
   */
  @Before
  public void setUp() {
    dao = DaoFactory.getInstance().getAuthorizationKeyDao();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testIds() throws Exception {
    List<Integer> ids = dao.getAllIds();
    Assert.assertNotNull(ids);
    Assert.assertFalse(ids.isEmpty());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAttributes() throws Exception {
    LocalDateTime startTime = LocalDateTime.now();

    String auditComment = "audit comment";
    boolean deletedFlag = false;
    String keyName = "key name";
    AuthorizationKeyType keyType = AuthorizationKeyType.NORMAL;
    AuthorizationKeyMode keyMode = AuthorizationKeyMode.READ;

    AuthorizationKeyModel model = new AuthorizationKeyPersistable();
    model.setAuditComment(auditComment);
    model.setDeletedFlag(deletedFlag);
    model.setKeyName(keyName);
    model.setType(keyType);
    model.setMode(keyMode);

    try {

      model = dao.save(model, 0);

      Assert.assertEquals(keyName, model.getKeyName());
      Assert.assertEquals(keyType, model.getType());
      Assert.assertEquals(keyMode, model.getMode());
      Assert.assertEquals(auditComment, model.getAuditComment());
      Assert.assertEquals(deletedFlag, model.getDeletedFlag());
      Assert.assertEquals(Integer.valueOf(0), model.getCreator());
      Assert.assertEquals(Integer.valueOf(0), model.getModifier());
      Assert.assertTrue(startTime.isBefore(model.getCreationDateTime()));
      Assert.assertTrue(startTime.isBefore(model.getModificationDateTime()));

    } finally {
      if (model.getRecordId() != null) dao.remove(model.getRecordId());
    }
  }
}
