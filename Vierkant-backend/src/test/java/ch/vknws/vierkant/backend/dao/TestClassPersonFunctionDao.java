package ch.vknws.vierkant.backend.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.FunctionType;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.PersonFunctionModel;
import ch.vknws.vierkant.type.impl.OrganizationFunctionPersistable;
import ch.vknws.vierkant.type.impl.PersonFunctionPersistable;

/**
 * Test class for the RankDao.
 *
 * @author Benjamin Weber
 */
public class TestClassPersonFunctionDao {

  private PersonFunctionDao dao;

  /**
   * Sets the test up.
   */
  @Before
  public void setUp() {
    dao = DaoFactory.getInstance().getPersonFunctionDao();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testIds() throws Exception {
    List<Integer> ids = dao.getAllIds();
    Assert.assertNotNull(ids);
    Assert.assertFalse(ids.isEmpty());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAttributes() throws Exception {
    LocalDateTime startTime = LocalDateTime.now();

    String auditComment = "audit comment";
    boolean deletedFlag = false;
    String supervisorComment = "supervisor comment";

    OrganizationFunctionModel function = new OrganizationFunctionPersistable();
    function.setFunctionName("functionname");
    function.setType(FunctionType.NORMAL);

    PersonFunctionModel model = new PersonFunctionPersistable();
    model.setAuditComment(auditComment);
    model.setDeletedFlag(deletedFlag);
    model.setSupervisorComment(supervisorComment);

    try {
      function = DaoFactory.getInstance().getOrganizationFunctionDao().save(function, 0);
      model.setRefFunction(function);

      model = dao.save(model, 0);

      Assert.assertEquals(supervisorComment, model.getSupervisorComment());
      Assert.assertEquals(auditComment, model.getAuditComment());
      Assert.assertEquals(deletedFlag, model.getDeletedFlag());
      Assert.assertEquals(Integer.valueOf(0), model.getCreator());
      Assert.assertEquals(Integer.valueOf(0), model.getModifier());
      Assert.assertTrue(startTime.isBefore(model.getCreationDateTime()));
      Assert.assertTrue(startTime.isBefore(model.getModificationDateTime()));

    } finally {
      if (model.getRecordId() != null && dao.containsId(model.getRecordId())) {
        dao.remove(model.getRecordId());
      }
      if (function.getRecordId() != null
          && DaoFactory.getInstance().getOrganizationFunctionDao().containsId(function.getRecordId())) {
        DaoFactory.getInstance().getOrganizationFunctionDao().remove(function.getRecordId());
      }
    }
  }
}
