package ch.vknws.vierkant.backend.function;

import java.util.LinkedList;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.impl.PersonPersistable;

/**
 * Test class for shorthands.
 *
 * @author Benjamin Weber
 */
public class TestClassPersonShorthandGenerator {

  private PersonPersistable createPerson(String givenName, String familyName) {
    PersonPersistable person = new PersonPersistable();
    person.setGivenName(givenName);
    person.setFamilyName(familyName);
    return person;
  }

  /**
   * Sets up the test.
   */
  @Before
  public void setUp() {
    PersonShorthandGenerator.testTabooShorthands = new LinkedList<>();
  }

  /**
   * Finalises the class.
   */
  @AfterClass
  public static void tearDown() {
    PersonShorthandGenerator.testTabooShorthands = null;
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testGenerate() throws Exception {
    String shorthand;

    shorthand = PersonShorthandGenerator.generate(createPerson("Benjamin", "Weber"));
    Assert.assertEquals("BeWe", shorthand);
    PersonShorthandGenerator.testTabooShorthands.add(shorthand);

    shorthand = PersonShorthandGenerator.generate(createPerson("Benjamin", "Wernli"));
    Assert.assertEquals("BeWr", shorthand);
    PersonShorthandGenerator.testTabooShorthands.add(shorthand);

    shorthand = PersonShorthandGenerator.generate(createPerson("Benjamin", "Weber"));
    Assert.assertEquals("BeWb", shorthand);
    PersonShorthandGenerator.testTabooShorthands.add(shorthand);

    shorthand = PersonShorthandGenerator.generate(createPerson("Benjamin", "Weber"));
    Assert.assertEquals("BnWe", shorthand);
    PersonShorthandGenerator.testTabooShorthands.add(shorthand);

    shorthand = PersonShorthandGenerator.generate(createPerson("Matthijs", "de Leeuw"));
    Assert.assertEquals("MaLe", shorthand);
    PersonShorthandGenerator.testTabooShorthands.add(shorthand);

    shorthand = PersonShorthandGenerator.generate(createPerson("Nicola", "Baschung"));
    Assert.assertEquals("NiBa", shorthand);
    PersonShorthandGenerator.testTabooShorthands.add(shorthand);

    shorthand = PersonShorthandGenerator.generate(createPerson("Dominique", "Vögtli"));
    Assert.assertEquals("DoVo", shorthand);
    PersonShorthandGenerator.testTabooShorthands.add(shorthand);

    shorthand = PersonShorthandGenerator.generate(createPerson("Étienne", "Çava"));
    Assert.assertEquals("EtCa", shorthand);
    PersonShorthandGenerator.testTabooShorthands.add(shorthand);
  }
}
