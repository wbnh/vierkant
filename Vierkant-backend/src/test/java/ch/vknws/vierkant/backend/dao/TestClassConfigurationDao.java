package ch.vknws.vierkant.backend.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.ConfigurationModel;
import ch.vknws.vierkant.type.impl.ConfigurationPersistable;

/**
 * Test class for the RankDao.
 *
 * @author Benjamin Weber
 */
public class TestClassConfigurationDao {

  private ConfigurationDao dao;

  /**
   * Sets the test up.
   */
  @Before
  public void setUp() {
    dao = DaoFactory.getInstance().getConfigurationDao();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testIds() throws Exception {
    List<Integer> ids = dao.getAllIds();
    Assert.assertNotNull(ids);
    Assert.assertFalse(ids.isEmpty());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAttributes() throws Exception {
    LocalDateTime startTime = LocalDateTime.now();

    String auditComment = "audit comment";
    boolean deletedFlag = false;
    String userName = "user name";
    String context = "context";
    String keyName = "keyName";
    String value = "value";

    ConfigurationModel model = new ConfigurationPersistable();
    model.setAuditComment(auditComment);
    model.setDeletedFlag(deletedFlag);
    model.setUserName(userName);
    model.setContext(context);
    model.setKeyName(keyName);
    model.setValue(value);

    try {

      model = dao.save(model, 0);

      Assert.assertEquals(userName, model.getUserName());
      Assert.assertEquals(context, model.getContext());
      Assert.assertEquals(keyName, model.getKeyName());
      Assert.assertEquals(value, model.getValue());
      Assert.assertEquals(auditComment, model.getAuditComment());
      Assert.assertEquals(deletedFlag, model.getDeletedFlag());
      Assert.assertEquals(Integer.valueOf(0), model.getCreator());
      Assert.assertEquals(Integer.valueOf(0), model.getModifier());
      Assert.assertTrue(startTime.isBefore(model.getCreationDateTime()));
      Assert.assertTrue(startTime.isBefore(model.getModificationDateTime()));

    } finally {
      if (model.getRecordId() != null) dao.remove(model.getRecordId());
    }
  }
}
