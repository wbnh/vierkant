package ch.vknws.vierkant.backend.dao;

import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.SexType;
import ch.vknws.vierkant.type.TrainingCourseInstanceModel;
import ch.vknws.vierkant.type.TrainingCourseModel;
import ch.vknws.vierkant.type.impl.PersonPersistable;
import ch.vknws.vierkant.type.impl.TrainingCourseInstancePersistable;
import ch.vknws.vierkant.type.impl.TrainingCoursePersistable;

/**
 * Test class for the RankDao.
 *
 * @author Benjamin Weber
 */
public class TestClassTrainingCourseInstanceDao {

  private TrainingCourseInstanceDao dao;

  /**
   * Sets the test up.
   */
  @Before
  public void setUp() {
    dao = DaoFactory.getInstance().getTrainingCourseInstanceDao();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAttributes() throws Exception {
    LocalDateTime startTime = LocalDateTime.now();

    String auditComment = "audit comment";
    boolean deletedFlag = false;

    TrainingCourseModel template = new TrainingCoursePersistable();
    template.setTrainingCourseName("name");

    PersonModel responsible = new PersonPersistable();
    responsible.setFamilyName("familyname");
    responsible.setSex(SexType.FEMALE);

    TrainingCourseInstanceModel model = new TrainingCourseInstancePersistable();
    model.setAuditComment(auditComment);
    model.setDeletedFlag(deletedFlag);

    try {

      template = DaoFactory.getInstance().getTrainingCourseDao().save(template, 0);
      model.setRefTemplate(template);

      responsible = DaoFactory.getInstance().getPersonDao().save(responsible, 0);
      model.setRefResponsible(responsible);

      model = dao.save(model, 0);

      Assert.assertEquals(auditComment, model.getAuditComment());
      Assert.assertEquals(deletedFlag, model.getDeletedFlag());
      Assert.assertEquals(Integer.valueOf(0), model.getCreator());
      Assert.assertEquals(Integer.valueOf(0), model.getModifier());
      Assert.assertTrue(startTime.isBefore(model.getCreationDateTime()));
      Assert.assertTrue(startTime.isBefore(model.getModificationDateTime()));

    } finally {
      if (model.getRecordId() != null && dao.containsId(model.getRecordId())) {
        dao.remove(model.getRecordId());
      }
      if (template.getRecordId() != null
          && DaoFactory.getInstance().getTrainingCourseDao().containsId(template.getRecordId())) {
        DaoFactory.getInstance().getTrainingCourseDao().remove(template.getRecordId());
      }
      if (responsible.getRecordId() != null
          && DaoFactory.getInstance().getPersonDao().containsId(responsible.getRecordId())) {
        DaoFactory.getInstance().getPersonDao().remove(responsible.getRecordId());
      }
    }
  }
}
