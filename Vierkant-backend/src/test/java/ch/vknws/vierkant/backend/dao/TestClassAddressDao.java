package ch.vknws.vierkant.backend.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.AddressModel;
import ch.vknws.vierkant.type.impl.AddressPersistable;

/**
 * Test class for the RankDao.
 *
 * @author Benjamin Weber
 */
public class TestClassAddressDao {

  private AddressDao dao;

  /**
   * Sets the test up.
   */
  @Before
  public void setUp() {
    dao = DaoFactory.getInstance().getAddressDao();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testIds() throws Exception {
    List<Integer> ids = dao.getAllIds();
    Assert.assertNotNull(ids);
    Assert.assertFalse(ids.isEmpty());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAttributes() throws Exception {
    LocalDateTime startTime = LocalDateTime.now();

    String auditComment = "audit comment";
    boolean deletedFlag = false;
    String line1 = "line 1";
    String line2 = "line 2";
    String zipcode = "1234";
    String city = "city";
    String canton = "BL";
    String country = "CH";

    AddressModel model = new AddressPersistable();
    model.setAuditComment(auditComment);
    model.setDeletedFlag(deletedFlag);
    model.setLine1(line1);
    model.setLine2(line2);
    model.setZipCode(zipcode);
    model.setCity(city);
    model.setCanton(canton);
    model.setCountry(country);

    try {

      model = dao.save(model, 0);

      Assert.assertEquals(line1, model.getLine1());
      Assert.assertEquals(line2, model.getLine2());
      Assert.assertEquals(zipcode, model.getZipCode());
      Assert.assertEquals(city, model.getCity());
      Assert.assertEquals(canton, model.getCanton());
      Assert.assertEquals(country, model.getCountry());
      Assert.assertEquals(auditComment, model.getAuditComment());
      Assert.assertEquals(deletedFlag, model.getDeletedFlag());
      Assert.assertEquals(Integer.valueOf(0), model.getCreator());
      Assert.assertEquals(Integer.valueOf(0), model.getModifier());
      Assert.assertTrue(startTime.isBefore(model.getCreationDateTime()));
      Assert.assertTrue(startTime.isBefore(model.getModificationDateTime()));

    } finally {
      if (model.getRecordId() != null) dao.remove(model.getRecordId());
    }
  }
}
