package ch.vknws.vierkant.rules.module.secretary;

/**
 * Actions to perform upon mutation.
 *
 * @author Benjamin Weber
 */
public enum MutationAction {

  /**
   * Generate a new shorthand.
   */
  GENERATE_SHORTHAND,

  /**
   * Appoint the person as aspirant.
   */
  APPOINT_ASPIRANT,

  /**
   * Appoint the person as president.
   */
  APPOINT_PRESIDENT,

  /**
   * Appoint the person as a selected rank.
   */
  APPOINT_RANK,

  /**
   * Brevet the person to the next rank.
   */
  BREVET,

  /**
   * Remove the rank from a person.
   */
  REMOVE_RANK,

  /**
   * Revoke president status.
   */
  REVOKE_PRESIDENT,

  /**
   * Start sending the newsletter.
   */
  SEND_NEWSLETTER,

  /**
   * Stop sending the newsletter.
   */
  REVOKE_NEWSLETTER,

  /**
   * Remove the shorthand.
   */
  REVOKE_SHORTHAND;
}
