package ch.vknws.vierkant.rules.module.secretary;

import ch.vknws.vierkant.type.RankModel;

/**
 * Loads ranks from an external source.
 *
 * @author Benjamin Weber
 */
public interface RankLoader {

  /**
   * Loads a rank by abbreviation.
   * 
   * @param abbreviation the rank's abbreviation
   * @return the associated rank model
   */
  RankModel loadByAbbreviation(String abbreviation);
}
