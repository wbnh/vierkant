package ch.vknws.vierkant.rules;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.kie.api.KieBase;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.utils.KieHelper;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.faa.v1.core.util.Pair;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.vknws.vierkant.rules.data.CantonData;
import ch.vknws.vierkant.rules.module.secretary.MutationAction;
import ch.vknws.vierkant.rules.module.secretary.RankLoader;
import ch.vknws.vierkant.type.MemberCategoryType;
import ch.vknws.vierkant.type.PersonModel;

/**
 * Registry to execute the rules on a model.
 *
 * @author Benjamin Weber
 */
public class RuleRegistry {
  private static final CantonData CANTON_DATA = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(CantonData.class));
  private static final List<String> COUNTRIES = Arrays.asList(Locale.getISOCountries());

  private static final String[] MUTATION_RULES = { "/rules/mutation/MutationActions.drl",
      "/rules/mutation/MutationFacts.drl", "/rules/mutation/PerformMutationActions.drl" };

  private static final String[] VALIDATION_RULES = { "/rules/validation/AargauLicensePersistable.drl",
      "rules/validation/AargauPersonLicensePersistable.drl", "rules/validation/AbstractPersistableRules.drl",
      "rules/validation/AddressPersistable.drl", "rules/validation/AuthorizationKeyPersistable.drl",
      "rules/validation/ConfigurationPersistable.drl", "rules/validation/ContactPersonPersistable.drl",
      "rules/validation/FunctionGroupPersistable.drl", "rules/validation/MedicalInformationPersistable.drl",
      "rules/validation/MemberGroupPersistable.drl", "rules/validation/OrganizationFunctionPersistable.drl",
      "rules/validation/PersonFunctionPersistable.drl", "rules/validation/PersonPersistable.drl",
      "rules/validation/PrerequisitePersistable.drl", "rules/validation/RankPersistable.drl",
      "rules/validation/TechnicalEventPersistable.drl", "rules/validation/TrainingCourseInstancePersistable.drl",
      "rules/validation/TrainingCoursePersistable.drl" };

  private static final String[] NORMALIZATION_RULES = { "/rules/normalization/AargauLicensePersistable.drl",
      "rules/normalization/AargauPersonLicensePersistable.drl", "rules/normalization/AbstractPersistableRules.drl",
      "rules/normalization/AddressPersistable.drl", "rules/normalization/AuthorizationKeyPersistable.drl",
      "rules/normalization/ConfigurationPersistable.drl", "rules/normalization/ContactPersonPersistable.drl",
      "rules/normalization/FunctionGroupPersistable.drl", "rules/normalization/MedicalInformationPersistable.drl",
      "rules/normalization/MemberGroupPersistable.drl", "rules/normalization/OrganizationFunctionPersistable.drl",
      "rules/normalization/PersonFunctionPersistable.drl", "rules/normalization/PersonPersistable.drl",
      "rules/normalization/PrerequisitePersistable.drl", "rules/normalization/RankPersistable.drl",
      "rules/normalization/TechnicalEventPersistable.drl", "rules/normalization/TrainingCourseInstancePersistable.drl",
      "rules/normalization/TrainingCoursePersistable.drl" };

  private static KieBase normalizationBase;
  private static KieBase validationBase;
  private static KieBase mutationBase;

  static {
    KieHelper normalizationHelper = createHelper();
    addRuleFilesToHelper(normalizationHelper, NORMALIZATION_RULES);
    normalizationBase = normalizationHelper.build();

    KieHelper validationHelper = createHelper();
    addRuleFilesToHelper(validationHelper, VALIDATION_RULES);
    validationBase = validationHelper.build();

    KieHelper mutationHelper = createHelper();
    addRuleFilesToHelper(mutationHelper, MUTATION_RULES);
    mutationBase = mutationHelper.build();
  }

  private static void addRuleFilesToHelper(KieHelper helper, String[] fileNames) {
    for (String fileName : fileNames) {
      URL resource = RuleRegistry.class.getClassLoader().getResource(fileName);
      if (resource == null) {
        try {
          resource = Paths.get("../Vierkant-rules/src/main/resources", fileName).toUri().toURL();
        } catch (MalformedURLException e) {
          throw new RuntimeException(e);
        }
      }

      helper.addResource(ResourceFactory.newUrlResource(resource), ResourceType.DRL);
    }
  }

  private static KieHelper createHelper() {
    URL kmodule = RuleRegistry.class.getClassLoader().getResource("/META-INF/kmodule.xml");

    if (kmodule == null) {
      try {
        kmodule = Paths.get("../Vierkant-rules/src/main/resources/META-INF/kmodule.xml").toUri().toURL();
      } catch (MalformedURLException e) {
        throw new RuntimeException(e);
      }
    }

    KieHelper helper = new KieHelper();
    helper.addResource(ResourceFactory.newUrlResource(kmodule));

    return helper;
  }

  /**
   * Normalises the values in the passed model.
   * 
   * @param persistable the model to normalise
   */
  public static void normalizeModel(DatabaseModel persistable) {

    KieSession kieSession = normalizationBase.newKieSession();

    try {
      kieSession.insert(persistable);
      kieSession.fireAllRules();
    } finally {
      if (kieSession != null) {
        kieSession.dispose();
      }
    }
  }

  /**
   * Checks if the model has valid values.
   * 
   * @param persistable the model to check
   * @return the result of the validation
   */
  public static ValidationResult validateModel(DatabaseModel persistable) {
    normalizeModel(persistable);

    KieSession kSession = validationBase.newKieSession();
    ValidationResult result = new ValidationResult();

    try {
      kSession.setGlobal("result", result);
      kSession.setGlobal("cantons", CANTON_DATA);
      kSession.setGlobal("countries", COUNTRIES);
      kSession.insert(persistable);
      kSession.fireAllRules();
    } finally {
      if (kSession != null) {
        kSession.destroy();
      }
    }

    return result;
  }

  /**
   * Loads the member categories that can be mutated to form the passed current
   * category.
   * 
   * @param source the source category of the mutation
   * @return the categories that can be mutated into
   */
  public static Set<MemberCategoryType> loadFollowingCategories(MemberCategoryType source) {

    KieSession kSession = mutationBase.newKieSession();

    Set<MemberCategoryType> targetCategories = new LinkedHashSet<>();

    try {
      kSession.setGlobal("targetCategories", targetCategories);
      kSession.insert(source);
      kSession.fireAllRules();
    } finally {
      if (kSession != null) {
        kSession.destroy();
      }
    }

    return targetCategories;
  }

  /**
   * Loads the mutation actions for the passed mutation.
   * 
   * @param person      the person to mutate
   * @param newCategory the category to mutate to
   * @return the mutation actions
   */
  public static Set<MutationAction> loadMutationActions(PersonModel person, MemberCategoryType newCategory) {

    KieSession kSession = mutationBase.newKieSession();

    Set<MutationAction> mutationActions = new HashSet<>();
    Pair<MemberCategoryType, MemberCategoryType> mutation = new Pair<>(person.getMemberCategory(), newCategory);

    try {
      kSession.setGlobal("mutationActions", mutationActions);
      kSession.insert(mutation);
      kSession.fireAllRules();
    } finally {
      if (kSession != null) {
        kSession.destroy();
      }
    }

    return mutationActions;
  }

  /**
   * Mutates a person into a new category.
   * 
   * @param person             the person to mutate
   * @param newCategory        the category to mutate to
   * @param rankLoader         the loader of rank information
   * @param generatedShorthand the shorthand to use if necessary
   * @return the mutation actions that still need to be performed
   */
  public static Set<MutationAction> mutatePerson(PersonModel person, MemberCategoryType newCategory,
      RankLoader rankLoader, String generatedShorthand) {
    return mutatePerson(person, loadMutationActions(person, newCategory), rankLoader, generatedShorthand);
  }

  /**
   * Mutates a person into a new category.
   * 
   * @param person             the person to mutate
   * @param mutationActions    the predetermined mutation actions
   * @param rankLoader         the loader of rank information
   * @param generatedShorthand the shorthand to use if necessary
   * @return the mutation actions that may still need to be performed
   */
  public static Set<MutationAction> mutatePerson(PersonModel person, Set<MutationAction> mutationActions,
      RankLoader rankLoader, String generatedShorthand) {
    if (mutationActions == null) { return mutationActions; }

    KieSession kSession = mutationBase.newKieSession();

    try {
      kSession.setGlobal("person", person);
      kSession.setGlobal("rankLoader", rankLoader);
      kSession.setGlobal("generatedShorthand", generatedShorthand);
      mutationActions.forEach(a -> kSession.insert(a));
      kSession.fireAllRules();
    } finally {
      if (kSession != null) {
        kSession.destroy();
      }
    }

    return mutationActions;
  }

}
