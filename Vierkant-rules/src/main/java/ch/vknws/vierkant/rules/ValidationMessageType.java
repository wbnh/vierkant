package ch.vknws.vierkant.rules;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.Configuration;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;

/**
 * The different types a message can be about.
 *
 * @author Benjamin Weber
 */
public enum ValidationMessageType {

  /**
   * The value was out of the allowed bounds.
   */
  OUT_OF_BOUNDS,

  /**
   * The value was null.
   */
  NULL,

  /**
   * The value was not in the allowed list.
   */
  NOT_IN_VALID_LIST,

  /**
   * The value was not null.
   */
  NOT_NULL,

  /**
   * The value has already been added.
   */
  MODEL_ALREADY_ADDED,

  /**
   * The email value is incorrectly formatted.
   */
  INVALID_EMAIL,

  /**
   * The upper bound is less than the lower bound.
   */
  MAXIMUM_LESS_THAN_MINIMUM,

  /**
   * The date is in the future, but shouldn't.
   */
  DATE_IN_FUTURE,

  /**
   * The given file does not exist.
   */
  NO_SUCH_FILE;

  private static final Configuration texts = ServiceRegistry.getConfigurationProxy()
      .loadConfiguration(new ConfigurationProperties<>(ValidationMessageType.class));

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return texts.getStringValue(name());
  }
}
