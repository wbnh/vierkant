package ch.vknws.vierkant.rules.module.secretary;

import java.util.Set;

import ch.bwe.faa.v1.core.util.Pair;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.rules.rights.AuthorizationRules;
import ch.vknws.vierkant.rules.rights.UserRules;
import ch.vknws.vierkant.type.AuthorizationKeyMode;
import ch.vknws.vierkant.type.PersonModel;

/**
 * Rules for the secretary module.
 *
 * @author Benjamin Weber
 */
public class SecretaryRules {
  private static SecretaryRules instance = new SecretaryRules();

  /**
   * @return the instance
   */
  public static SecretaryRules getInstance() {
    return instance;
  }

  /**
   * Constructor handling initialization of mandatory fields.
   */
  private SecretaryRules() {
  }

  /**
   * Checks if medical information is visible to the passed user.
   * 
   * @param target       the person whose information shall be displayed
   * @param loggedInUser the user requesting the information
   * @return whether it shall be visible
   */
  public boolean isMedicalInformationVisible(PersonModel target, PersonModel loggedInUser) {
    Set<Pair<String, AuthorizationKeyMode>> authorizationKeys = AuthorizationRules.getInstance()
        .getEffectiveAuthorizationKeys(loggedInUser);

    if (AuthorizationRules.getInstance()
        .hasAuthorization(SecretaryAuthorizationKeys.MEDICAL_INFORMATION_ANY, authorizationKeys)
        .isPresent()) { return true; }

    if (AuthorizationRules.getInstance()
        .hasAuthorization(SecretaryAuthorizationKeys.MEDICAL_INFORMATION_SUB, authorizationKeys).isPresent()
        && UserRules.getInstance().isSubordinate(target, loggedInUser)) { return true; }

    return false;
  }

  /**
   * Checks if classified medical information is visible to the passed user.
   * 
   * @param target       the person whose information shall be displayed
   * @param loggedInUser the user requesting the information
   * @return whether it shall be visible
   */
  public boolean isClassifiedMedicalInformationVisible(PersonModel target, PersonModel loggedInUser) {
    Set<Pair<String, AuthorizationKeyMode>> authorizationKeys = AuthorizationRules.getInstance()
        .getEffectiveAuthorizationKeys(loggedInUser);

    if (AuthorizationRules.getInstance()
        .hasAuthorization(SecretaryAuthorizationKeys.CLASSIFIED_MEDICAL_INFORMATION_ANY, authorizationKeys)
        .isPresent()) { return true; }

    if (AuthorizationRules.getInstance()
        .hasAuthorization(SecretaryAuthorizationKeys.CLASSIFIED_MEDICAL_INFORMATION_SUB, authorizationKeys).isPresent()
        && UserRules.getInstance().isSubordinate(target, loggedInUser)) { return true; }

    return false;
  }
}
