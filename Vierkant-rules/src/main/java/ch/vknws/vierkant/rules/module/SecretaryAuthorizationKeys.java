package ch.vknws.vierkant.rules.module;

/**
 * Authorization keys for the secretary module.
 *
 * @author Benjamin Weber
 */
public interface SecretaryAuthorizationKeys {

  /**
   * Concerns all aargau licenses.
   */
  final String AARGAU_LICENSE_ANY = "aargauLicenseAny";

  /**
   * Concerns admin privileges.
   */
  final String ADMIN = "admin";

  /**
   * Concerns medical information of any person.
   */
  final String MEDICAL_INFORMATION_ANY = "medicalInformationAny";

  /**
   * Concerns medical information of subordinates.
   */
  final String MEDICAL_INFORMATION_SUB = "medicalInformationSub";

  /**
   * Concerns classified medical information of any person.
   */
  final String CLASSIFIED_MEDICAL_INFORMATION_ANY = "classifiedMedicalInformationAny";

  /**
   * Concerns classified medical information of subordinates.
   */
  final String CLASSIFIED_MEDICAL_INFORMATION_SUB = "classifiedMedicalInformationSub";

  /**
   * Concerns all function data.
   */
  final String FUNCTION_ANY = "functionAny";

  /**
   * Concerns the object history of any object.
   */
  final String HISTORY_ANY = "historyAny";

  /**
   * Concerns the person's own data.
   */
  final String OWN_DATA = "ownData";

  /**
   * Concerns all person data.
   */
  final String PERSON_ANY = "personAny";

  /**
   * Concerns subordinates.
   */
  final String PERSON_SUB = "personSub";

  /**
   * Concerns any rank data.
   */
  final String RANK_ANY = "rankAny";

  /**
   * Concerns all technical events.
   */
  final String TECHNICAL_EVENTS_ANY = "technicalEventsAny";

  /**
   * Concerns all training courses.
   */
  final String TRAINING_COURSE_ANY = "trainingCourseAny";

  /**
   * Concerns training course where the person is responsible.
   */
  final String TRAINING_COURSE_OWN = "trainingCourseOwn";

  /**
   * Concerns all training course instances.
   */
  final String TRAINING_COURSE_INSTANCE_ANY = "trainingCourseInstanceAny";

  /**
   * Concerns the training course instances where the person is a teacher.
   */
  final String TRAINING_COURSE_INSTANCE_OWN = "trainingCourseInstanceOwn";

  /**
   * Concerns the meeting types.
   */
  final String MEETING_TYPE_ANY = "meetingTypeAny";

  /**
   * Concerns any meeting.
   */
  final String MEETING_ANY = "meetingAny";

  /**
   * Concerns own meetings.
   */
  final String MEETING_OWN = "meetingOwn";
}
