package ch.vknws.vierkant.rules.rights;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import ch.bwe.faa.v1.core.util.Pair;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.type.AuthorizationKeyMode;
import ch.vknws.vierkant.type.AuthorizationKeyModel;
import ch.vknws.vierkant.type.PersonFunctionModel;
import ch.vknws.vierkant.type.PersonModel;

/**
 * Utility for authorization.
 *
 * @author Benjamin Weber
 */
public class AuthorizationRules {
  private static AuthorizationRules instance = new AuthorizationRules();

  /**
   * @return the instance
   */
  public static AuthorizationRules getInstance() {
    return instance;
  }

  /**
   * Constructor handling initialization of mandatory fields.
   */
  private AuthorizationRules() {
  }

  /**
   * Checks whether the passed person has the passed authorisation.
   * 
   * @param authorizationKey the authorisation to check
   * @param person           the person requesting access
   * @param mode             the requested authorisation mode
   * @return the effective authorisation mode
   */
  public Optional<AuthorizationKeyMode> hasAuthorization(String authorizationKey, PersonModel person,
      AuthorizationKeyMode mode) {
    return hasAuthorization(authorizationKey, getEffectiveAuthorizationKeys(person), mode);
  }

  /**
   * Checks whether the passed authorisation has the passed nested authorisation.
   * 
   * @param authorizationKey the authorisation to check
   * @param keys             the nested authorisations
   * @return the effective mode
   */
  public Optional<AuthorizationKeyMode> hasAuthorization(String authorizationKey,
      Set<Pair<String, AuthorizationKeyMode>> keys) {
    return hasAuthorization(authorizationKey, keys, null);
  }

  /**
   * Checks whether the passed person has the passed authorisation.
   * 
   * @param authorizationKey the authorisation to check
   * @param person           the person to check for
   * @return the effective mode
   */
  public Optional<AuthorizationKeyMode> hasAuthorization(String authorizationKey, PersonModel person) {
    return hasAuthorization(authorizationKey, getEffectiveAuthorizationKeys(person));
  }

  /**
   * Checks whether the passed authorisation has the passed nested authorisation.
   * 
   * @param authorizationKey the authorisation to check
   * @param keys             the nested authorisations
   * @param mode             the requested mode
   * @return the effective mode
   */
  public Optional<AuthorizationKeyMode> hasAuthorization(String authorizationKey,
      Set<Pair<String, AuthorizationKeyMode>> keys, AuthorizationKeyMode mode) {
    if (authorizationKey == null || keys == null || keys.isEmpty()) { return Optional.empty(); }

    if (isAdmin(keys)) { return Optional.of(AuthorizationKeyMode.WRITE); }

    for (Pair<String, AuthorizationKeyMode> pair : keys) {
      if (Objects.equals(pair.getA(), authorizationKey)
          && (mode == null || Objects.equals(mode, pair.getB()))) { return Optional.of(pair.getB()); }
    }

    return Optional.empty();
  }

  private boolean isAdmin(Set<Pair<String, AuthorizationKeyMode>> keys) {
    for (Pair<String, AuthorizationKeyMode> pair : keys) {
      if (SecretaryAuthorizationKeys.ADMIN.equals(pair.getA())) { return true; }
    }
    return false;
  }

  /**
   * Gets the effective authorisation keys and modes for the passed person.
   * 
   * @param person the person to check
   * @return the authorisation information
   */
  public Set<Pair<String, AuthorizationKeyMode>> getEffectiveAuthorizationKeys(PersonModel person) {
    Set<Pair<String, AuthorizationKeyMode>> keys = new HashSet<>();
    if (person == null) { return keys; }

    addEffectiveAuthorizationKeys(person.getRefAuthorizationKey(), keys);

    if (person.getRefRank() != null) {
      addEffectiveAuthorizationKeys(person.getRefRank().getRefAuthorizationKey(), keys);
    }

    if (person.getRefFunctions() != null && !person.getRefFunctions().isEmpty()) {
      for (PersonFunctionModel function : person.getRefFunctions()) {
        if (function.getRefFunction().getRefGroups() != null) {
          function.getRefFunction().getRefGroups()
              .forEach(g -> addEffectiveAuthorizationKeys(g.getRefAuthorizationKey(), keys));
        }
        addEffectiveAuthorizationKeys(function.getRefFunction().getRefAuthorizationKey(), keys);
      }
    }

    return keys;
  }

  private void addEffectiveAuthorizationKeys(AuthorizationKeyModel authorizationKeyModel,
      Set<Pair<String, AuthorizationKeyMode>> keys) {
    HashSet<Pair<String, AuthorizationKeyMode>> denied = new HashSet<>();
    addEffectiveAuthorizationKeys0(authorizationKeyModel, keys, denied);

    for (Iterator<Pair<String, AuthorizationKeyMode>> iterator = keys.iterator(); iterator.hasNext();) {
      Pair<String, AuthorizationKeyMode> pair = iterator.next();
      if (denied.contains(pair)) {
        iterator.remove();
      }
    }
  }

  private void addEffectiveAuthorizationKeys0(AuthorizationKeyModel authorizationKeyModel,
      Set<Pair<String, AuthorizationKeyMode>> keys, Set<Pair<String, AuthorizationKeyMode>> denied) {
    if (authorizationKeyModel == null) { return; }
    if (authorizationKeyModel.getRefSubordinates() != null
        && !(authorizationKeyModel.getKeyName() != null && authorizationKeyModel.getKeyName().startsWith("admin"))) {
      for (AuthorizationKeyModel subordinate : authorizationKeyModel.getRefSubordinates()) {
        addEffectiveAuthorizationKeys0(subordinate, keys, denied);
      }
    }

    if (authorizationKeyModel.getType() == null) {
      keys.add(new Pair<>(authorizationKeyModel.getKeyName(), AuthorizationKeyMode.WRITE));
    } else {
      switch (authorizationKeyModel.getType()) {
      case ALLOW:
        keys.add(new Pair<>(authorizationKeyModel.getKeyName(), authorizationKeyModel.getMode()));
        break;
      case DENY:
        denied.add(new Pair<>(authorizationKeyModel.getKeyName(), authorizationKeyModel.getMode()));
        break;
      case NORMAL:
        keys.add(new Pair<>(authorizationKeyModel.getKeyName(), AuthorizationKeyMode.WRITE));
        break;
      }
    }
  }
}
