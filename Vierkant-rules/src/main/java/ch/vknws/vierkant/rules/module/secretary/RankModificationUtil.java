package ch.vknws.vierkant.rules.module.secretary;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility class to help with rank modification.
 *
 * @author Benjamin Weber
 */
public class RankModificationUtil {

  /**
   * All possible promotions.
   */
  public static final Map<String, String> PROMOTIONS;
  static {
    Map<String, String> values = new HashMap<>();
    values.put("VK", "Gfr");
    values.put("Gfr", "Obgfr");
    values.put("Kpl", "Wm");
    values.put("Fw", "Hptfw");
    values.put("Stabsadj", "Hptadj");
    values.put("Lt", "Oblt");
    values.put("Hptm", "Maj");
    values.put("Maj", "Oberstlt");

    PROMOTIONS = Collections.unmodifiableMap(values);
  }

  /**
   * All possible brevets.
   */
  public static final Map<String, String> BREVETS;
  static {
    Map<String, String> values = new HashMap<>();
    values.put("Asp", "VK");
    values.put("VK", "Kpl");
    values.put("Gfr", "Kpl");
    values.put("Obgfr", "Kpl");
    values.put("Kpl", "Lt");
    values.put("Wm", "Lt");
    values.put("Fw", "Lt");
    values.put("Hptfw", "Lt");
    values.put("Adj Uof", "Lt");
    values.put("Stabsadj", "Lt");
    values.put("Hptadj", "Lt");

    BREVETS = Collections.unmodifiableMap(values);
  }

  /**
   * The abbreviation of the aspirant rank.
   */
  public static final String ASPIRANT = "Asp";

  /**
   * The abbreviation of the verkehrskadett rank.
   */
  public static final String VK = "VK";

  /**
   * The abbreviation of the president rank.
   */
  public static final String PRESIDENT = "Oberst";

  /**
   * The abbreviation of the rank a person receives after not being president any
   * more.
   */
  public static final String PRESIDENT_REVOKED = "Oberstlt";

}
