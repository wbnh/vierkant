package ch.vknws.vierkant.rules.rights;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;

import ch.vknws.vierkant.type.FunctionGroupModel;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.PersonFunctionModel;
import ch.vknws.vierkant.type.PersonModel;

/**
 * Utility about rules and capabilities of the currently logged in user.
 *
 * @author Benjamin Weber
 */
public class UserRules {
  private static UserRules instance = new UserRules();

  /**
   * @return the instance
   */
  public static UserRules getInstance() {
    return instance;
  }

  /**
   * Constructor handling initialization of mandatory fields.
   */
  private UserRules() {
  }

  /**
   * Checks whether superiority exists.
   * 
   * @param subordinate the supposed subordinate
   * @param superior    the supposed superior
   * @return whether the superiority exists
   */
  public boolean isSubordinate(PersonModel subordinate, PersonModel superior) {
    if (subordinate == null || superior == null) { return false; }

    List<Integer> superiorFunctions = new LinkedList<>();
    for (PersonFunctionModel function : superior.getRefFunctions()) {
      superiorFunctions.add(function.getRefFunction().getRecordId());
    }

    Queue<OrganizationFunctionModel> functions = new LinkedList<>();
    for (PersonFunctionModel function : subordinate.getRefFunctions()) {
      functions.addAll(function.getRefFunction().getRefSuperiors());
    }

    while (!functions.isEmpty()) {
      OrganizationFunctionModel function = functions.poll();
      functions.addAll(function.getRefSuperiors());

      if (superiorFunctions.contains(function.getRecordId())) { return true; }
    }

    return false;
  }

  /**
   * Checks if the passed person is in the passed function group.
   * 
   * @param person        the person to check
   * @param functionGroup the function group to assume
   * @return whether group membership exists
   */
  public boolean isInFunctionGroup(PersonModel person, FunctionGroupModel functionGroup) {
    if (person == null || functionGroup == null) { return false; }

    Set<OrganizationFunctionModel> groupMembers = functionGroup.getRefGroupMembers();

    for (OrganizationFunctionModel member : groupMembers) {
      for (PersonFunctionModel personFunction : person.getRefFunctions()) {
        if (Objects.equals(member.getRecordId(), personFunction.getRefFunction().getRecordId())) { return true; }
      }
    }

    return false;
  }

}
