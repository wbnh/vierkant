package ch.vknws.vierkant.rules.rights;

/**
 * Rule utility about members of the organization.
 *
 * @author Benjamin Weber
 *
 */
public class MemberRules {
	private static MemberRules instance = new MemberRules();

	/**
	 * @return the instance
	 */
	public static MemberRules getInstance() {
		return instance;
	}

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	private MemberRules() {
	}

}
