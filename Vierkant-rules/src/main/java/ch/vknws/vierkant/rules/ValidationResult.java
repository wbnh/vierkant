package ch.vknws.vierkant.rules;

import java.util.LinkedList;
import java.util.List;

/**
 * Result of a validation.
 *
 * @author Benjamin Weber
 */
public class ValidationResult {

  private List<ValidationMessage> messages = new LinkedList<>();

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public ValidationResult() {
  }

  /**
   * @param result the result to add the messages of
   */
  public void add(ValidationResult result) {
    if (result != null) {
      messages.addAll(result.getMessages());
    }
  }

  /**
   * @param message the message to add
   */
  public void add(ValidationMessage message) {
    if (message != null) {
      messages.add(message);
    }
  }

  /**
   * @return the messages in the result
   */
  public List<ValidationMessage> getMessages() {
    return messages;
  }

  /**
   * @return whether validation was successful
   */
  public boolean isSuccessful() {
    return messages.isEmpty();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    for (ValidationMessage message : messages) {
      if (builder.length() > 0) {
        builder.append(", ");
      }
      builder.append(message);
    }
    return builder.toString();
  }
}
