package ch.vknws.vierkant.rules;

import java.util.Collection;

import ch.bwe.faa.v1.core.util.AssertUtils;

/**
 * Message about a validation problem.
 *
 * @author Benjamin Weber
 */
public class ValidationMessage {

  private String field;
  private ValidationMessageType type;
  private int lowerBound;
  private int upperBound;
  private Collection<Object> allowedValues;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param field the field that is concerned
   * @param type  the type of the message
   */
  public ValidationMessage(String field, ValidationMessageType type) {
    AssertUtils.notNull(field);
    AssertUtils.notNull(type);
    this.field = field;
    this.type = type;
  }

  /**
   * @return the message type
   */
  public ValidationMessageType getType() {
    return type;
  }

  /**
   * @return the lower bound for the value if applicable
   */
  public int getLowerBound() {
    return lowerBound;
  }

  /**
   * @param lowerBound the new lower bound
   */
  public void setLowerBound(int lowerBound) {
    this.lowerBound = lowerBound;
  }

  /**
   * @return the upper bound if applicable
   */
  public int getUpperBound() {
    return upperBound;
  }

  /**
   * @param upperBound the new upper bound
   */
  public void setUpperBound(int upperBound) {
    this.upperBound = upperBound;
  }

  /**
   * @return the concerned field
   */
  public String getField() {
    return field;
  }

  /**
   * @param field the new concerned field
   */
  public void setField(String field) {
    this.field = field;
  }

  /**
   * @return the allowed values in the field
   */
  public Collection<Object> getAllowedValues() {
    return allowedValues;
  }

  /**
   * @param allowedValues the new allowed values in the field
   */
  @SuppressWarnings("unchecked")
  public void setAllowedValues(@SuppressWarnings("rawtypes") Collection allowedValues) {
    this.allowedValues = allowedValues;
  }

  /**
   * @param type the new message type
   */
  public void setType(ValidationMessageType type) {
    this.type = type;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return type.toString();
    // return type.name();
  }

}
