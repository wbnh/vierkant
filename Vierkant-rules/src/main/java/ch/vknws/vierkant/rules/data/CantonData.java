package ch.vknws.vierkant.rules.data;

import java.util.List;

/**
 * Data about the swiss cantons.
 *
 * @author Benjamin Weber
 */
public interface CantonData {

  /**
   * @return the list of shorthand canton names
   */
  List<String> cantonCodes();

}
