package ch.vknws.vierkant.ui.admin;

import java.util.LinkedList;
import java.util.List;

import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

import ch.vknws.vierkant.type.impl.PersonPersistable;
import ch.vknws.vierkant.ui.dialog.OneButtonViewDialog;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * Field to select a PersonModel.
 *
 * @author Benjamin Weber
 */
public class PersonSelectField extends HorizontalLayout {

  private static final long serialVersionUID = -3005363565231879353L;

  private Button selectButton;
  private Label valueLabel;
  private PersonPersistable selectedPerson;
  private List<SelectionListener> selectionListeners = new LinkedList<>();
  private List<Integer> disallowedIds;

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public PersonSelectField() {

    setUp();
  }

  /**
   * @param disallowedIds the disallowedIds to set
   */
  public void setDisallowedIds(List<Integer> disallowedIds) {
    this.disallowedIds = disallowedIds;
  }

  private void setUp() {
    setMargin(false);
    setSpacing(true);

    selectButton = new Button();
    addComponent(selectButton);
    selectButton.setIcon(FontAwesome.PENCIL);
    selectButton.addClickListener(this::onOpen);
    selectButton.addStyleName(VierkantTheme.BUTTON_ICON_ONLY);

    valueLabel = new Label();
    addComponent(valueLabel);
    valueLabel.setWidth("100%");
  }

  private void onOpen(ClickEvent event) {

    OneButtonViewDialog dialog = new OneButtonViewDialog();

    PersonSearchView view = new PersonSearchView();
    dialog.setView(view);
    view.setDisallowedIds(disallowedIds);
    view.addSelectionListener(e -> {
      if (e.getSelected().isEmpty()) {
        selectedPerson = null;
        valueLabel.setValue(null);
      } else {
        selectedPerson = (PersonPersistable) e.getSelected().iterator().next();
        valueLabel.setValue(selectedPerson.toString());
        dialog.close();
      }
      for (SelectionListener selectionListener : selectionListeners) {
        selectionListener.select(e);
      }
    });

    dialog.open();
  }

  /**
   * @param listener the listener to be invoked when the selection changes
   */
  public void addSelectionListener(SelectionListener listener) {
    selectionListeners.add(listener);
  }

  /**
   * @param listener the listener to be removed
   */
  public void removeSelectionListener(SelectionListener listener) {
    selectionListeners.remove(listener);
  }
}
