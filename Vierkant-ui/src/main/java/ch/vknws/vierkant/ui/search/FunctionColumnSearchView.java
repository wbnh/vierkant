package ch.vknws.vierkant.ui.search;

import java.util.LinkedList;
import java.util.List;

import ch.bwe.fac.v1.type.restriction.AbstractJunctionRestriction;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.ConjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.DisjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.InRestriction;
import ch.bwe.fac.v1.type.restriction.LikeRestriction;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.impl.OrganizationFunctionPersistable;

/**
 * View to search for person models.
 *
 * @author Benjamin Weber
 */
public class FunctionColumnSearchView extends AbstractColumnSearchView<OrganizationFunctionModel> {

  private static final long serialVersionUID = -6921444898893831089L;

  private List<Integer> disallowedIds;

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public FunctionColumnSearchView() {
    this(new LinkedList<>());
  }

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param disallowedIds the IDs of the models to ignore
   */
  public FunctionColumnSearchView(List<Integer> disallowedIds) {
    super(OrganizationFunctionPersistable.class, DaoFactory.getInstance().getOrganizationFunctionDao());
    this.disallowedIds = disallowedIds;
  }

  /**
   * @param disallowedIds the disallowedIds to set
   */
  public void setDisallowedIds(List<Integer> disallowedIds) {
    this.disallowedIds = disallowedIds;
  }

  /**
   * Creates the filter.
   */
  @Override
  protected AbstractRestriction createRestriction() {
    String searchString = getSearchField().getValue();
    if (searchString == null) { return null; }

    String[] split = searchString.split(" ");
    List<String> terms = new LinkedList<>();

    for (String term : split) {
      terms.add("%" + term + "%");
    }

    List<AbstractRestriction> restrictions = new LinkedList<>();
    restrictions.add(LikeRestriction.create(OrganizationFunctionModel.PropertyId.FUNCTION_NAME, terms));
    restrictions.add(LikeRestriction.create(OrganizationFunctionModel.PropertyId.EMAIL, terms));

    ConjunctionRestriction root = new ConjunctionRestriction();
    root.setLeft(new InRestriction(OrganizationFunctionModel.PropertyId.RECORD_ID, disallowedIds, true));

    AbstractJunctionRestriction currentJunction = root;
    for (int i = 0; i < restrictions.size(); i++) {
      AbstractRestriction currentRestriction = restrictions.get(i);

      if (i >= restrictions.size() - 1) {
        currentJunction.setRight(currentRestriction);
        break;
      }
      DisjunctionRestriction newJunction = new DisjunctionRestriction();
      newJunction.setLeft(currentRestriction);
      currentJunction.setRight(newJunction);
      currentJunction = newJunction;
    }
    return root;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getItemCaption(OrganizationFunctionModel model) {
    return model.toString();
  }

}
