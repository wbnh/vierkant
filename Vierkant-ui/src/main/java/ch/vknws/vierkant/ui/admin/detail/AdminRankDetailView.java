package ch.vknws.vierkant.ui.admin.detail;

import java.util.EnumSet;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.RankCategory;
import ch.vknws.vierkant.type.RankModel;
import ch.vknws.vierkant.type.caption.RankPersistableTexts;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.NavigationIds;

/**
 * Detail view for the RankModel in the Admin section.
 *
 * @author Benjamin Weber
 */
public class AdminRankDetailView extends AbstractDetailView<RankModel> {

  private RankPersistableTexts rankModelTexts;

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param displayedModel the model to display
   */
  public AdminRankDetailView(RankModel displayedModel) {
    super(displayedModel);
  }

  private static final long serialVersionUID = 4435343949809545784L;

  private TextField abbreviation;
  private TextField name;
  private TextField defaultFunction;
  private TextField hierarchyIndex;
  private ComboBox category;
  private TextField recordId;
  private TextField revisionId;

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FormLayout setUp() {
    rankModelTexts = ServiceRegistry.getTypedConfigurationProxy()
        .get(new ConfigurationProperties<>(RankPersistableTexts.class));

    FormLayout layout = new FormLayout();

    layout.setMargin(true);

    recordId = new TextField();
    layout.addComponent(recordId);
    recordId.setCaption(rankModelTexts.recordId());
    recordId.setPropertyDataSource(getDisplayedItem().getItemProperty(DatabaseModel.PropertyId.RECORD_ID));
    recordId.setNullRepresentation("");
    saveGroup.bind(recordId, DatabaseModel.PropertyId.RECORD_ID);

    revisionId = new TextField();
    layout.addComponent(revisionId);
    revisionId.setCaption(rankModelTexts.revisionId());
    revisionId.setPropertyDataSource(getDisplayedItem().getItemProperty(DatabaseModel.PropertyId.REVISION_ID));
    revisionId.setNullRepresentation("");
    saveGroup.bind(revisionId, DatabaseModel.PropertyId.REVISION_ID);

    abbreviation = new TextField();
    layout.addComponent(abbreviation);
    abbreviation.setCaption(rankModelTexts.abbreviation());
    abbreviation.setPropertyDataSource(getDisplayedItem().getItemProperty(RankModel.PropertyId.ABBREVIATION));
    abbreviation.setNullRepresentation("");
    saveGroup.bind(abbreviation, RankModel.PropertyId.ABBREVIATION);

    name = new TextField();
    layout.addComponent(name);
    name.setCaption(rankModelTexts.rankName());
    name.setPropertyDataSource(getDisplayedItem().getItemProperty(RankModel.PropertyId.RANK_NAME));
    name.setNullRepresentation("");
    saveGroup.bind(name, RankModel.PropertyId.RANK_NAME);

    defaultFunction = new TextField();
    layout.addComponent(defaultFunction);
    defaultFunction.setCaption(rankModelTexts.defaultFunction());
    defaultFunction.setPropertyDataSource(getDisplayedItem().getItemProperty(RankModel.PropertyId.DEFAULT_FUNCTION));
    defaultFunction.setNullRepresentation("");
    saveGroup.bind(defaultFunction, RankModel.PropertyId.DEFAULT_FUNCTION);

    hierarchyIndex = new TextField();
    layout.addComponent(hierarchyIndex);
    hierarchyIndex.setCaption(rankModelTexts.hierarchyIndex());
    hierarchyIndex.setPropertyDataSource(getDisplayedItem().getItemProperty(RankModel.PropertyId.HIERARCHY_INDEX));
    hierarchyIndex.setNullRepresentation("");
    saveGroup.bind(hierarchyIndex, RankModel.PropertyId.HIERARCHY_INDEX);

    category = new ComboBox();
    layout.addComponent(category);
    category.setCaption(rankModelTexts.category());
    category.setContainerDataSource(createCategoryContainer());
    category.setTextInputAllowed(false);
    category.setNullSelectionAllowed(false);
    category.setItemCaptionMode(ItemCaptionMode.ID_TOSTRING);
    category.setValue(getDisplayedModel().getRankCategory());
    category
        .addValueChangeListener(e -> getDisplayedModel().setRankCategory((RankCategory) e.getProperty().getValue()));
    saveGroup.bind(category, RankModel.PropertyId.CATEGORY);

    return layout;
  }

  private BeanItemContainer<RankCategory> createCategoryContainer() {
    BeanItemContainer<RankCategory> container = new BeanItemContainer<>(RankCategory.class);

    container.addAll(EnumSet.allOf(RankCategory.class));

    return container;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return NavigationIds.admin_rank_details;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();

    applyRecordIdVisibility();
    applyRevisionIdVisibility();
    applyAbbreviationVisibility();
    applyNameVisibility();
    applyDefaultFunctionVisibility();
    applyCategoryVisibility();

    applyRecordIdEditability();
    applyRevisionIdEditability();
    applyAbbreviationEditability();
    applyNameEditability();
    applyDefaultFunctionEditability();
    applyCategoryEditability();
  }

  private void applyRecordIdVisibility() {
    recordId.setVisible(true);
  }

  private void applyRevisionIdVisibility() {
    revisionId.setVisible(true);
  }

  private void applyAbbreviationVisibility() {
    abbreviation.setVisible(true);
  }

  private void applyNameVisibility() {
    name.setVisible(true);
  }

  private void applyDefaultFunctionVisibility() {
    defaultFunction.setVisible(true);
  }

  private void applyCategoryVisibility() {
    category.setVisible(true);
  }

  private void applyRecordIdEditability() {
    recordId.setEnabled(false);
  }

  private void applyRevisionIdEditability() {
    revisionId.setEnabled(false);
  }

  private void applyAbbreviationEditability() {
    abbreviation.setEnabled(true);
  }

  private void applyNameEditability() {
    name.setEnabled(true);
  }

  private void applyDefaultFunctionEditability() {
    defaultFunction.setEnabled(true);
  }

  private void applyCategoryEditability() {
    category.setEnabled(true);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void saveDisplayedModel() throws SaveException {
    DaoFactory.getInstance().getRankDao().save(getDisplayedModel(),
        ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getViewCaption() {
    return getDisplayedModel().toString();
  }
}
