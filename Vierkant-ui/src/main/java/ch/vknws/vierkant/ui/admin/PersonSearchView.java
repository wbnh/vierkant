package ch.vknws.vierkant.ui.admin;

import java.util.LinkedList;
import java.util.List;

import ch.bwe.fac.v1.type.restriction.AbstractJunctionRestriction;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.ConjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.DisjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.InRestriction;
import ch.bwe.fac.v1.type.restriction.LikeRestriction;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.RankModel;
import ch.vknws.vierkant.type.impl.PersonPersistable;
import ch.vknws.vierkant.ui.AbstractSearchView;

/**
 * View to search for person models.
 *
 * @author Benjamin Weber
 */
public class PersonSearchView extends AbstractSearchView<PersonModel> {

  private static final long serialVersionUID = -6921444898893831089L;

  private static final String[] COLUMNS = { PersonModel.PropertyId.REF_RANK, PersonModel.PropertyId.GIVEN_NAME,
      PersonModel.PropertyId.FAMILY_NAME };

  private List<Integer> disallowedIds;

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public PersonSearchView() {
    this(new LinkedList<>());
  }

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param disallowedIds the IDs of the models that must not be displayed
   */
  public PersonSearchView(List<Integer> disallowedIds) {
    super(PersonPersistable.class, DaoFactory.getInstance().getPersonDao(), COLUMNS);
    this.disallowedIds = disallowedIds;
  }

  /**
   * @param disallowedIds the disallowedIds to set
   */
  public void setDisallowedIds(List<Integer> disallowedIds) {
    this.disallowedIds = disallowedIds;
  }

  /**
   * Creates the restrictions to only show the relevant models.
   */
  @Override
  protected AbstractRestriction createRestriction() {
    String searchString = getSearchField().getValue();
    if (searchString == null) { return null; }

    String[] split = searchString.split(" ");
    List<String> terms = new LinkedList<>();

    for (String term : split) {
      terms.add("%" + term + "%");
    }

    List<AbstractRestriction> restrictions = new LinkedList<>();
    restrictions
        .add(LikeRestriction.create(PersonModel.PropertyId.REF_RANK + "." + RankModel.PropertyId.RANK_NAME, terms));
    restrictions
        .add(LikeRestriction.create(PersonModel.PropertyId.REF_RANK + "." + RankModel.PropertyId.ABBREVIATION, terms));
    restrictions.add(LikeRestriction.create(PersonModel.PropertyId.GIVEN_NAME, terms));
    restrictions.add(LikeRestriction.create(PersonModel.PropertyId.FAMILY_NAME, terms));

    ConjunctionRestriction root = new ConjunctionRestriction();
    root.setLeft(new InRestriction(PersonModel.PropertyId.RECORD_ID, disallowedIds, true));

    AbstractJunctionRestriction currentJunction = root;
    for (int i = 0; i < restrictions.size(); i++) {
      AbstractRestriction currentRestriction = restrictions.get(i);

      if (i >= restrictions.size() - 1) {
        currentJunction.setRight(currentRestriction);
        break;
      }
      DisjunctionRestriction newJunction = new DisjunctionRestriction();
      newJunction.setLeft(currentRestriction);
      currentJunction.setRight(newJunction);
      currentJunction = newJunction;
    }
    return root;
  }

}
