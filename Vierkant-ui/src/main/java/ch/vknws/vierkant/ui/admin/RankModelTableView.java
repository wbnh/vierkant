package ch.vknws.vierkant.ui.admin;

import com.vaadin.data.Container.Indexed;

import ch.bwe.fac.v1.type.DeleteException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.RankContainer;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.type.RankModel;
import ch.vknws.vierkant.type.impl.RankPersistable;
import ch.vknws.vierkant.ui.AbstractTableView;
import ch.vknws.vierkant.ui.NavigationIds;
import ch.vknws.vierkant.ui.admin.detail.AdminRankDetailView;

/**
 * View for managing material.
 *
 * @author Benjamin Weber
 *
 */
public class RankModelTableView extends AbstractTableView<RankModel> {

	private static final long serialVersionUID = 6054141657716877803L;

	private static final Object[] COLUMNS = { RankModel.PropertyId.ABBREVIATION, RankModel.PropertyId.RANK_NAME,
					RankModel.PropertyId.DEFAULT_FUNCTION, RankModel.PropertyId.CATEGORY };

	/**
	 * Constructor handling initialization of mandatory fields.
	 */
	public RankModelTableView() {
		super(COLUMNS);

		personAuthorizations.add(SecretaryAuthorizationKeys.ADMIN);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getViewId() {
		return NavigationIds.admin_rank;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected RankModel loadModelFromDatabase(Integer recordId) {
		return DaoFactory.getInstance().getRankDao().load(recordId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void deleteModelFromDatabase(Integer recordId) throws DeleteException {
		DaoFactory.getInstance().getRankDao().delete(recordId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected AdminRankDetailView createDetailView(RankModel selectedModel) {
		if (selectedModel == null) {
			selectedModel = new RankPersistable();
		}
		return new AdminRankDetailView(selectedModel);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Indexed createContainer() {
		return new RankContainer();
	}

}
