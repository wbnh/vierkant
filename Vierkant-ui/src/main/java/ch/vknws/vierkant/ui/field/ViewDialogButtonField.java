package ch.vknws.vierkant.ui.field;

import java.util.function.Function;

import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

import ch.bwe.faa.v1.core.util.AssertUtils;
import ch.vknws.vierkant.ui.AbstractView;
import ch.vknws.vierkant.ui.ValidityChangeListener;
import ch.vknws.vierkant.ui.dialog.OneButtonViewDialog;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * Field with a button to open a dialog for editing.
 *
 * @author Benjamin Weber
 */
public class ViewDialogButtonField extends CustomField<Object> implements ValidityChangeListener {
  private static final long serialVersionUID = -6024482469153489160L;

  private Function<Object, String> displayTransformer;
  private AbstractView view;

  private HorizontalLayout wrapper;
  private Button editButton;
  private Label label;

  private String dialogHeight;
  private String dialogWidth;

  private OneButtonViewDialog dialog;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param view the view to display
   */
  public ViewDialogButtonField(AbstractView view) {
    super();

    AssertUtils.notNull(view);

    this.view = view;

    view.addValidityChangeListener(this);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected Component initContent() {
    wrapper = new HorizontalLayout();
    wrapper.setMargin(false);
    wrapper.setSpacing(true);
    wrapper.setWidth("100%");

    editButton = new Button();
    wrapper.addComponent(editButton);
    editButton.setIcon(FontAwesome.EDIT);
    editButton.addClickListener(this::handleButtonClick);
    editButton.addStyleName(VierkantTheme.BUTTON_ICON_ONLY);
    editButton.setEnabled(isEnabled());
    wrapper.setExpandRatio(editButton, 0.2f);

    label = new Label();
    wrapper.addComponent(label);
    label.setContentMode(ContentMode.HTML);
    wrapper.setExpandRatio(label, 0.8f);

    executeRules();

    return wrapper;
  }

  /**
   * Updates the interface according to the rules.
   */
  public void executeRules() {

    updateLabel();
  }

  private void updateLabel() {
    if (label != null) {
      label.setValue(displayTransformer.apply(getPropertyDataSource().getValue()));
    }
  }

  private void handleDialogClose(CloseEvent event) {
    if (view instanceof CloseListener) {
      ((CloseListener) view).windowClose(event);
    }
    dialog = null;
    executeRules();
    fireValueChange(true);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setEnabled(boolean enabled) {
    super.setEnabled(enabled);
    if (editButton != null) editButton.setEnabled(enabled);
  }

  private void handleButtonClick(ClickEvent event) {
    if (dialog != null) { return; }
    dialog = new OneButtonViewDialog();
    dialog.setView(view);

    if (dialogHeight != null) dialog.setHeight(dialogHeight);
    if (dialogWidth != null) dialog.setWidth(dialogWidth);

    dialog.addCloseListener(this::handleDialogClose);

    validityChange(view.isValid());

    dialog.open();
  }

  /**
   * @return the height of the dialog
   */
  public String getDialogHeight() {
    return dialogHeight;
  }

  /**
   * @param dialogHeight the height of the dialog
   */
  public void setDialogHeight(String dialogHeight) {
    this.dialogHeight = dialogHeight;
  }

  /**
   * @return the width of the dialog
   */
  public String getDialogWidth() {
    return dialogWidth;
  }

  /**
   * @param dialogWidth the width of the dialog
   */
  public void setDialogWidth(String dialogWidth) {
    this.dialogWidth = dialogWidth;
  }

  /**
   * @param displayTransformer the displayTransformer to set
   */
  public void setDisplayTransformer(Function<Object, String> displayTransformer) {
    this.displayTransformer = displayTransformer;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Class<? extends Object> getType() {
    return Object.class;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void validityChange(boolean valid) {
    if (dialog != null) {
      dialog.getOkButton().setEnabled(valid);
    }
  }

}
