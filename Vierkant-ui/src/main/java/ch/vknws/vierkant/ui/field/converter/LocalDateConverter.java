package ch.vknws.vierkant.ui.field.converter;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

import com.vaadin.data.util.converter.Converter;

/**
 * Converter to convert between Date and LocalDate.
 *
 * @author Benjamin Weber
 *
 */
public class LocalDateConverter implements Converter<Date, LocalDate> {

	private static final long serialVersionUID = 2019360816123482369L;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LocalDate convertToModel(Date value, Class<? extends LocalDate> targetType, Locale locale)
					throws com.vaadin.data.util.converter.Converter.ConversionException {
		if (value == null) {
			return null;
		}
		return value.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Date convertToPresentation(LocalDate value, Class<? extends Date> targetType, Locale locale)
					throws com.vaadin.data.util.converter.Converter.ConversionException {
		if (value == null) {
			return null;
		}
		return Date.from(value.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<LocalDate> getModelType() {
		return LocalDate.class;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<Date> getPresentationType() {
		return Date.class;
	}

}
