package ch.vknws.vierkant.ui.field.converter;

import java.util.Locale;

import com.vaadin.data.util.converter.Converter;

import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.RankModel;

/**
 * Converter for rank models.
 *
 * @author Benjamin Weber
 */
public class RankModelConverter implements Converter<Object, RankModel> {

  private static final long serialVersionUID = -4049802806505138209L;

  /**
   * {@inheritDoc}
   */
  @Override
  public RankModel convertToModel(Object value, Class<? extends RankModel> targetType, Locale locale)
      throws com.vaadin.data.util.converter.Converter.ConversionException {
    if (value == null) { return null; }
    return DaoFactory.getInstance().getRankDao().load((Integer) value);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer convertToPresentation(RankModel value, Class<? extends Object> targetType, Locale locale)
      throws com.vaadin.data.util.converter.Converter.ConversionException {
    if (value == null) { return null; }
    return value.getRecordId();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Class<RankModel> getModelType() {
    return RankModel.class;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public Class<Object> getPresentationType() {
    Class clazz = Integer.class;
    return clazz;
  }

}
