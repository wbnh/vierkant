package ch.vknws.vierkant.ui;

/**
 * Messages for success.
 *
 * @author Benjamin Weber
 */
public interface SuccessMessages {

  /**
   * @return the text
   */
  String interestedRegistrationTitle();

  /**
   * @param recordId the ID of the new person model
   * @return the text
   */
  String interestedRegistrationMessage(Integer recordId);

  /**
   * @return the text
   */
  String savedTitle();

  /**
   * @param representation the text to represent the model that was saved
   * @return the text
   */
  String savedMessage(String representation);
}
