package ch.vknws.vierkant.ui.field.converter;

import java.time.LocalDateTime;
import java.util.Locale;

import com.vaadin.data.util.converter.Converter;

import ch.vknws.vierkant.ApplicationRegistry;

/**
 * Converter for rank models.
 *
 * @author Benjamin Weber
 */
public class LocalDateTimeStringConverter implements Converter<String, LocalDateTime> {

  private static final long serialVersionUID = -4049802806505138209L;

  /**
   * {@inheritDoc}
   */
  @Override
  public LocalDateTime convertToModel(String value, Class<? extends LocalDateTime> targetType, Locale locale)
      throws com.vaadin.data.util.converter.Converter.ConversionException {
    if (value == null) { return null; }
    return (LocalDateTime) ApplicationRegistry.getInstance().getDateTimeFormat().parse(value);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String convertToPresentation(LocalDateTime value, Class<? extends String> targetType, Locale locale)
      throws com.vaadin.data.util.converter.Converter.ConversionException {
    if (value == null) { return null; }
    return ApplicationRegistry.getInstance().getDateTimeFormat().format(value);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Class<LocalDateTime> getModelType() {
    return LocalDateTime.class;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Class<String> getPresentationType() {
    return String.class;
  }

}
