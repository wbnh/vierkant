package ch.vknws.vierkant.ui.theme;

import com.vaadin.ui.themes.ValoTheme;

/**
 * Class to contain CSS class names to customise Valo.
 *
 * @author Benjamin Weber
 */
public class VierkantTheme extends ValoTheme {

  /**
   * A slim margin.
   */
  public static final String MARGIN_SLIM = "margin-slim margin-top-slim margin-left-slim margin-bottom-slim margin-right-slim";

  /**
   * Top part of the slim margin.
   */
  public static final String MARGIN_TOP_SLIM = "margin-top-slim";

  /**
   * Left part of the slim margin.
   */
  public static final String MARGIN_LEFT_SLIM = "margin-left-slim";

  /**
   * Bottom part of the slim margin.
   */
  public static final String MARGIN_BOTTOM_SLIM = "margin-bottom-slim";

  /**
   * Right part of the slim margin.
   */
  public static final String MARGIN_RIGHT_SLIM = "margin-right-slim";

  /**
   * Align to the right.
   */
  public static final String RIGHT_ALIGN = "right-align";

  /**
   * Very large label.
   */
  public static final String LABEL_HUMONGOUS = "humongous";
}
