package ch.vknws.vierkant.ui;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Label;

import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * View for overview.
 *
 * @author Benjamin Weber
 *
 */
public class OverviewView extends AbstractView {

	private static final long serialVersionUID = 4670766650303661042L;

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public OverviewView() {

		setUp();
	}

	private void setUp() {

		Label label = new Label("Übersicht");
		addComponent(label);
		label.setStyleName(VierkantTheme.LABEL_HUMONGOUS);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void enter(ViewChangeEvent event) {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getViewId() {
		return NavigationIds.overview;
	}

}
