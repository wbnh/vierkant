package ch.vknws.vierkant.ui.dialog;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * Dialog with two buttons.
 *
 * @author Benjamin Weber
 */
public class TwoButtonQueryDialog extends Dialog {
  /**
   * The type of field to display.
   * 
   * @author benjamin
   */
  public static enum FieldMode {

    /**
     * Display a text field.
     */
    TEXTFIELD,

    /**
     * Display a text area.
     */
    TEXTAREA
  }

  private static final long serialVersionUID = 235138174869336896L;
  private Button okButton;
  private Button cancelButton;

  private TextField field;
  private TextArea area;
  private Label query;
  private CssLayout root;
  private FieldMode fieldMode;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param title   the title to display
   * @param message the message to display
   */
  public TwoButtonQueryDialog(String title, String message) {
    this(title, message, FieldMode.TEXTFIELD);
  }

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param title       the title to display
   * @param message     the message to display
   * @param messageMode the mode the message shall be displayed in
   */
  public TwoButtonQueryDialog(String title, String message, ContentMode messageMode) {
    this(title, message, messageMode, FieldMode.TEXTFIELD);
  }

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param title   the title to display
   * @param message the message to display
   * @param mode    the field mode to use
   */
  public TwoButtonQueryDialog(String title, String message, FieldMode mode) {
    this(title, message, ContentMode.TEXT, mode);
  }

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param title       the title to display
   * @param message     the message to display
   * @param messageMode the mode the message should be displayed in
   * @param mode        the field mode to use
   */
  public TwoButtonQueryDialog(String title, String message, ContentMode messageMode, FieldMode mode) {
    super();
    setCaption(title);
    this.fieldMode = mode;
    query.setValue(message);
    query.setContentMode(messageMode);

    setUpField();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected CssLayout setUp() {
    root = new CssLayout();

    query = new Label();
    root.addComponent(query);
    query.setWidth("100%");

    return root;
  }

  private void setUpField() {
    switch (fieldMode) {
    case TEXTFIELD:
      field = new TextField();
      root.addComponent(field);
      field.setWidth("100%");
      break;
    case TEXTAREA:
      area = new TextArea();
      root.addComponent(area);
      area.setWidth("100%");
      break;
    }
  }

  /**
   * @return the field
   */
  public TextField getField() {
    return field;
  }

  /**
   * @return the area
   */
  public TextArea getTextArea() {
    return area;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void setUpButtons() {

    okButton = new Button();
    getButtonBar().addComponent(okButton);
    okButton.addClickListener(closeListener);
    okButton.setCaption(dialogTexts.ok());
    okButton.addStyleName(VierkantTheme.BUTTON_PRIMARY);

    cancelButton = new Button();
    getButtonBar().addComponent(cancelButton);
    cancelButton.addClickListener(closeListener);
    cancelButton.setCaption(dialogTexts.cancel());
    cancelButton.addStyleName(VierkantTheme.BUTTON_LINK);
  }

  /**
   * @return the okButton
   */
  public Button getOkButton() {
    return okButton;
  }

  /**
   * @return the cancelButton
   */
  public Button getCancelButton() {
    return cancelButton;
  }

}
