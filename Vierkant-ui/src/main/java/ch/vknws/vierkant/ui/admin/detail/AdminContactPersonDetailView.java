package ch.vknws.vierkant.ui.admin.detail;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.ContactPersonModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.caption.ContactPersonPersistableTexts;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.admin.PersonSelectField;

/**
 * Detail view for the ContactPersonModel in the Admin section.
 *
 * @author Benjamin Weber
 */
public class AdminContactPersonDetailView extends AbstractDetailView<ContactPersonModel> {

  private static final long serialVersionUID = -3817113861355162837L;

  private ContactPersonPersistableTexts contactPersonTexts;

  private TextField relationship;
  private PersonSelectField refPerson;

  private TextField recordId;
  private TextField revisionId;

  private PersonModel personModel;

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param displayedModel the model to display
   * @param personModel    the person whose contacts shall be displayed
   */
  public AdminContactPersonDetailView(ContactPersonModel displayedModel, PersonModel personModel) {
    super(displayedModel);

    this.personModel = personModel;
    refPerson.setDisallowedIds(Arrays.asList(personModel.getRecordId()));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getViewCaption() {
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FormLayout setUp() {
    contactPersonTexts = ServiceRegistry.getTypedConfigurationProxy()
        .get(new ConfigurationProperties<>(ContactPersonPersistableTexts.class));

    FormLayout layout = new FormLayout();

    layout.setMargin(true);

    recordId = new TextField();
    layout.addComponent(recordId);
    recordId.setCaption(contactPersonTexts.recordId());
    recordId.setPropertyDataSource(getDisplayedItem().getItemProperty(DatabaseModel.PropertyId.RECORD_ID));
    recordId.setNullRepresentation("");
    saveGroup.bind(recordId, DatabaseModel.PropertyId.RECORD_ID);

    revisionId = new TextField();
    layout.addComponent(revisionId);
    revisionId.setCaption(contactPersonTexts.revisionId());
    revisionId.setPropertyDataSource(getDisplayedItem().getItemProperty(DatabaseModel.PropertyId.REVISION_ID));
    revisionId.setNullRepresentation("");
    saveGroup.bind(revisionId, DatabaseModel.PropertyId.REVISION_ID);

    relationship = new TextField();
    layout.addComponent(relationship);
    relationship.setCaption(contactPersonTexts.relationship());
    relationship.setPropertyDataSource(getDisplayedItem().getItemProperty(ContactPersonModel.PropertyId.RELATIONSHIP));
    relationship.setNullRepresentation("");
    saveGroup.bind(relationship, ContactPersonModel.PropertyId.RELATIONSHIP);

    refPerson = new PersonSelectField();
    layout.addComponent(refPerson);
    refPerson.addSelectionListener(e -> {
      if (e.getSelected().isEmpty()) {
        getDisplayedModel().setRefPerson(null);
      } else {
        getDisplayedModel().setRefPerson((PersonModel) e.getSelected().iterator().next());
      }
    });

    return layout;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void saveDisplayedModel() throws SaveException {

    Set<ContactPersonModel> contactPersons = personModel.getRefContactPersons();

    if (contactPersons == null) {
      contactPersons = new HashSet<>();
    }

    if (getDisplayedModel().getRecordId() == null) {
      contactPersons.add(getDisplayedModel());
    }

    personModel.setRefContactPersons(contactPersons);

    DaoFactory.getInstance().getContactPersonDao().save(getDisplayedModel(),
        ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

}
