package ch.vknws.vierkant.ui.login;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.vknws.vierkant.ApplicationConstants;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.ApplicationTexts;
import ch.vknws.vierkant.backend.configuration.ConfigurationUtils;
import ch.vknws.vierkant.backend.configuration.VierkantConfigurations;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.backend.function.EmailFunctions;
import ch.vknws.vierkant.backend.ldap.AuthenticationUtil;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.ui.AbstractView;
import ch.vknws.vierkant.ui.NavigationIds;
import ch.vknws.vierkant.ui.dialog.DialogUtils;
import ch.vknws.vierkant.ui.dialog.OneButtonDialog;
import ch.vknws.vierkant.ui.dialog.TwoButtonQueryDialog;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * View for people to log in.
 *
 * @author Benjamin Weber
 */
public class LoginView extends AbstractView {

  private static final long serialVersionUID = 2853296915934143141L;
  private static final ApplicationTexts applicationTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(ApplicationTexts.class));

  private List<ClickListener> loginListeners = new LinkedList<>();

  private TextField username;
  private PasswordField password;
  private Button login;
  private Button resetPassword;
  private Button interestedButton;
  private FormLayout loginForm;
  private InterestedView interestedView;
  private boolean interested = false;

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public LoginView() {

    CssLayout root = new CssLayout();
    addComponent(root);
    root.setSizeFull();

    HorizontalLayout topBar = new HorizontalLayout();
    root.addComponent(topBar);
    topBar.setMargin(true);
    topBar.setWidth("100%");

    Image logo = new Image("", new ThemeResource("images/logo_vknws.png"));
    topBar.addComponent(logo);
    logo.addStyleName("logo");

    interestedButton = new Button();
    topBar.addComponent(interestedButton);
    topBar.setComponentAlignment(interestedButton, Alignment.MIDDLE_RIGHT);
    interestedButton.setCaption(applicationTexts.interested());
    interestedButton.setCaptionAsHtml(true);
    interestedButton.addClickListener(this::onInterested);
    interestedButton.addStyleName(VierkantTheme.BUTTON_HUGE);
    interestedButton.addStyleName(VierkantTheme.BUTTON_LINK);

    VerticalLayout content = new VerticalLayout();
    root.addComponent(content);
    content.setDefaultComponentAlignment(Alignment.TOP_CENTER);
    content.setSizeFull();

    loginForm = new FormLayout();
    content.addComponent(loginForm);
    loginForm.setWidth("350px");
    loginForm.setHeight("200px");

    Label title = new Label(applicationTexts.login() + " " + ApplicationConstants.APPLICATION_NAME);
    loginForm.addComponent(title);
    title.addStyleName(VierkantTheme.LABEL_H1);
    title.setWidth("100%");

    username = new TextField();
    loginForm.addComponent(username);
    username.setCaption(applicationTexts.usernameField());
    username.setCaptionAsHtml(true);
    username.setWidth("100%");

    password = new PasswordField();
    loginForm.addComponent(password);
    password.setCaption(applicationTexts.passwordField());
    password.setCaptionAsHtml(true);
    password.setWidth("100%");

    HorizontalLayout buttonBar = new HorizontalLayout();
    loginForm.addComponent(buttonBar);
    buttonBar.setWidth("100%");
    buttonBar.setSpacing(true);

    login = new Button();
    buttonBar.addComponent(login);
    buttonBar.setComponentAlignment(login, Alignment.BOTTOM_LEFT);
    login.setCaption(applicationTexts.login());
    login.setCaptionAsHtml(true);
    login.addClickListener(this::onLogin);
    login.setClickShortcut(KeyCode.ENTER);
    login.addStyleName(VierkantTheme.BUTTON_PRIMARY);

    resetPassword = new Button();
    buttonBar.addComponent(resetPassword);
    buttonBar.setComponentAlignment(resetPassword, Alignment.BOTTOM_RIGHT);
    resetPassword.setCaption(applicationTexts.resetPassword());
    resetPassword.setCaptionAsHtml(true);
    resetPassword.addClickListener(this::onResetPassword);
    resetPassword.addStyleName(VierkantTheme.BUTTON_LINK);

    interestedView = new InterestedView();
    content.addComponent(interestedView);
    interestedView.addCancelListener(this::onInterested);

    username.focus();

    executeRules();
  }

  /**
   * @param listener the listener to be invoked upon successful login
   */
  public void addLoginListener(ClickListener listener) {
    loginListeners.add(listener);
  }

  private void onResetPassword(ClickEvent event) {
    TwoButtonQueryDialog dialog = new TwoButtonQueryDialog(applicationTexts.resetPassword(),
        applicationTexts.usernameField(), ContentMode.HTML);
    dialog.getOkButton().addClickListener(e -> {
      String userId = dialog.getField().getValue();
      if (userId == null || userId.isEmpty()) { return; }
      PersonModel user = DaoFactory.getInstance().getPersonDao().loadByShorthand(userId);
      if (user == null) {
        DialogUtils.showErrorDialog(applicationTexts.resetPassword(), errorMessages.noSuchUserMessage(userId), null);
        return;
      }
      String code = AuthenticationUtil.createPasswordResetCode(userId);
      String link = ApplicationRegistry.getInstance().createPasswordResetLink(code);

      OneButtonDialog infoDialog = new OneButtonDialog(applicationTexts.resetPassword(),
          applicationTexts.resetPasswordMailSent());
      infoDialog.open();

      new Thread(() -> EmailFunctions.sendResetPasswordMail(user.getGivenName(), link, user.getEmailOrganization()))
          .start();
    });
    dialog.open();
  }

  private void onLogin(ClickEvent event) {
    password.setComponentError(null);

    String userId = username.getValue();
    String pass = password.getValue();

    PersonModel authenticatedPerson = AuthenticationUtil.authenticate(userId, pass);
    if (authenticatedPerson == null) {
      onLoginFailed();
    } else {
      onLoginSuccessful(event, authenticatedPerson);
    }
  }

  private void onLoginSuccessful(ClickEvent event, PersonModel authenticatedPerson) {
    PersonModel person = authenticatedPerson;
    VaadinSession.getCurrent().getAttribute(ApplicationRegistry.class).setLoggedInUser(person);
    for (ClickListener listener : loginListeners) {
      listener.buttonClick(event);
    }

    showAdminUserMessage();

    handleForceResetPassword(person);
  }

  private void onLoginFailed() {
    password.setComponentError(new UserError(errorMessages.loginFailed()));
    password.clear();
    password.focus();
  }

  private void showAdminUserMessage() {
    String message = ConfigurationUtils.getConfiguration(VierkantConfigurations.ADMIN_USER_MESSAGE);
    String expiration = ConfigurationUtils.getConfiguration(VierkantConfigurations.ADMIN_USER_MESSAGE_EXPIRATION);

    if (message == null || expiration == null) { return; }

    LocalDate expirationLocalDate = LocalDate.from(ApplicationRegistry.getInstance().getDateFormat().parse(expiration));
    if (!LocalDate.now().isAfter(expirationLocalDate)) {
      OneButtonDialog dialog = new OneButtonDialog(applicationTexts.adminMessageTitle(), message);
      dialog.getOkButton().setClickShortcut(KeyCode.ENTER);
      dialog.open();
    }
  }

  private void handleForceResetPassword(PersonModel person) {
    boolean mustChangePassword = AuthenticationUtil.isMustChangePassword(person.getShorthand());
    if (mustChangePassword) {
      PasswordResetDialog dialog = new PasswordResetDialog(person, false);
      dialog.open();
    }
  }

  private void onInterested(ClickEvent event) {
    interested = !interested;
    executeRules();
    if (interested) {
      interestedButton.setCaption(applicationTexts.login());
    } else {
      interestedButton.setCaption(applicationTexts.interested());
      username.focus();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return NavigationIds.login;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();

    applyLoginFormVisibility();
    applyInterestedFormVisibility();
  }

  private void applyLoginFormVisibility() {
    loginForm.setVisible(!interested);
  }

  private void applyInterestedFormVisibility() {
    interestedView.setVisible(interested);
    if (interested) {
      interestedView.enter(null);
    }
  }

}
