package ch.vknws.vierkant.ui.secretary;

import com.vaadin.data.Container.Indexed;

import ch.bwe.fac.v1.type.DeleteException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.MeetingTypeContainer;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.type.MeetingTypeModel;
import ch.vknws.vierkant.type.impl.MeetingTypePersistable;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.AbstractTableView;
import ch.vknws.vierkant.ui.NavigationIds;

/**
 * Overview over the people for the secretary.
 *
 * @author Benjamin Weber
 */
public class MeetingTypeTableView extends AbstractTableView<MeetingTypeModel> {
  private static final long serialVersionUID = 959061418371194537L;
  private static final Object[] COLUMNS = { MeetingTypeModel.PropertyId.TYPE_NAME,
      MeetingTypeModel.PropertyId.DESCRIPTION };

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public MeetingTypeTableView() {
    super(COLUMNS);

    personAuthorizations.add(SecretaryAuthorizationKeys.MEETING_TYPE_ANY);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected Indexed createContainer() {
    return new MeetingTypeContainer();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected AbstractDetailView<MeetingTypeModel> createDetailView(MeetingTypeModel selectedModel) {
    if (selectedModel == null) {
      selectedModel = new MeetingTypePersistable();
    }
    return new MeetingTypeDetailsView(selectedModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void deleteModelFromDatabase(Integer recordId) throws DeleteException {
    DaoFactory.getInstance().getMeetingTypeDao().delete(recordId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected MeetingTypeModel loadModelFromDatabase(Integer recordId) {
    return DaoFactory.getInstance().getMeetingTypeDao().load(recordId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return NavigationIds.meeting_meetingtype;
  }

}
