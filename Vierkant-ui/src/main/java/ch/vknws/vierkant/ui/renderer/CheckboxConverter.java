package ch.vknws.vierkant.ui.renderer;

import java.util.Locale;

import com.vaadin.data.util.converter.Converter;

/**
 * Converter between string and checkbox
 *
 * @author Benjamin Weber
 *
 */
public class CheckboxConverter implements Converter<String, Boolean> {
	private static final long serialVersionUID = 4422915113777808372L;

	@Override
	public Boolean convertToModel(String value, Class<? extends Boolean> targetType, Locale locale)
					throws ConversionException {
		return value.contains("checked");
	}

	@Override
	public String convertToPresentation(Boolean value, Class<? extends String> targetType, Locale locale)
					throws ConversionException {
		return "<input type='checkbox' disabled='disabled'" + (Boolean.TRUE.equals(value) ? " checked" : "") + " />";
	}

	@Override
	public Class<Boolean> getModelType() {
		return Boolean.class;
	}

	@Override
	public Class<String> getPresentationType() {
		return String.class;
	}
}
