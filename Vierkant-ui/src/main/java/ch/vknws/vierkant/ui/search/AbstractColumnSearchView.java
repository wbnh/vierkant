package ch.vknws.vierkant.ui.search;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TwinColSelect;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.faa.v1.core.util.AssertUtils;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.type.DAO;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.vknws.vierkant.ui.AbstractView;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * View to search for person models.
 *
 * @author Benjamin Weber
 * @param <P> the type of model to look for
 */
public abstract class AbstractColumnSearchView<P extends DatabaseModel> extends AbstractView {

  private static final long serialVersionUID = -6921444898893831089L;

  private static final SearchTexts searchTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(SearchTexts.class));

  private CssLayout root;
  private TextField searchField;
  private Button searchButton;
  private TwinColSelect select;

  private DAO<P> dao;
  @SuppressWarnings("unused") // is needed for type parameter
  private Class<? extends P> modelClass;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param modelClass the class of the model to look for
   * @param dao        the dao to load the search results
   */
  public AbstractColumnSearchView(Class<? extends P> modelClass, DAO<P> dao) {
    AssertUtils.notNull(modelClass);
    AssertUtils.notNull(dao);

    this.modelClass = modelClass;
    this.dao = dao;

    setUp();
  }

  private void setUp() {

    root = new CssLayout();
    addComponent(root);

    HorizontalLayout searchWrapper = new HorizontalLayout();
    root.addComponent(searchWrapper);
    searchWrapper.setWidth("100%");
    searchWrapper.setMargin(false);
    searchWrapper.setSpacing(true);

    searchField = new TextField();
    searchWrapper.addComponent(searchField);
    searchField.setWidth("100%");

    searchButton = new Button();
    searchWrapper.addComponent(searchButton);
    searchButton.setIcon(FontAwesome.SEARCH);
    searchButton.addStyleName(VierkantTheme.BUTTON_ICON_ONLY);
    searchButton.addClickListener(this::onSearch);
    searchButton.setClickShortcut(KeyCode.ENTER);

    select = new TwinColSelect();
    root.addComponent(select);
    select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    select.setLeftColumnCaption(searchTexts.availableColumn());
    select.setRightColumnCaption(searchTexts.selectedColumn());

  }

  /**
   * @param model the model to get the caption of
   * @return the loaded caption
   */
  protected abstract String getItemCaption(P model);

  /**
   * @return the filter
   */
  protected abstract AbstractRestriction createRestriction();

  @SuppressWarnings("unchecked")
  private void onSearch(ClickEvent event) {

    AbstractRestriction restriction = createRestriction();

    Set<P> value = null;
    if (!select.isEmpty()) {
      if (select.isMultiSelect()) {
        value = (Set<P>) select.getValue();
      } else {
        value = new HashSet<>();
        value.add((P) select.getValue());
      }
    }

    ServiceRegistry.getLogProxy().trace(this, "Searching: {0}", restriction);
    List<P> persistableList = dao.find(restriction, 0, Integer.MAX_VALUE);

    List<P> valuesToUse = new LinkedList<>();
    if (value != null) {
      List<P> copy = new LinkedList<>(persistableList);
      for (P m : value) {
        boolean contains = false;
        for (P n : copy) {
          if (m != null && n != null && Objects.equals(m.getRecordId(), n.getRecordId())) {
            contains = true;
            valuesToUse.add(n);
            break;
          }
        }
        if (!contains) {
          valuesToUse.add(m);
          persistableList.add(m);
        }
      }
    } else {
      valuesToUse = null;
    }

    BeanItemContainer<P> container = new BeanItemContainer<>(DatabaseModel.class, persistableList);

    for (P p : persistableList) {
      select.setItemCaption(p, p.toString());
    }

    select.setContainerDataSource(container);

    if (valuesToUse != null) {
      for (P m : valuesToUse) {
        select.select(m);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

  /**
   * @return the searchField
   */
  public TextField getSearchField() {
    return searchField;
  }

  /**
   * @return the select
   */
  public TwinColSelect getSelect() {
    return select;
  }
}
