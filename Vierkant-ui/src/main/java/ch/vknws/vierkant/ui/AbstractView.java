package ch.vknws.vierkant.ui;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.faa.v1.core.util.Pair;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.configuration.VierkantConfigurations;
import ch.vknws.vierkant.backend.ldap.AuthenticationUtil;
import ch.vknws.vierkant.rules.rights.AuthorizationRules;
import ch.vknws.vierkant.type.AuthorizationKeyMode;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.ui.field.converter.LocalDateConverter;
import ch.vknws.vierkant.ui.field.converter.LocalDateTimeConverter;

/**
 * Abstract view. All views must extend this class.
 *
 * @author Benjamin Weber
 */
public abstract class AbstractView extends VerticalLayout implements View {

  private static final long serialVersionUID = 5800451488337740611L;

  private Integer selectedId;

  /**
   * The error message texts.
   */
  protected static final ErrorMessages errorMessages = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(ErrorMessages.class));

  /**
   * The success message texts.
   */
  protected static final SuccessMessages successMessages = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(SuccessMessages.class));

  /**
   * Listener to execute the rules.
   */
  protected final ValueChangeListener ruleExecutionListener = e -> executeRules();

  /**
   * Listeners for validity change.
   */
  private List<ValidityChangeListener> validityChangeListeners = new LinkedList<>();

  /**
   * The authorisations the user has on the page.
   */
  protected final List<String> personAuthorizations = new LinkedList<>();

  /**
   * Whether to override admin privileges.
   */
  protected boolean overrideAdmin = false;

  /**
   * Constructor handling initialization of mandatory fields.
   */
  protected AbstractView() {
    setUp();
  }

  private void setUp() {
    setSizeFull();
  }

  /**
   * @return the View ID. Relevant for Navigation.
   */
  public abstract String getViewId();

  /**
   * Reflects the rules on the interface.
   */
  public void executeRules() {

  }

  /**
   * @return whether the user is allowed to see this page.
   */
  public boolean isUserAllowedToVisit() {
    return getUserAuthorization().isPresent();
  }

  /**
   * @return whether the form is valid
   */
  public boolean isValid() {
    return true;
  }

  /**
   * Inform the listeners of a change in validity.
   */
  protected final void fireValidityChange() {
    validityChangeListeners.forEach(l -> l.validityChange(isValid()));
  }

  /**
   * @param listener the listener to be invoked on validity change
   */
  public void addValidityChangeListener(ValidityChangeListener listener) {
    validityChangeListeners.add(listener);
  }

  /**
   * @param listener the listener to remove
   */
  public void removeValidityChangeListener(ValidityChangeListener listener) {
    validityChangeListeners.remove(listener);
  }

  /**
   * @return the authorisations the user has
   */
  public Optional<AuthorizationKeyMode> getUserAuthorization() {
    if (personAuthorizations.isEmpty()) { return Optional.of(AuthorizationKeyMode.WRITE); }

    PersonModel user = getLoggedInUser();

    if (AuthenticationUtil.isAdministrator(user.getShorthand())) { return Optional.of(AuthorizationKeyMode.WRITE); }

    Set<Pair<String, AuthorizationKeyMode>> authorizationKeys = AuthorizationRules.getInstance()
        .getEffectiveAuthorizationKeys(user);

    AuthorizationKeyMode maxMode = null;
    for (Pair<String, AuthorizationKeyMode> authorization : authorizationKeys) {
      if (personAuthorizations.contains(authorization.getA())) {
        if (maxMode == null || maxMode.ordinal() < authorization.getB().ordinal()) {
          maxMode = authorization.getB();
        }
      }
    }
    return Optional.ofNullable(maxMode);
  }

  /**
   * @return the selected ID.
   */
  public Integer getSelectedId() {
    return selectedId;
  }

  /**
   * @param selectedId the ID to select.
   */
  public void setSelectedId(Integer selectedId) {
    this.selectedId = selectedId;
  }

  /**
   * @return the user that is currently logged in
   */
  protected final PersonModel getLoggedInUser() {
    return ApplicationRegistry.getInstance().getLoggedInUser();
  }

  /**
   * Creates a text field ready to be used on the form.
   * 
   * @param caption the caption for the field
   * @return the new field
   */
  protected final TextField createTextField(String caption) {
    TextField textField = new TextField();
    textField.setCaption(caption);
    textField.setNullRepresentation("");
    textField.setNullSettingAllowed(true);
    textField.addValueChangeListener(ruleExecutionListener);

    return textField;
  }

  /**
   * Creates a text area ready to be used on the form.
   * 
   * @param caption the caption for the field
   * @return the new field
   */
  protected final TextArea createTextArea(String caption) {
    TextArea textArea = new TextArea();
    textArea.setCaption(caption);
    textArea.setNullRepresentation("");
    textArea.setNullSettingAllowed(true);
    textArea.addValueChangeListener(ruleExecutionListener);

    return textArea;
  }

  /**
   * Creates a date field ready to be used on the form.
   * 
   * @param caption the caption for the field
   * @return the new field
   */
  protected final DateField createDateField(String caption) {
    DateField dateField = new DateField();
    dateField.setCaption(caption);
    dateField.addValueChangeListener(ruleExecutionListener);
    dateField.setConverter(new LocalDateConverter());
    dateField.setDateFormat(VierkantConfigurations.DATE_FORMAT_PATTERN);

    return dateField;
  }

  /**
   * Creates a date time field ready to be used on the form.
   * 
   * @param caption the caption for the field
   * @return the new field
   */
  protected final DateField createDateTimeField(String caption) {
    DateField dateTimeField = new DateField();
    dateTimeField.setCaption(caption);
    dateTimeField.addValueChangeListener(ruleExecutionListener);
    dateTimeField.setConverter(new LocalDateTimeConverter());
    dateTimeField.setDateFormat(VierkantConfigurations.DATE_TIME_MINUTE_FORMAT_PATTERN);
    dateTimeField.setResolution(Resolution.MINUTE);

    return dateTimeField;
  }

  /**
   * Creates a combo box ready to be used on the form.
   * 
   * @param caption the caption of the field
   * @return the newly created field
   */
  protected final ComboBox createComboBox(String caption) {
    ComboBox comboBox = new ComboBox();
    comboBox.setCaption(caption);
    comboBox.addValueChangeListener(ruleExecutionListener);

    return comboBox;
  }
}
