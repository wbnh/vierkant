package ch.vknws.vierkant.ui;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.ApplicationTexts;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.rules.RuleRegistry;
import ch.vknws.vierkant.rules.ValidationMessage;
import ch.vknws.vierkant.rules.ValidationResult;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.ui.admin.detail.DetailViewTexts;
import ch.vknws.vierkant.ui.dialog.DialogUtils;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * Abstract implementation for detail views for models. Used to show and edit
 * details of the model.
 *
 * @author Benjamin Weber
 * @param <T> the displayed model
 */
public abstract class AbstractDetailView<T extends DatabaseModel> extends AbstractView {

  /**
   * The mode in which the view shall be displayed.
   * 
   * @author benjamin
   */
  public static enum DisplayMode {
    /**
     * Everything should be displayed.
     */
    NORMAL,

    /**
     * Display only the content, no buttons or wrappers.
     */
    CONTENT_ONLY;
  }

  private static final long serialVersionUID = 7411985604705425361L;

  private DisplayMode displayMode;
  private T displayedModel;
  private BeanItem<T> displayedItem;
  private List<Listener> closeListeners = new LinkedList<>();

  /**
   * The texts.
   */
  protected DetailViewTexts detailViewTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(DetailViewTexts.class));

  /**
   * The texts.
   */
  protected ApplicationTexts applicationTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(ApplicationTexts.class));

  /**
   * The save button.
   */
  protected Button save;

  /**
   * The cancel button.
   */
  protected Button cancel;

  private List<BeanFieldGroup<? extends DatabaseModel>> fieldGroups = new LinkedList<>();

  /**
   * The field group relevant for saving.
   */
  protected BeanFieldGroup<? extends DatabaseModel> saveGroup;

  private Panel root;

  private boolean initFinished = false;

  /**
   * Creates a new field group.
   * 
   * @param clazz the class of the contained models
   * @return the new field group
   */
  protected final <U extends DatabaseModel> BeanFieldGroup<U> createFieldGroup(Class<U> clazz) {
    BeanFieldGroup<U> group = new BeanFieldGroup<>(clazz);
    group.setBuffered(false);
    fieldGroups.add(group);
    return group;
  }

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param displayedModel the model to display
   */
  protected AbstractDetailView(T displayedModel) {
    this(displayedModel, DisplayMode.NORMAL);
  }

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param displayedModel the model to display
   * @param mode           the mode in which to display the view
   */
  protected AbstractDetailView(T displayedModel, DisplayMode mode) {
    displayMode = mode;
    setDisplayedModel(displayedModel);

    init();
  }

  /**
   * @return the caption to display for the view
   */
  protected abstract String getViewCaption();

  private void init() {
    saveGroup = new BeanFieldGroup<>(displayedModel.getClass());
    saveGroup.setBuffered(false);
    saveGroup.setItemDataSource(displayedItem);

    root = new Panel();
    addComponent(root);
    root.setSizeFull();
    updateViewCaption();

    CssLayout layout = new CssLayout();
    root.setContent(layout);
    FormLayout content = setUp();
    content.setWidth("100%");
    content.setHeightUndefined();
    layout.addComponent(content);

    if (!DisplayMode.CONTENT_ONLY.equals(displayMode)) {
      FormLayout metaFields = new FormLayout();
      layout.addComponent(metaFields);
      metaFields.setWidth("100%");
      metaFields.setHeightUndefined();
      metaFields.setMargin(content.getMargin());

      HorizontalLayout buttonBarWrapper = new HorizontalLayout();
      layout.addComponent(buttonBarWrapper);
      buttonBarWrapper.setWidth("100%");
      buttonBarWrapper.setDefaultComponentAlignment(Alignment.MIDDLE_RIGHT);
      buttonBarWrapper.setMargin(true);

      HorizontalLayout buttonBar = new HorizontalLayout();
      buttonBarWrapper.addComponent(buttonBar);
      buttonBar.setHeight("37px");
      buttonBar.setWidthUndefined();
      buttonBar.setDefaultComponentAlignment(Alignment.MIDDLE_RIGHT);
      buttonBar.setMargin(false);
      buttonBar.setSpacing(true);

      save = new Button();
      buttonBar.addComponent(save);
      save.setIcon(FontAwesome.SAVE);
      save.setCaption(detailViewTexts.save());
      save.setStyleName(VierkantTheme.BUTTON_PRIMARY);
      save.addClickListener(this::handleSaveClick);

      cancel = new Button();
      buttonBar.addComponent(cancel);
      cancel.setCaption(detailViewTexts.cancel());
      cancel.setStyleName(VierkantTheme.BUTTON_LINK);
      cancel.addClickListener(this::handleCancelClick);
    }

    initFinished = true;
    executeRules();
  }

  private void updateViewCaption() {
    if (root == null) { return; }
    String viewCaption = getViewCaption();
    if (viewCaption == null || DisplayMode.CONTENT_ONLY.equals(displayMode)) {
      root.addStyleName(VierkantTheme.PANEL_BORDERLESS);
    } else {
      ApplicationRegistry registry = VaadinSession.getCurrent().getAttribute(ApplicationRegistry.class);
      if (displayedModel.getModificationDateTime() != null) {
        StringBuilder builder = new StringBuilder(viewCaption);
        builder.append(" [");
        builder.append(registry.getDateTimeFormat().format(displayedModel.getModificationDateTime()));
        if (displayedModel.getModifier() != null) {
          builder.append(" / ");
          if (displayedModel.getModifier().equals(0)) {
            builder.append(applicationTexts.systemUserShortcut());
          } else {
            builder.append(DaoFactory.getInstance().getPersonDao().getProperty(displayedModel.getModifier(),
                PersonModel.PropertyId.SHORTHAND));
          }
        }
        builder.append("]");
        viewCaption = builder.toString();
      }

      root.setCaption(viewCaption);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();

    if (!initFinished) { return; }

    applySaveEditability();
    validateFieldGroups();
  }

  private void validateFieldGroups() {
    saveGroup.getFields().forEach(f -> ((AbstractField<?>) f).setComponentError(null));
    fieldGroups.forEach(g -> g.getFields().forEach(f -> ((AbstractField<?>) f).setComponentError(null)));

    ValidationResult result = RuleRegistry.validateModel(getDisplayedModel());
    if (!result.isSuccessful()) {
      boolean relevant = false;
      for (ValidationMessage message : result.getMessages()) {
        AbstractField<?> field = (AbstractField<?>) saveGroup.getField(message.getField());

        Iterator<BeanFieldGroup<? extends DatabaseModel>> iterator = fieldGroups.iterator();
        while (field == null && iterator.hasNext()) {
          field = (AbstractField<?>) iterator.next().getField(message.getField());
        }

        if (field != null && field.isVisible() && field.isEnabled()) {
          field.setComponentError(new UserError(message.toString()));
          relevant = true;
        }
      }
      if (relevant) {
        save.setEnabled(false);
      }
    }
  }

  private void applySaveEditability() {
    if (save == null || saveGroup == null) { return; }

    boolean valid = true;

    valid &= saveGroup.isValid();
    for (var fieldGroup : fieldGroups) {
      valid &= fieldGroup.isValid();
    }

    save.setEnabled(valid);
  }

  /**
   * Sets up the view.
   * 
   * @return the top-level layout
   */
  protected abstract FormLayout setUp();

  /**
   * @param displayedModel the displayedModel to set
   */
  public void setDisplayedModel(T displayedModel) {
    this.displayedModel = displayedModel;
    this.displayedItem = new BeanItem<T>(displayedModel);
    updateViewCaption();
    if (saveGroup != null) {
      saveGroup.setItemDataSource(displayedItem);
    }
  }

  /**
   * Rebinds all the fields.
   */
  protected void rebindFields() {
  }

  /**
   * @return the displayedModel
   */
  public T getDisplayedModel() {
    return displayedModel;
  }

  /**
   * @return the displayedItem
   */
  public BeanItem<T> getDisplayedItem() {
    return displayedItem;
  }

  /**
   * Request the view be closed.
   */
  protected void requestClose() {
    Event event = new Event(this);
    for (Listener listener : closeListeners) {
      listener.componentEvent(event);
    }
  }

  private void handleSaveClick(ClickEvent event) {
    if (!saveGroup.isValid()) {
      DialogUtils.showErrorDialog(errorMessages.invalidFormTitle(), errorMessages.invalidFormMessage(), null);
      return;
    }

    DialogUtils.showAuditCommentDialog(displayedModel, e -> {
      try {
        ApplicationRegistry registry = VaadinSession.getCurrent().getAttribute(ApplicationRegistry.class);

        if (displayedModel != null) {
          if (displayedModel.getCreator() == null) {
            displayedModel.setCreator(registry.getLoggedInUser().getRecordId());
          }
          displayedModel.setModifier(registry.getLoggedInUser().getRecordId());
        }
        saveDisplayedModel();

        requestClose();

        onSave(event);

        Notification.show(successMessages.savedTitle(), successMessages.savedMessage(getDisplayedModel().toString()),
            Notification.Type.ASSISTIVE_NOTIFICATION);
      } catch (SaveException ex) {
        ServiceRegistry.getLogProxy().error(this, "Could not save Model {0}", ex, getDisplayedModel().getRecordId());

        DialogUtils.showErrorDialog(errorMessages.saveFailTitle(), errorMessages.saveFailMessage(getViewCaption()), ex);
      }
    });
  }

  private void handleCancelClick(ClickEvent event) {
    requestClose();

    onCancel(event);
  }

  /**
   * Executed on save.
   * 
   * @param event the event
   */
  protected void onSave(ClickEvent event) {
  }

  /**
   * Executed on cancel.
   * 
   * @param event the event
   */
  protected void onCancel(ClickEvent event) {
  }

  /**
   * Saves the displayed model.
   * 
   * @throws SaveException if a save is impossible
   */
  protected abstract void saveDisplayedModel() throws SaveException;

  /**
   * Adds a listener for closing.
   * 
   * @param listener the listener
   */
  public void addCloseListener(Listener listener) {
    closeListeners.add(listener);
  }

  /**
   * Removes a listener for closing.
   * 
   * @param listener the listener
   */
  public void removeCloseListener(Listener listener) {
    closeListeners.remove(listener);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setCaption(String caption) {
    root.setCaption(caption);
  }

  /**
   * @return the save button
   */
  public Button getSaveButton() {
    return save;
  }

  /**
   * @return the cancel button
   */
  public Button getCancelButton() {
    return cancel;
  }

  /**
   * @return the display mode
   */
  public DisplayMode getDisplayMode() {
    return displayMode;
  }

  /**
   * @return the initFinished
   */
  protected boolean isInitFinished() {
    return initFinished;
  }
}
