package ch.vknws.vierkant.ui.secretary;

import java.util.List;

import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.AargauLicenseModel;
import ch.vknws.vierkant.type.AargauPersonLicenseModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.caption.AargauPersonLicensePersistableTexts;
import ch.vknws.vierkant.type.impl.AargauPersonLicensePersistable;
import ch.vknws.vierkant.ui.AbstractView;
import ch.vknws.vierkant.ui.field.converter.LocalDateConverter;

/**
 * View for editing of member groups.
 *
 * @author Benjamin Weber
 */
public class AargauPersonLicenseEditView extends AbstractView implements CloseListener {
  private static final long serialVersionUID = -8936498798045864382L;
  private static final AargauPersonLicensePersistableTexts personLicenseTexts = ServiceRegistry
      .getTypedConfigurationProxy().get(new ConfigurationProperties<>(AargauPersonLicensePersistableTexts.class));

  private PersonModel person;
  private AargauPersonLicenseModel personLicense;

  private TextField invoiceNumber;
  private DateField validityEnd;
  private DateField registrationDate;
  private ComboBox refLicense;

  private Item licenseItem;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param person the model to display
   */
  public AargauPersonLicenseEditView(PersonModel person) {
    this.person = person;
    if (person.getRefAargauLicense() == null) {
      personLicense = new AargauPersonLicensePersistable();
    } else {
      personLicense = person.getRefAargauLicense();
    }

    licenseItem = new BeanItem<>(personLicense);

    setUp();
  }

  private void setUp() {
    BeanFieldGroup<AargauPersonLicensePersistable> saveGroup = new BeanFieldGroup<>(
        AargauPersonLicensePersistable.class);
    saveGroup.setBuffered(false);
    saveGroup.setItemDataSource(licenseItem);

    VerticalLayout wrapper = new VerticalLayout();
    addComponent(wrapper);
    wrapper.setMargin(true);

    FormLayout layout = new FormLayout();
    wrapper.addComponent(layout);

    invoiceNumber = new TextField();
    layout.addComponent(invoiceNumber);
    invoiceNumber.setCaption(personLicenseTexts.invoiceNumber());
    invoiceNumber.setNullRepresentation("");
    saveGroup.bind(invoiceNumber, AargauPersonLicenseModel.PropertyId.INVOICE_NUMBER);

    validityEnd = new DateField();
    layout.addComponent(validityEnd);
    validityEnd.setCaption(personLicenseTexts.validityEnd());
    validityEnd.setConverter(new LocalDateConverter());
    saveGroup.bind(validityEnd, AargauPersonLicenseModel.PropertyId.VALIDITY_END_DATE);

    registrationDate = new DateField();
    layout.addComponent(registrationDate);
    registrationDate.setCaption(personLicenseTexts.registrationDate());
    registrationDate.setConverter(new LocalDateConverter());
    saveGroup.bind(registrationDate, AargauPersonLicenseModel.PropertyId.REGISTRATION_DATE);

    refLicense = new ComboBox();
    layout.addComponent(refLicense);
    refLicense.setNewItemsAllowed(false);
    refLicense.setNullSelectionAllowed(false);
    refLicense.setTextInputAllowed(true);
    refLicense.setCaption(personLicenseTexts.refLicense());
    refLicense.setItemCaptionMode(ItemCaptionMode.ID_TOSTRING);
    refLicense.setContainerDataSource(createLicenseContainer());
    saveGroup.bind(refLicense, AargauPersonLicenseModel.PropertyId.REF_LICENSE);

    executeRules();
  }

  private BeanItemContainer<AargauLicenseModel> createLicenseContainer() {
    BeanItemContainer<AargauLicenseModel> container = new BeanItemContainer<>(AargauLicenseModel.class);

    List<Integer> ids = DaoFactory.getInstance().getAargauLicenseDao().getAllIds();

    for (Integer id : ids) {
      AargauLicenseModel persistable = DaoFactory.getInstance().getAargauLicenseDao().load(id);
      container.addBean(persistable);
    }

    return container;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void windowClose(CloseEvent e) {
    if (person.getRefAargauLicense() == null) {
      person.setRefAargauLicense(personLicense);
    }
  }

}
