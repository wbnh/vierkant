package ch.vknws.vierkant.ui.admin;

import com.vaadin.data.Container.Indexed;

import ch.bwe.fac.v1.type.DeleteException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.ContactPersonContainer;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.type.ContactPersonModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.impl.ContactPersonPersistable;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.AbstractTableView;
import ch.vknws.vierkant.ui.admin.detail.AdminContactPersonDetailView;

/**
 * Table view for the ContactPersonModel.
 *
 * @author Benjamin Weber
 */
public class ContactPersonModelTableView extends AbstractTableView<ContactPersonModel> {

  private static final long serialVersionUID = 3362739932304550007L;

  private static final Object[] COLUMNS = { ContactPersonModel.PropertyId.REF_PERSON,
      ContactPersonModel.PropertyId.RELATIONSHIP };

  private PersonModel person;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param person the person whose contacts shall be displayed
   */
  public ContactPersonModelTableView(PersonModel person) {
    super(COLUMNS);

    personAuthorizations.add(SecretaryAuthorizationKeys.ADMIN);

    this.person = person;
    refreshGrid();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected Indexed createContainer() {
    if (person == null) return null;

    return new ContactPersonContainer(person);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected AbstractDetailView<ContactPersonModel> createDetailView(ContactPersonModel selectedModel) {
    if (selectedModel == null) {
      selectedModel = new ContactPersonPersistable();
    }
    return new AdminContactPersonDetailView(selectedModel, person);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void deleteModelFromDatabase(Integer recordId) throws DeleteException {
    DaoFactory.getInstance().getContactPersonDao().delete(recordId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected ContactPersonModel loadModelFromDatabase(Integer recordId) {
    return DaoFactory.getInstance().getContactPersonDao().load(recordId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

}
