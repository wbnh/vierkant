package ch.vknws.vierkant.ui.search;

import java.util.LinkedList;
import java.util.List;

import ch.bwe.fac.v1.type.restriction.AbstractJunctionRestriction;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.ConjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.DisjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.InRestriction;
import ch.bwe.fac.v1.type.restriction.LikeRestriction;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.FunctionGroupModel;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.impl.FunctionGroupPersistable;

/**
 * View to search for person models.
 *
 * @author Benjamin Weber
 */
public class FunctionGroupColumnSearchView extends AbstractColumnSearchView<FunctionGroupModel> {

  private static final long serialVersionUID = -6921444898893831089L;

  private List<Integer> disallowedIds;

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public FunctionGroupColumnSearchView() {
    this(new LinkedList<>());
  }

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param disallowedIds the list of IDs to ignore
   */
  public FunctionGroupColumnSearchView(List<Integer> disallowedIds) {
    super(FunctionGroupPersistable.class, DaoFactory.getInstance().getFunctionGroupDao());
    this.disallowedIds = disallowedIds;
  }

  /**
   * @param disallowedIds the disallowedIds to set
   */
  public void setDisallowedIds(List<Integer> disallowedIds) {
    this.disallowedIds = disallowedIds;
  }

  /**
   * Creates the filter.
   */
  @Override
  protected AbstractRestriction createRestriction() {
    String searchString = getSearchField().getValue();
    if (searchString == null) { return null; }

    String[] split = searchString.split(" ");
    List<String> terms = new LinkedList<>();

    for (String term : split) {
      terms.add("%" + term + "%");
    }

    List<AbstractRestriction> restrictions = new LinkedList<>();
    restrictions.add(LikeRestriction.create(
        FunctionGroupModel.PropertyId.REF_DETAILS + "." + OrganizationFunctionModel.PropertyId.FUNCTION_NAME, terms));

    ConjunctionRestriction root = new ConjunctionRestriction();
    root.setLeft(new InRestriction(FunctionGroupModel.PropertyId.RECORD_ID, disallowedIds, true));

    AbstractJunctionRestriction currentJunction = root;
    for (int i = 0; i < restrictions.size(); i++) {
      AbstractRestriction currentRestriction = restrictions.get(i);

      if (i >= restrictions.size() - 1) {
        currentJunction.setRight(currentRestriction);
        break;
      }
      DisjunctionRestriction newJunction = new DisjunctionRestriction();
      newJunction.setLeft(currentRestriction);
      currentJunction.setRight(newJunction);
      currentJunction = newJunction;
    }
    return root;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getItemCaption(FunctionGroupModel model) {
    return model.toString();
  }

}
