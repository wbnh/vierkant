package ch.vknws.vierkant.ui.admin;

import com.vaadin.data.Container.Indexed;

import ch.bwe.fac.v1.type.DeleteException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.OrganizationFunctionContainer;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.impl.OrganizationFunctionPersistable;
import ch.vknws.vierkant.ui.AbstractTableView;
import ch.vknws.vierkant.ui.NavigationIds;
import ch.vknws.vierkant.ui.admin.detail.AdminOrganizationFunctionModelDetailView;

/**
 * View for managing material.
 *
 * @author Benjamin Weber
 *
 */
public class OrganizationFunctionModelTableView extends AbstractTableView<OrganizationFunctionModel> {

	private static final long serialVersionUID = 6054141657716877803L;

	private static final Object[] COLUMNS = { OrganizationFunctionModel.PropertyId.FUNCTION_NAME,
					OrganizationFunctionModel.PropertyId.EMAIL };

	/**
	 * Constructor handling initialization of mandatory fields.
	 */
	public OrganizationFunctionModelTableView() {
		super(COLUMNS);

		personAuthorizations.add(SecretaryAuthorizationKeys.ADMIN);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getViewId() {
		return NavigationIds.admin_function;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected OrganizationFunctionModel loadModelFromDatabase(Integer recordId) {
		return DaoFactory.getInstance().getOrganizationFunctionDao().load(recordId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void deleteModelFromDatabase(Integer recordId) throws DeleteException {
		DaoFactory.getInstance().getOrganizationFunctionDao().delete(recordId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected AdminOrganizationFunctionModelDetailView createDetailView(OrganizationFunctionModel selectedModel) {
		if (selectedModel == null) {
			selectedModel = new OrganizationFunctionPersistable();
		}
		return new AdminOrganizationFunctionModelDetailView(selectedModel);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Indexed createContainer() {
		return new OrganizationFunctionContainer();
	}

}
