package ch.vknws.vierkant.ui.field.converter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import com.vaadin.data.util.converter.Converter;
import com.vaadin.data.util.converter.DefaultConverterFactory;

import ch.vknws.vierkant.type.impl.MeetingTypePersistable;
import ch.vknws.vierkant.type.impl.RankPersistable;

/**
 * Converter factory to use for Vierkant.
 *
 * @author Benjamin Weber
 */
public class VierkantConverterFactory extends DefaultConverterFactory {
  private static final long serialVersionUID = -3591247236418129492L;

  @Override
  @SuppressWarnings("unchecked")
  protected <PRESENTATION, MODEL> Converter<PRESENTATION, MODEL> findConverter(Class<PRESENTATION> presentationType,
      Class<MODEL> modelType) {
    Converter<PRESENTATION, MODEL> converter = super.findConverter(presentationType, modelType);

    if (converter != null) { return converter; }

    if (presentationType == Integer.class) {
      converter = (Converter<PRESENTATION, MODEL>) createIntegerConverter(modelType);
      if (converter != null) { return converter; }
    }
    return null;
  }

  private Converter<?, ?> createIntegerConverter(Class<?> sourceType) {

    if (RankPersistable.class.isAssignableFrom(sourceType)) {
      return new RankModelConverter();
    } else if (MeetingTypePersistable.class.isAssignableFrom(sourceType)) {
      return new MeetingTypeModelConverter();
    } else {
      return null;
    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected Converter<String, ?> createStringConverter(Class<?> sourceType) {
    Converter<String, ?> converter = super.createStringConverter(sourceType);
    if (converter != null) { return converter; }

    if (LocalDate.class.isAssignableFrom(sourceType)) {
      return new LocalDateStringConverter();
    } else if (LocalDateTime.class.isAssignableFrom(sourceType)) {
      return new LocalDateTimeStringConverter();
    } else if (RankPersistable.class.isAssignableFrom(sourceType)) {
      return new RankNameConverter();
    } else if (MeetingTypePersistable.class.isAssignableFrom(sourceType)) {
      return new MeetingTypeNameConverter();
    } else {
      return null;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected Converter<Date, ?> createDateConverter(Class<?> sourceType) {
    Converter<Date, ?> converter = super.createDateConverter(sourceType);
    if (converter != null) { return converter; }

    if (LocalDate.class.isAssignableFrom(sourceType)) {
      return new LocalDateConverter();
    } else if (LocalDateTime.class.isAssignableFrom(sourceType)) {
      return new LocalDateTimeConverter();
    } else {
      return null;
    }
  }
}
