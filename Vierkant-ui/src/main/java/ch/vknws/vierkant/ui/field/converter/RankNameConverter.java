package ch.vknws.vierkant.ui.field.converter;

import java.util.Locale;

import com.vaadin.data.util.converter.Converter;

import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.RankModel;

/**
 * Converter for rank models.
 *
 * @author Benjamin Weber
 */
public class RankNameConverter implements Converter<String, RankModel> {

  private static final long serialVersionUID = -4049802806505138209L;

  /**
   * {@inheritDoc}
   */
  @Override
  public RankModel convertToModel(String value, Class<? extends RankModel> targetType, Locale locale)
      throws com.vaadin.data.util.converter.Converter.ConversionException {
    if (value == null) { return null; }
    AbstractRestriction restriction = new EqualityRestriction(RankModel.PropertyId.RANK_NAME, value);
    return DaoFactory.getInstance().getRankDao().find(restriction, 0, 1).get(0);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String convertToPresentation(RankModel value, Class<? extends String> targetType, Locale locale)
      throws com.vaadin.data.util.converter.Converter.ConversionException {
    if (value == null) { return null; }
    return value.getRankName();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Class<RankModel> getModelType() {
    return RankModel.class;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Class<String> getPresentationType() {
    return String.class;
  }

}
