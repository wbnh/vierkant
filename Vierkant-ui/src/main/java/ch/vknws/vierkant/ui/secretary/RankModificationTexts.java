package ch.vknws.vierkant.ui.secretary;

/**
 * Texts for the rank modification view.
 *
 * @author Benjamin Weber
 */
public interface RankModificationTexts {

  /**
   * @return the text
   */
  String shorthands();

  /**
   * @return the text
   */
  String appoint();

  /**
   * @return the text
   */
  String promote();

  /**
   * @return the text
   */
  String brevet();

  /**
   * @return the text
   */
  String appointed();

  /**
   * @return the text
   */
  String promoted();

  /**
   * @return the text
   */
  String breveted();

  /**
   * @return the text
   */
  String appointAuditComment();

  /**
   * @return the text
   */
  String promoteAuditComment();

  /**
   * @return the text
   */
  String brevetAuditComment();

  /**
   * @return the text
   */
  String rankQueryTitle();

  /**
   * @return the text
   */
  String rankQueryMessage();

  /**
   * @return the text
   */
  String instructions();

  /**
   * @return the text
   * @param action    the action that should have been performed
   * @param shortcuts the shortcut that was invoked
   */
  String noActionPerformedMessage(String action, String shortcuts);
}
