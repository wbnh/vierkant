package ch.vknws.vierkant.ui.personal;

import java.util.Iterator;
import java.util.Set;

import com.vaadin.data.Container.Indexed;

import ch.bwe.fac.v1.type.DeleteException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.ContactPersonContainer;
import ch.vknws.vierkant.type.ContactPersonModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.impl.ContactPersonPersistable;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.AbstractTableView;
import ch.vknws.vierkant.ui.NavigationIds;

/**
 * Table view for contact persons.
 *
 * @author Benjamin Weber
 */
public class PersonalContactPersonTableView extends AbstractTableView<ContactPersonModel> {
  private static final long serialVersionUID = -8379589618206497727L;

  private static final Object[] COLUMNS = new Object[] { ContactPersonModel.PropertyId.REF_PERSON,
      ContactPersonModel.PropertyId.RELATIONSHIP };

  private PersonModel personModel;

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public PersonalContactPersonTableView() {
    this(ApplicationRegistry.getInstance().getLoggedInUser());
  }

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param personModel the model to display
   */
  public PersonalContactPersonTableView(PersonModel personModel) {
    super(COLUMNS);

    this.personModel = personModel;
    refreshGrid();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected Indexed createContainer() {
    if (personModel == null) { return null; }
    personModel = DaoFactory.getInstance().getPersonDao().load(personModel.getRecordId());
    return new ContactPersonContainer(personModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected AbstractDetailView<ContactPersonModel> createDetailView(ContactPersonModel selectedModel) {
    if (selectedModel == null) {
      selectedModel = new ContactPersonPersistable();
    }
    return new PersonalContactPersonDataView(selectedModel, personModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void deleteModelFromDatabase(Integer recordId) throws DeleteException {
    Set<ContactPersonModel> refContactPersons = personModel.getRefContactPersons();
    for (Iterator<ContactPersonModel> iterator = refContactPersons.iterator(); iterator.hasNext();) {
      if (iterator.next().getRecordId().equals(recordId)) {
        iterator.remove();
        break;
      }
    }
    personModel.setRefContactPersons(refContactPersons);

    DaoFactory.getInstance().getContactPersonDao().delete(recordId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected ContactPersonModel loadModelFromDatabase(Integer recordId) {
    return DaoFactory.getInstance().getContactPersonDao().load(recordId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return NavigationIds.personal_contactPerson;
  }

}
