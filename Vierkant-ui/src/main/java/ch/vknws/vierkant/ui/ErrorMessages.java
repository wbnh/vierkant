package ch.vknws.vierkant.ui;

/**
 * Captions for problems.
 *
 * @author Benjamin Weber
 */
public interface ErrorMessages {

  /**
   * @return the text
   */
  String deleteFailTitle();

  /**
   * @param name the name of the model that could not be deleted
   * @return the text
   */
  String deleteFailMessage(String name);

  /**
   * @return the text
   */
  String saveFailTitle();

  /**
   * @param name the name of the model that could not be saved
   * @return the text
   */
  String saveFailMessage(String name);

  /**
   * @return the text
   */
  String auditCommentMissing();

  /**
   * @return the text
   */
  String invalidFormTitle();

  /**
   * @return the text
   */
  String invalidFormMessage();

  /**
   * @return the text
   */
  String loginFailed();

  /**
   * @return the text
   */
  String mutationFailedTitle();

  /**
   * @return the text
   */
  String mutationFailedMessage();

  /**
   * @return the text
   */
  String passwordsNotMatch();

  /**
   * @return the text
   */
  String passwordResetCodeInvalidTitle();

  /**
   * @return the text
   */
  String passwordResetCodeInvalidMessage();

  /**
   * @param userId the ID that could not be found
   * @return the text
   */
  String noSuchUserMessage(String userId);

  /**
   * @return the text
   */
  String invalidEmail();

  /**
   * @return the text
   */
  String testErrorTitle();

  /**
   * @return the text
   */
  String testErrorMessage();
}
