package ch.vknws.vierkant.ui.search;

import java.util.LinkedList;
import java.util.List;

import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.ConjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.InRestriction;
import ch.bwe.fac.v1.type.restriction.LikeRestriction;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.TrainingCourseModel;
import ch.vknws.vierkant.type.impl.TrainingCoursePersistable;

/**
 * View to search for person models.
 *
 * @author Benjamin Weber
 */
public class TrainingCourseColumnSearchView extends AbstractColumnSearchView<TrainingCourseModel> {

  private static final long serialVersionUID = -6921444898893831089L;

  private List<Integer> disallowedIds;

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public TrainingCourseColumnSearchView() {
    this(new LinkedList<>());
  }

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param disallowedIds the list of IDs to ignore
   */
  public TrainingCourseColumnSearchView(List<Integer> disallowedIds) {
    super(TrainingCoursePersistable.class, DaoFactory.getInstance().getTrainingCourseDao());
    this.disallowedIds = disallowedIds;
  }

  /**
   * @param disallowedIds the disallowedIds to set
   */
  public void setDisallowedIds(List<Integer> disallowedIds) {
    this.disallowedIds = disallowedIds;
  }

  /**
   * Creates the filter
   */
  @Override
  protected AbstractRestriction createRestriction() {
    String searchString = getSearchField().getValue();
    if (searchString == null) { return null; }

    String[] split = searchString.split(" ");
    List<String> terms = new LinkedList<>();

    for (String term : split) {
      terms.add("%" + term + "%");
    }

    ConjunctionRestriction root = new ConjunctionRestriction();
    root.setLeft(new InRestriction(DatabaseModel.PropertyId.RECORD_ID, disallowedIds, true));
    root.setRight(LikeRestriction.create(TrainingCourseModel.PropertyId.TRAINING_COURSE_NAME, terms));

    return root;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getItemCaption(TrainingCourseModel model) {
    return model.toString();
  }

}
