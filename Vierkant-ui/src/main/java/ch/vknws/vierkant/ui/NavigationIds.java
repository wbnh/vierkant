package ch.vknws.vierkant.ui;

/**
 * Contains the different IDs for navigation.
 *
 * @author Benjamin Weber
 */
public interface NavigationIds {

  /**
   * Prefix for views that should not be displayed in the navigation bar.
   */
  final String NO_MENU_PREFIX = "~";

  /**
   * Login view.
   */
  String login = "login";

  /**
   * Personal overview.
   */
  String overview = "overview";

  /**
   * Open processes.
   */
  String process_processes = "process/processes";

  /**
   * Training course instances.
   */
  String training_instances = "training/instances";

  /**
   * Person management.
   */
  String secretary_person = "secretary/person";

  /**
   * Rank management.
   */
  String secretary_rankmod = "secretary/rankmod";

  /**
   * Function management.
   */
  String secretary_function = "secretary/function";

  /**
   * Aargau license management.
   */
  String secretary_aargaulicense = "secretary/aargaulicense";

  /**
   * Training course management.
   */
  String secretary_trainingcourse = "secretary/trainingcourse";

  /**
   * Meeting type management.
   */
  String meeting_meetingtype = "meeting/meetingtype";

  /**
   * Meeting management.
   */
  String meeting_meetings = "meeting/meetings";

  /**
   * Management of own data.
   */
  String personal_person = "personal/person";

  /**
   * Management of contact persons.
   */
  String personal_contactPerson = "personal/contactPerson";

  /**
   * Information about the application.
   */
  String metapage = "metapage";

  /**
   * Overview of administrative actions.
   */
  String admin_overview = "admin/overview";

  /**
   * Administrative material view.
   */
  String admin_material = "admin/material";

  /**
   * Administrative person view.
   */
  String admin_person = "admin/person";

  /**
   * Administrative rank view.
   */
  String admin_rank = "admin/rank";

  /**
   * Administrative function view.
   */
  String admin_function = "admin/function";

  /**
   * Administrative address view.
   */
  String admin_address = "admin/address";

  /**
   * Administrative configuration view.
   */
  String admin_config = "admin/config";

  /**
   * Details view.
   */
  String admin_rank_details = NO_MENU_PREFIX + admin_rank + "/details";

  /**
   * Details view.
   */
  String admin_function_details = NO_MENU_PREFIX + admin_function + "/details";

  /**
   * Details view.
   */
  String admin_material_details = NO_MENU_PREFIX + admin_material + "/details";

  /**
   * Details view.
   */
  String admin_person_details = NO_MENU_PREFIX + admin_person + "/details";

  /**
   * Details view.
   */
  String admin_address_details = NO_MENU_PREFIX + admin_address + "/details";

  /**
   * Details view.
   */
  String admin_config_details = NO_MENU_PREFIX + admin_config + "/details";

  /**
   * Log out of the application.
   */
  String logout = "logout";
}
