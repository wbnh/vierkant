package ch.vknws.vierkant.ui.dialog;

import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.function.EmailFunctions;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * Dialog to show and report errors.
 *
 * @author Benjamin Weber
 */
public class ErrorDialog extends TwoButtonDialog {

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param title   the error title
   * @param message the error message
   * @param problem the problem that occurred
   */
  public ErrorDialog(String title, String message, Throwable problem) {
    super(title, message, dialogTexts.report(), dialogTexts.ok());

    if (problem == null) {
      getOkButton().setVisible(false);
    } else {
      getOkButton().addClickListener(
          e -> EmailFunctions.setErrorReportMail(ApplicationRegistry.getInstance().getLoggedInUser(), problem));
      getOkButton().addStyleName(VierkantTheme.BUTTON_PRIMARY);
    }
  }

  private static final long serialVersionUID = 2817931201500640850L;

}
