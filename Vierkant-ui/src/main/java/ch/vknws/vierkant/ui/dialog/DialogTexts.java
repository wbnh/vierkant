package ch.vknws.vierkant.ui.dialog;

/**
 * Captions for dialogs.
 *
 * @author Benjamin Weber
 */
public interface DialogTexts {

  /**
   * @param modelName the name of the model to delete
   * @return the text
   */
  String deleteTitle(String modelName);

  /**
   * @param modelName the name of the model to delete
   * @return the text
   */
  String deleteText(String modelName);

  /**
   * @return the text
   */
  String leaveOrganizationTitle();

  /**
   * @param modelName the name of the model that wants to leave
   * @return the text
   */
  String leaveOrganizationText(String modelName);

  /**
   * @return the text
   */
  String auditCommentTitle();

  /**
   * @return the text
   */
  String auditCommentTextMandatory();

  /**
   * @return the text
   */
  String auditCommentTextOptional();

  /**
   * @return the text
   */
  String ok();

  /**
   * @return the text
   */
  String cancel();

  /**
   * @return the text
   */
  String delete();

  /**
   * @return the text
   */
  String leaveOrganization();

  /**
   * @return the text
   */
  String report();

  /**
   * @return the text
   */
  String select();

  /**
   * @return the text
   */
  String close();

  /**
   * @return the text
   */
  String download();
}
