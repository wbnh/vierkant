package ch.vknws.vierkant.ui.admin.detail;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.ConfigurationModel;
import ch.vknws.vierkant.type.caption.ConfigurationPersistableTexts;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.NavigationIds;

/**
 * Detail view for the ConfigurationModel in the Admin section.
 *
 * @author Benjamin Weber
 */
public class AdminConfigurationModelDetailView extends AbstractDetailView<ConfigurationModel> {

  private static final long serialVersionUID = 3187482689576874439L;

  private ConfigurationPersistableTexts configurationModelTexts;

  private TextField user;
  private TextField context;
  private TextField key;
  private TextField value;
  private TextField recordId;
  private TextField revisionId;

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param displayedModel the model to display
   */
  public AdminConfigurationModelDetailView(ConfigurationModel displayedModel) {
    super(displayedModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getViewCaption() {
    return getDisplayedModel().toString();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FormLayout setUp() {
    configurationModelTexts = ServiceRegistry.getTypedConfigurationProxy()
        .get(new ConfigurationProperties<>(ConfigurationPersistableTexts.class));

    FormLayout layout = new FormLayout();

    layout.setMargin(true);

    recordId = new TextField();
    layout.addComponent(recordId);
    recordId.setCaption(configurationModelTexts.recordId());
    recordId.setPropertyDataSource(getDisplayedItem().getItemProperty(DatabaseModel.PropertyId.RECORD_ID));
    recordId.setNullRepresentation("");
    saveGroup.bind(recordId, DatabaseModel.PropertyId.RECORD_ID);

    revisionId = new TextField();
    layout.addComponent(revisionId);
    revisionId.setCaption(configurationModelTexts.revisionId());
    revisionId.setPropertyDataSource(getDisplayedItem().getItemProperty(DatabaseModel.PropertyId.REVISION_ID));
    revisionId.setNullRepresentation("");
    saveGroup.bind(revisionId, DatabaseModel.PropertyId.REVISION_ID);

    user = new TextField();
    layout.addComponent(user);
    user.setCaption(configurationModelTexts.user());
    user.setPropertyDataSource(getDisplayedItem().getItemProperty(ConfigurationModel.PropertyId.USER_NAME));
    user.setNullRepresentation("");
    saveGroup.bind(user, ConfigurationModel.PropertyId.USER_NAME);

    context = new TextField();
    layout.addComponent(context);
    context.setCaption(configurationModelTexts.context());
    context.setPropertyDataSource(getDisplayedItem().getItemProperty(ConfigurationModel.PropertyId.CONTEXT));
    context.setNullRepresentation("");
    saveGroup.bind(context, ConfigurationModel.PropertyId.CONTEXT);

    key = new TextField();
    layout.addComponent(key);
    key.setCaption(configurationModelTexts.key());
    key.setPropertyDataSource(getDisplayedItem().getItemProperty(ConfigurationModel.PropertyId.KEY_NAME));
    key.setNullRepresentation("");
    saveGroup.bind(key, ConfigurationModel.PropertyId.KEY_NAME);

    value = new TextField();
    layout.addComponent(value);
    value.setCaption(configurationModelTexts.value());
    value.setPropertyDataSource(getDisplayedItem().getItemProperty(ConfigurationModel.PropertyId.VALUE));
    value.setNullRepresentation("");
    saveGroup.bind(value, ConfigurationModel.PropertyId.VALUE);

    return layout;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void saveDisplayedModel() throws SaveException {
    DaoFactory.getInstance().getConfigurationDao().save(getDisplayedModel(),
        ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return NavigationIds.admin_config_details;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();

    applyRecordIdEditability();
    applyRevisionIdEditability();
    applyUserEditability();
    applyContextEditability();
    applyKeyEditability();
    applyValueEditability();
  }

  private void applyRecordIdEditability() {
    recordId.setEnabled(false);
  }

  private void applyRevisionIdEditability() {
    revisionId.setEnabled(false);
  }

  private void applyUserEditability() {
    user.setEnabled(true);
  }

  private void applyContextEditability() {
    context.setEnabled(true);
  }

  private void applyKeyEditability() {
    key.setEnabled(true);
  }

  private void applyValueEditability() {
    value.setEnabled(true);
  }

}
