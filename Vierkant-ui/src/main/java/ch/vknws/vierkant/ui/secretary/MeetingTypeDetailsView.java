package ch.vknws.vierkant.ui.secretary;

import java.util.Optional;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.rules.rights.AuthorizationRules;
import ch.vknws.vierkant.type.AuthorizationKeyMode;
import ch.vknws.vierkant.type.MeetingTypeModel;
import ch.vknws.vierkant.type.caption.MeetingTypePersistableTexts;
import ch.vknws.vierkant.ui.AbstractDetailView;

/**
 * Details view of a person for the secretary.
 *
 * @author Benjamin Weber
 */
public class MeetingTypeDetailsView extends AbstractDetailView<MeetingTypeModel> {
  private static final long serialVersionUID = -7589784662167949340L;

  private static final MeetingTypePersistableTexts meetingTypeTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(MeetingTypePersistableTexts.class));

  private TextField typeName;

  private TextArea description;

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param displayedModel the model to display
   */
  protected MeetingTypeDetailsView(MeetingTypeModel displayedModel) {
    super(displayedModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getViewCaption() {
    return getDisplayedModel().toString();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FormLayout setUp() {

    FormLayout layout = new FormLayout();
    layout.setMargin(true);

    typeName = createTextField(meetingTypeTexts.typeName());
    layout.addComponent(typeName);
    saveGroup.bind(typeName, MeetingTypeModel.PropertyId.TYPE_NAME);

    description = createTextArea(meetingTypeTexts.description());
    layout.addComponent(description);
    saveGroup.bind(description, MeetingTypeModel.PropertyId.DESCRIPTION);

    return layout;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();
    if (!isInitFinished()) { return; }

    applyDataEditability();
  }

  private void applyDataEditability() {
    Optional<AuthorizationKeyMode> authorization = AuthorizationRules.getInstance()
        .hasAuthorization(SecretaryAuthorizationKeys.MEETING_TYPE_ANY, getLoggedInUser(), AuthorizationKeyMode.WRITE);
    boolean editable = authorization.isPresent();

    typeName.setEnabled(editable);
    description.setEnabled(editable);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void saveDisplayedModel() throws SaveException {
    DaoFactory.getInstance().getMeetingTypeDao().save(getDisplayedModel(),
        ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

}
