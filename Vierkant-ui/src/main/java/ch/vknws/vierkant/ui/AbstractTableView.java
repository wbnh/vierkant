package ch.vknws.vierkant.ui;

import java.util.LinkedList;
import java.util.Objects;
import java.util.Optional;

import com.vaadin.data.Container.Indexed;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SortEvent;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.renderers.HtmlRenderer;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.Configuration;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.faa.v1.core.util.StringUtils;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.type.DeleteException;
import ch.vknws.vierkant.VierkantUI;
import ch.vknws.vierkant.type.AuthorizationKeyMode;
import ch.vknws.vierkant.ui.dialog.DialogUtils;
import ch.vknws.vierkant.ui.dialog.OneButtonViewDialog;
import ch.vknws.vierkant.ui.renderer.CheckboxConverter;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * Abstract implementation of a table view. Used to show an overview over
 * several models.
 *
 * @author Benjamin Weber
 * @param <T> the class of the model to be displayed
 */
public abstract class AbstractTableView<T extends DatabaseModel> extends AbstractView {

  /**
   * How the details view should be displayed.
   * 
   * @author benjamin
   */
  public static enum DetailViewMode {

    /**
     * Details are displayed partially.
     */
    PARTIAL,

    /**
     * All details are displayed.
     */
    FULL;
  }

  private static final long serialVersionUID = 5525644750005623109L;

  private Configuration columnNames = ServiceRegistry.getConfigurationProxy()
      .getConfiguration(new ConfigurationProperties<>(getClass()));

  private DetailViewMode detailViewMode;

  private Grid grid;
  private Indexed container;
  private Object[] columns;

  private Button addButton;
  private Button deleteButton;
  private Button refreshButton;

  private HorizontalLayout root;

  private AbstractDetailView<T> detailView;

  private CssLayout buttonBar;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param columns the columns to display
   */
  public AbstractTableView(Object[] columns) {
    this(columns, DetailViewMode.PARTIAL);
  }

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param columns        the columns to display
   * @param detailViewMode the display mode
   */
  public AbstractTableView(Object[] columns, DetailViewMode detailViewMode) {

    this.detailViewMode = detailViewMode;
    this.container = createContainer();
    this.columns = columns;

    setUp();
  }

  /**
   * @return a new instance of the underlying container
   */
  protected abstract Indexed createContainer();

  /**
   * @return the grid
   */
  public Grid getGrid() {
    return grid;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();

    applyButtonVisibility();
    applyDeleteButtonEditability();

    if (getSelectedId() != null && grid.getContainerDataSource().containsId(getSelectedId())) {
      grid.select(getSelectedId());
    }
  }

  private void applyDeleteButtonEditability() {
    if (deleteButton == null) return;
    boolean editable = isDeletable((Integer) grid.getSelectedRow());

    deleteButton.setEnabled(editable);
  }

  private void applyButtonVisibility() {
    if (addButton == null || deleteButton == null) return;
    boolean visible = AuthorizationKeyMode.WRITE.equals(getUserAuthorization().orElse(AuthorizationKeyMode.READ));

    addButton.setVisible(visible);
    deleteButton.setVisible(visible);
  }

  /**
   * Creates a new instance of a details view for the passed model.
   * 
   * @param selectedModel the model to display the details of
   * @return the new view
   */
  protected abstract AbstractDetailView<T> createDetailView(T selectedModel);

  /**
   * Removes the model with the passed ID.
   * 
   * @param recordId the ID to delete
   * @throws DeleteException if we could not delete
   */
  protected abstract void deleteModelFromDatabase(Integer recordId) throws DeleteException;

  /**
   * Checks if the model with the passed ID can be deleted.
   * 
   * @param recordId the ID to check
   * @return whether the underlying model can be deleted
   */
  protected boolean isDeletable(Integer recordId) {
    return true;
  }

  /**
   * Loads the model with the passed ID from the database.
   * 
   * @param recordId the ID to load
   * @return the loaded model
   */
  protected abstract T loadModelFromDatabase(Integer recordId);

  /**
   * Executed on a click on the add button.
   * 
   * @param event the event
   */
  protected void onAdd(ClickEvent event) {
  }

  /**
   * Executed on a click on the delete button.
   * 
   * @param event the event
   */
  protected void onDelete(ClickEvent event) {
  }

  /**
   * Executed on a click on the details button.
   * 
   * @param event the event
   */
  protected void onDetails(SelectionEvent event) {
  }

  /**
   * @return the addButton
   */
  protected Button getAddButton() {
    return addButton;
  }

  /**
   * @return the deleteButton
   */
  protected Button getDeleteButton() {
    return deleteButton;
  }

  private void handleAddClick(ClickEvent event) {
    addButton.addStyleName(VierkantTheme.BUTTON_FRIENDLY);
    addButton.removeStyleName(VierkantTheme.BUTTON_QUIET);

    openDetailsView(null);

    onAdd(event);
  }

  private void handleDeleteClick(ClickEvent event) {
    Integer selectedId = (Integer) getGrid().getSelectedRow();

    if (selectedId != null) {
      String name = loadModelFromDatabase(selectedId).toString();
      DialogUtils.showDeleteConfirmDialog(name, e -> {
        try {
          deleteModelFromDatabase(selectedId);
        } catch (DeleteException ex) {
          DialogUtils.showErrorDialog(errorMessages.deleteFailTitle(),
              errorMessages.deleteFailMessage(selectedId.toString()), ex);
        }
        refreshGrid();
      });
    }

    onDelete(event);
  }

  private void handleDetailsClick(SelectionEvent event) {
    Integer selectedId = (Integer) getGrid().getSelectedRow();
    setSelectedId(selectedId);

    closeDetailsView();
    if (selectedId != null) {
      T selectedModel = loadModelFromDatabase(selectedId);
      openDetailsView(selectedModel);
    }

    onDetails(event);
    executeRules();
  }

  private void handleRefreshClick(ClickEvent event) {
    refreshGrid();
  }

  private void openDetailsView(T selectedModel) {
    if (detailView != null) {
      closeDetailsView();
    }

    detailView = createDetailView(selectedModel);

    if (detailView == null) { return; }

    Optional<AuthorizationKeyMode> userAuthorization = getUserAuthorization();
    if (!userAuthorization.isPresent() || userAuthorization.get().equals(AuthorizationKeyMode.READ)) {
      detailView.setEnabled(false);
    }

    switch (detailViewMode) {
    case PARTIAL:
      root.addComponent(detailView);
      break;
    case FULL:
      OneButtonViewDialog dialog = new OneButtonViewDialog();

      dialog.setView(detailView);

      switch (detailView.getDisplayMode()) {
      case CONTENT_ONLY:
        detailView.getSaveButton().setVisible(false);
        detailView.getCancelButton().setVisible(false);
        dialog.getOkButton().setVisible(true);
        break;
      case NORMAL:
        detailView.getSaveButton().addClickListener(e -> dialog.close());
        detailView.getCancelButton().addClickListener(e -> dialog.close());
        dialog.getOkButton().setVisible(false);
        break;
      }

      dialog.open();
      break;
    }

    detailView.addCloseListener(e -> {
      closeDetailsView();
      refreshGrid();
    });
  }

  private void closeDetailsView() {
    if (detailView != null) {
      addButton.removeStyleName(VierkantTheme.BUTTON_FRIENDLY);
      addButton.addStyleName(VierkantTheme.BUTTON_QUIET);
      root.removeComponent(detailView);
      detailView = null;
    }
  }

  /**
   * Refreshes the grid datasource.
   */
  protected void refreshGrid() {
    UI.getCurrent().access(() -> {
      getGrid().setContainerDataSource(createContainer());
      executeRules();
    });
  }

  private void setUp() {
    root = new HorizontalLayout();
    addComponent(root);
    root.setSizeFull();

    CssLayout gridView = new CssLayout();
    root.addComponent(gridView);
    gridView.setSizeFull();

    buttonBar = new CssLayout();
    gridView.addComponent(buttonBar);
    // buttonBar.setHeight("37px");
    buttonBar.setHeightUndefined();
    buttonBar.setWidth("100%");

    addButton = new Button();
    buttonBar.addComponent(addButton);
    addButton.setIcon(FontAwesome.PLUS);
    addButton.addClickListener(this::handleAddClick);
    addButton.addStyleName(VierkantTheme.BUTTON_QUIET);
    addButton.addStyleName(VierkantTheme.BUTTON_ICON_ONLY);

    deleteButton = new Button();
    buttonBar.addComponent(deleteButton);
    deleteButton.setIcon(FontAwesome.MINUS);
    deleteButton.addClickListener(this::handleDeleteClick);
    deleteButton.addStyleName(VierkantTheme.BUTTON_QUIET);
    deleteButton.addStyleName(VierkantTheme.BUTTON_ICON_ONLY);

    refreshButton = new Button();
    buttonBar.addComponent(refreshButton);
    refreshButton.setIcon(FontAwesome.REFRESH);
    refreshButton.addClickListener(this::handleRefreshClick);
    refreshButton.addStyleName(VierkantTheme.BUTTON_QUIET);
    refreshButton.addStyleName(VierkantTheme.BUTTON_ICON_ONLY);

    grid = new Grid(container);
    gridView.addComponent(grid);
    grid.setWidth("100%");
    grid.setHeight(Page.getCurrent().getBrowserWindowHeight() - 266, Unit.PIXELS);
    Page.getCurrent().addBrowserWindowResizeListener(e -> {
      grid.setHeight(Page.getCurrent().getBrowserWindowHeight() - 266, Unit.PIXELS);
    });
    grid.setColumns(columns);
    grid.setImmediate(true);
    grid.addSelectionListener(this::handleDetailsClick);
    grid.addSortListener(this::handleSort);

    for (Column column : grid.getColumns()) {
      column.setEditable(false);
      column.setHeaderCaption(columnNames.getStringValue(column.getPropertyId().toString()));
      if (container != null && Boolean.class.isAssignableFrom(container.getType(column.getPropertyId()))) {
        column.setRenderer(new HtmlRenderer(), new CheckboxConverter());
      }
    }

    setSelectedId(VierkantUI.getIdFromNavigation(UI.getCurrent().getNavigator().getState()));

    executeRules();
  }

  private void handleSort(SortEvent event) {

    boolean sorted = Objects.equals(grid.getSortOrder(), grid.getData());
    grid.setData(new LinkedList<>(grid.getSortOrder()));

    if (!sorted) {
      refreshGrid();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
    updateViewParameters(event);
    grid.clearSortOrder();
    executeRules();
  }

  private void updateViewParameters(ViewChangeEvent event) {
    String parameters = event.getParameters();
    if (StringUtils.isInteger(parameters)) {
      setSelectedId(Integer.valueOf(parameters));
    }
  }

  /**
   * Hides the grid buttons.
   */
  public void hideButtons() {
    buttonBar.setVisible(false);
  }

  /**
   * Shows the grid buttons.
   */
  public void showButtons() {
    buttonBar.setVisible(true);
  }

}
