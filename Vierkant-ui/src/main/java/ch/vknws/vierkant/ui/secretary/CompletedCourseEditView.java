package ch.vknws.vierkant.ui.secretary;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.TrainingCourseModel;
import ch.vknws.vierkant.ui.AbstractView;
import ch.vknws.vierkant.ui.search.TrainingCourseColumnSearchView;

/**
 * View for editing of member groups.
 *
 * @author Benjamin Weber
 */
public class CompletedCourseEditView extends AbstractView implements CloseListener {
  private static final long serialVersionUID = -8936498798045864382L;

  private PersonModel person;

  private TrainingCourseColumnSearchView searchView;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param person the model to display
   */
  public CompletedCourseEditView(PersonModel person) {
    this.person = person;
    setUp();
  }

  private void setUp() {

    VerticalLayout wrapper = new VerticalLayout();
    addComponent(wrapper);
    wrapper.setMargin(true);

    FormLayout layout = new FormLayout();
    wrapper.addComponent(layout);

    searchView = new TrainingCourseColumnSearchView(Arrays.asList());
    layout.addComponent(searchView);
    searchView.setHeight(150, Unit.PIXELS);

    if (person.getRefCompletedCourses() != null && !person.getRefCompletedCourses().isEmpty()) {

      Set<TrainingCourseModel> groupMembers = person.getRefCompletedCourses();

      TwinColSelect select = searchView.getSelect();
      select.setContainerDataSource(new BeanItemContainer<>(TrainingCourseModel.class, groupMembers));

      for (TrainingCourseModel trainingCourse : groupMembers) {
        select.setItemCaption(trainingCourse, trainingCourse.toString());
      }

      for (TrainingCourseModel model : groupMembers) {
        select.select(model);
      }
    }

    executeRules();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();

  }

  /**
   * @return the searchView
   */
  public TrainingCourseColumnSearchView getSearchView() {
    return searchView;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void windowClose(CloseEvent e) {
    @SuppressWarnings("unchecked")
    Set<TrainingCourseModel> values = (Set<TrainingCourseModel>) getSearchView().getSelect().getValue();
    person.setRefCompletedCourses(new HashSet<>(values));
  }

}
