package ch.vknws.vierkant.ui.field.converter;

import java.time.LocalDate;
import java.util.Locale;

import com.vaadin.data.util.converter.Converter;

import ch.vknws.vierkant.ApplicationRegistry;

/**
 * Converter for rank models.
 *
 * @author Benjamin Weber
 */
public class LocalDateStringConverter implements Converter<String, LocalDate> {

  private static final long serialVersionUID = -4049802806505138209L;

  /**
   * {@inheritDoc}
   */
  @Override
  public LocalDate convertToModel(String value, Class<? extends LocalDate> targetType, Locale locale)
      throws com.vaadin.data.util.converter.Converter.ConversionException {
    if (value == null) { return null; }
    return (LocalDate) ApplicationRegistry.getInstance().getDateFormat().parse(value);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String convertToPresentation(LocalDate value, Class<? extends String> targetType, Locale locale)
      throws com.vaadin.data.util.converter.Converter.ConversionException {
    if (value == null) { return null; }
    return ApplicationRegistry.getInstance().getDateFormat().format(value);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Class<LocalDate> getModelType() {
    return LocalDate.class;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Class<String> getPresentationType() {
    return String.class;
  }

}
