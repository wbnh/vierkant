package ch.vknws.vierkant.ui.secretary;

import java.util.Set;

import ch.vknws.vierkant.rules.module.secretary.MutationAction;
import ch.vknws.vierkant.type.impl.PersonPersistable;

/**
 * Handles persisting mutations.
 *
 * @author Benjamin Weber
 */
public interface MutationPersister {

  /**
   * Prepares persisting
   * 
   * @param mutationActions the actions to perform
   * @param modelsToSave    the additional models that need to be saved
   */
  void prepare(Set<MutationAction> mutationActions, Set<PersonPersistable> modelsToSave);
}
