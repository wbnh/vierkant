package ch.vknws.vierkant.ui.view;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Resource;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.vknws.vierkant.ApplicationConstants;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.ApplicationTexts;
import ch.vknws.vierkant.backend.configuration.ConfigurationUtils;
import ch.vknws.vierkant.backend.configuration.VierkantConfigurations;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.backend.ldap.AuthenticationUtil;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.ui.AbstractView;
import ch.vknws.vierkant.ui.NavigationIds;
import ch.vknws.vierkant.ui.dialog.DialogUtils;

/**
 * View to display the current application information.
 *
 * @author Benjamin Weber
 *
 */
public class ApplicationInformationView extends AbstractView {
	private static final String COPYRIGHT = "\u00a9 Benjamin Weber (2018)";

	private static final long serialVersionUID = 8819893354059691909L;

	private static final ApplicationTexts APPLICATION_TEXTS = ServiceRegistry.getTypedConfigurationProxy()
					.get(new ConfigurationProperties<>(ApplicationTexts.class));

	private Label appVersion;
	private Label appBuildTime;
	private Label appName;
	private Label maintainerName;
	private Link maintainerEmail;
	private Label copyright;
	private PersonModel maintainerPerson;
	private Button testErrorReport;

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public ApplicationInformationView() {
		setUp();
	}

	private void setUp() {
		loadMaintainerPerson();

		FormLayout layout = new FormLayout();
		addComponent(layout);
		layout.setMargin(true);

		appName = new Label();
		layout.addComponent(appName);
		appName.setCaption(APPLICATION_TEXTS.applicationNameLabel());
		appName.setValue(ApplicationConstants.APPLICATION_NAME);

		appVersion = new Label();
		layout.addComponent(appVersion);
		appVersion.setCaption(APPLICATION_TEXTS.applicationVersionLabel());
		appVersion.setValue(APPLICATION_TEXTS.applicationVersion());

		appBuildTime = new Label();
		layout.addComponent(appBuildTime);
		appBuildTime.setCaption(APPLICATION_TEXTS.applicationBuildTimeLabel());
		appBuildTime.setValue(getBuildTime());

		HorizontalLayout maintainer = new HorizontalLayout();
		layout.addComponent(maintainer);
		maintainer.setMargin(false);
		maintainer.setSpacing(true);
		maintainer.setCaption(APPLICATION_TEXTS.maintainer());

		maintainerName = new Label();
		maintainer.addComponent(maintainerName);
		maintainerName.setValue(getMaintainerName());

		maintainerEmail = new Link();
		maintainer.addComponent(maintainerEmail);
		maintainerEmail.setResource(getMaintainerEmail());
		maintainerEmail.setCaption(maintainerPerson.getEmailOrganization());

		copyright = new Label();
		layout.addComponent(copyright);
		copyright.setValue(COPYRIGHT);

		if (AuthenticationUtil.isAdministrator(getLoggedInUser().getShorthand())) {
			testErrorReport = new Button();
			layout.addComponent(testErrorReport);
			testErrorReport.setCaption(APPLICATION_TEXTS.sendTestErrorReport());
			testErrorReport.addClickListener(e -> DialogUtils.showErrorDialog(errorMessages.testErrorTitle(),
							errorMessages.testErrorMessage(), new NullPointerException()));
		}

	}

	private String getBuildTime() {
		String raw = APPLICATION_TEXTS.applicationBuildTime();

		Instant instant = Instant.parse(raw);

		LocalDateTime date = instant.atZone(ZoneId.of("UTC")).withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();

		return VaadinSession.getCurrent().getAttribute(ApplicationRegistry.class).getDateTimeFormat().format(date);
	}

	private void loadMaintainerPerson() {
		String maintainerShorthand = ConfigurationUtils.getConfiguration(VierkantConfigurations.MAINTAINER_SHORTHAND);

		PersonModel personPersistable = DaoFactory.getInstance().getPersonDao().loadByShorthand(maintainerShorthand);

		maintainerPerson = personPersistable;
	}

	private String getMaintainerName() {
		return maintainerPerson.toString();
	}

	private Resource getMaintainerEmail() {
		String email = maintainerPerson.getEmailOrganization();
		ExternalResource resource = new ExternalResource("mailto:" + email);
		return resource;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void enter(ViewChangeEvent event) {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getViewId() {
		return NavigationIds.metapage;
	}

}
