package ch.vknws.vierkant.ui.secretary;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.faa.v1.core.util.Pair;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.rules.module.secretary.RankModificationUtil;
import ch.vknws.vierkant.rules.rights.AuthorizationRules;
import ch.vknws.vierkant.type.AuthorizationKeyMode;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.RankModel;
import ch.vknws.vierkant.ui.AbstractView;
import ch.vknws.vierkant.ui.NavigationIds;
import ch.vknws.vierkant.ui.dialog.DialogUtils;
import ch.vknws.vierkant.ui.dialog.TwoButtonViewDialog;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * View for rank changes.
 *
 * @author Benjamin Weber
 */
public class SecretaryRankModificationView extends AbstractView {
  private static final long serialVersionUID = 8283549802401911389L;
  private static final RankModificationTexts rankTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(RankModificationTexts.class));

  private TextArea appointShorthands;
  private TextArea promoteShorthands;
  private TextArea brevetShorthands;

  private Button appointButton;
  private Button promoteButton;
  private Button brevetButton;

  private Label appointErrorLabel;
  private Label promoteErrorLabel;
  private Label brevetErrorLabel;

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public SecretaryRankModificationView() {

    personAuthorizations.add(SecretaryAuthorizationKeys.PERSON_ANY);

    VerticalLayout wrapper = new VerticalLayout();
    addComponent(wrapper);
    wrapper.setMargin(true);
    wrapper.setSpacing(true);

    Label instructions = new Label();
    wrapper.addComponent(instructions);
    instructions.setValue(rankTexts.instructions());
    instructions.addStyleName(VierkantTheme.LABEL_LARGE);

    HorizontalLayout promote = new HorizontalLayout();
    wrapper.addComponent(promote);
    promote.setMargin(false);
    promote.setSpacing(true);

    promoteShorthands = new TextArea();
    promote.addComponent(promoteShorthands);

    promoteButton = new Button();
    promote.addComponent(promoteButton);
    promoteButton.setCaption(rankTexts.promote());
    promoteButton.addClickListener(this::promote);

    promoteErrorLabel = new Label();
    promote.addComponent(promoteErrorLabel);
    promoteErrorLabel.addStyleName(VierkantTheme.LABEL_FAILURE);

    HorizontalLayout brevet = new HorizontalLayout();
    wrapper.addComponent(brevet);
    brevet.setMargin(false);
    brevet.setSpacing(true);

    brevetShorthands = new TextArea();
    brevet.addComponent(brevetShorthands);

    brevetButton = new Button();
    brevet.addComponent(brevetButton);
    brevetButton.setCaption(rankTexts.brevet());
    brevetButton.addClickListener(this::brevet);

    brevetErrorLabel = new Label();
    brevet.addComponent(brevetErrorLabel);
    brevetErrorLabel.addStyleName(VierkantTheme.LABEL_FAILURE);

    HorizontalLayout appoint = new HorizontalLayout();
    wrapper.addComponent(appoint);
    appoint.setMargin(false);
    appoint.setSpacing(true);

    appointShorthands = new TextArea();
    appoint.addComponent(appointShorthands);

    appointButton = new Button();
    appoint.addComponent(appointButton);
    appointButton.setCaption(rankTexts.appoint());
    appointButton.addClickListener(this::appoint);

    appointErrorLabel = new Label();
    appoint.addComponent(appointErrorLabel);
    appointErrorLabel.addStyleName(VierkantTheme.LABEL_FAILURE);

    promoteErrorLabel.setVisible(false);
    brevetErrorLabel.setVisible(false);
    appointErrorLabel.setVisible(false);

    executeRules();
  }

  private Set<String> getShorthands(TextArea source) {
    String value = source.getValue().trim();
    String[] split = value.split("\\n");
    Set<String> shorthands = new HashSet<>();

    for (String shorthand : split) {
      if (shorthand == null || shorthand.isEmpty()) {
        continue;
      }
      shorthands.add(shorthand.trim());
    }

    return shorthands;
  }

  private PersonModel loadPerson(String shorthand) {
    return DaoFactory.getInstance().getPersonDao().loadByShorthand(shorthand);
  }

  private void savePerson(PersonModel person, List<String> errorShorthands) {
    try {
      DaoFactory.getInstance().getPersonDao().save(person,
          ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
    } catch (SaveException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not save rankModification", e);
      DialogUtils.showErrorDialog(errorMessages.saveFailTitle(), errorMessages.saveFailMessage(person.toString()), e);
      errorShorthands.add(person.getShorthand());
    }
  }

  private void displayNonModified(String action, List<String> shorthands, Label label) {
    if (shorthands == null || shorthands.isEmpty()) {
      label.setValue("");
      label.setVisible(false);
    } else {
      StringBuilder builder = new StringBuilder();
      boolean first = true;
      for (String shorthand : shorthands) {
        if (first) {
          first = false;
        } else {
          builder.append(", ");
        }

        builder.append(shorthand);
      }

      label.setValue(rankTexts.noActionPerformedMessage(action, builder.toString()));
      label.setVisible(true);
    }
  }

  private void appoint(ClickEvent event) {
    TwoButtonViewDialog dialog = new TwoButtonViewDialog();
    dialog.setCaption(rankTexts.rankQueryTitle());
    RankQueryView view = new RankQueryView(rankTexts.rankQueryMessage());
    dialog.setView(view);

    dialog.getOkButton().addClickListener(e -> {
      Set<String> shorthands = getShorthands(appointShorthands);
      appointShorthands.clear();
      List<String> errorShorthands = new LinkedList<>();
      for (String shorthand : shorthands) {
        PersonModel person = loadPerson(shorthand);
        if (person == null) {
          errorShorthands.add(shorthand);
          continue;
        }
        person.setRefRank(view.getSelectedRank());
        person.setAuditComment(rankTexts.appointAuditComment());
        savePerson(person, errorShorthands);
      }
      displayNonModified(rankTexts.appointed(), errorShorthands, appointErrorLabel);
    });
    dialog.open();
  }

  private void promote(ClickEvent event) {
    Set<String> shorthands = getShorthands(promoteShorthands);
    promoteShorthands.clear();
    List<String> errorShorthands = new LinkedList<>();
    for (String shorthand : shorthands) {
      PersonModel person = loadPerson(shorthand);
      if (person == null) {
        errorShorthands.add(shorthand);
        continue;
      }
      RankModel rank = person.getRefRank();
      if (rank == null) {
        errorShorthands.add(shorthand);
        continue;
      }

      String newRankAbbreviation = RankModificationUtil.PROMOTIONS.get(rank.getAbbreviation());
      if (newRankAbbreviation == null) {
        errorShorthands.add(shorthand);
        continue;
      }
      RankModel newRank = DaoFactory.getInstance().getRankDao().loadByAbbreviation(newRankAbbreviation);
      if (newRank == null) {
        errorShorthands.add(shorthand);
        continue;
      }
      person.setRefRank(newRank);
      person.setAuditComment(rankTexts.promoteAuditComment());
      savePerson(person, errorShorthands);
    }
    displayNonModified(rankTexts.promoted(), errorShorthands, promoteErrorLabel);
  }

  private void brevet(ClickEvent event) {
    Set<String> shorthands = getShorthands(brevetShorthands);
    brevetShorthands.clear();
    List<String> errorShorthands = new LinkedList<>();
    for (String shorthand : shorthands) {
      PersonModel person = loadPerson(shorthand);
      if (person == null) {
        errorShorthands.add(shorthand);
        continue;
      }
      RankModel rank = person.getRefRank();
      if (rank == null) {
        errorShorthands.add(shorthand);
        continue;
      }

      String newRankAbbreviation = RankModificationUtil.BREVETS.get(rank.getAbbreviation());
      if (newRankAbbreviation == null) {
        errorShorthands.add(shorthand);
        continue;
      }
      RankModel newRank = DaoFactory.getInstance().getRankDao().loadByAbbreviation(newRankAbbreviation);
      if (newRank == null) {
        errorShorthands.add(shorthand);
        continue;
      }
      person.setRefRank(newRank);
      person.setAuditComment(rankTexts.brevetAuditComment());
      savePerson(person, errorShorthands);
    }
    displayNonModified(rankTexts.breveted(), errorShorthands, brevetErrorLabel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();

    Set<Pair<String, AuthorizationKeyMode>> keys = AuthorizationRules.getInstance()
        .getEffectiveAuthorizationKeys(getLoggedInUser());
    applyAppointEditability(keys);
    applyPromoteEditability(keys);
    applyBrevetEditability(keys);
  }

  private void applyAppointEditability(Set<Pair<String, AuthorizationKeyMode>> keys) {
    Optional<AuthorizationKeyMode> authorization = AuthorizationRules.getInstance()
        .hasAuthorization(SecretaryAuthorizationKeys.PERSON_ANY, keys, AuthorizationKeyMode.WRITE);
    boolean editable = authorization.isPresent();

    appointButton.setEnabled(editable);
    appointShorthands.setEnabled(editable);
  }

  private void applyPromoteEditability(Set<Pair<String, AuthorizationKeyMode>> keys) {
    Optional<AuthorizationKeyMode> authorization = AuthorizationRules.getInstance()
        .hasAuthorization(SecretaryAuthorizationKeys.PERSON_ANY, keys, AuthorizationKeyMode.WRITE);
    boolean editable = authorization.isPresent();

    promoteButton.setEnabled(editable);
    promoteShorthands.setEnabled(editable);
  }

  private void applyBrevetEditability(Set<Pair<String, AuthorizationKeyMode>> keys) {
    Optional<AuthorizationKeyMode> authorization = AuthorizationRules.getInstance()
        .hasAuthorization(SecretaryAuthorizationKeys.PERSON_ANY, keys, AuthorizationKeyMode.WRITE);
    boolean editable = authorization.isPresent();

    brevetButton.setEnabled(editable);
    brevetShorthands.setEnabled(editable);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return NavigationIds.secretary_rankmod;
  }

}
