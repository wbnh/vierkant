package ch.vknws.vierkant.ui.secretary;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

import org.apache.directory.api.ldap.model.exception.LdapException;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.backend.function.MutationFunctions;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.rules.module.secretary.MutationAction;
import ch.vknws.vierkant.rules.module.secretary.SecretaryRules;
import ch.vknws.vierkant.rules.rights.AuthorizationRules;
import ch.vknws.vierkant.type.AddressModel;
import ch.vknws.vierkant.type.AuthorizationKeyMode;
import ch.vknws.vierkant.type.MedicalInformationModel;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.PersonFunctionModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.SexType;
import ch.vknws.vierkant.type.TrainingCourseModel;
import ch.vknws.vierkant.type.VKA;
import ch.vknws.vierkant.type.caption.AddressPersistableTexts;
import ch.vknws.vierkant.type.caption.MedicalInformationPersistableTexts;
import ch.vknws.vierkant.type.caption.PersonPersistableTexts;
import ch.vknws.vierkant.type.impl.AddressPersistable;
import ch.vknws.vierkant.type.impl.MedicalInformationPersistable;
import ch.vknws.vierkant.type.impl.PersonPersistable;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.dialog.DialogUtils;
import ch.vknws.vierkant.ui.dialog.OneButtonViewDialog;
import ch.vknws.vierkant.ui.field.ViewDialogButtonField;
import ch.vknws.vierkant.ui.field.converter.LocalDateConverter;
import ch.vknws.vierkant.ui.field.converter.LocalDateStringConverter;
import ch.vknws.vierkant.ui.field.converter.RankNameConverter;
import ch.vknws.vierkant.ui.personal.PersonalContactPersonTableView;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * Details view of a person for the secretary.
 *
 * @author Benjamin Weber
 */
public class SecretaryPersonDetailsView extends AbstractDetailView<PersonModel> implements MutationPersister {
  private static final long serialVersionUID = -7589784662167949340L;

  private static final PersonPersistableTexts personTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(PersonPersistableTexts.class));
  private static final AddressPersistableTexts addressTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(AddressPersistableTexts.class));
  private static final MedicalInformationPersistableTexts medicalInformationTexts = ServiceRegistry
      .getTypedConfigurationProxy().get(new ConfigurationProperties<>(MedicalInformationPersistableTexts.class));

  private Set<PersonPersistable> otherModelsToSave = new HashSet<>();

  private BeanFieldGroup<AddressModel> addressGroup;
  private BeanFieldGroup<MedicalInformationModel> medicalInformationGroup;

  private TextField title;
  private Label rank; // editable through other processes
  private TextField givenName;
  private TextField familyName;
  private Label shorthand; // non-editable, as other systems depend on it
  private TextField addressLine1;
  private TextField addressLine2;
  private TextField addressZipCode;
  private TextField addressCity;
  private TextField addressCanton;
  private TextField addressCountry;
  private TextField telephone;
  private TextField mobile;
  private TextField emailPrivate;
  private Label emailOrganization; // non-editable as it is created based on the
                                   // shorthand
  private DateField birthdate; // usually not to edit, but mistakes happen
  private TextArea allergyComment;
  private TextArea medicalComment;
  private TextArea classifiedConditions;
  private TextArea classifiedMedicalComment;
  private TextArea secretaryComment;
  // supervisor comment not visible to secretary
  private CheckBox vkNews;
  private ComboBox sex;

  private ViewDialogButtonField memberGroup;
  private ViewDialogButtonField functions;
  private ComboBox vka;
  private ViewDialogButtonField completedCourses;
  private Label lastUpdateCheck; // is updated automatically
  private Button contactPerson;
  private Button leaveOrganization;
  private ViewDialogButtonField aargauLicenses;

  private Set<MutationAction> mutationActions = null;
  private String initialShorthand;

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param displayedModel the model to display
   */
  protected SecretaryPersonDetailsView(PersonModel displayedModel) {
    super(displayedModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getViewCaption() {
    return getDisplayedModel().toString();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FormLayout setUp() {
    initialShorthand = getDisplayedModel().getShorthand();

    if (getDisplayedModel().getRefHomeAddress() == null) {
      getDisplayedModel().setRefHomeAddress(new AddressPersistable());
    }
    if (getDisplayedModel().getRefMedicalInformation() == null) {
      getDisplayedModel().setRefMedicalInformation(new MedicalInformationPersistable());
    }

    addressGroup = createFieldGroup(AddressModel.class);
    addressGroup.setItemDataSource(getDisplayedModel().getRefHomeAddress());
    medicalInformationGroup = createFieldGroup(MedicalInformationModel.class);
    medicalInformationGroup.setItemDataSource(getDisplayedModel().getRefMedicalInformation());

    FormLayout layout = new FormLayout();
    layout.setMargin(true);

    title = new TextField();
    layout.addComponent(title);
    title.setCaption(personTexts.title());
    title.setNullRepresentation("");
    title.setWidth(200f, Unit.PIXELS);
    title.addValueChangeListener(ruleExecutionListener);
    saveGroup.bind(title, PersonModel.PropertyId.TITLE);

    rank = new Label();
    layout.addComponent(rank);
    rank.setCaption(personTexts.refRank());
    rank.setWidth(200f, Unit.PIXELS);
    rank.setConverter(new RankNameConverter());
    rank.addValueChangeListener(ruleExecutionListener);
    rank.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.REF_RANK));

    givenName = new TextField();
    layout.addComponent(givenName);
    givenName.setCaption(personTexts.givenName());
    givenName.setNullRepresentation("");
    givenName.setWidth(200f, Unit.PIXELS);
    givenName.addValueChangeListener(ruleExecutionListener);
    saveGroup.bind(givenName, PersonModel.PropertyId.GIVEN_NAME);

    familyName = new TextField();
    layout.addComponent(familyName);
    familyName.setCaption(personTexts.familyName());
    familyName.setNullRepresentation("");
    familyName.setWidth(200f, Unit.PIXELS);
    familyName.addValueChangeListener(ruleExecutionListener);
    saveGroup.bind(familyName, PersonModel.PropertyId.FAMILY_NAME);

    shorthand = new Label();
    layout.addComponent(shorthand);
    shorthand.setCaption(personTexts.shorthand());
    shorthand.setWidth(200f, Unit.PIXELS);
    shorthand.addValueChangeListener(ruleExecutionListener);
    shorthand.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.SHORTHAND));

    addressLine1 = new TextField();
    layout.addComponent(addressLine1);
    addressLine1.setCaption(addressTexts.line1());
    addressLine1.setNullRepresentation("");
    addressLine1.setWidth(200f, Unit.PIXELS);
    addressLine1.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(addressLine1, AddressModel.PropertyId.LINE1);

    addressLine2 = new TextField();
    layout.addComponent(addressLine2);
    addressLine2.setCaption(addressTexts.line2());
    addressLine2.setNullRepresentation("");
    addressLine2.setWidth(200f, Unit.PIXELS);
    addressLine2.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(addressLine2, AddressModel.PropertyId.LINE2);

    HorizontalLayout zipCity = new HorizontalLayout();
    layout.addComponent(zipCity);
    zipCity.setMargin(false);
    zipCity.setSpacing(false);
    zipCity.setWidth(200f, Unit.PIXELS);
    zipCity.setCaption(addressTexts.zipCode() + "/" + addressTexts.city());

    addressZipCode = new TextField();
    zipCity.addComponent(addressZipCode);
    zipCity.setExpandRatio(addressZipCode, 0.4f);
    addressZipCode.setWidth("100%");
    addressZipCode.setNullRepresentation("");
    addressZipCode.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(addressZipCode, AddressModel.PropertyId.ZIP_CODE);

    addressCity = new TextField();
    zipCity.addComponent(addressCity);
    zipCity.setExpandRatio(addressCity, 0.6f);
    addressCity.setWidth("100%");
    addressCity.setNullRepresentation("");
    addressCity.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(addressCity, AddressModel.PropertyId.CITY);

    addressCanton = new TextField();
    layout.addComponent(addressCanton);
    addressCanton.setCaption(addressTexts.canton());
    addressCanton.setNullRepresentation("");
    addressCanton.setWidth(200f, Unit.PIXELS);
    addressCanton.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(addressCanton, AddressModel.PropertyId.CANTON);

    addressCountry = new TextField();
    layout.addComponent(addressCountry);
    addressCountry.setCaption(addressTexts.country());
    addressCountry.setNullRepresentation("");
    addressCountry.setWidth(200f, Unit.PIXELS);
    addressCountry.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(addressCountry, AddressModel.PropertyId.COUNTRY);

    telephone = new TextField();
    layout.addComponent(telephone);
    telephone.setCaption(personTexts.telephone());
    telephone.setNullRepresentation("");
    telephone.setWidth(200f, Unit.PIXELS);
    telephone.addValueChangeListener(ruleExecutionListener);
    saveGroup.bind(telephone, PersonModel.PropertyId.TELEPHONE);

    mobile = new TextField();
    layout.addComponent(mobile);
    mobile.setCaption(personTexts.mobile());
    mobile.setNullRepresentation("");
    mobile.setWidth(200f, Unit.PIXELS);
    mobile.addValueChangeListener(ruleExecutionListener);
    saveGroup.bind(mobile, PersonModel.PropertyId.MOBILE);

    emailPrivate = new TextField();
    layout.addComponent(emailPrivate);
    emailPrivate.setCaption(personTexts.emailPrivate());
    emailPrivate.setNullRepresentation("");
    emailPrivate.setWidth(200f, Unit.PIXELS);
    emailPrivate.addValueChangeListener(ruleExecutionListener);
    saveGroup.bind(emailPrivate, PersonModel.PropertyId.EMAIL_PRIVATE);

    emailOrganization = new Label();
    layout.addComponent(emailOrganization);
    emailOrganization.setCaption(personTexts.emailOrganization());
    emailOrganization.setWidth(200f, Unit.PIXELS);
    emailOrganization
        .setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.EMAIL_ORGANISATION));

    birthdate = new DateField();
    layout.addComponent(birthdate);
    birthdate.setCaption(personTexts.birthdate());
    birthdate.setWidth(200f, Unit.PIXELS);
    birthdate.setConverter(new LocalDateConverter());
    birthdate.addValueChangeListener(ruleExecutionListener);
    saveGroup.bind(birthdate, PersonModel.PropertyId.BIRTHDATE);

    allergyComment = new TextArea();
    layout.addComponent(allergyComment);
    allergyComment.setCaption(medicalInformationTexts.allergies());
    allergyComment.setNullRepresentation("");
    allergyComment.setWidth(200f, Unit.PIXELS);
    allergyComment.addValueChangeListener(ruleExecutionListener);
    medicalInformationGroup.bind(allergyComment, MedicalInformationModel.PropertyId.ALLERGIES);

    medicalComment = new TextArea();
    layout.addComponent(medicalComment);
    medicalComment.setCaption(medicalInformationTexts.comment());
    medicalComment.setNullRepresentation("");
    medicalComment.setWidth(200f, Unit.PIXELS);
    medicalComment.addValueChangeListener(ruleExecutionListener);
    medicalInformationGroup.bind(medicalComment, MedicalInformationModel.PropertyId.COMMENT);

    classifiedConditions = new TextArea();
    layout.addComponent(classifiedConditions);
    classifiedConditions.setCaption(medicalInformationTexts.classifiedConditions());
    classifiedConditions.setNullRepresentation("");
    classifiedConditions.setWidth(200f, Unit.PIXELS);
    classifiedConditions.addValueChangeListener(ruleExecutionListener);
    medicalInformationGroup.bind(classifiedConditions, MedicalInformationModel.PropertyId.CLASSIFIED_CONDITIONS);

    classifiedMedicalComment = new TextArea();
    layout.addComponent(classifiedMedicalComment);
    classifiedMedicalComment.setCaption(medicalInformationTexts.classifiedComment());
    classifiedMedicalComment.setNullRepresentation("");
    classifiedMedicalComment.setWidth(200f, Unit.PIXELS);
    classifiedMedicalComment.addValueChangeListener(ruleExecutionListener);
    medicalInformationGroup.bind(classifiedMedicalComment, MedicalInformationModel.PropertyId.CLASSIFIED_COMMENT);

    secretaryComment = new TextArea();
    layout.addComponent(secretaryComment);
    secretaryComment.setCaption(personTexts.secretaryComment());
    secretaryComment.setNullRepresentation("");
    secretaryComment.setWidth(200f, Unit.PIXELS);
    secretaryComment.addValueChangeListener(ruleExecutionListener);
    saveGroup.bind(secretaryComment, PersonModel.PropertyId.SECRETARY_COMMENT);

    vkNews = new CheckBox();
    layout.addComponent(vkNews);
    vkNews.setCaption(personTexts.vkNews());
    vkNews.setWidth(200f, Unit.PIXELS);
    vkNews.addValueChangeListener(ruleExecutionListener);
    saveGroup.bind(vkNews, PersonModel.PropertyId.VK_NEWS);

    sex = new ComboBox();
    layout.addComponent(sex);
    sex.setCaption(personTexts.sex());
    sex.setContainerDataSource(createSexContainer());
    sex.setWidth(200f, Unit.PIXELS);
    sex.setTextInputAllowed(false);
    sex.setNullSelectionAllowed(false);
    sex.setItemCaptionMode(ItemCaptionMode.ID_TOSTRING);
    sex.addValueChangeListener(e -> executeRules());
    sex.addValueChangeListener(ruleExecutionListener);
    saveGroup.bind(sex, PersonModel.PropertyId.SEX);

    memberGroup = new ViewDialogButtonField(new MemberGroupEditView(getDisplayedModel(), this));
    layout.addComponent(memberGroup);
    memberGroup.setDisplayTransformer(o -> getMemberGroupName());
    memberGroup.setCaption(personTexts.memberCategory());
    memberGroup.setWidth(200f, Unit.PIXELS);
    memberGroup.setDialogWidth("500px");
    memberGroup.addValueChangeListener(ruleExecutionListener);
    saveGroup.bind(memberGroup, PersonModel.PropertyId.MEMBER_CATEGORY);

    functions = new ViewDialogButtonField(new FunctionEditView(getDisplayedModel()));
    layout.addComponent(functions);
    functions.setDisplayTransformer(o -> getFunctionLabel());
    functions.setCaption(personTexts.functions());
    functions.setWidth(200f, Unit.PIXELS);
    functions.setDialogWidth("600px");
    saveGroup.bind(functions, PersonModel.PropertyId.REF_FUNCTIONS);

    vka = new ComboBox();
    layout.addComponent(vka);
    vka.setCaption(personTexts.vka());
    vka.setContainerDataSource(createVkaContainer());
    vka.setTextInputAllowed(false);
    vka.setNullSelectionAllowed(true);
    vka.setItemCaptionMode(ItemCaptionMode.ID_TOSTRING);
    vka.setValue(getDisplayedModel().getVka());
    vka.addValueChangeListener(e -> getDisplayedModel().setVka((VKA) e.getProperty().getValue()));
    vka.setWidth(200f, Unit.PIXELS);
    vka.addValueChangeListener(ruleExecutionListener);
    saveGroup.bind(vka, PersonModel.PropertyId.VKA);

    completedCourses = new ViewDialogButtonField(new CompletedCourseEditView(getDisplayedModel()));
    layout.addComponent(completedCourses);
    completedCourses.setDisplayTransformer(o -> getCompletedCoursesLabel());
    completedCourses.setCaption(personTexts.completedCourses());
    completedCourses.setWidth(200f, Unit.PIXELS);
    completedCourses.setDialogWidth("600px");
    completedCourses.setDialogHeight("500px");
    saveGroup.bind(completedCourses, PersonModel.PropertyId.REF_COMPLETED_COURSES);

    lastUpdateCheck = new Label();
    layout.addComponent(lastUpdateCheck);
    lastUpdateCheck.setCaption(personTexts.lastUpdateCheck());
    lastUpdateCheck.setWidth(200f, Unit.PIXELS);
    lastUpdateCheck.setConverter(new LocalDateStringConverter());
    lastUpdateCheck.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.LAST_UPDATE_CHECK));

    aargauLicenses = new ViewDialogButtonField(new AargauPersonLicenseEditView(getDisplayedModel()));
    layout.addComponent(aargauLicenses);
    aargauLicenses.setDisplayTransformer(o -> getAargauPersonLicenseLabel());
    aargauLicenses.setCaption(personTexts.aargauLicense());
    aargauLicenses.setWidth(200f, Unit.PIXELS);
    // aargauLicenses.setDialogWidth("600px");
    // aargauLicenses.setDialogHeight("500px");
    saveGroup.bind(aargauLicenses, PersonModel.PropertyId.REF_AARGAU_LICENSE);

    contactPerson = new Button();
    layout.addComponent(contactPerson);
    contactPerson.setCaption(personTexts.contactPersons());
    contactPerson.addClickListener(this::handleContactPersonClick);
    contactPerson.setWidth(200f, Unit.PIXELS);

    leaveOrganization = new Button();
    layout.addComponent(leaveOrganization);
    leaveOrganization.setCaption(personTexts.leaveOrganization());
    leaveOrganization.addClickListener(this::handleLeaveOrganizationClick);
    leaveOrganization.addStyleName(VierkantTheme.BUTTON_DANGER);
    leaveOrganization.setWidth(200f, Unit.PIXELS);

    title.focus();

    return layout;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void prepare(Set<MutationAction> mutationActions, Set<PersonPersistable> modelsToSave) {
    this.mutationActions = mutationActions;
    modelsToSave.stream().filter(p -> p != getDisplayedModel()).forEach(p -> otherModelsToSave.add(p));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();
    if (!isInitFinished()) { return; }

    rebindLabels();
    rebindFields();
    memberGroup.executeRules();
    functions.executeRules();
    completedCourses.executeRules();
    aargauLicenses.executeRules();

    applyMedicalInformationVisibility();
    applyClassifiedMedicalInformationVisibility();
    applyLeaveOrganizationVisibility();

    applyPersonDataEditability();
    applyMedicalInformationEditability();
    applyClassifiedMedicalInformationEditability();
    applySecretaryCommentEditability();
  }

  private void applyLeaveOrganizationVisibility() {
    boolean visible = getDisplayedModel().getMemberCategory() != null;

    leaveOrganization.setVisible(visible);
  }

  private void applyMedicalInformationVisibility() {
    boolean visible = SecretaryRules.getInstance().isMedicalInformationVisible(getDisplayedModel(), getLoggedInUser());

    allergyComment.setVisible(visible);
    medicalComment.setVisible(visible);
  }

  private void applyClassifiedMedicalInformationVisibility() {
    boolean visible = SecretaryRules.getInstance().isClassifiedMedicalInformationVisible(getDisplayedModel(),
        getLoggedInUser());

    classifiedConditions.setVisible(visible);
    classifiedMedicalComment.setVisible(visible);
  }

  private void applyPersonDataEditability() {
    Optional<AuthorizationKeyMode> authorization = AuthorizationRules.getInstance()
        .hasAuthorization(SecretaryAuthorizationKeys.PERSON_ANY, getLoggedInUser(), AuthorizationKeyMode.WRITE);
    boolean editable = authorization.isPresent();

    title.setEnabled(editable);
    givenName.setEnabled(editable);
    familyName.setEnabled(editable);
    addressLine1.setEnabled(editable);
    addressLine2.setEnabled(editable);
    addressZipCode.setEnabled(editable);
    addressCity.setEnabled(editable);
    addressCanton.setEnabled(editable);
    addressCountry.setEnabled(editable);
    telephone.setEnabled(editable);
    mobile.setEnabled(editable);
    emailPrivate.setEnabled(editable);
    birthdate.setEnabled(editable);
    vkNews.setEnabled(editable);
    sex.setEnabled(editable);
    memberGroup.setEnabled(editable);
    functions.setEnabled(editable);
    vka.setEnabled(editable);
    completedCourses.setEnabled(editable);
    contactPerson.setEnabled(editable);
    aargauLicenses.setEnabled(editable);
  }

  private void applyMedicalInformationEditability() {
    Optional<AuthorizationKeyMode> authorization = AuthorizationRules.getInstance().hasAuthorization(
        SecretaryAuthorizationKeys.MEDICAL_INFORMATION_ANY, getLoggedInUser(), AuthorizationKeyMode.WRITE);
    boolean editable = authorization.isPresent();

    allergyComment.setEnabled(editable);
    medicalComment.setEnabled(editable);
  }

  private void applyClassifiedMedicalInformationEditability() {
    Optional<AuthorizationKeyMode> authorization = AuthorizationRules.getInstance().hasAuthorization(
        SecretaryAuthorizationKeys.CLASSIFIED_MEDICAL_INFORMATION_ANY, getLoggedInUser(), AuthorizationKeyMode.WRITE);
    boolean editable = authorization.isPresent();

    classifiedConditions.setEnabled(editable);
    classifiedMedicalComment.setEnabled(editable);
  }

  private void applySecretaryCommentEditability() {
    Optional<AuthorizationKeyMode> authorization = AuthorizationRules.getInstance().hasAuthorization("secretary",
        getLoggedInUser());
    boolean editable = authorization.isPresent();

    secretaryComment.setEnabled(editable);
  }

  private BeanItemContainer<SexType> createSexContainer() {
    BeanItemContainer<SexType> container = new BeanItemContainer<>(SexType.class);

    container.addAll(EnumSet.allOf(SexType.class));

    return container;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void rebindFields() {
    super.rebindFields();

    if (vkNews != null) {
      saveGroup.unbind(vkNews);
      saveGroup.bind(vkNews, PersonModel.PropertyId.VK_NEWS);
    }
  }

  private void rebindLabels() {
    if (shorthand != null) {
      shorthand.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.SHORTHAND));
    }
    if (emailOrganization != null) {
      emailOrganization
          .setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.EMAIL_ORGANISATION));
    }
    if (rank != null) {
      rank.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.REF_RANK));
    }
    if (lastUpdateCheck != null) {
      lastUpdateCheck
          .setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.LAST_UPDATE_CHECK));
    }
  }

  private String getMemberGroupName() {
    if (getDisplayedModel().getMemberCategory() == null) { return ""; }
    return getDisplayedModel().getMemberCategory().toString();
  }

  private String getFunctionLabel() {
    StringBuilder builder = new StringBuilder();

    OrganizationFunctionModel refPrimaryFunction = getDisplayedModel().getRefPrimaryFunction();
    if (getDisplayedModel().getRefFunctions() != null) {
      if (refPrimaryFunction != null) {
        builder.append("<b>");
        builder.append(refPrimaryFunction.getFunctionName());
        builder.append("</b>");
      }
      for (PersonFunctionModel function : getDisplayedModel().getRefFunctions()) {
        if (function == null || (refPrimaryFunction != null
            && function.getRefFunction().getRecordId().equals(refPrimaryFunction.getRecordId()))) {
          continue;
        }
        if (builder.length() > 0) {
          builder.append(", ");
        }
        builder.append(function.getRefFunction().getFunctionName());
      }
    }

    return builder.toString();
  }

  private void handleContactPersonClick(ClickEvent event) {
    OneButtonViewDialog dialog = new OneButtonViewDialog();
    dialog.setSizeFull();
    dialog.setResizable(false);

    PersonalContactPersonTableView view = new PersonalContactPersonTableView(getDisplayedModel());

    dialog.setView(view);
    dialog.open();
  }

  private void handleLeaveOrganizationClick(ClickEvent event) {
    DialogUtils.showLeaveOrganizationConfirmDialog(getDisplayedModel().toString(), e -> {
      if (mutationActions == null) {
        mutationActions = new HashSet<>();
      }
      mutationActions.addAll(MutationFunctions.mutate(getDisplayedModel(), null));
      executeRules();
    });
  }

  private String getAargauPersonLicenseLabel() {
    if (getDisplayedModel() == null || getDisplayedModel().getRefAargauLicense() == null
        || getDisplayedModel().getRefAargauLicense().getRefLicense() == null) { return null; }
    return getDisplayedModel().getRefAargauLicense().getRefLicense().getLicenseName();
  }

  private String getCompletedCoursesLabel() {
    if (getDisplayedModel() == null || getDisplayedModel().getRefCompletedCourses() == null) { return null; }
    StringBuilder builder = new StringBuilder();
    for (TrainingCourseModel trainingCourse : getDisplayedModel().getRefCompletedCourses()) {
      if (builder.length() > 0) {
        builder.append(", ");
      }
      builder.append(trainingCourse.getTrainingCourseName());
    }

    return builder.toString();
  }

  private BeanItemContainer<VKA> createVkaContainer() {
    BeanItemContainer<VKA> container = new BeanItemContainer<>(VKA.class);

    container.addAll(EnumSet.allOf(VKA.class));

    return container;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void saveDisplayedModel() throws SaveException {
    handleMutations();

    saveOtherMemberGroupMembers();

    DaoFactory.getInstance().getPersonDao().save(getDisplayedModel(),
        ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
  }

  private void handleMutations() {
    if (mutationActions != null) {
      try {
        if (mutationActions.contains(MutationAction.GENERATE_SHORTHAND)) {
          MutationFunctions.createLogin(getDisplayedModel());
        } else if (mutationActions.contains(MutationAction.REVOKE_SHORTHAND)) {
          MutationFunctions.revokeLogin(initialShorthand);
        }
      } catch (LdapException e) {
        ServiceRegistry.getLogProxy().error(this, "Could not modify LDAP account for person {0}", e,
            getDisplayedModel());
        DialogUtils.showErrorDialog(errorMessages.mutationFailedTitle(), errorMessages.mutationFailedMessage(), e);
      }
      mutationActions = null;
    }
  }

  private void saveOtherMemberGroupMembers() {
    for (Iterator<PersonPersistable> iterator = otherModelsToSave.iterator(); iterator.hasNext();) {
      PersonPersistable personModel = iterator.next();
      try {
        if (personModel.getRefMemberGroup() != null) {
          personModel.getRefMemberGroup().setAuditComment(getDisplayedModel().getAuditComment());
        }
        DaoFactory.getInstance().getPersonDao().save(personModel,
            ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
        iterator.remove();
      } catch (SaveException e) {
        ServiceRegistry.getLogProxy().error(this, "Could not save model", e);
        DialogUtils.showErrorDialog(errorMessages.saveFailTitle(),
            errorMessages.saveFailMessage(personModel.toString()), e);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

}
