package ch.vknws.vierkant.ui;

/**
 * Listeners for changes in validity.
 *
 * @author Benjamin Weber
 */
@FunctionalInterface
public interface ValidityChangeListener {

  /**
   * The validity has changed.
   * 
   * @param valid the new state
   */
  void validityChange(boolean valid);
}
