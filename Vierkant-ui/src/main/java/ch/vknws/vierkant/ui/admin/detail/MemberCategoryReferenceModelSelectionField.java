package ch.vknws.vierkant.ui.admin.detail;

import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.MemberGroupModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.ui.AbstractTableView;
import ch.vknws.vierkant.ui.ReferenceModelSelectionField;

/**
 * Reference selection field for MemberCategoryModel.
 *
 * @author Benjamin Weber
 */
public class MemberCategoryReferenceModelSelectionField extends ReferenceModelSelectionField<MemberGroupModel> {

  private static final long serialVersionUID = -3119236352449405304L;

  private PersonModel personModel;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param personModel the model to filter on
   */
  public MemberCategoryReferenceModelSelectionField(PersonModel personModel) {
    super(MemberCategoryReferenceTableView.class, DaoFactory.getInstance().getMemberCategoryDao());

    this.personModel = personModel;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected AbstractTableView<?> createTable() {
    return new MemberCategoryReferenceTableView(personModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void setValueLabel(MemberGroupModel value) {

    MemberGroupModel model = personModel.getRefMemberGroup();

    if (model == null) {
      valueLabel.setValue(null);
    } else {

      valueLabel.setValue(model.toString());
    }
  }
}
