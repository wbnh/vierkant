package ch.vknws.vierkant.ui.secretary;

/**
 * The texts for the secretary.
 * 
 * @author benjamin
 */
public interface SecretaryTexts {

  /**
   * @return the text.
   */
  String downloadMutationLetterDialogTitle();

  /**
   * @return the text.
   */
  String downloadMutationLetterDialogMessage();
}
