package ch.vknws.vierkant.ui.admin;

/**
 * Captions for the admin part.
 *
 * @author Benjamin Weber
 */
public interface AdminTexts {

  /**
   * @return the text
   */
  String userMessageTitle();

  /**
   * @return the text
   */
  String userMessage();

  /**
   * @return the text
   */
  String userMessageExpiration();

  /**
   * @return the text
   */
  String submitUserMessage();

  /**
   * @param start the start day of the window
   * @param end   the end day of the window
   * @return the text
   */
  String managementWindow(String start, String end);

  /**
   * @return the text
   */
  String managementWindowTitle();

  /**
   * @return the text
   */
  String managementWindowFrom();

  /**
   * @return the text
   */
  String managementWindowTo();
}
