package ch.vknws.vierkant.ui.secretary;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.AargauLicenseModel;
import ch.vknws.vierkant.type.caption.AargauLicensePersistableTexts;
import ch.vknws.vierkant.ui.AbstractDetailView;

/**
 * Details view of a person for the secretary.
 *
 * @author Benjamin Weber
 */
public class SecretaryAargauLicenseDetailsView extends AbstractDetailView<AargauLicenseModel> {
  private static final long serialVersionUID = -7589784662167949340L;

  private static final AargauLicensePersistableTexts licenseTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(AargauLicensePersistableTexts.class));

  private TextField licenseName;
  private TextField licenseNumber;

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param displayedModel the model to display
   */
  protected SecretaryAargauLicenseDetailsView(AargauLicenseModel displayedModel) {
    super(displayedModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getViewCaption() {
    return getDisplayedModel().toString();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FormLayout setUp() {

    FormLayout layout = new FormLayout();
    layout.setMargin(true);

    licenseName = new TextField();
    layout.addComponent(licenseName);
    licenseName.setCaption(licenseTexts.licenseName());
    licenseName.setNullRepresentation("");
    saveGroup.bind(licenseName, AargauLicenseModel.PropertyId.LICENSE_NAME);

    licenseNumber = new TextField();
    layout.addComponent(licenseNumber);
    licenseNumber.setCaption(licenseTexts.licenseNumber());
    licenseNumber.setNullRepresentation("");
    saveGroup.bind(licenseNumber, AargauLicenseModel.PropertyId.LICENSE_NUMBER);

    return layout;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void saveDisplayedModel() throws SaveException {
    DaoFactory.getInstance().getAargauLicenseDao().save(getDisplayedModel(),
        ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

}
