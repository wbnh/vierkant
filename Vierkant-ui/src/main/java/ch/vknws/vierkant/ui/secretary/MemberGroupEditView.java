package ch.vknws.vierkant.ui.secretary;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.BrowserWindowOpener;
import com.vaadin.server.FileResource;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.faa.v1.core.util.Pair;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.backend.function.MutationFunctions;
import ch.vknws.vierkant.backend.function.MutationUtil;
import ch.vknws.vierkant.backend.function.PersonShorthandGenerator;
import ch.vknws.vierkant.backend.reports.ReportUtils;
import ch.vknws.vierkant.data.RankContainer;
import ch.vknws.vierkant.rules.RuleRegistry;
import ch.vknws.vierkant.rules.module.secretary.MutationAction;
import ch.vknws.vierkant.type.MemberCategoryType;
import ch.vknws.vierkant.type.MemberGroupModel;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.RankModel;
import ch.vknws.vierkant.type.caption.MemberGroupPersistableTexts;
import ch.vknws.vierkant.type.caption.PersonPersistableTexts;
import ch.vknws.vierkant.type.impl.MemberGroupPersistable;
import ch.vknws.vierkant.type.impl.PersonPersistable;
import ch.vknws.vierkant.ui.AbstractView;
import ch.vknws.vierkant.ui.dialog.DialogTexts;
import ch.vknws.vierkant.ui.dialog.DialogUtils;
import ch.vknws.vierkant.ui.dialog.TwoButtonDialog;
import ch.vknws.vierkant.ui.field.converter.RankModelConverter;
import ch.vknws.vierkant.ui.search.PersonColumnSearchView;
import net.sf.jasperreports.engine.JRException;

/**
 * View for editing of member groups.
 *
 * @author Benjamin Weber
 */
public class MemberGroupEditView extends AbstractView implements CloseListener {
  private static final long serialVersionUID = -8936498798045864382L;
  private static final MemberGroupPersistableTexts memberGroupTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(MemberGroupPersistableTexts.class));
  private static final PersonPersistableTexts personModelTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(PersonPersistableTexts.class));
  private static final DialogTexts dialogTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(DialogTexts.class));
  private static final SecretaryTexts secretaryTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(SecretaryTexts.class));

  private PersonModel person;

  private ComboBox categoryBox;
  private PersonColumnSearchView searchView;

  private ComboBox rank;
  private CheckBox newsletter;
  private TextField shorthand;
  private BeanItem<PersonModel> personItem;

  private MutationPersister persister;

  private MemberCategoryType initialCategory;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param person    the model to display
   * @param persister the persister to use after modification
   */
  public MemberGroupEditView(PersonModel person, MutationPersister persister) {
    this.person = person;
    this.initialCategory = person.getMemberCategory();
    this.persister = persister;
    personItem = new BeanItem<>(person);
    setUp();
  }

  private void setUp() {

    VerticalLayout wrapper = new VerticalLayout();
    addComponent(wrapper);
    wrapper.setMargin(true);

    FormLayout layout = new FormLayout();
    wrapper.addComponent(layout);

    categoryBox = new ComboBox();
    layout.addComponent(categoryBox);
    categoryBox.setCaption(memberGroupTexts.category());
    categoryBox.setContainerDataSource(createCategoryContainer());
    categoryBox.setTextInputAllowed(false);
    categoryBox.setNullSelectionAllowed(false);
    categoryBox.setItemCaptionMode(ItemCaptionMode.ID_TOSTRING);
    categoryBox.setValue(person.getMemberCategory());
    categoryBox.addValueChangeListener(e -> executeRules());

    searchView = new PersonColumnSearchView(Arrays.asList(person.getRecordId()));
    layout.addComponent(searchView);
    searchView.setHeight(150, Unit.PIXELS);

    if (person.getRefMemberGroup() != null && person.getMemberCategory() != null) {
      categoryBox.setValue(person.getMemberCategory());

      List<PersonModel> groupMembers = DaoFactory.getInstance().getPersonDao().getMemberGroupMembers(person);

      TwinColSelect select = searchView.getSelect();
      select.setContainerDataSource(new BeanItemContainer<>(PersonModel.class, groupMembers));

      for (PersonModel personModel : groupMembers) {
        select.setItemCaption(personModel, personModel.toString());
      }

      for (PersonModel model : groupMembers) {
        select.select(model);
      }
    }

    rank = new ComboBox();
    layout.addComponent(rank);
    rank.setCaption(personModelTexts.refRank());
    rank.setNullSelectionItemId(null);
    rank.setTextInputAllowed(false);
    rank.setNullSelectionAllowed(true);
    rank.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    rank.setItemCaptionPropertyId(RankModel.PropertyId.RANK_NAME);
    rank.setContainerDataSource(new RankContainer());
    rank.setPropertyDataSource(personItem.getItemProperty(PersonModel.PropertyId.REF_RANK));
    rank.setConverter(new RankModelConverter());
    if (person.getRefRank() == null) {
      rank.setValue(null);
    } else {
      rank.setValue(person.getRefRank().getRecordId());
    }
    rank.setConverter(new RankModelConverter());
    rank.setVisible(false);

    newsletter = new CheckBox();
    layout.addComponent(newsletter);
    newsletter.setCaption(personModelTexts.vkNews());
    newsletter.setPropertyDataSource(personItem.getItemProperty(PersonModel.PropertyId.VK_NEWS));
    newsletter.setVisible(false);

    shorthand = new TextField();
    layout.addComponent(shorthand);
    shorthand.setCaption(personModelTexts.shorthand());
    shorthand.setPropertyDataSource(personItem.getItemProperty(PersonModel.PropertyId.SHORTHAND));
    shorthand.setNullRepresentation("");
    shorthand.addFocusListener(e -> PersonShorthandGenerator.maybeGenerateShorthand(person));
    shorthand.setVisible(false);
    shorthand.addValueChangeListener(this::emailOrganizationShorthandSync);

    executeRules();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();

    Set<MutationAction> mutationActions = null;
    if (categoryBox != null) {
      mutationActions = MutationUtil.MUTATION_ACTIONS
          .get(new Pair<>(person.getMemberCategory(), (MemberCategoryType) categoryBox.getValue()));
      MutationFunctions.performMutationActions(person, mutationActions);
    }

    applyPersonSearchViewVisibility();
    applyRankVisibility(mutationActions);
    applyRankEditability(mutationActions);
    applyNewsletterVisibility(mutationActions);
    applyShorthandVisibility(mutationActions);
  }

  private void applyRankVisibility(Set<MutationAction> mutationActions) {
    if (rank == null || mutationActions == null) { return; }

    boolean visible = mutationActions.contains(MutationAction.APPOINT_ASPIRANT)
        || mutationActions.contains(MutationAction.APPOINT_RANK) || mutationActions.contains(MutationAction.BREVET);
    rank.setVisible(visible);
    rank.setPropertyDataSource(personItem.getItemProperty(PersonModel.PropertyId.REF_RANK));
  }

  private void applyRankEditability(Set<MutationAction> mutationActions) {
    if (rank == null || mutationActions == null) { return; }

    boolean editable = mutationActions.contains(MutationAction.APPOINT_RANK);
    rank.setEnabled(editable);
  }

  private void applyNewsletterVisibility(Set<MutationAction> mutationActions) {
    if (newsletter == null || mutationActions == null) { return; }

    boolean visible = mutationActions.contains(MutationAction.SEND_NEWSLETTER)
        || mutationActions.contains(MutationAction.REVOKE_NEWSLETTER);
    newsletter.setVisible(visible);
    newsletter.setPropertyDataSource(personItem.getItemProperty(PersonModel.PropertyId.VK_NEWS));
  }

  private void applyShorthandVisibility(Set<MutationAction> mutationActions) {
    if (shorthand == null || mutationActions == null) { return; }

    boolean visible = mutationActions.contains(MutationAction.GENERATE_SHORTHAND)
        || mutationActions.contains(MutationAction.REVOKE_SHORTHAND);
    shorthand.setVisible(visible);
    shorthand.setPropertyDataSource(personItem.getItemProperty(PersonModel.PropertyId.SHORTHAND));
  }

  private void applyPersonSearchViewVisibility() {
    MemberCategoryType value = (MemberCategoryType) categoryBox.getValue();

    boolean visible = value == null ? false : value.isComposite();
    searchView.setVisible(visible);
  }

  private BeanItemContainer<MemberCategoryType> createCategoryContainer() {
    BeanItemContainer<MemberCategoryType> container = new BeanItemContainer<>(MemberCategoryType.class);

    if (person.getMemberCategory() == null) {
      container.addAll(EnumSet.allOf(MemberCategoryType.class));
    } else {
      container.addAll(RuleRegistry.loadFollowingCategories(person.getMemberCategory()));
      container.addItemAt(0, person.getMemberCategory());
    }

    return container;
  }

  /**
   * @return the searchView
   */
  public PersonColumnSearchView getSearchView() {
    return searchView;
  }

  /**
   * @return the categoryBox
   */
  public ComboBox getCategoryBox() {
    return categoryBox;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

  private void emailOrganizationShorthandSync(ValueChangeEvent event) {
    String shorthand = person.getShorthand();
    if (shorthand == null || shorthand.isEmpty()) {
      person.setEmailOrganization(null);
    } else {
      person.setEmailOrganization(shorthand.toLowerCase() + "@vk-nws.ch");
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void windowClose(CloseEvent e) {
    MemberCategoryType newCategory = (MemberCategoryType) getCategoryBox().getValue();
    Set<MutationAction> mutationActions = null;
    Set<PersonPersistable> modelsToSave = new HashSet<>();
    if (getCategoryBox() != null) {
      mutationActions = MutationUtil.MUTATION_ACTIONS.get(new Pair<>(person.getMemberCategory(), newCategory));
      person.setMemberCategory(newCategory);
    }

    @SuppressWarnings("unchecked")
    Set<PersonPersistable> selected = (Set<PersonPersistable>) getSearchView().getSelect().getValue();
    if (!getSearchView().isVisible()) {
      selected = new HashSet<>();
    }

    final boolean needsMemberGroup = newCategory == null ? false : newCategory.isComposite();
    MemberGroupModel memberGroup = null;
    if (needsMemberGroup) {
      memberGroup = new MemberGroupPersistable();

      try {
        memberGroup = DaoFactory.getInstance().getMemberCategoryDao().save(memberGroup,
            ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
      } catch (SaveException e1) {
        ServiceRegistry.getLogProxy().error(this, "Could not save member group", e1);
        DialogUtils.showErrorDialog(errorMessages.saveFailTitle(),
            errorMessages.saveFailMessage(Objects.toString(newCategory)), e1);
        return;
      }
    }

    for (PersonPersistable personModel : selected) {
      personModel.setRefMemberGroup(memberGroup);
      personModel.setMemberCategory(newCategory);
      personModel.setAuditComment(memberGroupTexts.changeAuditComment());
      modelsToSave.add(personModel);
    }

    person.setRefMemberGroup(memberGroup);
    person.setMemberCategory(newCategory);

    if (persister != null && mutationActions != null) persister.prepare(mutationActions, modelsToSave);

    handleMutationLetter();
  }

  private PersonModel getSecretary() {
    OrganizationFunctionModel secretary = DaoFactory.getInstance().getOrganizationFunctionDao().loadSecretary();
    List<PersonModel> secretaries = DaoFactory.getInstance().getPersonDao().loadByPrimaryFunction(secretary);

    if (secretaries == null || secretaries.isEmpty()) { return null; }

    // as there should only ever be one secretary we can just return the first
    // result
    return secretaries.get(0);
  }

  private void handleMutationLetter() {
    Path mutationLetter = createMutationLetter();
    if (mutationLetter != null) {
      openMutationLetter(mutationLetter);
    }
  }

  private Path createMutationLetter() {

    PersonModel secretary = getSecretary();
    MemberCategoryType formerCategory = initialCategory;
    MemberCategoryType currentCategory = person.getMemberCategory();
    LocalDate boardOfDirectorsMeeting = DaoFactory.getInstance().getMeetingDao().findLatestBoardMeeting().getStart()
        .toLocalDate();
    LocalDate generalAssemblyDate = DaoFactory.getInstance().getMeetingDao().findLatestGeneralAssembly().getStart()
        .toLocalDate();

    try {
      return ReportUtils.createMutationLetter(secretary, person, formerCategory, currentCategory,
          boardOfDirectorsMeeting, generalAssemblyDate);
    } catch (JRException | IOException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not create mutation letter", e);
      return null;
    }
  }

  private void openMutationLetter(Path letter) {
    BrowserWindowOpener opener = new BrowserWindowOpener(new FileResource(letter.toFile()));

    TwoButtonDialog dialog = new TwoButtonDialog(secretaryTexts.downloadMutationLetterDialogTitle(),
        secretaryTexts.downloadMutationLetterDialogMessage(), dialogTexts.download(), dialogTexts.close());
    dialog.getOkButton().removeClickListener(dialog.closeListener);
    opener.extend(dialog.getOkButton());

    dialog.open();
  }

}
