package ch.vknws.vierkant.ui.personal;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Set;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.AddressModel;
import ch.vknws.vierkant.type.MedicalInformationModel;
import ch.vknws.vierkant.type.PersonFunctionModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.TrainingCourseModel;
import ch.vknws.vierkant.type.caption.AddressPersistableTexts;
import ch.vknws.vierkant.type.caption.MedicalInformationPersistableTexts;
import ch.vknws.vierkant.type.caption.PersonPersistableTexts;
import ch.vknws.vierkant.type.impl.AddressPersistable;
import ch.vknws.vierkant.type.impl.MedicalInformationPersistable;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.NavigationIds;
import ch.vknws.vierkant.ui.field.converter.LocalDateStringConverter;
import ch.vknws.vierkant.ui.field.converter.RankNameConverter;

/**
 * View for a person to change their own data.
 *
 * @author Benjamin Weber
 */
public class PersonalPersonDataView extends AbstractDetailView<PersonModel> {

  private static final long serialVersionUID = 1414609240780094717L;

  private static final PersonPersistableTexts personTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(PersonPersistableTexts.class));
  private static final AddressPersistableTexts addressTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(AddressPersistableTexts.class));
  private static final MedicalInformationPersistableTexts medicalInformationTexts = ServiceRegistry
      .getTypedConfigurationProxy().get(new ConfigurationProperties<>(MedicalInformationPersistableTexts.class));

  private TextField title;
  private Label rank;
  private TextField givenName;
  private TextField familyName;
  private Label shorthand;
  private TextField addressLine1;
  private TextField addressLine2;
  private TextField addressZipCode;
  private TextField addressCity;
  private TextField addressCanton;
  private TextField addressCountry;
  private TextField telephone;
  private TextField mobile;
  private TextField emailPrivate;
  private Label emailOrganization;
  private Label birthdate;
  private TextArea allergyComment;
  private CheckBox vkNews;
  private Label memberGroupCategory;
  private Label functions;
  private Label vka;
  private Label completedCourses;
  private Label lastUpdateCheck;

  /**
   * The field group for the address model.
   */
  protected BeanFieldGroup<? extends DatabaseModel> addressGroup;

  /**
   * The field group for the medical information model.
   */
  protected BeanFieldGroup<? extends DatabaseModel> medicalInformationGroup;

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param displayedModel the model to display
   */
  public PersonalPersonDataView(PersonModel displayedModel) {
    super(displayedModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
    PersonModel load = DaoFactory.getInstance().getPersonDao().load(getLoggedInUser().getRecordId());
    setDisplayedModel(load);
    updateLabels();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setDisplayedModel(PersonModel displayedModel) {
    super.setDisplayedModel(displayedModel);

    if (addressGroup != null) {
      if (displayedModel.getRefHomeAddress() == null) {
        displayedModel.setRefHomeAddress(new AddressPersistable());
      }
      addressGroup.setItemDataSource(new BeanItem<>(displayedModel.getRefHomeAddress()));
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getViewCaption() {
    return getDisplayedModel().toString();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FormLayout setUp() {

    addressGroup = new BeanFieldGroup<>(AddressPersistable.class);
    if (getDisplayedModel().getRefHomeAddress() != null) {
      addressGroup.setItemDataSource(new BeanItem<AddressModel>(getDisplayedModel().getRefHomeAddress()));
    }
    addressGroup.setBuffered(false);

    medicalInformationGroup = new BeanFieldGroup<>(MedicalInformationPersistable.class);
    if (getDisplayedModel().getRefMedicalInformation() != null) {
      medicalInformationGroup
          .setItemDataSource(new BeanItem<MedicalInformationModel>(getDisplayedModel().getRefMedicalInformation()));
    }
    medicalInformationGroup.setBuffered(false);

    FormLayout layout = new FormLayout();
    layout.setSizeFull();
    layout.setMargin(true);

    title = new TextField();
    layout.addComponent(title);
    title.setCaption(personTexts.title());
    title.setNullRepresentation("");
    title.setWidth(200f, Unit.PIXELS);
    saveGroup.bind(title, PersonModel.PropertyId.TITLE);

    rank = new Label();
    layout.addComponent(rank);
    rank.setCaption(personTexts.refRank());
    rank.setWidth(200f, Unit.PIXELS);
    rank.setConverter(new RankNameConverter());
    rank.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.REF_RANK));

    givenName = new TextField();
    layout.addComponent(givenName);
    givenName.setCaption(personTexts.givenName());
    givenName.setNullRepresentation("");
    givenName.setWidth(200f, Unit.PIXELS);
    saveGroup.bind(givenName, PersonModel.PropertyId.GIVEN_NAME);

    familyName = new TextField();
    layout.addComponent(familyName);
    familyName.setCaption(personTexts.familyName());
    familyName.setNullRepresentation("");
    familyName.setWidth(200f, Unit.PIXELS);
    saveGroup.bind(familyName, PersonModel.PropertyId.FAMILY_NAME);

    shorthand = new Label();
    layout.addComponent(shorthand);
    shorthand.setCaption(personTexts.shorthand());
    shorthand.setWidth(200f, Unit.PIXELS);
    shorthand.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.SHORTHAND));

    addressLine1 = new TextField();
    layout.addComponent(addressLine1);
    addressLine1.setCaption(addressTexts.line1());
    addressLine1.setNullRepresentation("");
    addressLine1.setWidth(200f, Unit.PIXELS);
    addressGroup.bind(addressLine1, AddressModel.PropertyId.LINE1);

    addressLine2 = new TextField();
    layout.addComponent(addressLine2);
    addressLine2.setCaption(addressTexts.line2());
    addressLine2.setNullRepresentation("");
    addressLine2.setWidth(200f, Unit.PIXELS);
    addressGroup.bind(addressLine2, AddressModel.PropertyId.LINE2);

    HorizontalLayout zipCity = new HorizontalLayout();
    layout.addComponent(zipCity);
    zipCity.setMargin(false);
    zipCity.setSpacing(false);
    zipCity.setWidth(200f, Unit.PIXELS);
    zipCity.setCaption(addressTexts.zipCode() + "/" + addressTexts.city());

    addressZipCode = new TextField();
    zipCity.addComponent(addressZipCode);
    zipCity.setExpandRatio(addressZipCode, 0.4f);
    addressZipCode.setWidth("100%");
    addressZipCode.setNullRepresentation("");
    addressGroup.bind(addressZipCode, AddressModel.PropertyId.ZIP_CODE);

    addressCity = new TextField();
    zipCity.addComponent(addressCity);
    zipCity.setExpandRatio(addressCity, 0.6f);
    addressCity.setWidth("100%");
    addressCity.setNullRepresentation("");
    addressGroup.bind(addressCity, AddressModel.PropertyId.CITY);

    addressCanton = new TextField();
    layout.addComponent(addressCanton);
    addressCanton.setCaption(addressTexts.canton());
    addressCanton.setNullRepresentation("");
    addressCanton.setWidth(200f, Unit.PIXELS);
    addressGroup.bind(addressCanton, AddressModel.PropertyId.CANTON);

    addressCountry = new TextField();
    layout.addComponent(addressCountry);
    addressCountry.setCaption(addressTexts.country());
    addressCountry.setNullRepresentation("");
    addressCountry.setWidth(200f, Unit.PIXELS);
    addressGroup.bind(addressCountry, AddressModel.PropertyId.COUNTRY);

    telephone = new TextField();
    layout.addComponent(telephone);
    telephone.setCaption(personTexts.telephone());
    telephone.setNullRepresentation("");
    telephone.setWidth(200f, Unit.PIXELS);
    saveGroup.bind(telephone, PersonModel.PropertyId.TELEPHONE);

    mobile = new TextField();
    layout.addComponent(mobile);
    mobile.setCaption(personTexts.mobile());
    mobile.setNullRepresentation("");
    mobile.setWidth(200f, Unit.PIXELS);
    saveGroup.bind(mobile, PersonModel.PropertyId.MOBILE);

    emailPrivate = new TextField();
    layout.addComponent(emailPrivate);
    emailPrivate.setCaption(personTexts.emailPrivate());
    emailPrivate.setNullRepresentation("");
    emailPrivate.setWidth(200f, Unit.PIXELS);
    saveGroup.bind(emailPrivate, PersonModel.PropertyId.EMAIL_PRIVATE);

    emailOrganization = new Label();
    layout.addComponent(emailOrganization);
    emailOrganization.setCaption(personTexts.emailOrganization());
    emailOrganization.setWidth(200f, Unit.PIXELS);
    emailOrganization
        .setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.EMAIL_ORGANISATION));

    birthdate = new Label();
    layout.addComponent(birthdate);
    birthdate.setCaption(personTexts.birthdate());
    birthdate.setWidth(200f, Unit.PIXELS);
    birthdate.setConverter(new LocalDateStringConverter());
    birthdate.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.BIRTHDATE));

    allergyComment = new TextArea();
    layout.addComponent(allergyComment);
    allergyComment.setCaption(medicalInformationTexts.allergies());
    allergyComment.setNullRepresentation("");
    allergyComment.setWidth(200f, Unit.PIXELS);
    medicalInformationGroup.bind(allergyComment, MedicalInformationModel.PropertyId.ALLERGIES);

    vkNews = new CheckBox();
    layout.addComponent(vkNews);
    vkNews.setCaption(personTexts.vkNews());
    vkNews.setWidth(200f, Unit.PIXELS);
    saveGroup.bind(vkNews, PersonModel.PropertyId.VK_NEWS);

    memberGroupCategory = new Label();
    layout.addComponent(memberGroupCategory);
    memberGroupCategory.setCaption(personTexts.memberCategory());
    memberGroupCategory.setWidth(200f, Unit.PIXELS);
    memberGroupCategory.setValue(getMemberGroupCategory());

    functions = new Label();
    layout.addComponent(functions);
    functions.setCaption(personTexts.functions());
    functions.setWidth(200f, Unit.PIXELS);
    functions.setContentMode(ContentMode.HTML);
    functions.setValue(getFunctions());

    vka = new Label();
    layout.addComponent(vka);
    vka.setCaption(personTexts.vka());
    vka.setWidth(200f, Unit.PIXELS);
    vka.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.VKA));

    completedCourses = new Label();
    layout.addComponent(completedCourses);
    completedCourses.setCaption(personTexts.completedCourses());
    completedCourses.setWidth(200f, Unit.PIXELS);
    completedCourses.setValue(getCompletedCourses());

    lastUpdateCheck = new Label();
    layout.addComponent(lastUpdateCheck);
    lastUpdateCheck.setCaption(personTexts.lastUpdateCheck());
    lastUpdateCheck.setWidth(200f, Unit.PIXELS);
    lastUpdateCheck.setValue(getLastUpdateCheck());

    return layout;
  }

  private String getLastUpdateCheck() {
    if (getDisplayedModel().getLastUpdateCheck() == null) { return ""; }
    DateTimeFormatter dateFormat = VaadinSession.getCurrent().getAttribute(ApplicationRegistry.class).getDateFormat();
    return dateFormat.format(getDisplayedModel().getLastUpdateCheck());
  }

  private String getCompletedCourses() {
    if (getDisplayedModel().getRefCompletedCourses() == null
        || getDisplayedModel().getRefCompletedCourses().isEmpty()) { return ""; }

    StringBuilder builder = new StringBuilder();

    boolean first = true;
    for (TrainingCourseModel course : getDisplayedModel().getRefCompletedCourses()) {
      if (first) {
        first = false;
      } else {
        builder.append(", ");
      }
      builder.append(course.getTrainingCourseName());
    }

    return builder.toString();
  }

  private String getFunctions() {
    Set<PersonFunctionModel> functions = getDisplayedModel().getRefFunctions();
    if (functions == null || functions.isEmpty()) { return ""; }

    StringBuilder builder = new StringBuilder();

    builder.append("<b>");
    for (PersonFunctionModel function : functions) {
      if (getDisplayedModel().getRefPrimaryFunction().equals(function.getRefFunction())) {
        builder.append(function.getRefFunction().getFunctionName());
        break;
      }
    }
    builder.append("</b>");

    for (PersonFunctionModel function : functions) {
      if (getDisplayedModel().getRefPrimaryFunction().equals(function.getRefFunction())) {
        continue;
      }
      builder.append(", ");
      builder.append(function.getRefFunction().getFunctionName());
    }

    return builder.toString();
  }

  private String getMemberGroupCategory() {
    if (getDisplayedModel() == null || getDisplayedModel().getMemberCategory() == null) { return ""; }
    return getDisplayedModel().getMemberCategory().toString();
  }

  private void updateLabels() {
    lastUpdateCheck.setValue(getLastUpdateCheck());
    completedCourses.setValue(getCompletedCourses());
    functions.setValue(getFunctions());
    memberGroupCategory.setValue(getMemberGroupCategory());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void saveDisplayedModel() throws SaveException {
    getDisplayedModel().setLastUpdateCheck(LocalDate.now());
    DaoFactory.getInstance().getPersonDao().save(getDisplayedModel(),
        ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void onSave(ClickEvent event) {
    super.onSave(event);

    ApplicationRegistry.getInstance().getNavigator().navigateTo(NavigationIds.overview);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void onCancel(ClickEvent event) {
    super.onCancel(event);

    PersonModel person = DaoFactory.getInstance().getPersonDao().load(getDisplayedModel().getRecordId());
    person.setLastUpdateCheck(LocalDate.now());
    try {
      DaoFactory.getInstance().getPersonDao().save(person,
          ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
    } catch (SaveException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not save new last update check for person {0}", e,
          person.getRecordId());
      // no need to show the user, he did not want to save anyway
    }

    ApplicationRegistry.getInstance().getNavigator().navigateTo(NavigationIds.overview);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return NavigationIds.personal_person;
  }

}
