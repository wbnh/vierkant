package ch.vknws.vierkant.ui;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;

import ch.bwe.fac.v1.database.DatabaseModel;
import ch.vknws.vierkant.ui.dialog.OneButtonViewDialog;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * Field to list reference models.
 *
 * @author Benjamin Weber
 * @param <T> the type of model to display
 */
public class ReferenceModelListField<T extends DatabaseModel> extends HorizontalLayout {

  private static final long serialVersionUID = -4476036912518263832L;

  private Button openButton;
  private AbstractTableView<T> view;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param view the view to insert ourselves in
   */
  public ReferenceModelListField(AbstractTableView<T> view) {
    this.view = view;

    setUp();
  }

  private void setUp() {

    openButton = new Button();
    addComponent(openButton);
    openButton.setIcon(FontAwesome.PENCIL);
    openButton.addStyleName(VierkantTheme.BUTTON_ICON_ONLY);
    openButton.addClickListener(this::onOpen);
  }

  private void onOpen(ClickEvent event) {
    OneButtonViewDialog dialog = new OneButtonViewDialog();

    dialog.setView(view);

    dialog.open();
  }
}
