package ch.vknws.vierkant.ui.secretary;

import java.util.Optional;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.rules.rights.AuthorizationRules;
import ch.vknws.vierkant.type.AuthorizationKeyMode;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.RankModel;
import ch.vknws.vierkant.type.TrainingCourseModel;
import ch.vknws.vierkant.type.caption.PersonPersistableTexts;
import ch.vknws.vierkant.type.caption.RankPersistableTexts;
import ch.vknws.vierkant.type.caption.TrainingCoursePersistableTexts;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.field.converter.PrerequisiteStringConverter;

/**
 * Details view of a person for the secretary.
 *
 * @author Benjamin Weber
 */
public class SecretaryTrainingCourseDetailsView extends AbstractDetailView<TrainingCourseModel> {
  private static final long serialVersionUID = -7589784662167949340L;

  private static final TrainingCoursePersistableTexts trainingCourseTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(TrainingCoursePersistableTexts.class));

  private TextField trainingCourseName;
  private TextArea personPrerequisite;
  private TextArea personRankPrerequisite;

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param displayedModel the model to display
   */
  protected SecretaryTrainingCourseDetailsView(TrainingCourseModel displayedModel) {
    super(displayedModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getViewCaption() {
    return getDisplayedModel().toString();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FormLayout setUp() {

    FormLayout layout = new FormLayout();
    layout.setMargin(true);

    trainingCourseName = createTextField(trainingCourseTexts.name());
    layout.addComponent(trainingCourseName);
    saveGroup.bind(trainingCourseName, TrainingCourseModel.PropertyId.TRAINING_COURSE_NAME);

    personPrerequisite = createTextArea(trainingCourseTexts.personPrerequisite());
    layout.addComponent(personPrerequisite);
    personPrerequisite.setConverter(new PrerequisiteStringConverter(
        ServiceRegistry.getConfigurationProxy()
            .getConfiguration(new ConfigurationProperties<>(PersonPersistableTexts.class)),
        PersonModel.PropertyId.PROPERTY_TYPES.keySet()));
    personPrerequisite.addBlurListener(e -> {
      personPrerequisite.setConvertedValue(personPrerequisite.getConvertedValue());
    });
    saveGroup.bind(personPrerequisite, TrainingCourseModel.PropertyId.REF_PERSON_PREREQUISITE);

    personRankPrerequisite = createTextArea(trainingCourseTexts.rankPrerequisite());
    layout.addComponent(personRankPrerequisite);
    personRankPrerequisite
        .setConverter(new PrerequisiteStringConverter(
            ServiceRegistry.getConfigurationProxy()
                .getConfiguration(new ConfigurationProperties<>(RankPersistableTexts.class)),
            RankModel.PropertyId.PROPERTY_TYPES.keySet()));
    personRankPrerequisite.addBlurListener(e -> {
      personRankPrerequisite.setConvertedValue(personRankPrerequisite.getConvertedValue());
    });
    saveGroup.bind(personRankPrerequisite, TrainingCourseModel.PropertyId.REF_RANK_PREREQUISITE);

    return layout;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();

    applyDataEditability();
  }

  private void applyDataEditability() {
    Optional<AuthorizationKeyMode> authorization = AuthorizationRules.getInstance().hasAuthorization(
        SecretaryAuthorizationKeys.TRAINING_COURSE_ANY, getLoggedInUser(), AuthorizationKeyMode.WRITE);
    boolean editable = authorization.isPresent();

    if (trainingCourseName != null) trainingCourseName.setEnabled(editable);
    if (personPrerequisite != null) personPrerequisite.setEnabled(editable);
    if (personRankPrerequisite != null) personRankPrerequisite.setEnabled(editable);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void saveDisplayedModel() throws SaveException {
    DaoFactory.getInstance().getTrainingCourseDao().save(getDisplayedModel(),
        ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

}
