package ch.vknws.vierkant.ui.dialog;

import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;

/**
 * Dialog with one button.
 *
 * @author Benjamin Weber
 */
public class OneButtonDialog extends Dialog {

  private static final long serialVersionUID = 235138174869336896L;
  private Button closeButton;
  private Label label;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param title   the title to display
   * @param message the message to display
   */
  public OneButtonDialog(String title, String message) {
    this(title, message, null);
  }

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param title         the title to display
   * @param message       the message to display
   * @param buttonCaption the custom caption for the button
   */
  public OneButtonDialog(String title, String message, String buttonCaption) {
    super();

    setResizable(false);
    setClosable(false);

    setCaption(title);
    getLabel().setValue(message);
    getOkButton().setCaption(buttonCaption != null ? buttonCaption : dialogTexts.ok());
  }

  /**
   * @return the label
   */
  public Label getLabel() {
    return label;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected CssLayout setUp() {
    CssLayout root = new CssLayout();

    label = new Label();
    getLayout().addComponent(label);

    return root;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void setUpButtons() {

    closeButton = new Button();
    getButtonBar().addComponent(closeButton);
    closeButton.addClickListener(closeListener);
  }

  /**
   * @return the okButton
   */
  public Button getOkButton() {
    return closeButton;
  }

}
