package ch.vknws.vierkant.ui.login;

import java.util.Objects;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.UserError;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.vknws.vierkant.ApplicationTexts;
import ch.vknws.vierkant.backend.ldap.AuthenticationUtil;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.ui.AbstractView;
import ch.vknws.vierkant.ui.dialog.TwoButtonViewDialog;

/**
 * Dialog to reset the password.
 *
 * @author Benjamin Weber
 */
public class PasswordResetDialog extends TwoButtonViewDialog {
  private static final long serialVersionUID = 2634138353283684422L;

  private static final ApplicationTexts loginTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(ApplicationTexts.class));

  private PasswordResetView view;
  private String userId;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param person            the person who wants to reset
   * @param verifyOldPassword whether we need to verify the old password
   */
  public PasswordResetDialog(PersonModel person, boolean verifyOldPassword) {
    super();
    if (person != null) {
      userId = person.getShorthand();
    }

    setModal(true);
    center();

    view = new PasswordResetView(userId, verifyOldPassword);
    setView(view);

    getOkButton().removeClickListener(clickCloseListener);
    getOkButton().addClickListener(this::reset);
  }

  private void reset(ClickEvent event) {
    String userId = this.userId;
    if (view.userIdField != null) {
      userId = view.userIdField.getValue();
    }
    if (view.oldPassword != null) {
      view.oldPassword.setComponentError(null);
      String oldPassword = view.oldPassword.getValue();
      PersonModel authenticate = AuthenticationUtil.authenticate(userId, oldPassword);
      if (authenticate == null) {
        onVerificationFailed();
        return;
      }
    }

    view.passwordConfirm.setComponentError(null);
    String password = view.password.getValue();
    String passwordConfirm = view.passwordConfirm.getValue();
    if (!Objects.equals(password, passwordConfirm)) {
      onPasswordsNotMatch();
      return;
    }

    AuthenticationUtil.changeUserPassword(userId, passwordConfirm);
    close();
  }

  private void onVerificationFailed() {
    view.oldPassword.clear();

    view.oldPassword.setComponentError(new UserError(errorMessages.loginFailed()));
  }

  private void onPasswordsNotMatch() {
    view.password.clear();
    view.passwordConfirm.clear();

    view.passwordConfirm.setComponentError(new UserError(errorMessages.passwordsNotMatch()));
  }

  /**
   * The view to query the user for the new password.
   * 
   * @author benjamin
   */
  public static class PasswordResetView extends AbstractView {
    private static final long serialVersionUID = -5808348670959466388L;

    private Label userIdLabel;
    private TextField userIdField;
    private PasswordField oldPassword;
    private PasswordField password;
    private PasswordField passwordConfirm;

    /**
     * Constructor handling initialization of mandatory fields.
     * 
     * @param user              the user ID of the user who wants to reset
     * @param verifyOldPassword whether we need to verify the old password
     */
    public PasswordResetView(String user, boolean verifyOldPassword) {

      FormLayout layout = new FormLayout();
      addComponent(layout);
      layout.setMargin(true);

      if (user == null) {
        userIdField = new TextField();
        layout.addComponent(userIdField);
        userIdField.setCaption(loginTexts.usernameField());
        userIdField.setCaptionAsHtml(true);
      } else {
        userIdLabel = new Label();
        layout.addComponent(userIdLabel);
        userIdLabel.setValue(user);
        userIdLabel.setCaption(loginTexts.usernameField());
        userIdLabel.setCaptionAsHtml(true);
      }

      if (verifyOldPassword) {
        oldPassword = new PasswordField();
        layout.addComponent(oldPassword);
        oldPassword.setCaption(loginTexts.currentPasswordField());
        oldPassword.setCaptionAsHtml(true);
      }

      password = new PasswordField();
      layout.addComponent(password);
      password.setCaption(loginTexts.newPasswordField());
      password.setCaptionAsHtml(true);

      passwordConfirm = new PasswordField();
      layout.addComponent(passwordConfirm);
      passwordConfirm.setCaption(loginTexts.passwordConfirmField());
      passwordConfirm.setCaptionAsHtml(true);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void enter(ViewChangeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getViewId() {
      return null;
    }

  }
}
