package ch.vknws.vierkant.ui.field.converter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.vaadin.data.util.converter.Converter;

import ch.bwe.faa.v1.core.service.configuration.Configuration;
import ch.vknws.vierkant.type.impl.PrerequisitePersistable;

/**
 * Converter for prerequisites.
 *
 * @author Benjamin Weber
 */
public class PrerequisiteStringConverter implements Converter<String, PrerequisitePersistable> {
  private static final String GT_OPERATOR = ">";
  private static final String GE_OPERATOR = ">=";
  private static final String LT_OPERATOR = "<";
  private static final String LE_OPERATOR = "<=";
  private static final String EQ_OPERATOR = "=";
  private static final String NULL_OPERATOR = "E";
  private static final String NOT_NULL_OPERATOR = "NE";

  private static final List<String> OPERATORS = Arrays.asList(GT_OPERATOR, GE_OPERATOR, LT_OPERATOR, LE_OPERATOR,
      EQ_OPERATOR, NULL_OPERATOR, NOT_NULL_OPERATOR);

  private static final long serialVersionUID = 6388405011817539238L;

  private Configuration fieldNames;
  private Set<String> fieldKeys;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param fieldNames the names of the fields to use
   * @param fieldKeys  the keys of the fields to use
   */
  public PrerequisiteStringConverter(Configuration fieldNames, Set<String> fieldKeys) {
    this.fieldNames = fieldNames;
    this.fieldKeys = fieldKeys;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PrerequisitePersistable convertToModel(String value, Class<? extends PrerequisitePersistable> targetType,
      Locale locale) throws com.vaadin.data.util.converter.Converter.ConversionException {
    if (value == null || value.trim().isEmpty()) { return null; }
    PrerequisitePersistable model = new PrerequisitePersistable();

    String[] lines = value.split("\n");

    for (String line : lines) {
      if (line == null || line.trim().isEmpty()) {
        continue;
      }
      addLineToModel(line, model);
    }

    return model;
  }

  @SuppressWarnings("unchecked")
  private void addLineToModel(String line, PrerequisitePersistable model) {
    String[] raw = line.split("\\s");
    List<String> parts = new LinkedList<>();
    for (String part : raw) {
      if (part == null || part.trim().isEmpty()) {
        continue;
      }
      parts.add(part);
    }

    @SuppressWarnings("rawtypes")
    Map mapToUse = null;
    boolean integer = false;

    loop: for (String part : parts) {
      switch (part.toUpperCase()) {
      case GT_OPERATOR:
        integer = true;
        mapToUse = model.getGt();
        break loop;
      case GE_OPERATOR:
        integer = true;
        if (model.getGe() == null) {
          model.setGe(new HashMap<>());
        }
        mapToUse = model.getGe();
        break loop;
      case LT_OPERATOR:
        integer = true;
        if (model.getLt() == null) {
          model.setLt(new HashMap<>());
        }
        mapToUse = model.getLt();
        break loop;
      case LE_OPERATOR:
        integer = true;
        if (model.getLe() == null) {
          model.setLe(new HashMap<>());
        }
        mapToUse = model.getLe();
        break loop;
      case EQ_OPERATOR:
        integer = false;
        if (model.getEq() == null) {
          model.setEq(new HashMap<>());
        }
        mapToUse = model.getEq();
        break loop;
      case NULL_OPERATOR:
        integer = false;
        if (model.getIsNull() == null) {
          model.setIsNull(new HashMap<>());
        }
        mapToUse = model.getIsNull();
        break loop;
      case NOT_NULL_OPERATOR:
        integer = false;
        if (model.getIsNotNull() == null) {
          model.setIsNotNull(new HashMap<>());
        }
        mapToUse = model.getIsNotNull();
        break loop;
      default:
        // no keywort, ignore for now
        break;
      }
    }

    if (mapToUse == null) {
      // invalid line - ignore
      return;
    }

    StringBuilder fieldBuilder = new StringBuilder();
    StringBuilder valueBuilder = new StringBuilder();
    boolean fieldBuilt = false;
    for (String part : parts) {
      if (OPERATORS.contains(part)) {
        fieldBuilt = true;
        continue;
      }
      if (fieldBuilt) {
        if (valueBuilder.length() > 0) {
          valueBuilder.append(" ");
        }
        valueBuilder.append(part);
      } else {
        if (fieldBuilder.length() > 0) {
          fieldBuilder.append(" ");
        }

        fieldBuilder.append(part);
      }
    }

    String field = null;
    for (String key : fieldKeys) {
      if (fieldBuilder.toString().equals(key)) {
        field = key;
        break;
      }
      String caption = fieldNames.getStringValue(key);
      if (caption != null && caption.equals(fieldBuilder.toString())) {
        field = key;
        break;
      }
    }

    if (field == null) {
      // no such field - ignore
      return;
    }

    if (integer) {
      mapToUse.put(field, Integer.valueOf(valueBuilder.toString()));
    } else {
      mapToUse.put(field, valueBuilder.toString());
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String convertToPresentation(PrerequisitePersistable value, Class<? extends String> targetType, Locale locale)
      throws com.vaadin.data.util.converter.Converter.ConversionException {
    if (value == null) { return null; }

    StringBuilder builder = new StringBuilder();

    if (value.getGt() != null) {
      for (Entry<String, Integer> entry : value.getGt().entrySet()) {
        builder.append(fieldNames.getStringValue(entry.getKey()));
        builder.append(" ");
        builder.append(GT_OPERATOR);
        builder.append(" ");
        builder.append(entry.getValue());
        builder.append("\n");
      }
    }
    if (value.getGe() != null) {
      for (Entry<String, Integer> entry : value.getGe().entrySet()) {
        builder.append(fieldNames.getStringValue(entry.getKey()));
        builder.append(" ");
        builder.append(GE_OPERATOR);
        builder.append(" ");
        builder.append(entry.getValue());
        builder.append("\n");
      }
    }
    if (value.getLt() != null) {
      for (Entry<String, Integer> entry : value.getLt().entrySet()) {
        builder.append(fieldNames.getStringValue(entry.getKey()));
        builder.append(" ");
        builder.append(LT_OPERATOR);
        builder.append(" ");
        builder.append(entry.getValue());
        builder.append("\n");
      }
    }
    if (value.getLe() != null) {
      for (Entry<String, Integer> entry : value.getLe().entrySet()) {
        builder.append(fieldNames.getStringValue(entry.getKey()));
        builder.append(" ");
        builder.append(LE_OPERATOR);
        builder.append(" ");
        builder.append(entry.getValue());
        builder.append("\n");
      }
    }
    if (value.getEq() != null) {
      for (Entry<String, String> entry : value.getEq().entrySet()) {
        builder.append(fieldNames.getStringValue(entry.getKey()));
        builder.append(" ");
        builder.append(EQ_OPERATOR);
        builder.append(" ");
        builder.append(entry.getValue());
        builder.append("\n");
      }
    }
    if (value.getIsNull() != null) {
      for (Entry<String, String> entry : value.getIsNull().entrySet()) {
        builder.append(fieldNames.getStringValue(entry.getKey()));
        builder.append(" ");
        builder.append(NULL_OPERATOR);
        builder.append(" ");
        builder.append(entry.getValue());
        builder.append("\n");
      }
    }
    if (value.getIsNotNull() != null) {
      for (Entry<String, String> entry : value.getIsNotNull().entrySet()) {
        builder.append(fieldNames.getStringValue(entry.getKey()));
        builder.append(" ");
        builder.append(NOT_NULL_OPERATOR);
        builder.append(" ");
        builder.append(entry.getValue());
        builder.append("\n");
      }
    }

    return builder.toString();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Class<PrerequisitePersistable> getModelType() {
    return PrerequisitePersistable.class;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Class<String> getPresentationType() {
    return String.class;
  }

}
