package ch.vknws.vierkant.ui.search;

import java.util.LinkedList;
import java.util.List;

import ch.bwe.fac.v1.type.restriction.AbstractJunctionRestriction;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.ConjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.DisjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.InRestriction;
import ch.bwe.fac.v1.type.restriction.LikeRestriction;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.RankModel;
import ch.vknws.vierkant.type.impl.PersonPersistable;

/**
 * View to search for person models.
 *
 * @author Benjamin Weber
 */
public class PersonColumnSearchView extends AbstractColumnSearchView<PersonModel> {

  private static final long serialVersionUID = -6921444898893831089L;

  private List<Integer> disallowedIds;

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public PersonColumnSearchView() {
    this(new LinkedList<>());
  }

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param disallowedIds The list of IDs to ignore
   */
  public PersonColumnSearchView(List<Integer> disallowedIds) {
    super(PersonPersistable.class, DaoFactory.getInstance().getPersonDao());
    this.disallowedIds = disallowedIds;
  }

  /**
   * @param disallowedIds the disallowedIds to set
   */
  public void setDisallowedIds(List<Integer> disallowedIds) {
    if (disallowedIds == null) {
      disallowedIds = new LinkedList<>();
    }
    this.disallowedIds = disallowedIds;
  }

  /**
   * @return the disallowed ids
   */
  public List<Integer> getDisallowedIds() {
    return disallowedIds;
  }

  /**
   * Creates the filter.
   */
  @Override
  protected AbstractRestriction createRestriction() {
    String searchString = getSearchField().getValue();
    if (searchString == null) { return null; }

    String[] split = searchString.split(" ");
    List<String> terms = new LinkedList<>();

    for (String term : split) {
      terms.add("%" + term + "%");
    }

    List<AbstractRestriction> restrictions = new LinkedList<>();
    restrictions
        .add(LikeRestriction.create(PersonModel.PropertyId.REF_RANK + "." + RankModel.PropertyId.RANK_NAME, terms));
    restrictions
        .add(LikeRestriction.create(PersonModel.PropertyId.REF_RANK + "." + RankModel.PropertyId.ABBREVIATION, terms));
    restrictions.add(LikeRestriction.create(PersonModel.PropertyId.GIVEN_NAME, terms));
    restrictions.add(LikeRestriction.create(PersonModel.PropertyId.FAMILY_NAME, terms));

    ConjunctionRestriction root = new ConjunctionRestriction();
    root.setLeft(new InRestriction(PersonModel.PropertyId.RECORD_ID, disallowedIds, true));

    AbstractJunctionRestriction currentJunction = root;
    for (int i = 0; i < restrictions.size(); i++) {
      AbstractRestriction currentRestriction = restrictions.get(i);

      if (i >= restrictions.size() - 1) {
        currentJunction.setRight(currentRestriction);
        break;
      }
      DisjunctionRestriction newJunction = new DisjunctionRestriction();
      newJunction.setLeft(currentRestriction);
      currentJunction.setRight(newJunction);
      currentJunction = newJunction;
    }
    return root;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getItemCaption(PersonModel model) {
    return model.toString();
  }

}
