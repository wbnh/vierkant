package ch.vknws.vierkant.ui.admin;

import com.vaadin.data.Container.Indexed;

import ch.bwe.fac.v1.type.DeleteException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.PersonContainer;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.impl.PersonPersistable;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.AbstractTableView;
import ch.vknws.vierkant.ui.NavigationIds;
import ch.vknws.vierkant.ui.admin.detail.AdminPersonDetailView;

/**
 * View for managing material.
 *
 * @author Benjamin Weber
 *
 */
public class PersonModelTableView extends AbstractTableView<PersonModel> {

	private static final long serialVersionUID = 6054141657716877803L;

	private static final Object[] COLUMNS = { PersonModel.PropertyId.GIVEN_NAME, PersonModel.PropertyId.FAMILY_NAME,
					PersonModel.PropertyId.REF_RANK, PersonModel.PropertyId.LAST_UPDATE_CHECK };

	/**
	 * Constructor handling initialization of mandatory fields.
	 */
	public PersonModelTableView() {
		super(COLUMNS);

		personAuthorizations.add(SecretaryAuthorizationKeys.ADMIN);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getViewId() {
		return NavigationIds.admin_person;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected AbstractDetailView<PersonModel> createDetailView(PersonModel selectedModel) {
		if (selectedModel == null) {
			selectedModel = new PersonPersistable();
		}

		return new AdminPersonDetailView(selectedModel);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void deleteModelFromDatabase(Integer recordId) throws DeleteException {
		DaoFactory.getInstance().getPersonDao().delete(recordId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected PersonModel loadModelFromDatabase(Integer recordId) {
		return DaoFactory.getInstance().getPersonDao().load(recordId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Indexed createContainer() {
		return new PersonContainer();
	}

}
