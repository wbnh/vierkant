package ch.vknws.vierkant.ui.training;

import java.util.HashSet;
import java.util.Set;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.FormLayout;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.TrainingCourseInstanceModel;
import ch.vknws.vierkant.type.TrainingCourseModel;
import ch.vknws.vierkant.type.caption.TrainingCourseInstancePersistableTexts;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.search.PersonColumnSearchView;
import ch.vknws.vierkant.ui.search.TrainingCourseColumnSearchView;

/**
 * Details for training course instance.
 *
 * @author Benjamin Weber
 */
public class TrainingCourseInstanceDetailsView extends AbstractDetailView<TrainingCourseInstanceModel> {
  private static final long serialVersionUID = 3835413793788112705L;
  private static final TrainingCourseInstancePersistableTexts instanceTexts = ServiceRegistry
      .getTypedConfigurationProxy().get(new ConfigurationProperties<>(TrainingCourseInstancePersistableTexts.class));

  private TrainingCourseColumnSearchView template;
  private PersonColumnSearchView responsible;
  private PersonColumnSearchView teachers;
  private PersonColumnSearchView participants;

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param displayedModel the model to display
   */
  public TrainingCourseInstanceDetailsView(TrainingCourseInstanceModel displayedModel) {
    super(displayedModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getViewCaption() {
    return getDisplayedModel().toString();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FormLayout setUp() {
    FormLayout layout = new FormLayout();
    layout.setMargin(true);

    template = new TrainingCourseColumnSearchView();
    layout.addComponent(template);
    template.setCaption(instanceTexts.template() + "<br />max 1");
    template.setCaptionAsHtml(true);
    template.getSelect().setMultiSelect(false);

    responsible = new PersonColumnSearchView();
    layout.addComponent(responsible);
    responsible.setCaption(instanceTexts.responsible() + "<br />max 1");
    responsible.setCaptionAsHtml(true);
    responsible.getSelect().setMultiSelect(false);

    teachers = new PersonColumnSearchView();
    layout.addComponent(teachers);
    teachers.setCaption(instanceTexts.teachers());

    participants = new PersonColumnSearchView();
    layout.addComponent(participants);
    participants.setCaption(instanceTexts.participants());

    updateDisallowedParticipants();

    syncTemplate();
    syncResponsible();
    syncTeachers();
    syncParticipants();

    template.getSelect().addValueChangeListener(this::handleTemplateSelection);
    responsible.getSelect().addValueChangeListener(this::handleResponsibleSelection);
    teachers.getSelect().addValueChangeListener(this::handleTeachersSelection);
    participants.getSelect().addValueChangeListener(this::handleParticipantsSelection);

    return layout;
  }

  private void syncTemplate() {
    if (template == null) { return; }
    TrainingCourseModel templateModel = getDisplayedModel().getRefTemplate();
    if (templateModel != null) {
      template.getSelect().setContainerDataSource(new BeanItemContainer<>(TrainingCourseModel.class));
      template.getSelect().addItem(templateModel);
      template.getSelect().setItemCaption(templateModel, templateModel.toString());
      template.getSelect().select(templateModel);
    }
  }

  private void syncResponsible() {
    if (responsible == null) { return; }
    PersonModel responsibleModel = getDisplayedModel().getRefResponsible();
    if (responsibleModel != null) {
      responsible.getSelect().setContainerDataSource(new BeanItemContainer<>(PersonModel.class));
      responsible.getSelect().addItem(responsibleModel);
      responsible.getSelect().setItemCaption(responsibleModel, responsibleModel.toString());
      responsible.getSelect().select(responsibleModel);
    }
  }

  private void syncTeachers() {
    if (teachers == null) { return; }
    Set<PersonModel> teacherSet = getDisplayedModel().getRefTeachers();
    if (teacherSet != null) {
      teachers.getSelect().setContainerDataSource(new BeanItemContainer<>(PersonModel.class));

      for (PersonModel teacher : teacherSet) {
        teachers.getSelect().addItem(teacher);
        teachers.getSelect().setItemCaption(teacher, teacher.toString());
        teachers.getSelect().select(teacher);
      }
    }
  }

  private void syncParticipants() {
    if (participants == null) { return; }
    Set<PersonModel> participantSet = getDisplayedModel().getRefParticipants();
    if (participants != null) {
      participants.getSelect().setContainerDataSource(new BeanItemContainer<>(PersonModel.class, participantSet));

      if (participantSet != null) {
        for (PersonModel participant : participantSet) {
          for (Object id : participants.getSelect().getItemIds()) {
            PersonModel selectModel = (PersonModel) id;

            if (selectModel.getRecordId().equals(participant.getRecordId())) {
              participants.getSelect().setItemCaption(selectModel, selectModel.toString());
              participants.getSelect().select(selectModel);
              break;
            }
          }
        }
      }
    }
  }

  private void updateDisallowedParticipants() {
    if (participants == null) { return; }

    participants.getDisallowedIds().clear();

    if (getDisplayedModel().getRefResponsible() != null)
      participants.getDisallowedIds().add(getDisplayedModel().getRefResponsible().getRecordId());

    if (getDisplayedModel().getRefTeachers() != null) for (PersonModel teacher : getDisplayedModel().getRefTeachers()) {
      if (teacher != null) participants.getDisallowedIds().add(teacher.getRecordId());
    }
  }

  private void handleTemplateSelection(ValueChangeEvent event) {
    TrainingCourseModel value = (TrainingCourseModel) event.getProperty().getValue();
    getDisplayedModel().setRefTemplate(value);
  }

  private void handleResponsibleSelection(ValueChangeEvent event) {
    PersonModel value = (PersonModel) event.getProperty().getValue();
    getDisplayedModel().setRefResponsible(value);

    updateDisallowedParticipants();

    syncParticipants();
  }

  private void handleTeachersSelection(ValueChangeEvent event) {
    @SuppressWarnings("unchecked")
    Set<PersonModel> value = (Set<PersonModel>) event.getProperty().getValue();
    for (PersonModel personModel : value) {
      Set<PersonModel> participantSet = getDisplayedModel().getRefParticipants();
      if (participantSet != null && participantSet.contains(personModel)) {
        participantSet.remove(personModel);
        getDisplayedModel().setRefParticipants(participantSet);
      }
    }
    getDisplayedModel().setRefTeachers(new HashSet<>(value));

    updateDisallowedParticipants();

    syncParticipants();
  }

  private void handleParticipantsSelection(ValueChangeEvent event) {
    @SuppressWarnings("unchecked")
    Set<PersonModel> value = (Set<PersonModel>) event.getProperty().getValue();
    Set<PersonModel> values = new HashSet<>();
    for (PersonModel personModel : value) {
      if (!getDisplayedModel().getRefTeachers().contains(personModel)) {
        values.add(personModel);
      }
    }
    getDisplayedModel().setRefParticipants(values);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void saveDisplayedModel() throws SaveException {
    DaoFactory.getInstance().getTrainingCourseInstanceDao().save(getDisplayedModel(),
        ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

}
