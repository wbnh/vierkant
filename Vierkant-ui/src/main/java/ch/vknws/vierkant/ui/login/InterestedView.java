package ch.vknws.vierkant.ui.login;

import java.util.LinkedList;
import java.util.List;

import com.google.common.base.Strings;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.UserError;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.backend.function.EmailFunctions;
import ch.vknws.vierkant.rules.RuleRegistry;
import ch.vknws.vierkant.rules.ValidationMessage;
import ch.vknws.vierkant.rules.ValidationResult;
import ch.vknws.vierkant.type.AddressModel;
import ch.vknws.vierkant.type.MedicalInformationModel;
import ch.vknws.vierkant.type.MemberCategoryType;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.SexType;
import ch.vknws.vierkant.type.VKA;
import ch.vknws.vierkant.type.caption.AddressPersistableTexts;
import ch.vknws.vierkant.type.caption.MedicalInformationPersistableTexts;
import ch.vknws.vierkant.type.caption.PersonPersistableTexts;
import ch.vknws.vierkant.type.impl.AddressPersistable;
import ch.vknws.vierkant.type.impl.MedicalInformationPersistable;
import ch.vknws.vierkant.type.impl.PersonPersistable;
import ch.vknws.vierkant.ui.AbstractView;
import ch.vknws.vierkant.ui.admin.detail.DetailViewTexts;
import ch.vknws.vierkant.ui.dialog.DialogUtils;
import ch.vknws.vierkant.ui.dialog.OneButtonDialog;
import ch.vknws.vierkant.ui.field.converter.LocalDateConverter;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * View for interested people to register.
 *
 * @author Benjamin Weber
 */
public class InterestedView extends AbstractView {

  /**
   * 
   */
  private static final String AUDIT_COMMENT = "ist interessiert.";

  private static final long serialVersionUID = -6093180673244635707L;

  private static final PersonPersistableTexts personTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(PersonPersistableTexts.class));
  private static final AddressPersistableTexts addressTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(AddressPersistableTexts.class));
  private static final MedicalInformationPersistableTexts medicalInformationTexts = ServiceRegistry
      .getTypedConfigurationProxy().get(new ConfigurationProperties<>(MedicalInformationPersistableTexts.class));
  private static final DetailViewTexts detailTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(DetailViewTexts.class));

  private List<ClickListener> cancelListeners = new LinkedList<>();

  private ComboBox sex;
  private TextField title;
  private TextField givenName;
  private TextField familyName;
  private TextField addressLine1;
  private TextField addressLine2;
  private TextField addressZipCode;
  private TextField addressCity;
  private TextField addressCanton;
  private TextField addressCountry;
  private TextField telephone;
  private TextField mobile;
  private TextField emailPrivate;
  private DateField birthdate;
  private TextArea allergyComment;

  private Button submitButton;
  private Button cancelButton;

  private FieldGroup personGroup;
  private FieldGroup addressGroup;
  private FieldGroup medicalGroup;

  private PersonPersistable person;

  private MedicalInformationPersistable medicalInformation;

  private AddressPersistable address;

  private boolean initialized = false;

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public InterestedView() {
    setUp();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();

    if (!initialized) { return; }

    submitButton.setEnabled(true);

    validateFieldGroups();
  }

  private void validateFieldGroups() {
    personGroup.getFields().forEach(f -> ((AbstractField<?>) f).setComponentError(null));
    addressGroup.getFields().forEach(f -> ((AbstractField<?>) f).setComponentError(null));
    medicalGroup.getFields().forEach(f -> ((AbstractField<?>) f).setComponentError(null));

    ValidationResult result = RuleRegistry.validateModel(person);
    if (!result.isSuccessful()) {
      boolean relevant = false;
      for (ValidationMessage message : result.getMessages()) {
        AbstractField<?> field = (AbstractField<?>) personGroup.getField(message.getField());

        if (field == null) {
          field = (AbstractField<?>) addressGroup.getField(message.getField());
        }
        if (field == null) {
          field = (AbstractField<?>) medicalGroup.getField(message.getField());
        }

        if (field != null && field.isVisible() && field.isEnabled()) {
          field.setComponentError(new UserError(message.toString()));
          relevant = true;
        }
      }
      if (relevant) {
        submitButton.setEnabled(false);
      }
    }
  }

  /**
   * @param listener the listener to be invoked upon cancelling
   */
  public void addCancelListener(ClickListener listener) {
    cancelListeners.add(listener);
  }

  private void setUp() {

    person = new PersonPersistable();
    person.setVka(VKA.NWS);
    person.setMemberCategory(MemberCategoryType.INTERESTED);
    person.setCreator(0);
    person.setModifier(0);
    person.setDeletedFlag(false);
    person.setAuditComment(AUDIT_COMMENT);

    medicalInformation = new MedicalInformationPersistable();
    person.setRefMedicalInformation(medicalInformation);
    medicalInformation.setCreator(0);
    medicalInformation.setModifier(0);
    medicalInformation.setDeletedFlag(false);
    medicalInformation.setAuditComment(AUDIT_COMMENT);

    address = new AddressPersistable();
    person.setRefHomeAddress(address);
    address.setCreator(0);
    address.setModifier(0);
    address.setDeletedFlag(false);
    address.setAuditComment(AUDIT_COMMENT);

    personGroup = new FieldGroup(new BeanItem<>(person));
    personGroup.setBuffered(false);

    addressGroup = new FieldGroup(new BeanItem<>(address));
    addressGroup.setBuffered(false);

    medicalGroup = new FieldGroup(new BeanItem<>(medicalInformation));
    medicalGroup.setBuffered(false);

    HorizontalLayout wrapper = new HorizontalLayout();
    addComponent(wrapper);

    FormLayout left = new FormLayout();
    wrapper.addComponent(left);
    left.setSizeFull();
    left.setMargin(true);

    sex = new ComboBox();
    left.addComponent(sex);
    sex.setCaption(personTexts.sex());
    sex.setTextInputAllowed(false);
    sex.setNullSelectionAllowed(false);
    sex.setRequired(true);
    sex.setWidth(250f, Unit.PIXELS);
    sex.setItemCaptionMode(ItemCaptionMode.ID_TOSTRING);
    sex.setContainerDataSource(createSexContainer());
    sex.addValueChangeListener(ruleExecutionListener);
    sex.addValueChangeListener(e -> {
      if (title != null && title.getValue() == null)
        title.setValue(((SexType) e.getProperty().getValue()).getDefaultTitle());
    });
    personGroup.bind(sex, PersonModel.PropertyId.SEX);

    title = new TextField();
    left.addComponent(title);
    title.setCaption(personTexts.title());
    title.setNullRepresentation("");
    title.setRequired(true);
    title.setWidth(250f, Unit.PIXELS);
    title.addValueChangeListener(ruleExecutionListener);
    personGroup.bind(title, PersonModel.PropertyId.TITLE);

    givenName = new TextField();
    left.addComponent(givenName);
    givenName.setCaption(personTexts.givenName());
    givenName.setNullRepresentation("");
    givenName.setRequired(true);
    givenName.setWidth(250f, Unit.PIXELS);
    givenName.addValueChangeListener(ruleExecutionListener);
    personGroup.bind(givenName, PersonModel.PropertyId.GIVEN_NAME);

    familyName = new TextField();
    left.addComponent(familyName);
    familyName.setCaption(personTexts.familyName());
    familyName.setNullRepresentation("");
    familyName.setRequired(true);
    familyName.setWidth(250f, Unit.PIXELS);
    familyName.addValueChangeListener(ruleExecutionListener);
    personGroup.bind(familyName, PersonModel.PropertyId.FAMILY_NAME);

    addressLine1 = new TextField();
    left.addComponent(addressLine1);
    addressLine1.setCaption(addressTexts.line1());
    addressLine1.setNullRepresentation("");
    addressLine1.setRequired(true);
    addressLine1.setWidth(250f, Unit.PIXELS);
    addressLine1.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(addressLine1, AddressModel.PropertyId.LINE1);

    addressLine2 = new TextField();
    left.addComponent(addressLine2);
    addressLine2.setCaption(addressTexts.line2());
    addressLine2.setNullRepresentation("");
    addressLine2.setWidth(250f, Unit.PIXELS);
    addressLine2.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(addressLine2, AddressModel.PropertyId.LINE2);

    HorizontalLayout zipCity = new HorizontalLayout();
    left.addComponent(zipCity);
    zipCity.setMargin(false);
    zipCity.setSpacing(false);
    zipCity.setCaption(addressTexts.zipCode() + "/" + addressTexts.city());
    zipCity.setWidth(250f, Unit.PIXELS);

    addressZipCode = new TextField();
    zipCity.addComponent(addressZipCode);
    zipCity.setExpandRatio(addressZipCode, 0.4f);
    addressZipCode.setWidth("100%");
    addressZipCode.setNullRepresentation("");
    addressZipCode.setRequired(true);
    addressZipCode.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(addressZipCode, AddressModel.PropertyId.ZIP_CODE);

    addressCity = new TextField();
    zipCity.addComponent(addressCity);
    zipCity.setExpandRatio(addressCity, 0.6f);
    addressCity.setWidth("100%");
    addressCity.setNullRepresentation("");
    addressCity.setRequired(true);
    addressCity.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(addressCity, AddressModel.PropertyId.CITY);

    addressCanton = new TextField();
    left.addComponent(addressCanton);
    addressCanton.setCaption(addressTexts.canton());
    addressCanton.setNullRepresentation("");
    addressCanton.setWidth(250f, Unit.PIXELS);
    addressCanton.setRequired(true);
    addressCanton.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(addressCanton, AddressModel.PropertyId.CANTON);

    addressCountry = new TextField();
    left.addComponent(addressCountry);
    addressCountry.setCaption(addressTexts.country());
    addressCountry.setNullRepresentation("");
    addressCountry.setWidth(250f, Unit.PIXELS);
    addressCountry.setRequired(true);
    addressCountry.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(addressCountry, AddressModel.PropertyId.COUNTRY);

    FormLayout right = new FormLayout();
    wrapper.addComponent(right);
    right.setSizeFull();
    right.setMargin(true);

    telephone = new TextField();
    right.addComponent(telephone);
    telephone.setCaption(personTexts.telephone());
    telephone.setNullRepresentation("");
    telephone.setWidth(250f, Unit.PIXELS);
    telephone.addValueChangeListener(ruleExecutionListener);
    personGroup.bind(telephone, PersonModel.PropertyId.TELEPHONE);

    mobile = new TextField();
    right.addComponent(mobile);
    mobile.setCaption(personTexts.mobile());
    mobile.setNullRepresentation("");
    mobile.setWidth(250f, Unit.PIXELS);
    mobile.addValueChangeListener(ruleExecutionListener);
    personGroup.bind(mobile, PersonModel.PropertyId.MOBILE);

    emailPrivate = new TextField();
    right.addComponent(emailPrivate);
    emailPrivate.setCaption(personTexts.emailPrivate());
    emailPrivate.setNullRepresentation("");
    emailPrivate.setWidth(250f, Unit.PIXELS);
    emailPrivate.setRequired(true);
    emailPrivate.addValueChangeListener(ruleExecutionListener);
    personGroup.bind(emailPrivate, PersonModel.PropertyId.EMAIL_PRIVATE);

    birthdate = new DateField();
    right.addComponent(birthdate);
    birthdate.setCaption(personTexts.birthdate());
    birthdate.setRequired(true);
    birthdate.setConverter(new LocalDateConverter());
    birthdate.setWidth(250f, Unit.PIXELS);
    birthdate.addValueChangeListener(ruleExecutionListener);
    personGroup.bind(birthdate, PersonModel.PropertyId.BIRTHDATE);

    allergyComment = new TextArea();
    right.addComponent(allergyComment);
    allergyComment.setCaption(medicalInformationTexts.allergies());
    allergyComment.setNullRepresentation("");
    allergyComment.setWidth(250f, Unit.PIXELS);
    allergyComment.addValueChangeListener(ruleExecutionListener);
    medicalGroup.bind(allergyComment, MedicalInformationModel.PropertyId.ALLERGIES);

    HorizontalLayout buttonBar = new HorizontalLayout();
    right.addComponent(buttonBar);
    buttonBar.setWidth(250f, Unit.PIXELS);
    buttonBar.setMargin(false);
    buttonBar.setSpacing(true);

    submitButton = new Button();
    buttonBar.addComponent(submitButton);
    submitButton.setCaption(detailTexts.save());
    submitButton.addClickListener(this::onSave);
    submitButton.addStyleName(VierkantTheme.BUTTON_PRIMARY);

    cancelButton = new Button();
    buttonBar.addComponent(cancelButton);
    buttonBar.setComponentAlignment(cancelButton, Alignment.BOTTOM_RIGHT);
    cancelButton.setCaption(detailTexts.cancel());
    cancelButton.addClickListener(this::onCancel);
    cancelButton.addStyleName(VierkantTheme.BUTTON_LINK);

    initialized = true;
    executeRules();
  }

  private BeanItemContainer<SexType> createSexContainer() {
    BeanItemContainer<SexType> container = new BeanItemContainer<>(SexType.class);

    container.addBean(SexType.MALE);
    container.addBean(SexType.FEMALE);
    // only humans

    return container;
  }

  private void onSave(ClickEvent event) {

    try {
      PersonModel saved = DaoFactory.getInstance().getPersonDao().save(person, 0);

      OneButtonDialog dialog = new OneButtonDialog(successMessages.interestedRegistrationTitle(),
          successMessages.interestedRegistrationMessage(saved.getRecordId()));

      dialog.getOkButton().addClickListener(e -> {
        for (ClickListener listener : cancelListeners) {
          listener.buttonClick(event);
        }
      });

      dialog.open();
      if (!Strings.isNullOrEmpty(saved.getEmailPrivate())) {
        Thread mailThread = new Thread(() -> EmailFunctions.sendInterestedMail(saved.getGivenName(),
            saved.getRecordId(), saved.getEmailPrivate()));
        mailThread.start();
      }
    } catch (SaveException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not save interested person", e);

      DialogUtils.showErrorDialog(errorMessages.saveFailTitle(), errorMessages.saveFailMessage(person.toString()), e);
    }
  }

  private void onCancel(ClickEvent event) {
    for (ClickListener listener : cancelListeners) {
      listener.buttonClick(event);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
    if (sex != null) {
      sex.focus();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

}
