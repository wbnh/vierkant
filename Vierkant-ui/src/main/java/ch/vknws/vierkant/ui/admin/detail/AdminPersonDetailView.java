package ch.vknws.vierkant.ui.admin.detail;

import java.util.EnumSet;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.backend.function.PersonShorthandGenerator;
import ch.vknws.vierkant.data.RankContainer;
import ch.vknws.vierkant.type.AddressModel;
import ch.vknws.vierkant.type.ContactPersonModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.RankModel;
import ch.vknws.vierkant.type.VKA;
import ch.vknws.vierkant.type.caption.PersonPersistableTexts;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.NavigationIds;
import ch.vknws.vierkant.ui.ReferenceModelListField;
import ch.vknws.vierkant.ui.ReferenceModelSelectionField;
import ch.vknws.vierkant.ui.admin.ContactPersonModelTableView;
import ch.vknws.vierkant.ui.field.converter.LocalDateConverter;
import ch.vknws.vierkant.ui.field.converter.RankModelConverter;

/**
 * Detail view for the PersonModel in the Admin section.
 *
 * @author Benjamin Weber
 */
public class AdminPersonDetailView extends AbstractDetailView<PersonModel> {

  private static final long serialVersionUID = -7150958080205427938L;

  private PersonPersistableTexts personModelTexts;

  private TextField givenName;
  private TextField familyName;
  private TextField title;
  private TextField telephone;
  private TextField mobile;
  private TextField emailPrivate;
  private TextField emailOrganization;
  private DateField birthdate;
  private TextArea allergyComment;
  private TextArea secretaryComment;
  private TextArea supervisorComment;
  private CheckBox vkNews;
  private DateField lastUpdateCheck;
  private TextField shorthand;
  private ComboBox vka;
  private TextField recordId;
  private TextField revisionId;
  private ComboBox rank;
  private ReferenceModelSelectionField<AddressModel> homeAddress;
  private MemberCategoryReferenceModelSelectionField memberCategory;
  private ReferenceModelListField<ContactPersonModel> contactPersons;

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param displayedModel the model to display
   */
  public AdminPersonDetailView(PersonModel displayedModel) {
    super(displayedModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FormLayout setUp() {
    personModelTexts = ServiceRegistry.getTypedConfigurationProxy()
        .get(new ConfigurationProperties<>(PersonPersistableTexts.class));

    FormLayout layout = new FormLayout();

    layout.setMargin(true);

    recordId = new TextField();
    layout.addComponent(recordId);
    recordId.setCaption(personModelTexts.recordId());
    recordId.setPropertyDataSource(getDisplayedItem().getItemProperty(DatabaseModel.PropertyId.RECORD_ID));
    recordId.setNullRepresentation("");
    saveGroup.bind(recordId, DatabaseModel.PropertyId.RECORD_ID);

    revisionId = new TextField();
    layout.addComponent(revisionId);
    revisionId.setCaption(personModelTexts.revisionId());
    revisionId.setPropertyDataSource(getDisplayedItem().getItemProperty(DatabaseModel.PropertyId.REVISION_ID));
    revisionId.setNullRepresentation("");
    saveGroup.bind(revisionId, DatabaseModel.PropertyId.REVISION_ID);

    givenName = new TextField();
    layout.addComponent(givenName);
    givenName.setCaption(personModelTexts.givenName());
    givenName.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.GIVEN_NAME));
    givenName.setNullRepresentation("");
    givenName.addValueChangeListener(e -> maybeGenerateShorthand());
    saveGroup.bind(givenName, PersonModel.PropertyId.GIVEN_NAME);

    familyName = new TextField();
    layout.addComponent(familyName);
    familyName.setCaption(personModelTexts.familyName());
    familyName.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.FAMILY_NAME));
    familyName.setNullRepresentation("");
    familyName.addValueChangeListener(e -> maybeGenerateShorthand());
    saveGroup.bind(familyName, PersonModel.PropertyId.FAMILY_NAME);

    title = new TextField();
    layout.addComponent(title);
    title.setCaption(personModelTexts.title());
    title.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.TITLE));
    title.setNullRepresentation("");
    saveGroup.bind(title, PersonModel.PropertyId.TITLE);

    shorthand = new TextField();
    layout.addComponent(shorthand);
    shorthand.setCaption(personModelTexts.shorthand());
    shorthand.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.SHORTHAND));
    shorthand.setNullRepresentation("");
    shorthand.addFocusListener(e -> maybeGenerateShorthand());
    saveGroup.bind(shorthand, PersonModel.PropertyId.SHORTHAND);

    vka = new ComboBox();
    layout.addComponent(vka);
    vka.setCaption(personModelTexts.vka());
    vka.setContainerDataSource(createVkaContainer());
    vka.setTextInputAllowed(false);
    vka.setNullSelectionAllowed(true);
    vka.setItemCaptionMode(ItemCaptionMode.ID_TOSTRING);
    vka.setValue(getDisplayedModel().getVka());
    vka.addValueChangeListener(e -> getDisplayedModel().setVka((VKA) e.getProperty().getValue()));
    saveGroup.bind(vka, PersonModel.PropertyId.VKA);

    rank = new ComboBox();
    layout.addComponent(rank);
    rank.setCaption(personModelTexts.refRank());
    rank.setNullSelectionItemId(null);
    rank.setTextInputAllowed(false);
    rank.setNullSelectionAllowed(true);
    rank.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    rank.setItemCaptionPropertyId(RankModel.PropertyId.RANK_NAME);
    rank.setContainerDataSource(new RankContainer());
    if (getDisplayedModel().getRefRank() == null) {
      rank.setValue(null);
    } else {
      rank.setValue(getDisplayedModel().getRefRank().getRecordId());
    }
    rank.setConverter(new RankModelConverter());
    saveGroup.bind(rank, PersonModel.PropertyId.REF_RANK);

    homeAddress = new ReferenceModelSelectionField<AddressModel>(HomeAddressReferenceTableView.class,
        DaoFactory.getInstance().getAddressDao());
    layout.addComponent(homeAddress);
    homeAddress.setCaption(personModelTexts.homeAddress());
    homeAddress.addValueChangeListener(e -> getDisplayedModel().setRefHomeAddress(homeAddress.getValue()));
    homeAddress.setValue(getDisplayedModel().getRefHomeAddress());

    telephone = new TextField();
    layout.addComponent(telephone);
    telephone.setCaption(personModelTexts.telephone());
    telephone.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.TELEPHONE));
    telephone.setNullRepresentation("");
    saveGroup.bind(telephone, PersonModel.PropertyId.TELEPHONE);

    mobile = new TextField();
    layout.addComponent(mobile);
    mobile.setCaption(personModelTexts.mobile());
    mobile.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.MOBILE));
    mobile.setNullRepresentation("");
    saveGroup.bind(mobile, PersonModel.PropertyId.MOBILE);

    emailPrivate = new TextField();
    layout.addComponent(emailPrivate);
    emailPrivate.setCaption(personModelTexts.emailPrivate());
    emailPrivate.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.EMAIL_PRIVATE));
    emailPrivate.setNullRepresentation("");
    saveGroup.bind(emailPrivate, PersonModel.PropertyId.EMAIL_PRIVATE);

    emailOrganization = new TextField();
    layout.addComponent(emailOrganization);
    emailOrganization.setCaption(personModelTexts.emailOrganization());
    emailOrganization
        .setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.EMAIL_ORGANISATION));
    emailOrganization.setNullRepresentation("");
    saveGroup.bind(emailOrganization, PersonModel.PropertyId.EMAIL_ORGANISATION);

    birthdate = new DateField();
    layout.addComponent(birthdate);
    birthdate.setCaption(personModelTexts.birthdate());
    birthdate.setConverter(new LocalDateConverter());
    birthdate.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.BIRTHDATE));
    saveGroup.bind(birthdate, PersonModel.PropertyId.BIRTHDATE);

    memberCategory = new MemberCategoryReferenceModelSelectionField(getDisplayedModel());
    layout.addComponent(memberCategory);
    memberCategory.setCaption(personModelTexts.memberCategory());

    secretaryComment = new TextArea();
    layout.addComponent(secretaryComment);
    secretaryComment.setCaption(personModelTexts.secretaryComment());
    secretaryComment
        .setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.SECRETARY_COMMENT));
    secretaryComment.setNullRepresentation("");
    saveGroup.bind(secretaryComment, PersonModel.PropertyId.SECRETARY_COMMENT);

    contactPersons = new ReferenceModelListField<>(new ContactPersonModelTableView(getDisplayedModel()));
    layout.addComponent(contactPersons);
    contactPersons.setCaption(personModelTexts.contactPersons());

    vkNews = new CheckBox();
    layout.addComponent(vkNews);
    vkNews.setCaption(personModelTexts.vkNews());
    vkNews.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.VK_NEWS));
    saveGroup.bind(vkNews, PersonModel.PropertyId.VK_NEWS);

    lastUpdateCheck = new DateField();
    layout.addComponent(lastUpdateCheck);
    lastUpdateCheck.setCaption(personModelTexts.lastUpdateCheck());
    lastUpdateCheck.setConverter(new LocalDateConverter());
    lastUpdateCheck.setPropertyDataSource(getDisplayedItem().getItemProperty(PersonModel.PropertyId.LAST_UPDATE_CHECK));
    saveGroup.bind(lastUpdateCheck, PersonModel.PropertyId.LAST_UPDATE_CHECK);

    return layout;
  }

  private BeanItemContainer<VKA> createVkaContainer() {
    BeanItemContainer<VKA> container = new BeanItemContainer<>(VKA.class);

    container.addAll(EnumSet.allOf(VKA.class));

    return container;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void saveDisplayedModel() throws SaveException {
    DaoFactory.getInstance().getPersonDao().save(getDisplayedModel(),
        ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return NavigationIds.admin_person_details;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getViewCaption() {
    return getDisplayedModel().toString();
  }

  private void maybeGenerateShorthand() {
    if (getDisplayedModel() != null && getDisplayedModel().getGivenName() != null
        && getDisplayedModel().getFamilyName() != null && getDisplayedModel().getShorthand() == null) {

      String shorthand = PersonShorthandGenerator.generate(getDisplayedModel());
      this.shorthand.setValue(shorthand);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();

    applyRecordIdEditability();
    applyRevisionIdEditability();
    applyGivenNameEditability();
    applyFamilyNameEditability();
    applyTitleEditability();
    applyTelephoneEditability();
    applyMobileEditability();
    applyEmailPrivateEditability();
    applyEmailOrganizationEditability();
    applyBirthdateEditability();
    applyAllergyCommentEditability();
    applySecretaryCommentEditability();
    applySupervisorCommentEditability();
    applyVkNewsEditability();
    applyLastUpdateCheckEditability();
  }

  private void applyRecordIdEditability() {
    if (recordId == null) return;
    recordId.setEnabled(false);
  }

  private void applyRevisionIdEditability() {
    if (revisionId == null) return;
    revisionId.setEnabled(false);
  }

  private void applyGivenNameEditability() {
    if (givenName == null) return;
    givenName.setEnabled(true);
  }

  private void applyFamilyNameEditability() {
    if (familyName == null) return;
    familyName.setEnabled(true);
  }

  private void applyTitleEditability() {
    if (title == null) return;
    title.setEnabled(true);
  }

  private void applyTelephoneEditability() {
    if (telephone == null) return;
    telephone.setEnabled(true);
  }

  private void applyMobileEditability() {
    if (mobile == null) return;
    mobile.setEnabled(true);
  }

  private void applyEmailPrivateEditability() {
    if (emailPrivate == null) return;
    emailPrivate.setEnabled(true);
  }

  private void applyEmailOrganizationEditability() {
    if (emailOrganization == null) return;
    emailOrganization.setEnabled(true);
  }

  private void applyBirthdateEditability() {
    if (birthdate == null) return;
    birthdate.setEnabled(true);
  }

  private void applyAllergyCommentEditability() {
    if (allergyComment == null) return;
    allergyComment.setEnabled(true);
  }

  private void applySecretaryCommentEditability() {
    if (secretaryComment == null) return;
    secretaryComment.setEnabled(true);
  }

  private void applySupervisorCommentEditability() {
    if (supervisorComment == null) return;
    supervisorComment.setEnabled(true);
  }

  private void applyVkNewsEditability() {
    if (vkNews == null) return;
    vkNews.setEnabled(true);
  }

  private void applyLastUpdateCheckEditability() {
    if (lastUpdateCheck == null) return;
    lastUpdateCheck.setEnabled(true);
  }

}
