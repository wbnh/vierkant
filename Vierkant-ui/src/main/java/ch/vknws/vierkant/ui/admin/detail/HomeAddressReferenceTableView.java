package ch.vknws.vierkant.ui.admin.detail;

import com.vaadin.data.Container.Indexed;

import ch.bwe.fac.v1.type.DeleteException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.AddressContainer;
import ch.vknws.vierkant.type.AddressModel;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.AbstractTableView;

/**
 * Table view for the home address on a person.
 *
 * @author Benjamin Weber
 *
 */
public class HomeAddressReferenceTableView extends AbstractTableView<AddressModel> {

	private static final long serialVersionUID = 652601674419814777L;

	private static final Object[] COLUMNS = { AddressModel.PropertyId.LINE1, AddressModel.PropertyId.LINE2,
					AddressModel.PropertyId.ZIP_CODE, AddressModel.PropertyId.CITY, AddressModel.PropertyId.CANTON,
					AddressModel.PropertyId.COUNTRY };

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public HomeAddressReferenceTableView() {
		super(COLUMNS);

		hideButtons();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Indexed createContainer() {
		return new AddressContainer();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected AbstractDetailView<AddressModel> createDetailView(AddressModel selectedModel) {
		// no detail view and no editing here
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void deleteModelFromDatabase(Integer recordId) throws DeleteException {
		// no deleting
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected AddressModel loadModelFromDatabase(Integer recordId) {
		return DaoFactory.getInstance().getAddressDao().load(recordId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getViewId() {
		// no ID
		return null;
	}

}
