package ch.vknws.vierkant.ui.admin;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.configuration.ConfigurationUtils;
import ch.vknws.vierkant.backend.configuration.VierkantConfigurations;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.ui.AbstractView;
import ch.vknws.vierkant.ui.NavigationIds;
import ch.vknws.vierkant.ui.dialog.DialogUtils;
import ch.vknws.vierkant.ui.field.converter.LocalDateConverter;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * Overview for the admin.
 *
 * @author Benjamin Weber
 */
public class AdminOverview extends AbstractView {
  private static final long serialVersionUID = 3036674840770724545L;

  /**
   * The texts for administrators.
   */
  public static final AdminTexts texts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(AdminTexts.class));

  private TextArea userMessage;
  private DateField expirationDate;
  private Button submit;

  private DateField managementWindowFrom;
  private DateField managementWindowTo;

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public AdminOverview() {
    personAuthorizations.add(SecretaryAuthorizationKeys.ADMIN);

    HorizontalLayout root = new HorizontalLayout();
    addComponent(root);
    root.setMargin(false);

    FormLayout generalMessage = setUpGeneralMessage();
    root.addComponent(generalMessage);

    FormLayout managementWindow = setUpManagementWindow();
    root.addComponent(managementWindow);

  }

  private FormLayout setUpManagementWindow() {

    FormLayout layout = new FormLayout();
    layout.setMargin(true);

    Label header = new Label();
    layout.addComponent(header);
    header.setValue(texts.managementWindowTitle());
    header.addStyleName(VierkantTheme.LABEL_H1);

    managementWindowFrom = new DateField();
    layout.addComponent(managementWindowFrom);
    managementWindowFrom.setCaption(texts.managementWindowFrom());
    managementWindowFrom.setConverter(new LocalDateConverter());

    managementWindowTo = new DateField();
    layout.addComponent(managementWindowTo);
    managementWindowTo.setCaption(texts.managementWindowTo());
    managementWindowTo.setConverter(new LocalDateConverter());

    submit = new Button();
    layout.addComponent(submit);
    submit.setCaption(texts.submitUserMessage());
    submit.addClickListener(this::onManagementWindowSubmit);

    return layout;
  }

  private FormLayout setUpGeneralMessage() {

    FormLayout layout = new FormLayout();
    layout.setMargin(true);

    Label header = new Label();
    layout.addComponent(header);
    header.setValue(texts.userMessageTitle());
    header.addStyleName(VierkantTheme.LABEL_H1);

    userMessage = new TextArea();
    layout.addComponent(userMessage);
    userMessage.setCaption(texts.userMessage());

    expirationDate = new DateField();
    layout.addComponent(expirationDate);
    expirationDate.setCaption(texts.userMessageExpiration());
    expirationDate.setConverter(new LocalDateConverter());

    submit = new Button();
    layout.addComponent(submit);
    submit.setCaption(texts.submitUserMessage());
    submit.addClickListener(this::onUserMessageSubmit);

    updateGeneralMessage();

    return layout;
  }

  private void updateGeneralMessage() {

    String message = ConfigurationUtils.getConfiguration(VierkantConfigurations.ADMIN_USER_MESSAGE);
    String expiration = ConfigurationUtils.getConfiguration(VierkantConfigurations.ADMIN_USER_MESSAGE_EXPIRATION);

    if (message != null && expiration != null) {
      LocalDate expirationLocalDate = LocalDate
          .from(ApplicationRegistry.getInstance().getDateFormat().parse(expiration));
      if (!LocalDate.now().isAfter(expirationLocalDate)) {
        userMessage.setValue(message);
        expirationDate.setConvertedValue(expirationLocalDate);
      }
    }
  }

  private void onManagementWindowSubmit(ClickEvent event) {
    LocalDate from = (LocalDate) managementWindowFrom.getConvertedValue();
    LocalDate to = (LocalDate) managementWindowTo.getConvertedValue();
    DateTimeFormatter format = ApplicationRegistry.getInstance().getDateFormat();

    String message = texts.managementWindow(format.format(from), format.format(to));

    // use from because in case no shutdown happens, it doesn't matter and in
    // case it does happen, it doesn't matter either. If it is restarted early,
    // it does not matter either
    registerUserMessage(message, from);
  }

  private void onUserMessageSubmit(ClickEvent event) {
    String message = userMessage.getValue();

    registerUserMessage(message, (LocalDate) expirationDate.getConvertedValue());
  }

  private void registerUserMessage(String message, LocalDate expirationLocalDate) {
    String expiration = ApplicationRegistry.getInstance().getDateFormat().format(expirationLocalDate);
    try {
      ConfigurationUtils.setConfiguration(VierkantConfigurations.ADMIN_USER_MESSAGE, message,
          getLoggedInUser().getRecordId());
      ConfigurationUtils.setConfiguration(VierkantConfigurations.ADMIN_USER_MESSAGE_EXPIRATION, expiration,
          getLoggedInUser().getRecordId());

      Notification notification = new Notification(successMessages.savedMessage(""));
      notification.setDelayMsec(5000);
      notification.setPosition(Position.TOP_RIGHT);
      notification.show(Page.getCurrent());
    } catch (SaveException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not save admin message", e);
      DialogUtils.showErrorDialog(errorMessages.saveFailTitle(), errorMessages.saveFailMessage(""), e);
    }

    updateGeneralMessage();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return NavigationIds.admin_overview;
  }

}
