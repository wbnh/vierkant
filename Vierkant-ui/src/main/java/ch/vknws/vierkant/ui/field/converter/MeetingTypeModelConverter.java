package ch.vknws.vierkant.ui.field.converter;

import java.util.Locale;

import com.vaadin.data.util.converter.Converter;

import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.MeetingTypeModel;

/**
 * Converter for rank models.
 *
 * @author Benjamin Weber
 */
public class MeetingTypeModelConverter implements Converter<Object, MeetingTypeModel> {

  private static final long serialVersionUID = -4049802806505138209L;

  /**
   * {@inheritDoc}
   */
  @Override
  public MeetingTypeModel convertToModel(Object value, Class<? extends MeetingTypeModel> targetType, Locale locale)
      throws com.vaadin.data.util.converter.Converter.ConversionException {
    if (value == null) { return null; }
    return DaoFactory.getInstance().getMeetingTypeDao().load((Integer) value);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer convertToPresentation(MeetingTypeModel value, Class<? extends Object> targetType, Locale locale)
      throws com.vaadin.data.util.converter.Converter.ConversionException {
    if (value == null) { return null; }
    return value.getRecordId();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Class<MeetingTypeModel> getModelType() {
    return MeetingTypeModel.class;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public Class<Object> getPresentationType() {
    Class clazz = Integer.class;
    return clazz;
  }

}
