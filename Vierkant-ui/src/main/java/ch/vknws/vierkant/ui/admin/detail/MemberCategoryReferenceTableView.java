package ch.vknws.vierkant.ui.admin.detail;

import com.vaadin.data.Container.Indexed;

import ch.bwe.fac.v1.type.DeleteException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.MemberCategoryContainer;
import ch.vknws.vierkant.type.MemberGroupModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.impl.MemberGroupPersistable;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.AbstractTableView;

/**
 * Table view for MemberCategory references.
 *
 * @author Benjamin Weber
 */
public class MemberCategoryReferenceTableView extends AbstractTableView<MemberGroupModel> {

  private static final long serialVersionUID = -2147283306576791850L;

  private static final Object[] COLUMNS = {};

  private PersonModel personModel;

  private MemberCategoryContainer lastCreatedContainer;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param personModel the model to display
   */
  public MemberCategoryReferenceTableView(PersonModel personModel) {
    super(COLUMNS);

    this.personModel = personModel;
    lastCreatedContainer.setPersonId(personModel.getRecordId());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected Indexed createContainer() {
    lastCreatedContainer = new MemberCategoryContainer(null);
    return lastCreatedContainer;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected AbstractDetailView<MemberGroupModel> createDetailView(MemberGroupModel selectedModel) {
    if (selectedModel == null) {
      selectedModel = new MemberGroupPersistable();
    }
    return new AdminMemberGroupDetailsView(selectedModel, personModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void deleteModelFromDatabase(Integer recordId) throws DeleteException {
    DaoFactory.getInstance().getMemberCategoryDao().delete(recordId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected MemberGroupModel loadModelFromDatabase(Integer recordId) {
    return DaoFactory.getInstance().getMemberCategoryDao().load(recordId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

}
