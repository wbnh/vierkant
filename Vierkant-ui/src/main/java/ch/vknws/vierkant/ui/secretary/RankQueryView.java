package ch.vknws.vierkant.ui.secretary;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Label;

import ch.vknws.vierkant.data.RankContainer;
import ch.vknws.vierkant.type.RankModel;
import ch.vknws.vierkant.type.impl.RankPersistable;
import ch.vknws.vierkant.ui.AbstractView;
import ch.vknws.vierkant.ui.field.converter.RankModelConverter;

/**
 * View to ask for a rank.
 *
 * @author Benjamin Weber
 */
public class RankQueryView extends AbstractView {
  private static final long serialVersionUID = 3784008638452795946L;

  private ComboBox rank;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param queryText the text of the query
   */
  public RankQueryView(String queryText) {
    Label query = new Label();
    addComponent(query);
    query.setValue(queryText);
    query.setWidth("100%");

    rank = new ComboBox();
    addComponent(rank);
    rank.setNullSelectionItemId(null);
    rank.setTextInputAllowed(true);
    rank.setNullSelectionAllowed(true);
    rank.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    rank.setItemCaptionPropertyId(RankModel.PropertyId.RANK_NAME);
    rank.setContainerDataSource(new RankContainer());
    rank.setConverter(new RankModelConverter());
    rank.setWidth("100%");
  }

  /**
   * @return the rank that was selected
   */
  public RankPersistable getSelectedRank() {
    return (RankPersistable) rank.getConvertedValue();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

}
