package ch.vknws.vierkant.ui;

import java.util.HashMap;
import java.util.Map;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewProvider;

import ch.bwe.faa.v1.core.util.StringUtils;
import ch.vknws.vierkant.ApplicationRegistry;

/**
 * View provider to configure navigation.
 *
 * @author Benjamin Weber
 */
public class DefaultViewProvider implements ViewProvider {

  private static final long serialVersionUID = 8953977673861010197L;
  private Map<String, AbstractView> views = new HashMap<>();

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewName(String viewAndParameters) {
    if (ApplicationRegistry.getInstance().getLoggedInUser() == null) { return NavigationIds.login; }

    String[] split = viewAndParameters.split("\\?");

    String viewId = split[0];

    String[] viewSplit = viewId.split("/");
    StringBuilder viewIdBuilder = new StringBuilder();
    for (String string : viewSplit) {
      if (StringUtils.isInteger(string)) {
        break;
      }

      if (viewIdBuilder.length() > 0) viewIdBuilder.append("/");
      viewIdBuilder.append(string);
    }

    AbstractView view = views.get(viewIdBuilder.toString());
    if (view == null) { return null; }
    return view.getViewId();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public View getView(String viewName) {
    if (ApplicationRegistry.getInstance().getLoggedInUser() == null) { return views.get(NavigationIds.login); }
    AbstractView view = views.get(viewName);
    return view;
  }

  /**
   * @param view the view to provide if no view is explicitly specified
   */
  public void setDefaultView(AbstractView view) {
    views.put("", view);
  }

  /**
   * @param view the view to which we want to be able to navigate to
   */
  public void addView(AbstractView view) {
    views.put(view.getViewId(), view);
  }

}
