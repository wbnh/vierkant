package ch.vknws.vierkant.ui.secretary;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.UserError;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.rules.ValidationMessageType;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.PersonFunctionModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.caption.PersonPersistableTexts;
import ch.vknws.vierkant.type.impl.OrganizationFunctionPersistable;
import ch.vknws.vierkant.type.impl.PersonFunctionPersistable;
import ch.vknws.vierkant.ui.AbstractView;
import ch.vknws.vierkant.ui.search.FunctionColumnSearchView;

/**
 * View for editing of member groups.
 *
 * @author Benjamin Weber
 */
public class FunctionEditView extends AbstractView implements CloseListener {
  private static final long serialVersionUID = -8936498798045864382L;
  private static final PersonPersistableTexts personTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(PersonPersistableTexts.class));

  private PersonModel person;

  private FunctionColumnSearchView searchView;
  private ComboBox primaryFunction;

  private boolean wasValid = true;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param person the model to display
   */
  public FunctionEditView(PersonModel person) {
    this.person = person;
    setUp();
  }

  private void setUp() {

    VerticalLayout wrapper = new VerticalLayout();
    addComponent(wrapper);
    wrapper.setMargin(true);

    FormLayout layout = new FormLayout();
    wrapper.addComponent(layout);
    // layout.setHeight(250, Unit.PIXELS);

    searchView = new FunctionColumnSearchView(Arrays.asList());
    layout.addComponent(searchView);
    // searchView.setHeight(150, Unit.PIXELS);

    if (person.getRefFunctions() != null) {
      Set<PersonFunctionModel> personFunctions = person.getRefFunctions();
      Set<OrganizationFunctionModel> functions = new HashSet<>();
      for (PersonFunctionModel personFunctionPersistable : personFunctions) {
        functions.add(personFunctionPersistable.getRefFunction());
      }

      TwinColSelect select = searchView.getSelect();
      select.setContainerDataSource(new BeanItemContainer<>(OrganizationFunctionModel.class, functions));

      for (OrganizationFunctionModel functionModel : functions) {
        select.setItemCaption(functionModel, functionModel.toString());
      }

      for (OrganizationFunctionModel model : functions) {
        select.select(model);
      }
    }
    searchView.getSelect().addValueChangeListener(e -> executeRules());

    primaryFunction = new ComboBox();
    layout.addComponent(primaryFunction);
    primaryFunction.setCaption(personTexts.primaryFunction());
    primaryFunction.setContainerDataSource(createPrimaryFunctionContainer());
    primaryFunction.setTextInputAllowed(false);
    primaryFunction.setNullSelectionAllowed(false);
    primaryFunction.setItemCaptionMode(ItemCaptionMode.ID_TOSTRING);
    primaryFunction.addValueChangeListener(e -> updateValidity());
    layout.setComponentAlignment(primaryFunction, Alignment.BOTTOM_LEFT);

    executeRules();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isValid() {
    Object selected = getSearchView().getSelect().getValue();
    boolean selectOk = selected == null || ((Collection<?>) selected).isEmpty();
    boolean valid = getPrimaryFunction().getValue() != null || selectOk;

    return valid;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();

    updatePrimaryFunctionContainer();
    updateValidity();
  }

  private void updateValidity() {
    applyPrimaryFunctionValidity();

    if (isValid() ^ wasValid) {
      fireValidityChange();
      wasValid = isValid();
    }
  }

  private void applyPrimaryFunctionValidity() {
    if (isValid()) {
      getPrimaryFunction().setComponentError(null);
    } else {
      getPrimaryFunction().setComponentError(new UserError(ValidationMessageType.NULL.toString()));
    }
  }

  private void updatePrimaryFunctionContainer() {
    if (primaryFunction == null) { return; }
    primaryFunction.setContainerDataSource(createPrimaryFunctionContainer());

    if (person.getRefPrimaryFunction() != null && person.getRefFunctions() != null) {
      for (var function : person.getRefFunctions()) {
        if (Objects.equals(person.getRefPrimaryFunction().getRecordId(), function.getRefFunction().getRecordId())
            && primaryFunction.getContainerDataSource().containsId(function.getRefFunction())) {
          primaryFunction.setValue(function.getRefFunction());
        }
      }
    }
  }

  private BeanItemContainer<OrganizationFunctionPersistable> createPrimaryFunctionContainer() {
    BeanItemContainer<OrganizationFunctionPersistable> container = new BeanItemContainer<>(
        OrganizationFunctionPersistable.class);

    @SuppressWarnings("unchecked")
    Set<OrganizationFunctionPersistable> functions = (Set<OrganizationFunctionPersistable>) searchView.getSelect()
        .getValue();
    container.addAll(functions);
    return container;
  }

  /**
   * @return the searchView
   */
  public FunctionColumnSearchView getSearchView() {
    return searchView;
  }

  /**
   * @return the categoryBox
   */
  public ComboBox getPrimaryFunction() {
    return primaryFunction;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void windowClose(CloseEvent e) {
    @SuppressWarnings("unchecked")
    Set<OrganizationFunctionModel> functions = (Set<OrganizationFunctionModel>) getSearchView().getSelect().getValue();
    Set<PersonFunctionModel> existingPersonFunctions = new HashSet<>();
    if (person.getRefFunctions() != null) {
      existingPersonFunctions.addAll(person.getRefFunctions());
    }
    for (OrganizationFunctionModel function : functions) {
      boolean alreadyAdded = false;
      for (Iterator<PersonFunctionModel> iterator = existingPersonFunctions.iterator(); iterator.hasNext();) {
        PersonFunctionModel personFunction = iterator.next();
        if (Objects.equals(function.getRecordId(), personFunction.getRefFunction().getRecordId())) {
          iterator.remove();
          alreadyAdded = true;
          break;
        }
      }
      if (!alreadyAdded) {
        PersonFunctionModel personFunction = new PersonFunctionPersistable();
        personFunction.setCreator(ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
        personFunction.setRefFunction(function);
        Set<PersonFunctionModel> refFunctions = person.getRefFunctions();
        refFunctions.add(personFunction);
        person.setRefFunctions(refFunctions);
      }
    }
    for (PersonFunctionModel remainingFunction : existingPersonFunctions) {
      Set<PersonFunctionModel> refFunctions = person.getRefFunctions();
      refFunctions.remove(remainingFunction);
      person.setRefFunctions(refFunctions);
    }

    OrganizationFunctionModel primaryFunction = (OrganizationFunctionModel) getPrimaryFunction().getValue();
    person.setRefPrimaryFunction(primaryFunction);
  }

}
