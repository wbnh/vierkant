package ch.vknws.vierkant.ui;

import java.util.List;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.util.AssertUtils;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.type.DAO;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * View to search for person models.
 *
 * @author Benjamin Weber
 * @param <P> the class of the model we're looking for
 */
public abstract class AbstractSearchView<P extends DatabaseModel> extends AbstractView {

  private static final long serialVersionUID = -6921444898893831089L;

  private CssLayout root;
  private TextField searchField;
  private Button searchButton;
  private Grid resultGrid;

  private String[] columnNames;
  private DAO<P> dao;

  /**
   * The class of the model we're looking for.
   */
  @SuppressWarnings("unused") // The class must be loaded for type parameters to apply
  private Class<? extends P> modelClass;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param modelClass  the class of the model we're looking for
   * @param dao         the DAO to load the model
   * @param columnNames the columns to display
   */
  public AbstractSearchView(Class<? extends P> modelClass, DAO<P> dao, String... columnNames) {
    AssertUtils.notNull(modelClass);
    AssertUtils.notNull(dao);

    this.modelClass = modelClass;
    this.dao = dao;
    this.columnNames = columnNames;

    setUp();
  }

  private void setUp() {

    root = new CssLayout();
    addComponent(root);
    root.setSizeFull();

    HorizontalLayout searchWrapper = new HorizontalLayout();
    root.addComponent(searchWrapper);
    searchWrapper.setWidth("100%");
    searchWrapper.setMargin(false);
    searchWrapper.setSpacing(true);

    searchField = new TextField();
    searchWrapper.addComponent(searchField);
    searchField.setWidth("100%");
    searchField.focus();

    searchButton = new Button();
    searchWrapper.addComponent(searchButton);
    searchButton.setIcon(FontAwesome.SEARCH);
    searchButton.addStyleName(VierkantTheme.BUTTON_ICON_ONLY);
    searchButton.addClickListener(this::onSearch);
    searchButton.setClickShortcut(KeyCode.ENTER);

    resultGrid = new Grid();
    root.addComponent(resultGrid);
    resultGrid.setSizeFull();
    resultGrid.setColumns((Object[]) columnNames);

  }

  /**
   * @return the restriction to filter the view
   */
  protected abstract AbstractRestriction createRestriction();

  private void onSearch(ClickEvent event) {

    AbstractRestriction restriction = createRestriction();

    if (restriction == null && resultGrid.getContainerDataSource() != null) {
      resultGrid.getContainerDataSource().removeAllItems();
    }

    ServiceRegistry.getLogProxy().trace(this, "Searching Material: {0}", restriction);
    List<P> persistableList = dao.find(restriction, 0, Integer.MAX_VALUE);

    BeanItemContainer<P> container = new BeanItemContainer<>(DatabaseModel.class, persistableList);

    resultGrid.setContainerDataSource(container);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

  /**
   * @param listener the listener to be invoked when a model is selected
   */
  public void addSelectionListener(SelectionListener listener) {
    resultGrid.addSelectionListener(listener);
  }

  /**
   * @param listener the listener to be removed
   */
  public void removeSelectionListener(SelectionListener listener) {
    resultGrid.removeSelectionListener(listener);
  }

  /**
   * @return the searchField
   */
  public TextField getSearchField() {
    return searchField;
  }

  /**
   * @return the resultGrid
   */
  public Grid getResultGrid() {
    return resultGrid;
  }
}
