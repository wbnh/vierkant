package ch.vknws.vierkant.ui.admin.detail;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.caption.OrganizationFunctionPersistableTexts;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.NavigationIds;

/**
 * Detail view for the AddressModel in the Admin section.
 *
 * @author Benjamin Weber
 */
public class AdminOrganizationFunctionModelDetailView extends AbstractDetailView<OrganizationFunctionModel> {

  private static final long serialVersionUID = 3187482689576874439L;

  private OrganizationFunctionPersistableTexts functionModelTexts;

  private TextField functionName;
  private TextField email;
  private TextField recordId;
  private TextField revisionId;

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param displayedModel the model to display
   */
  public AdminOrganizationFunctionModelDetailView(OrganizationFunctionModel displayedModel) {
    super(displayedModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getViewCaption() {
    return getDisplayedModel().toString();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FormLayout setUp() {
    functionModelTexts = ServiceRegistry.getTypedConfigurationProxy()
        .get(new ConfigurationProperties<>(OrganizationFunctionPersistableTexts.class));

    FormLayout layout = new FormLayout();

    layout.setMargin(true);

    recordId = new TextField();
    layout.addComponent(recordId);
    recordId.setCaption(functionModelTexts.recordId());
    recordId.setPropertyDataSource(getDisplayedItem().getItemProperty(DatabaseModel.PropertyId.RECORD_ID));
    recordId.setNullRepresentation("");
    saveGroup.bind(recordId, DatabaseModel.PropertyId.RECORD_ID);

    revisionId = new TextField();
    layout.addComponent(revisionId);
    revisionId.setCaption(functionModelTexts.revisionId());
    revisionId.setPropertyDataSource(getDisplayedItem().getItemProperty(DatabaseModel.PropertyId.REVISION_ID));
    revisionId.setNullRepresentation("");
    saveGroup.bind(revisionId, DatabaseModel.PropertyId.REVISION_ID);

    functionName = new TextField();
    layout.addComponent(functionName);
    functionName.setCaption(functionModelTexts.functionName());
    functionName
        .setPropertyDataSource(getDisplayedItem().getItemProperty(OrganizationFunctionModel.PropertyId.FUNCTION_NAME));
    functionName.setNullRepresentation("");
    saveGroup.bind(functionName, OrganizationFunctionModel.PropertyId.FUNCTION_NAME);

    email = new TextField();
    layout.addComponent(email);
    email.setCaption(functionModelTexts.email());
    email.setPropertyDataSource(getDisplayedItem().getItemProperty(OrganizationFunctionModel.PropertyId.EMAIL));
    email.setNullRepresentation("");
    saveGroup.bind(email, OrganizationFunctionModel.PropertyId.EMAIL);

    return layout;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void saveDisplayedModel() throws SaveException {
    DaoFactory.getInstance().getOrganizationFunctionDao().save(getDisplayedModel(),
        ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return NavigationIds.admin_address_details;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();

    applyRecordIdEditability();
    applyRevisionIdEditability();
    applyFunctionNameEditability();
    applyEmailEditability();
  }

  private void applyRecordIdEditability() {
    recordId.setEnabled(false);
  }

  private void applyRevisionIdEditability() {
    revisionId.setEnabled(false);
  }

  private void applyFunctionNameEditability() {
    functionName.setEnabled(true);
  }

  private void applyEmailEditability() {
    email.setEnabled(true);
  }

}
