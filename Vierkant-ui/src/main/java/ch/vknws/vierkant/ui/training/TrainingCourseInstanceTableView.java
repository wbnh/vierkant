package ch.vknws.vierkant.ui.training;

import com.vaadin.data.Container.Indexed;

import ch.bwe.fac.v1.type.DeleteException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.TrainingCourseInstanceContainer;
import ch.vknws.vierkant.type.TrainingCourseInstanceModel;
import ch.vknws.vierkant.type.impl.TrainingCourseInstancePersistable;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.AbstractTableView;
import ch.vknws.vierkant.ui.NavigationIds;

/**
 * Table view for training course instances.
 *
 * @author Benjamin Weber
 *
 */
public class TrainingCourseInstanceTableView extends AbstractTableView<TrainingCourseInstanceModel> {
	private static final long serialVersionUID = 645882625066175232L;
	private static final Object[] COLUMNS = { TrainingCourseInstanceModel.PropertyId.REF_TEMPLATE,
					TrainingCourseInstanceModel.PropertyId.REF_RESPONSIBLE };

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public TrainingCourseInstanceTableView() {
		super(COLUMNS);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Indexed createContainer() {
		return new TrainingCourseInstanceContainer();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected AbstractDetailView<TrainingCourseInstanceModel> createDetailView(
					TrainingCourseInstanceModel selectedModel) {
		if (selectedModel == null) {
			selectedModel = new TrainingCourseInstancePersistable();
		}

		return new TrainingCourseInstanceDetailsView(selectedModel);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void deleteModelFromDatabase(Integer recordId) throws DeleteException {
		DaoFactory.getInstance().getTrainingCourseInstanceDao().delete(recordId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected TrainingCourseInstanceModel loadModelFromDatabase(Integer recordId) {
		return DaoFactory.getInstance().getTrainingCourseInstanceDao().load(recordId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getViewId() {
		return NavigationIds.training_instances;
	}

}
