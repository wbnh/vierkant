package ch.vknws.vierkant.ui.field.converter;

import java.util.Locale;

import com.vaadin.data.util.converter.Converter;

import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.MeetingTypeModel;

/**
 * Converter for rank models.
 *
 * @author Benjamin Weber
 */
public class MeetingTypeNameConverter implements Converter<String, MeetingTypeModel> {

  private static final long serialVersionUID = -4049802806505138209L;

  /**
   * {@inheritDoc}
   */
  @Override
  public MeetingTypeModel convertToModel(String value, Class<? extends MeetingTypeModel> targetType, Locale locale)
      throws com.vaadin.data.util.converter.Converter.ConversionException {
    if (value == null) { return null; }
    AbstractRestriction restriction = new EqualityRestriction(MeetingTypeModel.PropertyId.TYPE_NAME, value);
    return DaoFactory.getInstance().getMeetingTypeDao().find(restriction, 0, 1).get(0);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String convertToPresentation(MeetingTypeModel value, Class<? extends String> targetType, Locale locale)
      throws com.vaadin.data.util.converter.Converter.ConversionException {
    if (value == null) { return null; }
    return value.getTypeName();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Class<MeetingTypeModel> getModelType() {
    return MeetingTypeModel.class;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Class<String> getPresentationType() {
    return String.class;
  }

}
