package ch.vknws.vierkant.ui.admin.detail;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.type.MemberGroupModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.caption.MemberGroupPersistableTexts;
import ch.vknws.vierkant.ui.AbstractDetailView;

/**
 * Detail view for the MemberCategory in the Admin section.
 *
 * @author Benjamin Weber
 */
public class AdminMemberGroupDetailsView extends AbstractDetailView<MemberGroupModel> {

  private static final long serialVersionUID = -3817113861355162837L;

  private MemberGroupPersistableTexts memberGroupTexts;

  private TextField recordId;
  private TextField revisionId;

  private PersonModel personModel;

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param displayedModel the model to display
   * @param personModel    the person who is concerned
   */
  public AdminMemberGroupDetailsView(MemberGroupModel displayedModel, PersonModel personModel) {
    super(displayedModel);

    this.personModel = personModel;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getViewCaption() {
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FormLayout setUp() {
    memberGroupTexts = ServiceRegistry.getTypedConfigurationProxy()
        .get(new ConfigurationProperties<>(MemberGroupPersistableTexts.class));

    FormLayout layout = new FormLayout();

    layout.setMargin(true);

    recordId = new TextField();
    layout.addComponent(recordId);
    recordId.setCaption(memberGroupTexts.recordId());
    recordId.setPropertyDataSource(getDisplayedItem().getItemProperty(DatabaseModel.PropertyId.RECORD_ID));
    recordId.setNullRepresentation("");
    saveGroup.bind(recordId, DatabaseModel.PropertyId.RECORD_ID);

    revisionId = new TextField();
    layout.addComponent(revisionId);
    revisionId.setCaption(memberGroupTexts.revisionId());
    revisionId.setPropertyDataSource(getDisplayedItem().getItemProperty(DatabaseModel.PropertyId.REVISION_ID));
    revisionId.setNullRepresentation("");
    saveGroup.bind(revisionId, DatabaseModel.PropertyId.REVISION_ID);

    return layout;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void saveDisplayedModel() throws SaveException {

    personModel.setRefMemberGroup(getDisplayedModel());

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

}
