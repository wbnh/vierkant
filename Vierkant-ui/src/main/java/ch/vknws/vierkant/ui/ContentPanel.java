package ch.vknws.vierkant.ui;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.Configuration;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.faa.v1.core.util.AssertUtils;
import ch.vknws.vierkant.ApplicationConstants;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.configuration.ConfigurationUtils;
import ch.vknws.vierkant.backend.configuration.OrganisationConfigurations;
import ch.vknws.vierkant.backend.rules.UserRuleUtils;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.RankCategory;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * Content panel to contain all views and content of the application.
 *
 * @author Benjamin Weber
 */
public class ContentPanel extends CssLayout {

  private static final long serialVersionUID = 976520401301749942L;

  private Configuration navigationLabels = ServiceRegistry.getConfigurationProxy()
      .getConfiguration(new ConfigurationProperties<>(ContentPanel.class));

  private Panel root;
  private MenuBar menuBar;
  private Navigator navigator;
  private DefaultViewProvider viewProvider = new DefaultViewProvider();
  private Map<String, MenuItem> menuItems = new HashMap<>();

  private Label user;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param ui the UI under which we operate
   */
  public ContentPanel(UI ui) {
    setUp();

    navigator = new Navigator(ui, root);
    navigator.addProvider(viewProvider);

    navigator.addViewChangeListener(new ViewChangeListener() {
      private static final long serialVersionUID = -2040091710507830930L;

      @Override
      public boolean beforeViewChange(ViewChangeEvent event) {
        return true;
      }

      @Override
      public void afterViewChange(ViewChangeEvent event) {
        ApplicationRegistry registry = VaadinSession.getCurrent().getAttribute(ApplicationRegistry.class);
        PersonModel loggedInUser = registry.getLoggedInUser();
        user.setValue(getUserLabelValue(loggedInUser));

        AbstractView newView = (AbstractView) event.getNewView();
        String viewId = newView.getViewId();

        String[] split = viewId.split("/");
        StringBuilder display = new StringBuilder();
        StringBuilder currentId = new StringBuilder();

        boolean first = true;
        for (String part : split) {
          if (first) {
            first = false;
          } else {
            currentId.append("/");
            display.append(" / ");
          }
          currentId.append(part);
          display.append(navigationLabels.getStringValue(currentId.toString()));
        }

        root.setCaption(display.toString());
      }

    });

    ApplicationRegistry.getInstance().setNavigator(navigator);
  }

  private void setUp() {
    ApplicationRegistry registry = VaadinSession.getCurrent().getAttribute(ApplicationRegistry.class);
    PersonModel loggedInUser = registry.getLoggedInUser();

    setSizeFull();

    HorizontalLayout topBar = new HorizontalLayout();
    addComponent(topBar);
    topBar.setWidth("100%");
    topBar.setSpacing(true);
    topBar.setStyleName(VierkantTheme.MARGIN_SLIM);

    Image logo = new Image("", new ThemeResource("images/logo_vknws.png"));
    topBar.addComponent(logo);
    topBar.setExpandRatio(logo, 0.3f);
    logo.addStyleName("logo");

    Label name = new Label(getApplicationName());
    topBar.addComponent(name);
    topBar.setExpandRatio(name, 0.5f);
    topBar.setComponentAlignment(name, Alignment.MIDDLE_LEFT);
    name.setStyleName(VierkantTheme.LABEL_HUMONGOUS);

    user = new Label(getUserLabelValue(loggedInUser));
    topBar.addComponent(user);
    topBar.setExpandRatio(user, 0.2f);
    topBar.setComponentAlignment(user, Alignment.BOTTOM_RIGHT);
    user.setStyleName(VierkantTheme.LABEL_SMALL);
    user.addStyleName(VierkantTheme.MARGIN_SLIM);
    user.addStyleName(VierkantTheme.LABEL_BOLD);
    user.setSizeUndefined();
    user.setContentMode(ContentMode.HTML);

    menuBar = new MenuBar();
    addComponent(menuBar);
    menuBar.setWidth("100%");

    MenuItem logoutItem = menuBar.addItem(navigationLabels.getStringValue(NavigationIds.logout), FontAwesome.SIGN_OUT,
        i -> logout());
    logoutItem.setStyleName(VierkantTheme.RIGHT_ALIGN);

    root = new Panel();
    addComponent(root);
    root.setSizeFull();
    root.setHeight(Page.getCurrent().getBrowserWindowHeight() - 193, Unit.PIXELS);
    Page.getCurrent().addBrowserWindowResizeListener(e -> {
      root.setHeight(Page.getCurrent().getBrowserWindowHeight() - 193, Unit.PIXELS);
    });

    if (loggedInUser != null) {

      boolean board = UserRuleUtils.isOnBoardOfDirectors(loggedInUser);
      if (board) {
        user.addStyleName("board");
      } else if (loggedInUser.getRefRank() != null && loggedInUser.getRefRank().getRankCategory() != null) {
        RankCategory category = loggedInUser.getRefRank().getRankCategory();
        user.addStyleName(category.name().toLowerCase());
      }
    }
  }

  private String getApplicationName() {
    StringBuilder builder = new StringBuilder();

    builder.append(ApplicationConstants.APPLICATION_NAME);
    builder.append(" ");
    builder.append(ConfigurationUtils.getConfiguration(OrganisationConfigurations.ORGANISATION_NAME_SHORT));

    return builder.toString();
  }

  private void logout() {
    VaadinSession.getCurrent().getAttribute(ApplicationRegistry.class).setLoggedInUser(null);

    URI location = Page.getCurrent().getLocation();
    String string = location.toString();
    if (location.getRawFragment() != null) {
      string = string.replace("#" + location.getRawFragment(), "").replaceAll("[?&]passwordResetCode=[a-f0-9\\-]{36}",
          "");
    }
    Page.getCurrent().open(string, null);
  }

  private String getUserLabelValue(PersonModel loggedInUser) {
    if (loggedInUser == null) { return ""; }
    return loggedInUser.toString() + (loggedInUser.getRefPrimaryFunction() != null
        ? ("<br>" + getPrimaryFunctionName(loggedInUser.getRefPrimaryFunction()))
        : "");
  }

  private String getPrimaryFunctionName(OrganizationFunctionModel primaryFunction) {
    return primaryFunction.getFunctionName();
  }

  /**
   * @param view the view to be added to the navigation bar
   */
  public void addNavigation(AbstractView view) {
    AssertUtils.notNull(view);

    if (!view.isUserAllowedToVisit()) { return; }

    String viewId = view.getViewId();

    if (viewId.startsWith(NavigationIds.NO_MENU_PREFIX)) {
      viewId = viewId.substring(1);
    } else {
      String viewIdToUse = viewId;

      MenuItem parent = buildParents(viewId);

      MenuItem menuItem;

      if (parent == null) {
        menuItem = menuBar.addItem(navigationLabels.getStringValue(viewIdToUse), null,
            item -> navigator.navigateTo(viewIdToUse));
      } else {
        menuItem = parent.addItem(navigationLabels.getStringValue(viewIdToUse), null,
            item -> navigator.navigateTo(viewIdToUse));
      }
      menuItems.put(viewIdToUse, menuItem);

    }

    viewProvider.addView(view);
  }

  private MenuItem buildParents(String viewId) {
    String[] split = viewId.split("/");
    StringBuilder currentId = new StringBuilder();

    if (split.length < 2) { return null; }

    currentId.append(split[0]);
    if (!menuItems.containsKey(currentId.toString())) {
      menuItems.put(currentId.toString(), menuBar.addItem(navigationLabels.getStringValue(currentId.toString()), null));
    }
    MenuItem root = menuItems.get(currentId.toString());

    MenuItem current = root;
    for (int i = 1; i < split.length - 1; i++) {
      currentId.append("/");
      currentId.append(split[i]);
      if (!menuItems.containsKey(currentId.toString())) {
        menuItems.put(currentId.toString(),
            current.addItem(navigationLabels.getStringValue(currentId.toString()), null));
      }
      current = menuItems.get(currentId.toString());
    }

    return current;
  }

  /**
   * @param view the view to display if no view is specified
   */
  public void setDefaultView(AbstractView view) {
    viewProvider.setDefaultView(view);
  }

  /**
   * @return the root
   */
  public Panel getRoot() {
    return root;
  }

  /**
   * Refresh the page.
   */
  public void refresh() {
    navigator.navigateTo(navigator.getState());
  }
}
