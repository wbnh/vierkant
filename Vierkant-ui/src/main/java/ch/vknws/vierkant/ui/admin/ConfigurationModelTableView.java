package ch.vknws.vierkant.ui.admin;

import com.vaadin.data.Container.Indexed;

import ch.bwe.fac.v1.type.DeleteException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.ConfigurationContainer;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.type.ConfigurationModel;
import ch.vknws.vierkant.type.impl.ConfigurationPersistable;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.AbstractTableView;
import ch.vknws.vierkant.ui.NavigationIds;
import ch.vknws.vierkant.ui.admin.detail.AdminConfigurationModelDetailView;

/**
 * Table view for the AddressModel.
 *
 * @author Benjamin Weber
 */
public class ConfigurationModelTableView extends AbstractTableView<ConfigurationModel> {

  private static final long serialVersionUID = 4656892595813009356L;

  private static final Object[] COLUMNS = { ConfigurationModel.PropertyId.USER_NAME,
      ConfigurationModel.PropertyId.CONTEXT, ConfigurationModel.PropertyId.KEY_NAME,
      ConfigurationModel.PropertyId.VALUE };

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public ConfigurationModelTableView() {
    super(COLUMNS);

    personAuthorizations.add(SecretaryAuthorizationKeys.ADMIN);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected Indexed createContainer() {
    return new ConfigurationContainer();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected AbstractDetailView<ConfigurationModel> createDetailView(ConfigurationModel selectedModel) {
    if (selectedModel == null) {
      selectedModel = new ConfigurationPersistable();
    }
    return new AdminConfigurationModelDetailView(selectedModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void deleteModelFromDatabase(Integer recordId) throws DeleteException {
    DaoFactory.getInstance().getConfigurationDao().delete(recordId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected ConfigurationModel loadModelFromDatabase(Integer recordId) {
    return DaoFactory.getInstance().getConfigurationDao().load(recordId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return NavigationIds.admin_config;
  }

}
