package ch.vknws.vierkant.ui.admin.detail;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.AddressModel;
import ch.vknws.vierkant.type.caption.AddressPersistableTexts;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.NavigationIds;

/**
 * Detail view for the AddressModel in the Admin section.
 *
 * @author Benjamin Weber
 */
public class AdminAddressModelDetailView extends AbstractDetailView<AddressModel> {

  private static final long serialVersionUID = 3187482689576874439L;

  private AddressPersistableTexts addressModelTexts;

  private TextField line1;
  private TextField line2;
  private TextField zipCode;
  private TextField city;
  private TextField canton;
  private TextField country;
  private TextField recordId;
  private TextField revisionId;

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param displayedModel the model to display
   */
  public AdminAddressModelDetailView(AddressModel displayedModel) {
    super(displayedModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getViewCaption() {
    return getDisplayedModel().toString();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FormLayout setUp() {
    addressModelTexts = ServiceRegistry.getTypedConfigurationProxy()
        .get(new ConfigurationProperties<>(AddressPersistableTexts.class));

    FormLayout layout = new FormLayout();

    layout.setMargin(true);

    recordId = new TextField();
    layout.addComponent(recordId);
    recordId.setCaption(addressModelTexts.recordId());
    recordId.setPropertyDataSource(getDisplayedItem().getItemProperty(DatabaseModel.PropertyId.RECORD_ID));
    recordId.setNullRepresentation("");
    saveGroup.bind(recordId, DatabaseModel.PropertyId.RECORD_ID);

    revisionId = new TextField();
    layout.addComponent(revisionId);
    revisionId.setCaption(addressModelTexts.revisionId());
    revisionId.setPropertyDataSource(getDisplayedItem().getItemProperty(DatabaseModel.PropertyId.REVISION_ID));
    revisionId.setNullRepresentation("");
    saveGroup.bind(revisionId, DatabaseModel.PropertyId.REVISION_ID);

    line1 = new TextField();
    layout.addComponent(line1);
    line1.setCaption(addressModelTexts.line1());
    line1.setPropertyDataSource(getDisplayedItem().getItemProperty(AddressModel.PropertyId.LINE1));
    line1.setNullRepresentation("");
    saveGroup.bind(line1, AddressModel.PropertyId.LINE1);

    line2 = new TextField();
    layout.addComponent(line2);
    line2.setCaption(addressModelTexts.line2());
    line2.setPropertyDataSource(getDisplayedItem().getItemProperty(AddressModel.PropertyId.LINE2));
    line2.setNullRepresentation("");
    saveGroup.bind(line2, AddressModel.PropertyId.LINE2);

    zipCode = new TextField();
    layout.addComponent(zipCode);
    zipCode.setCaption(addressModelTexts.zipCode());
    zipCode.setPropertyDataSource(getDisplayedItem().getItemProperty(AddressModel.PropertyId.ZIP_CODE));
    zipCode.setNullRepresentation("");
    saveGroup.bind(zipCode, AddressModel.PropertyId.ZIP_CODE);

    city = new TextField();
    layout.addComponent(city);
    city.setCaption(addressModelTexts.city());
    city.setPropertyDataSource(getDisplayedItem().getItemProperty(AddressModel.PropertyId.CITY));
    city.setNullRepresentation("");
    saveGroup.bind(city, AddressModel.PropertyId.CITY);

    canton = new TextField();
    layout.addComponent(canton);
    canton.setCaption(addressModelTexts.canton());
    canton.setPropertyDataSource(getDisplayedItem().getItemProperty(AddressModel.PropertyId.CANTON));
    canton.setNullRepresentation("");
    saveGroup.bind(canton, AddressModel.PropertyId.CANTON);

    country = new TextField();
    layout.addComponent(country);
    country.setCaption(addressModelTexts.country());
    country.setPropertyDataSource(getDisplayedItem().getItemProperty(AddressModel.PropertyId.COUNTRY));
    country.setNullRepresentation("");
    saveGroup.bind(country, AddressModel.PropertyId.COUNTRY);

    return layout;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void saveDisplayedModel() throws SaveException {
    DaoFactory.getInstance().getAddressDao().save(getDisplayedModel(),
        ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return NavigationIds.admin_address_details;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();

    applyRecordIdEditability();
    applyRevisionIdEditability();
    applyLine1Editability();
    applyLine2Editability();
    applyZipCodeEditability();
    applyCityEditability();
    applyCantonEditability();
    applyCountryEditability();
  }

  private void applyRecordIdEditability() {
    recordId.setEnabled(false);
  }

  private void applyRevisionIdEditability() {
    revisionId.setEnabled(false);
  }

  private void applyLine1Editability() {
    line1.setEnabled(true);
  }

  private void applyLine2Editability() {
    line2.setEnabled(true);
  }

  private void applyZipCodeEditability() {
    zipCode.setEnabled(true);
  }

  private void applyCityEditability() {
    city.setEnabled(true);
  }

  private void applyCantonEditability() {
    canton.setEnabled(true);
  }

  private void applyCountryEditability() {
    country.setEnabled(true);
  }

}
