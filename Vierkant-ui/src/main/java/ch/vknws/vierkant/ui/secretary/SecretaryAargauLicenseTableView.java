package ch.vknws.vierkant.ui.secretary;

import com.vaadin.data.Container.Indexed;

import ch.bwe.fac.v1.type.DeleteException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.AargauLicenseContainer;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.type.AargauLicenseModel;
import ch.vknws.vierkant.type.impl.AargauLicensePersistable;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.AbstractTableView;
import ch.vknws.vierkant.ui.NavigationIds;

/**
 * Overview over the people for the secretary.
 *
 * @author Benjamin Weber
 *
 */
public class SecretaryAargauLicenseTableView extends AbstractTableView<AargauLicenseModel> {
	private static final long serialVersionUID = 959061418371194537L;
	private static final Object[] COLUMNS = { AargauLicenseModel.PropertyId.LICENSE_NAME,
					AargauLicenseModel.PropertyId.LICENSE_NUMBER };

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public SecretaryAargauLicenseTableView() {
		super(COLUMNS);

		personAuthorizations.add(SecretaryAuthorizationKeys.AARGAU_LICENSE_ANY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Indexed createContainer() {
		return new AargauLicenseContainer();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected AbstractDetailView<AargauLicenseModel> createDetailView(AargauLicenseModel selectedModel) {
		if (selectedModel == null) {
			selectedModel = new AargauLicensePersistable();
		}
		return new SecretaryAargauLicenseDetailsView(selectedModel);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void deleteModelFromDatabase(Integer recordId) throws DeleteException {
		DaoFactory.getInstance().getAargauLicenseDao().delete(recordId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected AargauLicenseModel loadModelFromDatabase(Integer recordId) {
		return DaoFactory.getInstance().getAargauLicenseDao().load(recordId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getViewId() {
		return NavigationIds.secretary_aargaulicense;
	}

}
