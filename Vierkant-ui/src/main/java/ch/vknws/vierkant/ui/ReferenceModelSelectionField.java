package ch.vknws.vierkant.ui;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;

import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Label.ValueChangeEvent;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.type.DAO;
import ch.vknws.vierkant.ui.dialog.TwoButtonViewDialog;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * Field to select reference models.
 *
 * @author Benjamin Weber
 * @param <T> the type to display
 */
public class ReferenceModelSelectionField<T extends DatabaseModel> extends HorizontalLayout {

  private static final long serialVersionUID = -3119236352449405304L;

  /**
   * Button to edit the value.
   */
  protected Button editButton;

  /**
   * Label to display the value.
   */
  protected Label valueLabel;

  /**
   * The value itself.
   */
  protected T value;
  private Class<? extends AbstractTableView<?>> tableClass;

  /**
   * DAO to load the values.
   */
  protected DAO<?> dao;

  /**
   * Table to select the loaded values.
   */
  protected AbstractTableView<?> table;

  private List<ValueChangeListener> valueChangeListeners = new LinkedList<>();

  private TwoButtonViewDialog dialog;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param tableClass the type of model to display
   * @param dao        the DAO to load the values
   */
  public ReferenceModelSelectionField(Class<? extends AbstractTableView<?>> tableClass, DAO<?> dao) {
    this.tableClass = tableClass;
    this.dao = dao;

    setUp();
  }

  private void setUp() {
    setMargin(false);
    setSpacing(true);

    editButton = new Button();
    addComponent(editButton);
    editButton.setIcon(getButtonIcon());
    editButton.addClickListener(this::onEdit);
    editButton.addStyleName(VierkantTheme.BUTTON_ICON_ONLY);

    valueLabel = new Label();
    addComponent(valueLabel);

    setComponentAlignment(valueLabel, Alignment.MIDDLE_LEFT);
  }

  private void onEdit(ClickEvent event) {
    CssLayout layout = new CssLayout();
    layout.setSizeFull();

    table = createTable();
    layout.addComponent(table);
    table.setSizeFull();

    if (value != null) {
      table.getGrid().select(value.getRecordId());
    }

    dialog = new TwoButtonViewDialog();
    dialog.setView(table);
    dialog.getOkButton().addClickListener(this::onSelect);
    dialog.getCancelButton().addClickListener(this::onCancel);

    dialog.open();
  }

  @SuppressWarnings("unchecked")
  private void onSelect(ClickEvent event) {
    Integer selectedId = (Integer) table.getGrid().getSelectedRow();

    if (selectedId == null) {
      setValue(null);
    } else {
      T load = (T) dao.load(selectedId);
      setValue(load);
    }

    dialog.close();
    dialog = null;
  }

  /**
   * @param value the value
   */
  public void setValue(T value) {
    this.value = value;

    setValueLabel(value);

    notifyValueChangeListeners();
  }

  /**
   * @param value the label
   */
  protected void setValueLabel(T value) {
    if (value == null) {
      valueLabel.setValue(null);
    } else {
      valueLabel.setValue(value.toString());
    }
  }

  /**
   * @return the value
   */
  public T getValue() {
    return value;
  }

  private void onCancel(ClickEvent event) {
    dialog.close();
    dialog = null;
  }

  /**
   * @return a new table instance
   */
  protected AbstractTableView<?> createTable() {
    try {
      return tableClass.getConstructor().newInstance();
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
        | NoSuchMethodException | SecurityException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not create table view", e);
      return null;
    }
  }

  private Resource getButtonIcon() {
    if (isEnabled()) { return FontAwesome.PENCIL; }
    return FontAwesome.EYE;
  }

  /**
   * Inform the value change listeners.
   */
  protected void notifyValueChangeListeners() {
    ValueChangeEvent event = new ValueChangeEvent(valueLabel);

    for (ValueChangeListener valueChangeListener : valueChangeListeners) {
      valueChangeListener.valueChange(event);
    }
  }

  /**
   * @param listener the listener to be invoke on value changes
   */
  public void addValueChangeListener(ValueChangeListener listener) {
    valueChangeListeners.add(listener);
  }

  /**
   * @param listener the listener to be removed
   */
  public void removeValueChangeListener(ValueChangeListener listener) {
    valueChangeListeners.remove(listener);
  }
}
