package ch.vknws.vierkant.ui.field.converter;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.util.converter.Converter;

/**
 * Converter for string lists.
 *
 * @author Benjamin Weber
 *
 */
@SuppressWarnings("rawtypes")
public class StringListStringConverter implements Converter<String, List> {
	private static final long serialVersionUID = -2223711132943672760L;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> convertToModel(String value, Class<? extends List> targetType, Locale locale)
					throws com.vaadin.data.util.converter.Converter.ConversionException {
		String[] values = value.split("\n");
		return Arrays.asList(values);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String convertToPresentation(List value, Class<? extends String> targetType, Locale locale)
					throws com.vaadin.data.util.converter.Converter.ConversionException {
		StringBuilder builder = new StringBuilder();

		for (Object string : value) {
			if (builder.length() > 0) {
				builder.append("\n");
			}
			builder.append(string);
		}

		return builder.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<List> getModelType() {
		return List.class;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<String> getPresentationType() {
		return String.class;
	}

}
