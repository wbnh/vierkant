package ch.vknws.vierkant.ui.secretary;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.AuthorizationKeyContainer;
import ch.vknws.vierkant.type.FunctionGroupModel;
import ch.vknws.vierkant.type.FunctionType;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.caption.OrganizationFunctionPersistableTexts;
import ch.vknws.vierkant.type.impl.AuthorizationKeyPersistable;
import ch.vknws.vierkant.type.impl.FunctionGroupPersistable;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.search.FunctionColumnSearchView;
import ch.vknws.vierkant.ui.search.FunctionGroupColumnSearchView;

/**
 * Details view of a person for the secretary.
 *
 * @author Benjamin Weber
 */
public class SecretaryFunctionDetailsView extends AbstractDetailView<OrganizationFunctionModel> {
  private static final String FUNCTION = "function";
  private static final String FUNCTION_GROUP = "functionGroup";

  private static final long serialVersionUID = -7589784662167949340L;

  private static final OrganizationFunctionPersistableTexts functionTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(OrganizationFunctionPersistableTexts.class));

  private ComboBox groupWrapped;
  private FunctionGroupModel displayedGroup;

  private TextField functionName;
  private TextField email;
  private TextField minPersonCount;
  private TextField maxPersonCount;
  private ComboBox type;
  private FunctionColumnSearchView superiors;
  private ComboBox authorizationKey;
  private FunctionGroupColumnSearchView group;

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param displayedModel the model to display
   */
  protected SecretaryFunctionDetailsView(OrganizationFunctionModel displayedModel) {
    super(displayedModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getViewCaption() {
    return getDisplayedModel().toString();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FormLayout setUp() {

    displayedGroup = DaoFactory.getInstance().getFunctionGroupDao().findFunctionGroup(getDisplayedModel());
    if (displayedGroup == null) {
      displayedGroup = new FunctionGroupPersistable();
      displayedGroup.setRefDetails(getDisplayedModel());
    } else {
      getDisplayedModel().setRefAuthorizationKey(displayedGroup.getRefAuthorizationKey());
    }

    FormLayout layout = new FormLayout();
    layout.setMargin(true);

    groupWrapped = new ComboBox();
    layout.addComponent(groupWrapped);
    groupWrapped.addItems(FUNCTION_GROUP, FUNCTION);
    groupWrapped.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    groupWrapped.setItemCaption(FUNCTION_GROUP, "Funktionsgruppe");
    groupWrapped.setItemCaption(FUNCTION, "Funktion");
    groupWrapped.setNullSelectionAllowed(false);
    groupWrapped.setTextInputAllowed(false);
    groupWrapped.setNewItemsAllowed(false);

    if (displayedGroup.getRecordId() == null) {
      groupWrapped.select(FUNCTION);
    } else {
      groupWrapped.select(FUNCTION_GROUP);
    }

    groupWrapped.addValueChangeListener(e -> executeRules());

    functionName = createTextField(functionTexts.functionName());
    layout.addComponent(functionName);
    saveGroup.bind(functionName, OrganizationFunctionModel.PropertyId.FUNCTION_NAME);

    email = createTextField(functionTexts.email());
    layout.addComponent(email);
    email.addValidator(new EmailValidator(""));
    saveGroup.bind(email, OrganizationFunctionModel.PropertyId.EMAIL);

    minPersonCount = createTextField(functionTexts.minPersonCount());
    layout.addComponent(minPersonCount);
    saveGroup.bind(minPersonCount, OrganizationFunctionModel.PropertyId.MIN_PERSON_COUNT);

    maxPersonCount = createTextField(functionTexts.maxPersonCount());
    layout.addComponent(maxPersonCount);
    saveGroup.bind(maxPersonCount, OrganizationFunctionModel.PropertyId.MAX_PERSON_COUNT);

    type = createComboBox(functionTexts.type());
    layout.addComponent(type);
    type.setTextInputAllowed(true);
    type.setNullSelectionAllowed(false);
    type.setNewItemsAllowed(false);
    type.setContainerDataSource(createTypeContainer());
    saveGroup.bind(type, OrganizationFunctionModel.PropertyId.TYPE);

    superiors = new FunctionColumnSearchView(Arrays.asList(getDisplayedModel().getRecordId()));
    layout.addComponent(superiors);
    superiors.setCaption(functionTexts.superiors());
    BeanItemContainer<OrganizationFunctionModel> superiorContainer = new BeanItemContainer<>(
        OrganizationFunctionModel.class);
    superiorContainer.addAll(getDisplayedModel().getRefSuperiors());
    superiors.getSelect().setContainerDataSource(superiorContainer);
    superiors.getSelect().setValue(getDisplayedModel().getRefSuperiors());
    superiors.getSelect().setItemCaptionMode(ItemCaptionMode.ID_TOSTRING);
    superiors.getSelect().addValueChangeListener(this::handleSuperiorSelect);

    authorizationKey = createComboBox(functionTexts.authorizationKey());
    layout.addComponent(authorizationKey);
    authorizationKey.setNullSelectionAllowed(true);
    authorizationKey.setTextInputAllowed(true);
    authorizationKey.setNewItemsAllowed(false);
    authorizationKey.setContainerDataSource(new AuthorizationKeyContainer());
    authorizationKey.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    authorizationKey.setItemCaptionPropertyId(AuthorizationKeyPersistable.PropertyId.KEY_NAME);
    saveGroup.bind(authorizationKey, OrganizationFunctionModel.PropertyId.REF_AUTHORIZATION_KEY);

    group = new FunctionGroupColumnSearchView();
    layout.addComponent(group);
    group.setCaption(functionTexts.functionGroup());
    BeanItemContainer<FunctionGroupModel> groupContainer = new BeanItemContainer<>(FunctionGroupModel.class);
    groupContainer.addAll(getDisplayedModel().getRefGroups());
    group.getSelect().setContainerDataSource(groupContainer);
    group.getSelect().setValue(getDisplayedModel().getRefGroups());
    group.getSelect().setItemCaptionMode(ItemCaptionMode.ID_TOSTRING);
    group.getSelect().addValueChangeListener(this::handleGroupSelect);

    return layout;
  }

  private void handleSuperiorSelect(ValueChangeEvent event) {
    @SuppressWarnings("unchecked")
    Set<OrganizationFunctionModel> value = (Set<OrganizationFunctionModel>) event.getProperty().getValue();
    getDisplayedModel().setRefSuperiors(value);
  }

  private void handleGroupSelect(ValueChangeEvent event) {
    @SuppressWarnings("unchecked")
    Set<FunctionGroupModel> value = (Set<FunctionGroupModel>) event.getProperty().getValue();
    getDisplayedModel().setRefGroups(value);
  }

  private BeanItemContainer<FunctionType> createTypeContainer() {
    BeanItemContainer<FunctionType> container = new BeanItemContainer<>(FunctionType.class);

    container.addAll(Arrays.asList(FunctionType.values()));

    return container;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();

    applyGroupVisibility();
  }

  private void applyGroupVisibility() {
    if (group == null) { return; }
    boolean visible = !isFunctionGroup();

    group.setVisible(visible);
    if (!visible) {
      getDisplayedModel().setRefGroups(new HashSet<>());
    }
  }

  private boolean isFunctionGroup() {
    return FUNCTION_GROUP.equals(groupWrapped.getValue());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void saveDisplayedModel() throws SaveException {

    if (isFunctionGroup()) {
      saveGroup.unbind(authorizationKey);
      displayedGroup.setRefAuthorizationKey(getDisplayedModel().getRefAuthorizationKey());
      getDisplayedModel().setRefAuthorizationKey(null);
      DaoFactory.getInstance().getFunctionGroupDao().save(displayedGroup, getLoggedInUser().getRecordId());
      saveGroup.bind(authorizationKey, OrganizationFunctionModel.PropertyId.REF_AUTHORIZATION_KEY);
    } else {
      DaoFactory.getInstance().getOrganizationFunctionDao().save(getDisplayedModel(), getLoggedInUser().getRecordId());
    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

}
