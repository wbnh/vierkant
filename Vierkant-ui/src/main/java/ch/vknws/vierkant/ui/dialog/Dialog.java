package ch.vknws.vierkant.ui.dialog;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.vknws.vierkant.ui.ErrorMessages;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * A dialog for the user to interact with.
 * 
 * @author benjamin
 */
public abstract class Dialog extends Window {
  private static final long serialVersionUID = 780599755522034300L;
  private VerticalLayout layout;
  private HorizontalLayout buttonBar;

  /**
   * Closes the dialog.
   */
  public final ClickListener closeListener = e -> close();

  /**
   * The texts.
   */
  protected static DialogTexts dialogTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(DialogTexts.class));

  /**
   * The error messages
   */
  protected static final ErrorMessages errorMessages = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(ErrorMessages.class));

  /**
   * Constructor handling initialization of mandatory fields.
   */
  protected Dialog() {
    super();

    setResizable(false);
    setClosable(false);
    addStyleName("dialog");
    center();
    setModal(true);

    layout = new VerticalLayout();
    setContent(layout);
    layout.addStyleName(VierkantTheme.MARGIN_SLIM);

    Layout content = setUp();
    content.addStyleName("content");
    content.setSizeFull();
    layout.addComponent(content);

    HorizontalLayout buttonBarWrapper = new HorizontalLayout();
    layout.addComponent(buttonBarWrapper);
    buttonBarWrapper.setWidth("100%");
    buttonBarWrapper.setDefaultComponentAlignment(Alignment.BOTTOM_RIGHT);
    buttonBarWrapper.addStyleName(VierkantTheme.MARGIN_TOP_SLIM);

    buttonBar = new HorizontalLayout();
    buttonBarWrapper.addComponent(buttonBar);
    buttonBar.setSpacing(true);

    setUpButtons();
  }

  /**
   * Set up the content.
   * 
   * @return the top-level content layout.
   */
  protected abstract CssLayout setUp();

  /**
   * Set up the dialog buttons.
   */
  protected abstract void setUpButtons();

  /**
   * @return the layout
   */
  public VerticalLayout getLayout() {
    return layout;
  }

  /**
   * @return the buttonBar
   */
  public HorizontalLayout getButtonBar() {
    return buttonBar;
  }

  /**
   * Open the dialog in the current window.
   */
  public void open() {
    UI.getCurrent().addWindow(this);
  }
}
