package ch.vknws.vierkant.ui.dialog;

import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.UI;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.AuditCommentMode;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * Utility to create and show the most important dialogs.
 *
 * @author Benjamin Weber
 */
public class DialogUtils {

  private static DialogTexts dialogTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(DialogTexts.class));

  /**
   * Shows a dialog to confirm the deletion.
   * 
   * @param modelName      the model to delete
   * @param deleteListener the listener to be invoke upon confirmation
   */
  public static void showDeleteConfirmDialog(String modelName, ClickListener deleteListener) {
    TwoButtonDialog dialog = new TwoButtonDialog(dialogTexts.deleteTitle(modelName), dialogTexts.deleteText(modelName),
        dialogTexts.delete(), dialogTexts.cancel());
    dialog.getOkButton().addClickListener(deleteListener);
    dialog.getOkButton().addStyleName(VierkantTheme.BUTTON_DANGER);

    UI.getCurrent().addWindow(dialog);
  }

  /**
   * Shows a dialog to confirm to leave the organisation.
   * 
   * @param modelName      the model that wants to leave
   * @param deleteListener the listener to invoke upon confirmation
   */
  public static void showLeaveOrganizationConfirmDialog(String modelName, ClickListener deleteListener) {
    TwoButtonDialog dialog = new TwoButtonDialog(dialogTexts.leaveOrganizationTitle(),
        dialogTexts.leaveOrganizationText(modelName), dialogTexts.leaveOrganization(), dialogTexts.cancel());
    dialog.getOkButton().addClickListener(deleteListener);
    dialog.getOkButton().addStyleName(VierkantTheme.BUTTON_DANGER);

    UI.getCurrent().addWindow(dialog);
  }

  /**
   * Show a dialog that an error occurred.
   * 
   * @param title   the error title
   * @param message the error message
   * @param problem the problem
   */
  public static void showErrorDialog(String title, String message, Throwable problem) {
    ErrorDialog errorDialog = new ErrorDialog(title, message, problem);
    UI.getCurrent().addWindow(errorDialog);
  }

  /**
   * Show an audit comment dialog
   * 
   * @param persistable  the model to add the comment to
   * @param saveListener the listener to be invoked upon submission
   */
  public static void showAuditCommentDialog(DatabaseModel persistable, ClickListener saveListener) {
    AuditCommentMode auditCommentMode = ApplicationRegistry.getInstance().getAuditCommentMode();
    if (AuditCommentMode.DISABLED.equals(auditCommentMode)) {
      saveListener.buttonClick(null);
      return;
    }

    String title = dialogTexts.auditCommentTitle();
    String message;
    if (AuditCommentMode.MANDATORY.equals(auditCommentMode)) {
      message = dialogTexts.auditCommentTextMandatory();
    } else {
      message = dialogTexts.auditCommentTextOptional();
    }

    TwoButtonQueryDialog dialog = new TwoButtonQueryDialog(title, message, TwoButtonQueryDialog.FieldMode.TEXTAREA);
    dialog.getTextArea().focus();

    if (AuditCommentMode.MANDATORY.equals(auditCommentMode)) {
      dialog.getOkButton().setEnabled(false);
      dialog.getTextArea()
          .addValueChangeListener(e -> dialog.getOkButton().setEnabled(e.getProperty().getValue() != null));
    }

    dialog.getOkButton().addClickListener(e -> {
      persistable.setAuditComment(dialog.getTextArea().getValue());
      saveListener.buttonClick(e);
    });

    dialog.open();
  }
}
