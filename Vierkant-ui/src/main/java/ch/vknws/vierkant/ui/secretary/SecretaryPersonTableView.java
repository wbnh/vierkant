package ch.vknws.vierkant.ui.secretary;

import com.vaadin.data.Container.Indexed;

import ch.bwe.fac.v1.type.DeleteException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.PersonContainer;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.impl.PersonPersistable;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.AbstractTableView;
import ch.vknws.vierkant.ui.NavigationIds;

/**
 * Overview over the people for the secretary.
 *
 * @author Benjamin Weber
 *
 */
public class SecretaryPersonTableView extends AbstractTableView<PersonModel> {
	private static final long serialVersionUID = 959061418371194537L;
	private static final Object[] COLUMNS = { PersonModel.PropertyId.SHORTHAND, PersonModel.PropertyId.GIVEN_NAME,
					PersonModel.PropertyId.FAMILY_NAME, PersonModel.PropertyId.REF_RANK };

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public SecretaryPersonTableView() {
		super(COLUMNS);
		personAuthorizations.add(SecretaryAuthorizationKeys.PERSON_ANY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Indexed createContainer() {
		return new PersonContainer();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected AbstractDetailView<PersonModel> createDetailView(PersonModel selectedModel) {
		if (selectedModel == null) {
			selectedModel = new PersonPersistable();
		}
		return new SecretaryPersonDetailsView(selectedModel);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void deleteModelFromDatabase(Integer recordId) throws DeleteException {
		DaoFactory.getInstance().getPersonDao().delete(recordId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected PersonModel loadModelFromDatabase(Integer recordId) {
		return DaoFactory.getInstance().getPersonDao().load(recordId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean isDeletable(Integer recordId) {
		return DaoFactory.getInstance().getPersonDao().isDeletable(recordId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getViewId() {
		return NavigationIds.secretary_person;
	}

}
