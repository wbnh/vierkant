package ch.vknws.vierkant.ui.dialog;

import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;

import ch.vknws.vierkant.ui.AbstractView;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * Dialog with one button.
 *
 * @author Benjamin Weber
 *
 */
public class OneButtonViewDialog extends Dialog {

	private static final long serialVersionUID = 235138174869336896L;
	private Button closeButton;
	private AbstractView view;
	private CssLayout root;

	/**
	 * Constructor handling initialization of mandatory fields.
	 */
	public OneButtonViewDialog() {
		super();

		setResizable(true);
	}

	/**
	 * @param view
	 *          the view to set
	 */
	public void setView(AbstractView view) {
		if (this.view != null)
			root.removeComponent(this.view);

		this.view = view;
		this.view.setSizeFull();

		if (this.view != null)
			root.addComponent(this.view);
	}

	/**
	 * @return the view
	 */
	public AbstractView getView() {
		return view;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected CssLayout setUp() {
		root = new CssLayout();
		return root;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void setUpButtons() {

		closeButton = new Button();
		getButtonBar().addComponent(closeButton);
		closeButton.addClickListener(closeListener);
		closeButton.setCaption(dialogTexts.close());
		closeButton.addStyleName(VierkantTheme.BUTTON_LINK);
	}

	/**
	 * @return the okButton
	 */
	public Button getOkButton() {
		return closeButton;
	}

}
