package ch.vknws.vierkant.ui.search;

/**
 * Search captions.
 *
 * @author Benjamin Weber
 */
public interface SearchTexts {

  /**
   * @return the text
   */
  String availableColumn();

  /**
   * @return the text
   */
  String selectedColumn();
}
