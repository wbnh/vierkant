package ch.vknws.vierkant.ui.secretary;

import java.util.Optional;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.MeetingTypeContainer;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.rules.rights.AuthorizationRules;
import ch.vknws.vierkant.type.AuthorizationKeyMode;
import ch.vknws.vierkant.type.MeetingModel;
import ch.vknws.vierkant.type.MeetingTypeModel;
import ch.vknws.vierkant.type.caption.MeetingPersistableTexts;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.field.converter.MeetingTypeModelConverter;

/**
 * Details view of a person for the secretary.
 *
 * @author Benjamin Weber
 */
public class MeetingDetailsView extends AbstractDetailView<MeetingModel> {
  private static final long serialVersionUID = -7589784662167949340L;

  private static final MeetingPersistableTexts meetingTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(MeetingPersistableTexts.class));

  private TextField meetingName;

  private TextArea description;

  private ComboBox refMeetingType;

  private DateField start;

  private DateField end;

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param displayedModel the model to display
   */
  protected MeetingDetailsView(MeetingModel displayedModel) {
    super(displayedModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getViewCaption() {
    return getDisplayedModel().toString();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FormLayout setUp() {

    FormLayout layout = new FormLayout();
    layout.setMargin(true);

    meetingName = createTextField(meetingTexts.meetingName());
    layout.addComponent(meetingName);
    saveGroup.bind(meetingName, MeetingModel.PropertyId.MEETING_NAME);

    description = createTextArea(meetingTexts.description());
    layout.addComponent(description);
    saveGroup.bind(description, MeetingModel.PropertyId.DESCRIPTION);

    refMeetingType = createComboBox(meetingTexts.refMeetingType());
    layout.addComponent(refMeetingType);
    refMeetingType.setContainerDataSource(new MeetingTypeContainer());
    refMeetingType.setTextInputAllowed(true);
    refMeetingType.setNullSelectionAllowed(false);
    refMeetingType.setFilteringMode(FilteringMode.CONTAINS);
    refMeetingType.setNewItemsAllowed(false);
    refMeetingType.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    refMeetingType.setConverter(new MeetingTypeModelConverter());
    refMeetingType.setItemCaptionPropertyId(MeetingTypeModel.PropertyId.TYPE_NAME);
    saveGroup.bind(refMeetingType, MeetingModel.PropertyId.REF_MEETING_TYPE);

    start = createDateTimeField(meetingTexts.start());
    layout.addComponent(start);
    saveGroup.bind(start, MeetingModel.PropertyId.START);

    end = createDateTimeField(meetingTexts.end());
    layout.addComponent(end);
    saveGroup.bind(end, MeetingModel.PropertyId.END);

    return layout;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeRules() {
    super.executeRules();
    if (!isInitFinished()) { return; }

    applyDataEditability();
  }

  private void applyDataEditability() {
    Optional<AuthorizationKeyMode> ownAuthorization = AuthorizationRules.getInstance()
        .hasAuthorization(SecretaryAuthorizationKeys.MEETING_OWN, getLoggedInUser(), AuthorizationKeyMode.WRITE);

    Optional<AuthorizationKeyMode> anyAuthorization = AuthorizationRules.getInstance()
        .hasAuthorization(SecretaryAuthorizationKeys.MEETING_ANY, getLoggedInUser(), AuthorizationKeyMode.WRITE);
    boolean editable = anyAuthorization.isPresent()
        || (ownAuthorization.isPresent() && getLoggedInUser().equals(getDisplayedModel().getRefResponsible()));

    meetingName.setEnabled(editable);
    description.setEnabled(editable);
    refMeetingType.setEnabled(editable);
    start.setEnabled(editable);
    end.setEnabled(editable);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void saveDisplayedModel() throws SaveException {
    DaoFactory.getInstance().getMeetingDao().save(getDisplayedModel(),
        ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

}
