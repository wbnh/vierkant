package ch.vknws.vierkant.ui.field.converter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

import com.vaadin.data.util.converter.Converter;

/**
 * Converter to convert between Date and LocalDate.
 *
 * @author Benjamin Weber
 *
 */
public class LocalDateTimeConverter implements Converter<Date, LocalDateTime> {

	private static final long serialVersionUID = 2019360816123482369L;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LocalDateTime convertToModel(Date value, Class<? extends LocalDateTime> targetType, Locale locale)
					throws com.vaadin.data.util.converter.Converter.ConversionException {
		if (value == null) {
			return null;
		}
		return value.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Date convertToPresentation(LocalDateTime value, Class<? extends Date> targetType, Locale locale)
					throws com.vaadin.data.util.converter.Converter.ConversionException {
		if (value == null) {
			return null;
		}
		return Date.from(value.atZone(ZoneId.systemDefault()).toInstant());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<LocalDateTime> getModelType() {
		return LocalDateTime.class;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<Date> getPresentationType() {
		return Date.class;
	}

}
