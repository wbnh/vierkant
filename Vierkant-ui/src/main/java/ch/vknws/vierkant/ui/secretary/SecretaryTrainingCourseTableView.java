package ch.vknws.vierkant.ui.secretary;

import com.vaadin.data.Container.Indexed;

import ch.bwe.fac.v1.type.DeleteException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.TrainingCourseContainer;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.type.TrainingCourseModel;
import ch.vknws.vierkant.type.impl.TrainingCoursePersistable;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.AbstractTableView;
import ch.vknws.vierkant.ui.NavigationIds;

/**
 * Overview over the people for the secretary.
 *
 * @author Benjamin Weber
 *
 */
public class SecretaryTrainingCourseTableView extends AbstractTableView<TrainingCourseModel> {
	private static final long serialVersionUID = 959061418371194537L;
	private static final Object[] COLUMNS = { TrainingCourseModel.PropertyId.TRAINING_COURSE_NAME };

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public SecretaryTrainingCourseTableView() {
		super(COLUMNS);

		personAuthorizations.add(SecretaryAuthorizationKeys.TRAINING_COURSE_ANY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Indexed createContainer() {
		return new TrainingCourseContainer();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected AbstractDetailView<TrainingCourseModel> createDetailView(TrainingCourseModel selectedModel) {
		if (selectedModel == null) {
			selectedModel = new TrainingCoursePersistable();
		}
		return new SecretaryTrainingCourseDetailsView(selectedModel);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void deleteModelFromDatabase(Integer recordId) throws DeleteException {
		DaoFactory.getInstance().getTrainingCourseDao().delete(recordId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected TrainingCourseModel loadModelFromDatabase(Integer recordId) {
		return DaoFactory.getInstance().getTrainingCourseDao().load(recordId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getViewId() {
		return NavigationIds.secretary_trainingcourse;
	}

}
