package ch.vknws.vierkant.ui.admin.detail;

/**
 * Captions for detail views.
 *
 * @author Benjamin Weber
 */
public interface DetailViewTexts {

  /**
   * @return the text
   */
  String save();

  /**
   * @return the text
   */
  String cancel();
}
