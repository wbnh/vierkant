package ch.vknws.vierkant.ui.dialog;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;

import ch.vknws.vierkant.ui.AbstractView;
import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * Dialog with two buttons.
 *
 * @author Benjamin Weber
 */
public class TwoButtonViewDialog extends Dialog {

  private static final long serialVersionUID = 235138174869336896L;
  private Button okButton;
  private Button cancelButton;

  private AbstractView view;
  private CssLayout root;
  /**
   * The listener for closing.
   */
  protected ClickListener clickCloseListener;

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public TwoButtonViewDialog() {
    super();
  }

  /**
   * @param view the view to set
   */
  public void setView(AbstractView view) {
    if (this.view != null) root.removeComponent(this.view);

    this.view = view;
    this.view.setSizeFull();

    if (this.view != null) root.addComponent(this.view);
  }

  /**
   * @return the view
   */
  public AbstractView getView() {
    return view;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected CssLayout setUp() {
    root = new CssLayout();
    return root;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void setUpButtons() {
    okButton = new Button();
    getButtonBar().addComponent(okButton);
    okButton.addClickListener(closeListener);
    okButton.setCaption(dialogTexts.ok());
    okButton.addStyleName(VierkantTheme.BUTTON_PRIMARY);

    cancelButton = new Button();
    getButtonBar().addComponent(cancelButton);
    cancelButton.addClickListener(closeListener);
    cancelButton.setCaption(dialogTexts.cancel());
    cancelButton.addStyleName(VierkantTheme.BUTTON_LINK);
  }

  /**
   * @return the okButton
   */
  public Button getOkButton() {
    return okButton;
  }

  /**
   * @return the cancelButton
   */
  public Button getCancelButton() {
    return cancelButton;
  }

}
