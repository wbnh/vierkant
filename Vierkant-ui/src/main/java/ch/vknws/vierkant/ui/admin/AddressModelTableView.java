package ch.vknws.vierkant.ui.admin;

import com.vaadin.data.Container.Indexed;

import ch.bwe.fac.v1.type.DeleteException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.AddressContainer;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.type.AddressModel;
import ch.vknws.vierkant.type.impl.AddressPersistable;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.AbstractTableView;
import ch.vknws.vierkant.ui.NavigationIds;
import ch.vknws.vierkant.ui.admin.detail.AdminAddressModelDetailView;

/**
 * Table view for the AddressModel.
 *
 * @author Benjamin Weber
 */
public class AddressModelTableView extends AbstractTableView<AddressModel> {

  private static final long serialVersionUID = 4656892595813009356L;

  private static final Object[] COLUMNS = { AddressModel.PropertyId.LINE1, AddressModel.PropertyId.LINE2,
      AddressModel.PropertyId.ZIP_CODE, AddressModel.PropertyId.CITY, AddressModel.PropertyId.CANTON,
      AddressModel.PropertyId.COUNTRY };

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public AddressModelTableView() {
    super(COLUMNS);

    personAuthorizations.add(SecretaryAuthorizationKeys.ADMIN);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected Indexed createContainer() {
    return new AddressContainer();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected AbstractDetailView<AddressModel> createDetailView(AddressModel selectedModel) {
    if (selectedModel == null) {
      selectedModel = new AddressPersistable();
    }
    return new AdminAddressModelDetailView(selectedModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void deleteModelFromDatabase(Integer recordId) throws DeleteException {
    DaoFactory.getInstance().getAddressDao().delete(recordId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected AddressModel loadModelFromDatabase(Integer recordId) {
    return DaoFactory.getInstance().getAddressDao().load(recordId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return NavigationIds.admin_address;
  }

}
