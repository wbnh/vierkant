package ch.vknws.vierkant.ui.secretary;

import com.vaadin.data.Container.Indexed;

import ch.bwe.fac.v1.type.DeleteException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.MeetingContainer;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.type.MeetingModel;
import ch.vknws.vierkant.type.impl.MeetingPersistable;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.AbstractTableView;
import ch.vknws.vierkant.ui.NavigationIds;

/**
 * Overview over the people for the secretary.
 *
 * @author Benjamin Weber
 */
public class MeetingTableView extends AbstractTableView<MeetingModel> {
  private static final long serialVersionUID = 959061418371194537L;
  private static final Object[] COLUMNS = { MeetingModel.PropertyId.MEETING_NAME, MeetingModel.PropertyId.START };

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public MeetingTableView() {
    super(COLUMNS);

    personAuthorizations.add(SecretaryAuthorizationKeys.MEETING_OWN);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected Indexed createContainer() {
    return new MeetingContainer();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected AbstractDetailView<MeetingModel> createDetailView(MeetingModel selectedModel) {
    if (selectedModel == null) {
      selectedModel = new MeetingPersistable();
    }
    return new MeetingDetailsView(selectedModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void deleteModelFromDatabase(Integer recordId) throws DeleteException {
    DaoFactory.getInstance().getMeetingDao().delete(recordId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected MeetingModel loadModelFromDatabase(Integer recordId) {
    return DaoFactory.getInstance().getMeetingDao().load(recordId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return NavigationIds.meeting_meetings;
  }

}
