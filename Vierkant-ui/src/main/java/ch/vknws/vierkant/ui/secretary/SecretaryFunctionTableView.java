package ch.vknws.vierkant.ui.secretary;

import com.vaadin.data.Container.Indexed;

import ch.bwe.fac.v1.type.DeleteException;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.data.OrganizationFunctionContainer;
import ch.vknws.vierkant.rules.module.SecretaryAuthorizationKeys;
import ch.vknws.vierkant.type.FunctionGroupModel;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.impl.OrganizationFunctionPersistable;
import ch.vknws.vierkant.ui.AbstractDetailView;
import ch.vknws.vierkant.ui.AbstractTableView;
import ch.vknws.vierkant.ui.NavigationIds;

/**
 * Overview over the people for the secretary.
 *
 * @author Benjamin Weber
 */
public class SecretaryFunctionTableView extends AbstractTableView<OrganizationFunctionModel> {
  private static final long serialVersionUID = 959061418371194537L;
  private static final Object[] COLUMNS = { OrganizationFunctionModel.PropertyId.FUNCTION_NAME,
      OrganizationFunctionModel.PropertyId.EMAIL };

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public SecretaryFunctionTableView() {
    super(COLUMNS);

    personAuthorizations.add(SecretaryAuthorizationKeys.FUNCTION_ANY);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected Indexed createContainer() {
    return new OrganizationFunctionContainer();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected AbstractDetailView<OrganizationFunctionModel> createDetailView(OrganizationFunctionModel selectedModel) {
    if (selectedModel == null) {
      selectedModel = new OrganizationFunctionPersistable();
    }
    return new SecretaryFunctionDetailsView(selectedModel);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void deleteModelFromDatabase(Integer recordId) throws DeleteException {
    DaoFactory.getInstance().getOrganizationFunctionDao().delete(recordId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected boolean isDeletable(Integer recordId) {
    OrganizationFunctionModel function = DaoFactory.getInstance().getOrganizationFunctionDao().load(recordId);
    if (function != null) {
      FunctionGroupModel functionGroup = DaoFactory.getInstance().getFunctionGroupDao().findFunctionGroup(function);
      if (functionGroup != null) { return functionGroup.getRefGroupMembers() == null
          || functionGroup.getRefGroupMembers().isEmpty(); }
      return true;
    }
    return false;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected OrganizationFunctionModel loadModelFromDatabase(Integer recordId) {
    return DaoFactory.getInstance().getOrganizationFunctionDao().load(recordId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return NavigationIds.secretary_function;
  }

}
