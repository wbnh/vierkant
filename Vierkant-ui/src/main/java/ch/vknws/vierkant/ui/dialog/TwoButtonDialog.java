package ch.vknws.vierkant.ui.dialog;

import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;

import ch.vknws.vierkant.ui.theme.VierkantTheme;

/**
 * Dialog with two buttons.
 * 
 * @author benjamin
 */
public class TwoButtonDialog extends Dialog {
  private static final long serialVersionUID = 780599755522034300L;
  private Button okButton;
  private Button cancelButton;
  private Label label;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param title         the title to display
   * @param message       the message to display
   * @param okCaption     the custom caption for the OK button
   * @param cancelCaption the custom caption for the cancel button
   */
  public TwoButtonDialog(String title, String message, String okCaption, String cancelCaption) {
    super();

    setCaption(title);
    getLabel().setValue(message);
    okButton.setCaption(okCaption);
    cancelButton.setCaption(cancelCaption);
  }

  /**
   * @return the okButton
   */
  public Button getOkButton() {
    return okButton;
  }

  /**
   * @return the cancelButton
   */
  public Button getCancelButton() {
    return cancelButton;
  }

  /**
   * @return the label
   */
  public Label getLabel() {
    return label;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected CssLayout setUp() {
    CssLayout layout = new CssLayout();

    label = new Label();
    getLayout().addComponent(label);

    return layout;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void setUpButtons() {

    okButton = new Button();
    getButtonBar().addComponent(okButton);
    okButton.addClickListener(closeListener);
    okButton.addStyleName(VierkantTheme.BUTTON_PRIMARY);

    cancelButton = new Button();
    getButtonBar().addComponent(cancelButton);
    cancelButton.addClickListener(closeListener);
    cancelButton.addStyleName(VierkantTheme.BUTTON_LINK);
  }
}
