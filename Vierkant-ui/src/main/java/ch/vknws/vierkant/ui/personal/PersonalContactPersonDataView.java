package ch.vknws.vierkant.ui.personal;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.fac.v1.type.SaveException;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.ConjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.bwe.fac.v1.type.restriction.GreaterEqualRestriction;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.AddressModel;
import ch.vknws.vierkant.type.ContactPersonModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.SexType;
import ch.vknws.vierkant.type.caption.AddressPersistableTexts;
import ch.vknws.vierkant.type.caption.ContactPersonPersistableTexts;
import ch.vknws.vierkant.type.caption.PersonPersistableTexts;
import ch.vknws.vierkant.type.impl.AddressPersistable;
import ch.vknws.vierkant.type.impl.PersonPersistable;
import ch.vknws.vierkant.ui.AbstractDetailView;

/**
 * View to display contact persons.
 *
 * @author Benjamin Weber
 */
public class PersonalContactPersonDataView extends AbstractDetailView<ContactPersonModel> {

  private static final long serialVersionUID = -1248333016994085781L;
  private static final ContactPersonPersistableTexts contactPersonTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(ContactPersonPersistableTexts.class));
  private static final PersonPersistableTexts personTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(PersonPersistableTexts.class));
  private static final AddressPersistableTexts addressTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(AddressPersistableTexts.class));

  private PersonModel personModel;

  private TextField relationship;
  private ComboBox sex;
  private TextField title;
  private TextField givenName;
  private TextField familyName;
  private TextField telephone;
  private TextField mobile;
  private TextField emailPrivate;
  private TextField line1;
  private TextField line2;
  private TextField zipCode;
  private TextField city;
  private TextField canton;
  private TextField country;
  private BeanFieldGroup<PersonModel> personGroup;
  private BeanFieldGroup<AddressModel> addressGroup;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param displayedModel the model to display
   * @param personModel    the model whose contacts we're working on
   */
  public PersonalContactPersonDataView(ContactPersonModel displayedModel, PersonModel personModel) {
    super(displayedModel);

    this.personModel = personModel;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected String getViewCaption() {
    return getDisplayedModel().toString();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setDisplayedModel(ContactPersonModel displayedModel) {
    if (displayedModel.getRefPerson() == null) {
      displayedModel.setRefPerson(new PersonPersistable());
    }
    if (displayedModel.getRefPerson().getRefHomeAddress() == null) {
      displayedModel.getRefPerson().setRefHomeAddress(new AddressPersistable());
    }

    super.setDisplayedModel(displayedModel);

    if (personGroup != null) {
      personGroup.setItemDataSource(getDisplayedModel().getRefPerson());
    }
    if (addressGroup != null) {
      addressGroup.setItemDataSource(getDisplayedModel().getRefPerson().getRefHomeAddress());
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FormLayout setUp() {
    if (getDisplayedModel().getRefPerson() == null) {
      getDisplayedModel().setRefPerson(new PersonPersistable());
    }
    if (getDisplayedModel().getRefPerson().getRefHomeAddress() == null) {
      getDisplayedModel().getRefPerson().setRefHomeAddress(new AddressPersistable());
    }

    personGroup = createFieldGroup(PersonModel.class);
    personGroup.setItemDataSource(getDisplayedModel().getRefPerson());

    addressGroup = createFieldGroup(AddressModel.class);
    addressGroup.setItemDataSource(getDisplayedModel().getRefPerson().getRefHomeAddress());

    FormLayout layout = new FormLayout();
    layout.setMargin(true);

    relationship = new TextField();
    layout.addComponent(relationship);
    relationship.setCaption(contactPersonTexts.relationship());
    relationship.setNullRepresentation("");
    relationship.setWidth(200f, Unit.PIXELS);
    relationship.addValueChangeListener(ruleExecutionListener);
    saveGroup.bind(relationship, ContactPersonModel.PropertyId.RELATIONSHIP);

    sex = new ComboBox();
    layout.addComponent(sex);
    sex.setCaption(personTexts.sex());
    sex.setContainerDataSource(createSexContainer());
    sex.setWidth(200f, Unit.PIXELS);
    sex.setTextInputAllowed(false);
    sex.setNullSelectionAllowed(false);
    sex.setItemCaptionMode(ItemCaptionMode.ID_TOSTRING);
    sex.addValueChangeListener(ruleExecutionListener);
    personGroup.bind(sex, PersonModel.PropertyId.SEX);

    title = new TextField();
    layout.addComponent(title);
    title.setCaption(personTexts.title());
    title.setNullRepresentation("");
    title.setWidth(200f, Unit.PIXELS);
    title.addValueChangeListener(ruleExecutionListener);
    personGroup.bind(title, PersonModel.PropertyId.TITLE);

    givenName = new TextField();
    layout.addComponent(givenName);
    givenName.setCaption(personTexts.givenName());
    givenName.setNullRepresentation("");
    givenName.setWidth(200f, Unit.PIXELS);
    givenName.addValueChangeListener(ruleExecutionListener);
    personGroup.bind(givenName, PersonModel.PropertyId.GIVEN_NAME);

    familyName = new TextField();
    layout.addComponent(familyName);
    familyName.setCaption(personTexts.familyName());
    familyName.setNullRepresentation("");
    familyName.setWidth(200f, Unit.PIXELS);
    familyName.addValueChangeListener(ruleExecutionListener);
    personGroup.bind(familyName, PersonModel.PropertyId.FAMILY_NAME);

    telephone = new TextField();
    layout.addComponent(telephone);
    telephone.setCaption(personTexts.telephone());
    telephone.setNullRepresentation("");
    telephone.setWidth(200f, Unit.PIXELS);
    telephone.addValueChangeListener(ruleExecutionListener);
    personGroup.bind(telephone, PersonModel.PropertyId.TELEPHONE);

    mobile = new TextField();
    layout.addComponent(mobile);
    mobile.setCaption(personTexts.mobile());
    mobile.setNullRepresentation("");
    mobile.setWidth(200f, Unit.PIXELS);
    mobile.addValueChangeListener(ruleExecutionListener);
    personGroup.bind(mobile, PersonModel.PropertyId.MOBILE);

    emailPrivate = new TextField();
    layout.addComponent(emailPrivate);
    emailPrivate.setCaption(personTexts.emailPrivate());
    emailPrivate.setNullRepresentation("");
    emailPrivate.setWidth(200f, Unit.PIXELS);
    emailPrivate.addValueChangeListener(ruleExecutionListener);
    personGroup.bind(emailPrivate, PersonModel.PropertyId.EMAIL_PRIVATE);

    line1 = new TextField();
    layout.addComponent(line1);
    line1.setCaption(addressTexts.line1());
    line1.setNullRepresentation("");
    line1.setWidth(200f, Unit.PIXELS);
    line1.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(line1, AddressModel.PropertyId.LINE1);

    line2 = new TextField();
    layout.addComponent(line2);
    line2.setCaption(addressTexts.line2());
    line2.setNullRepresentation("");
    line2.setWidth(200f, Unit.PIXELS);
    line2.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(line2, AddressModel.PropertyId.LINE2);

    HorizontalLayout zipCity = new HorizontalLayout();
    layout.addComponent(zipCity);
    zipCity.setMargin(false);
    zipCity.setSpacing(false);
    zipCity.setWidth(200f, Unit.PIXELS);
    zipCity.setCaption(addressTexts.zipCode() + "/" + addressTexts.city());

    zipCode = new TextField();
    zipCity.addComponent(zipCode);
    zipCity.setExpandRatio(zipCode, 0.4f);
    zipCode.setWidth("100%");
    zipCode.setNullRepresentation("");
    zipCode.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(zipCode, AddressModel.PropertyId.ZIP_CODE);

    city = new TextField();
    zipCity.addComponent(city);
    zipCity.setExpandRatio(city, 0.6f);
    city.setWidth("100%");
    city.setNullRepresentation("");
    city.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(city, AddressModel.PropertyId.CITY);

    canton = new TextField();
    layout.addComponent(canton);
    canton.setCaption(addressTexts.canton());
    canton.setNullRepresentation("");
    canton.setWidth(200f, Unit.PIXELS);
    canton.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(canton, AddressModel.PropertyId.CANTON);

    country = new TextField();
    layout.addComponent(country);
    country.setCaption(addressTexts.country());
    country.setNullRepresentation("");
    country.setWidth(200f, Unit.PIXELS);
    country.addValueChangeListener(ruleExecutionListener);
    addressGroup.bind(country, AddressModel.PropertyId.COUNTRY);

    return layout;
  }

  private BeanItemContainer<SexType> createSexContainer() {
    BeanItemContainer<SexType> container = new BeanItemContainer<>(SexType.class);

    container.addItem(SexType.MALE);
    container.addItem(SexType.FEMALE);

    return container;
  }

  private void prepareContactPerson() {

    PersonModel refPerson = getDisplayedModel().getRefPerson();

    if (refPerson.getRecordId() != null) {
      // the person already exists - nothing to do
      return;
    }

    AbstractRestriction restriction = new GreaterEqualRestriction(PersonModel.PropertyId.RECORD_ID, 0);
    if (refPerson.getTitle() != null) {
      restriction = new ConjunctionRestriction(restriction,
          new EqualityRestriction(PersonModel.PropertyId.TITLE, refPerson.getTitle()));
    }
    if (refPerson.getGivenName() != null) {
      restriction = new ConjunctionRestriction(restriction,
          new EqualityRestriction(PersonModel.PropertyId.GIVEN_NAME, refPerson.getGivenName()));
    }
    if (refPerson.getFamilyName() != null) {
      restriction = new ConjunctionRestriction(restriction,
          new EqualityRestriction(PersonModel.PropertyId.FAMILY_NAME, refPerson.getFamilyName()));
    }
    if (refPerson.getTelephone() != null) {
      restriction = new ConjunctionRestriction(restriction,
          new EqualityRestriction(PersonModel.PropertyId.TELEPHONE, refPerson.getTelephone()));
    }
    if (refPerson.getMobile() != null) {
      restriction = new ConjunctionRestriction(restriction,
          new EqualityRestriction(PersonModel.PropertyId.MOBILE, refPerson.getMobile()));
    }
    if (refPerson.getEmailPrivate() != null) {
      restriction = new ConjunctionRestriction(restriction,
          new EqualityRestriction(PersonModel.PropertyId.EMAIL_PRIVATE, refPerson.getEmailPrivate()));
    }

    List<PersonModel> foundPersons = DaoFactory.getInstance().getPersonDao().find(restriction, 0, Integer.MAX_VALUE);

    if (foundPersons == null || foundPersons.isEmpty()) {
      // no matches, nothing to do
      return;
    } else if (foundPersons.size() == 1) {
      // one person found, it is probably them - merge later, after someone who
      // may see personal data has reviewed TODO
    } else {
      // multiple people found - merge later, after someone who may see personal
      // data has reviewed TODO
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void saveDisplayedModel() throws SaveException {
    prepareContactPerson();

    DaoFactory.getInstance().getContactPersonDao().save(getDisplayedModel(),
        ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());

    PersonModel person = DaoFactory.getInstance().getPersonDao().load(personModel.getRecordId());
    Set<ContactPersonModel> list = person.getRefContactPersons();
    if (list == null) {
      list = new HashSet<>();
    }
    list.add(getDisplayedModel());
    person.setRefContactPersons(list);

    DaoFactory.getInstance().getPersonDao().save(person,
        ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());

    Set<ContactPersonModel> contactPersons = personModel.getRefContactPersons();
    if (contactPersons == null) {
      contactPersons = new HashSet<>();
    }
    contactPersons.add(getDisplayedModel());
    personModel.setRefContactPersons(contactPersons);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getViewId() {
    return null;
  }

}
