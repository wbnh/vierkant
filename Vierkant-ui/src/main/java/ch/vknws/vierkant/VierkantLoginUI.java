//package ch.vknws.vierkant;
//
//import javax.servlet.annotation.WebInitParam;
//import javax.servlet.annotation.WebServlet;
//
//import com.vaadin.annotations.Push;
//import com.vaadin.annotations.Theme;
//import com.vaadin.annotations.VaadinServletConfiguration;
//import com.vaadin.cdi.CDIUI;
//import com.vaadin.server.VaadinRequest;
//import com.vaadin.server.VaadinServlet;
//import com.vaadin.shared.communication.PushMode;
//import com.vaadin.ui.UI;
//
//import ch.vknws.vierkant.backend.ApplicationConstants;
//import ch.vknws.vierkant.ui.login.LoginView;
//
//@Theme("vierkant_theme")
//@Push(value = PushMode.AUTOMATIC)
//@CDIUI
//public class VierkantLoginUI extends UI {
//
//	private static final long serialVersionUID = 5196979089780729162L;
//
//	@Override
//	protected void init(VaadinRequest vaadinRequest) {
//		getPage().setTitle(ApplicationConstants.APPLICATION_NAME);
//
//		setContent(new LoginView());
//	}
//
//	@WebServlet(urlPatterns = "/", name = "VierkantUIServlet", asyncSupported = true, initParams = {
//					@WebInitParam(name = "UIProvider", value = "com.vaadin.cdi.CDIUIProvider") })
//	@VaadinServletConfiguration(ui = VierkantLoginUI.class, productionMode = false)
//	public static class VierkantUIServlet extends VaadinServlet {
//		private static final long serialVersionUID = 1418921361544435835L;
//	}
//}
