package ch.vknws.vierkant;

import java.io.Serializable;
import java.net.URI;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.vaadin.navigator.Navigator;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;

import ch.vknws.vierkant.backend.configuration.VierkantConfigurations;
import ch.vknws.vierkant.type.PersonModel;

/**
 * @author Benjamin Weber
 */
public class ApplicationRegistry implements Serializable {
  private static final long serialVersionUID = 4001598620952369210L;
  private static ApplicationRegistry testInstance;
  private LocalDateTime adminModeEntered;
  private PersonModel loggedInUser;
  private DateTimeFormatter dateFormat = VierkantConfigurations.DATE_FORMAT;
  private DateTimeFormatter dateTimeFormat = VierkantConfigurations.DATE_TIME_FORMAT;
  private Navigator navigator;
  private Duration adminModeExpiration = Duration.ofMinutes(5);

  /**
   * @return the mode in which audit comments should be asked for.
   */
  public AuditCommentMode getAuditCommentMode() {
    return AuditCommentMode.OPTIONAL;
  }

  /**
   * @return the navigator to navigate through the application
   */
  public Navigator getNavigator() {
    return navigator;
  }

  /**
   * @param navigator the new navigator
   */
  public void setNavigator(Navigator navigator) {
    this.navigator = navigator;
  }

  /**
   * @return the user that is currently logged in
   */
  public PersonModel getLoggedInUser() {
    return loggedInUser;
  }

  /**
   * @param loggedInUser the user that is currently logged in
   */
  public void setLoggedInUser(PersonModel loggedInUser) {
    this.loggedInUser = loggedInUser;
  }

  /**
   * @return the format for dates
   */
  public DateTimeFormatter getDateFormat() {
    return dateFormat;
  }

  /**
   * @return the format for datetimes
   */
  public DateTimeFormatter getDateTimeFormat() {
    return dateTimeFormat;
  }

  /**
   * @param adminModeEntered when admin mode was entered
   */
  public void setAdminModeEntered(LocalDateTime adminModeEntered) {
    this.adminModeEntered = adminModeEntered;
  }

  /**
   * @return when admin mode was entered
   */
  public LocalDateTime getAdminModeEntered() {
    return adminModeEntered;
  }

  /**
   * @return whether we currently are in admin mode
   */
  public boolean isAdminMode() {
    if (adminModeEntered == null) { return false; }
    LocalDateTime adminModeExpired = adminModeEntered.plus(adminModeExpiration);
    return LocalDateTime.now().isBefore(adminModeExpired);
  }

  /**
   * Creates a link for a user to reset their password
   * 
   * @param code the code for the reset
   * @return the link
   */
  public String createPasswordResetLink(String code) {
    URI location = Page.getCurrent().getLocation();
    StringBuilder builder = new StringBuilder();

    builder.append(location.toString().replace("#" + location.getRawFragment(), "")
        .replaceAll("[?&]passwordResetCode=[a-f0-9\\-]{36}", ""));
    if (builder.toString().contains("?")) {
      builder.append("&");
    } else {
      builder.append("?");
    }
    builder.append("passwordResetCode=");
    builder.append(code);
    if (location.getRawFragment() != null && !location.getRawFragment().isEmpty()
        && !"null".equals(location.getRawFragment())) {
      builder.append("#");
      builder.append(location.getRawFragment());
    }

    return builder.toString();
  }

  /**
   * @return the instance to use
   */
  public static ApplicationRegistry getInstance() {
    if (testInstance != null) { return testInstance; }
    return VaadinSession.getCurrent().getAttribute(ApplicationRegistry.class);
  }

  /**
   * Sets the instance under test.
   * 
   * @param instance the test instance
   */
  public static void setTestInstance(ApplicationRegistry instance) {
    testInstance = instance;
  }
}
