package ch.vknws.vierkant;

/**
 * System messages to display.
 *
 * @author Benjamin Weber
 */
public interface SystemMessages {

  /**
   * @return the text
   */
  String sessionExpiredTitle();

  /**
   * @return the text
   */
  String sessionExpiredMessage();

  /**
   * @return the text
   */
  String authenticationErrorTitle();

  /**
   * @return the text
   */
  String authenticationErrorMessage();

  /**
   * @return the text
   */
  String communicationErrorTitle();

  /**
   * @return the text
   */
  String communicationErrorMessage();

  /**
   * @return the text
   */
  String internalErrorTitle();

  /**
   * @return the text
   */
  String internalErrorMessage();

  /**
   * @return the text
   */
  String cookiesDisabledTitle();

  /**
   * @return the text
   */
  String cookiesDisabledMessage();
}
