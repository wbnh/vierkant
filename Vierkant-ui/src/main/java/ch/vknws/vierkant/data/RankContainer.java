package ch.vknws.vierkant.data;

import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.RankModel;

/**
 * Container for RankModels.
 *
 * @author Benjamin Weber
 *
 */
public class RankContainer extends AbstractDaoContainer<RankModel> {

	private static final long serialVersionUID = 7328426394282766949L;

	/**
	 * Constructor handling initialization of mandatory fields.
	 */
	public RankContainer() {
		super(DaoFactory.getInstance().getRankDao(), RankModel.PropertyId.PROPERTY_TYPES);
	}

}
