package ch.vknws.vierkant.data;

import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.PersonModel;

/**
 * Container for PersonModel.
 *
 * @author Benjamin Weber
 *
 */
public class PersonContainer extends AbstractDaoContainer<PersonModel> {

	private static final long serialVersionUID = 2955970409794417326L;

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public PersonContainer() {
		super(DaoFactory.getInstance().getPersonDao(), PersonModel.PropertyId.PROPERTY_TYPES);
	}

}
