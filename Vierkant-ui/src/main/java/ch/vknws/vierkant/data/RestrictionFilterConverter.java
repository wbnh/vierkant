package ch.vknws.vierkant.data;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.vaadin.data.Container.Filter;
import com.vaadin.data.util.filter.And;
import com.vaadin.data.util.filter.Between;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.data.util.filter.IsNull;
import com.vaadin.data.util.filter.Like;
import com.vaadin.data.util.filter.Not;
import com.vaadin.data.util.filter.Or;
import com.vaadin.data.util.filter.SimpleStringFilter;

import ch.bwe.fac.v1.type.restriction.AbstractJunctionRestriction;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.ConjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.DisjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.bwe.fac.v1.type.restriction.GreaterEqualRestriction;
import ch.bwe.fac.v1.type.restriction.GreaterThanRestriction;
import ch.bwe.fac.v1.type.restriction.LessEqualRestriction;
import ch.bwe.fac.v1.type.restriction.LessThanRestriction;
import ch.bwe.fac.v1.type.restriction.LikeRestriction;
import ch.bwe.fac.v1.type.restriction.NullRestriction;

/**
 * Converts Vaadin Filters to Restrictions and back.
 * 
 * @author benjamin
 */
public class RestrictionFilterConverter {

  /**
   * Maps the passed filter to a restriction
   * 
   * @param filter the filter
   * @return the restriction
   */
  public static AbstractRestriction convertFilterToRestriction(Filter filter) {
    return convertFilterToRestriction(filter, false);
  }

  private static AbstractRestriction convertFilterToRestriction(Filter filter, boolean not) {

    if (filter instanceof And) {
      And and = (And) filter;
      Collection<Filter> filters = and.getFilters();
      List<AbstractRestriction> restrictions = filters.stream()
          .map(RestrictionFilterConverter::convertFilterToRestriction).collect(Collectors.toList());

      ConjunctionRestriction topLevel = new ConjunctionRestriction(restrictions.get(0), null, not);
      ConjunctionRestriction currentParent = topLevel;
      ConjunctionRestriction currentLevel = null;

      for (int i = 1; i < restrictions.size() - 1; i++) {
        currentLevel = new ConjunctionRestriction(not);

        currentParent.setRight(currentLevel);
        currentLevel.setLeft(restrictions.get(i));

        currentParent = currentLevel;
      }

      if (currentLevel != null) {
        currentLevel.setRight(restrictions.get(restrictions.size() - 1));
      }

      return topLevel;

    } else if (filter instanceof Or) {
      Or or = (Or) filter;
      Collection<Filter> filters = or.getFilters();
      List<AbstractRestriction> restrictions = filters.stream()
          .map(RestrictionFilterConverter::convertFilterToRestriction).collect(Collectors.toList());

      DisjunctionRestriction topLevel = new DisjunctionRestriction(restrictions.get(0), null, not);
      DisjunctionRestriction currentParent = topLevel;
      DisjunctionRestriction currentLevel = null;

      for (int i = 1; i < restrictions.size() - 1; i++) {
        currentLevel = new DisjunctionRestriction(not);

        currentParent.setRight(currentLevel);
        currentLevel.setLeft(restrictions.get(i));

        currentParent = currentLevel;
      }

      if (currentLevel != null) {
        currentLevel.setRight(restrictions.get(restrictions.size() - 1));
      }

      return topLevel;

    } else if (filter instanceof Between) {
      Between between = (Between) filter;

      AbstractJunctionRestriction topLevel;
      if (not) {
        topLevel = new DisjunctionRestriction();
      } else {
        topLevel = new ConjunctionRestriction();
      }

      topLevel.setLeft(new GreaterThanRestriction((String) between.getPropertyId(), between.getStartValue(), not));
      topLevel.setRight(new LessThanRestriction((String) between.getPropertyId(), between.getStartValue(), not));

      return topLevel;

    } else if (filter instanceof Compare) {
      Compare compare = (Compare) filter;

      AbstractRestriction topLevel;

      switch (compare.getOperation()) {
      case EQUAL:
        topLevel = new EqualityRestriction((String) compare.getPropertyId(), compare.getValue(), not);
        break;
      case GREATER:
        topLevel = new GreaterThanRestriction((String) compare.getPropertyId(), compare.getValue(), not);
        break;
      case GREATER_OR_EQUAL:
        topLevel = new GreaterEqualRestriction((String) compare.getPropertyId(), compare.getValue(), not);
        break;
      case LESS:
        topLevel = new LessThanRestriction((String) compare.getPropertyId(), compare.getValue(), not);
        break;
      case LESS_OR_EQUAL:
        topLevel = new LessEqualRestriction((String) compare.getPropertyId(), compare.getValue(), not);
        break;
      default:
        throw new IllegalArgumentException(
            "The operation " + compare.getOperation() + " is unknown and cannot be converted");
      }

      return topLevel;

    } else if (filter instanceof IsNull) {
      IsNull isNull = (IsNull) filter;

      return new NullRestriction((String) isNull.getPropertyId(), not);

    } else if (filter instanceof Like) {
      Like like = (Like) filter;

      return new LikeRestriction((String) like.getPropertyId(), like.getValue(), not);

    } else if (filter instanceof Not) {
      Not notFilter = (Not) filter;

      // consecutive Not filters cancel out
      return convertFilterToRestriction(notFilter.getFilter(), !not);

    } else if (filter instanceof SimpleStringFilter) {
      SimpleStringFilter stringFilter = (SimpleStringFilter) filter;

      StringBuilder effectiveString = new StringBuilder();

      if (!stringFilter.isOnlyMatchPrefix()) {
        effectiveString.append('%');
      }
      effectiveString.append(stringFilter.getFilterString());
      effectiveString.append('%');

      return new LikeRestriction((String) stringFilter.getPropertyId(), effectiveString.toString(), not);
    }

    throw new IllegalArgumentException("Passed filter is not supported");
  }

  /**
   * Maps the passed restriction to a filter.
   * 
   * @param restriction the restriction
   * @return the filter
   */
  public static Filter convertRestrictionToFilter(AbstractRestriction restriction) {

    Filter filter = null;

    if (restriction instanceof ConjunctionRestriction) {
      ConjunctionRestriction conRestriction = (ConjunctionRestriction) restriction;

      filter = new And(convertRestrictionToFilter(conRestriction.getLeft()),
          convertRestrictionToFilter(conRestriction.getRight()));

    } else if (restriction instanceof DisjunctionRestriction) {
      DisjunctionRestriction disRestriction = (DisjunctionRestriction) restriction;

      filter = new Or(convertRestrictionToFilter(disRestriction.getLeft()),
          convertRestrictionToFilter(disRestriction.getRight()));

    } else if (restriction instanceof EqualityRestriction) {
      EqualityRestriction eq = (EqualityRestriction) restriction;

      filter = new Compare.Equal(eq.getField(), eq.getValue());

    } else if (restriction instanceof GreaterThanRestriction) {
      GreaterThanRestriction gt = (GreaterThanRestriction) restriction;

      filter = new Compare.Greater(gt.getField(), gt.getValue());

    } else if (restriction instanceof GreaterEqualRestriction) {
      GreaterEqualRestriction ge = (GreaterEqualRestriction) restriction;

      filter = new Compare.GreaterOrEqual(ge.getField(), ge.getValue());

    } else if (restriction instanceof LessThanRestriction) {
      LessThanRestriction lt = (LessThanRestriction) restriction;

      filter = new Compare.Less(lt.getField(), lt.getValue());

    } else if (restriction instanceof LessEqualRestriction) {
      LessEqualRestriction le = (LessEqualRestriction) restriction;

      filter = new Compare.LessOrEqual(le.getField(), le.getValue());

    } else if (restriction instanceof NullRestriction) {
      NullRestriction isNull = (NullRestriction) restriction;

      filter = new IsNull(isNull.getField());

    } else if (restriction instanceof LikeRestriction) {
      LikeRestriction like = (LikeRestriction) restriction;

      filter = new Like(like.getField(), (String) like.getValue());
    }

    if (filter == null) { throw new IllegalArgumentException("Passed restriction is not supported"); }

    if (restriction.isNot()) {
      filter = new Not(filter);
    }

    return filter;
  }
}
