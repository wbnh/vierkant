package ch.vknws.vierkant.data;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.AbstractContainer;
import com.vaadin.data.util.BeanItem;

import ch.bwe.faa.v1.core.util.AssertUtils;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.vknws.vierkant.type.ModelUtils;

/**
 * Abstract container implementation for in-memory data. Used for models that
 * are contained on other models.
 *
 * @author Benjamin Weber
 * @param <T> the top level model
 * @param <U> the referenced models
 */
public abstract class AbstractModelContainer<T extends DatabaseModel, U extends DatabaseModel> extends AbstractContainer
    implements Indexed {

  private static final long serialVersionUID = 7322894305016300106L;

  private List<U> displayItems;
  private Map<String, Class<?>> propertyTypes;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param model    the model under which the models to load are located
   * @param field    the field that contains the models
   * @param refClass the class of the referenced models
   */
  @SuppressWarnings("unchecked")
  protected AbstractModelContainer(T model, String field, Class<U> refClass) {
    super();

    AssertUtils.notNull(model);
    AssertUtils.notNull(field);

    Object refField = model.get(field);
    AssertUtils.assertTrue(refField == null || refField instanceof Set<?>);

    displayItems = new LinkedList<>((Set<U>) refField);

    propertyTypes = ModelUtils.getPropertyTypes(refClass);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object nextItemId(Object itemId) {
    if (displayItems == null) return null;
    return displayItems.get(indexOfId(itemId) + 1);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object prevItemId(Object itemId) {
    if (displayItems == null) return null;
    return displayItems.get(indexOfId(itemId) - 1);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object firstItemId() {
    if (displayItems == null) return null;
    return displayItems.get(0).getRecordId();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object lastItemId() {
    if (displayItems == null) return null;
    return displayItems.get(displayItems.size()).getRecordId();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isFirstId(Object itemId) {
    return Objects.equals(itemId, firstItemId());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isLastId(Object itemId) {
    return Objects.equals(itemId, lastItemId());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object addItemAfter(Object previousItemId) throws UnsupportedOperationException {
    if (displayItems == null) return null;
    U model = createModel();
    displayItems.add(indexOfId(previousItemId) + 1, model);
    return model;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Item addItemAfter(Object previousItemId, Object newItemId) throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Item getItem(Object itemId) {
    if (displayItems == null) return null;

    for (U model : displayItems) {
      if (Objects.equals(model.getRecordId(), itemId)) { return new BeanItem<>(model); }
    }

    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Collection<?> getContainerPropertyIds() {
    return propertyTypes.keySet();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Collection<?> getItemIds() {
    List<Integer> ids = new LinkedList<>();

    for (U model : displayItems) {
      ids.add(model.getRecordId());
    }

    return ids;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @SuppressWarnings("rawtypes")
  public Property getContainerProperty(Object itemId, Object propertyId) {
    return getItem(itemId).getItemProperty(propertyId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Class<?> getType(Object propertyId) {
    return propertyTypes.get(propertyId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int size() {
    if (displayItems == null) return 0;
    return displayItems.size();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean containsId(Object itemId) {
    if (displayItems == null) return false;
    for (U model : displayItems) {
      if (Objects.equals(model.getRecordId(), itemId)) return true;
    }
    return false;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Item addItem(Object itemId) throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object addItem() throws UnsupportedOperationException {
    if (displayItems == null) return null;
    U model = createModel();
    displayItems.add(model);
    return model;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean removeItem(Object itemId) throws UnsupportedOperationException {
    if (displayItems == null) return true;
    U model = null;
    for (int i = 0; i < displayItems.size(); i++) {
      model = displayItems.get(i);
      if (Objects.equals(itemId, model.getRecordId())) {
        break;
      }
    }
    if (model != null) {
      displayItems.remove(model);
      return true;
    }
    return false;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean addContainerProperty(Object propertyId, Class<?> type, Object defaultValue)
      throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean removeContainerProperty(Object propertyId) throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean removeAllItems() throws UnsupportedOperationException {
    if (displayItems == null) return true;
    displayItems.clear();
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int indexOfId(Object itemId) {
    if (displayItems == null) return -1;
    for (int i = 0; i < displayItems.size(); i++) {
      if (Objects.equals(displayItems.get(i).getRecordId(), itemId)) return i;
    }
    return -1;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object getIdByIndex(int index) {
    if (displayItems == null) return null;
    return displayItems.get(index).getRecordId();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<?> getItemIds(int startIndex, int numberOfItems) {
    List<Integer> ids = new LinkedList<>();

    if (displayItems == null) { return ids; }

    for (int i = startIndex; i < startIndex + numberOfItems; i++) {
      ids.add(displayItems.get(i).getRecordId());
    }

    return ids;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object addItemAt(int index) throws UnsupportedOperationException {
    if (displayItems == null) return null;
    U model = createModel();
    displayItems.add(index, model);
    return model;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Item addItemAt(int index, Object newItemId) throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  /**
   * @return a new referenced model
   */
  protected abstract U createModel();
}
