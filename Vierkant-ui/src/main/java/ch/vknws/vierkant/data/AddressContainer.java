package ch.vknws.vierkant.data;

import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.AddressModel;

/**
 * Container for AddressModel.
 *
 * @author Benjamin Weber
 *
 */
public class AddressContainer extends AbstractDaoContainer<AddressModel> {

	private static final long serialVersionUID = 6345554606916729189L;

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public AddressContainer() {
		super(DaoFactory.getInstance().getAddressDao(), AddressModel.PropertyId.PROPERTY_TYPES);
	}

}
