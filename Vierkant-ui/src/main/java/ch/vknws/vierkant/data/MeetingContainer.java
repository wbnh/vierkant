package ch.vknws.vierkant.data;

import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.MeetingModel;

/**
 * Container for RankModels.
 *
 * @author Benjamin Weber
 */
public class MeetingContainer extends AbstractDaoContainer<MeetingModel> {

  private static final long serialVersionUID = 7328426394282766949L;

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public MeetingContainer() {
    super(DaoFactory.getInstance().getMeetingDao(), MeetingModel.PropertyId.PROPERTY_TYPES);
  }

}
