package ch.vknws.vierkant.data;

import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.AuthorizationKeyModel;
import ch.vknws.vierkant.type.impl.AuthorizationKeyPersistable;

/**
 * Container for the MaterialModel.
 *
 * @author Benjamin Weber
 *
 */
public class AuthorizationKeyContainer extends AbstractDaoContainer<AuthorizationKeyModel> {

	private static final long serialVersionUID = 3212402674409382744L;

	/**
	 * Constructor handling initialization of mandatory fields.
	 */
	public AuthorizationKeyContainer() {
		super(DaoFactory.getInstance().getAuthorizationKeyDao(), AuthorizationKeyPersistable.PropertyId.PROPERTY_TYPES);
	}

}
