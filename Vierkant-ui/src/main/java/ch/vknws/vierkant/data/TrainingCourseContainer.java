package ch.vknws.vierkant.data;

import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.TrainingCourseModel;

/**
 * Container for the MaterialModel.
 *
 * @author Benjamin Weber
 *
 */
public class TrainingCourseContainer extends AbstractDaoContainer<TrainingCourseModel> {

	private static final long serialVersionUID = 3212402674409382744L;

	/**
	 * Constructor handling initialization of mandatory fields.
	 */
	public TrainingCourseContainer() {
		super(DaoFactory.getInstance().getTrainingCourseDao(), TrainingCourseModel.PropertyId.PROPERTY_TYPES);
	}

}
