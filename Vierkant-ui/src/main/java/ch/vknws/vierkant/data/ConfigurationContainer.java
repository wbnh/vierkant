package ch.vknws.vierkant.data;

import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.ConfigurationModel;

/**
 * Container for AddressModel.
 *
 * @author Benjamin Weber
 *
 */
public class ConfigurationContainer extends AbstractDaoContainer<ConfigurationModel> {

	private static final long serialVersionUID = 6345554606916729189L;

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public ConfigurationContainer() {
		super(DaoFactory.getInstance().getConfigurationDao(), ConfigurationModel.PropertyId.PROPERTY_TYPES);
	}

}
