package ch.vknws.vierkant.data;

import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.MeetingTypeModel;

/**
 * Container for RankModels.
 *
 * @author Benjamin Weber
 */
public class MeetingTypeContainer extends AbstractDaoContainer<MeetingTypeModel> {

  private static final long serialVersionUID = 7328426394282766949L;

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public MeetingTypeContainer() {
    super(DaoFactory.getInstance().getMeetingTypeDao(), MeetingTypeModel.PropertyId.PROPERTY_TYPES);
  }

}
