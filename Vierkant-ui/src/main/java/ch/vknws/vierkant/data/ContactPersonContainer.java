package ch.vknws.vierkant.data;

import ch.vknws.vierkant.type.ContactPersonModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.impl.ContactPersonPersistable;

/**
 * Container for the ContactPersonModel.
 *
 * @author Benjamin Weber
 */
public class ContactPersonContainer extends AbstractModelContainer<PersonModel, ContactPersonModel> {

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param model the person whose contact persons shall be contained
   */
  public ContactPersonContainer(PersonModel model) {
    super(model, PersonModel.PropertyId.REF_CONTACT_PERSONS, ContactPersonModel.class);
  }

  private static final long serialVersionUID = -1831621326783370834L;

  /**
   * {@inheritDoc}
   */
  @Override
  protected ContactPersonModel createModel() {
    return new ContactPersonPersistable();
  }

}
