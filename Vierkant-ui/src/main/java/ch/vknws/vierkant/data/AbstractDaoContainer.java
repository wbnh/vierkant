package ch.vknws.vierkant.data;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import com.vaadin.data.Container.Filterable;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Container.Ordered;
import com.vaadin.data.Container.Sortable;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.AbstractContainer;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.filter.UnsupportedFilterException;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.util.Pair;
import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.type.DAO;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.ConjunctionRestriction;

/**
 * Abstract container to implement data fetching from the databsase using a DAO.
 *
 * @author Benjamin Weber
 * @param <T> The model to contain
 */
public abstract class AbstractDaoContainer<T extends DatabaseModel> extends AbstractContainer
    implements Indexed, Ordered, Sortable, Filterable {

  private static final long serialVersionUID = 7328426394282766949L;

  private DAO<T> dao;
  private Map<String, Class<?>> propertyIds;
  private AbstractRestriction defaultRestriction;
  private AbstractRestriction compiledRestriction;
  private Set<Filter> filters = new HashSet<>();
  private List<Pair<String, Boolean>> sortOrders = new LinkedList<>();

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param dao         the dao to get the data
   * @param propertyIds the properties to display
   */
  protected AbstractDaoContainer(DAO<T> dao, Map<String, Class<?>> propertyIds) {
    this.dao = dao;
    this.propertyIds = propertyIds;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object nextItemId(Object itemId) {
    return dao.getNextId(compiledRestriction, getIdFromItemId(itemId), sortOrders);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object prevItemId(Object itemId) {
    return dao.getPreviousId(compiledRestriction, getIdFromItemId(itemId), sortOrders);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object firstItemId() {
    Object id = dao.getFirstId(compiledRestriction, sortOrders);
    ServiceRegistry.getLogProxy().trace(this, "First: {0}", id);
    return id;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object lastItemId() {
    Object id = dao.getLastId(compiledRestriction, sortOrders);
    ServiceRegistry.getLogProxy().trace(this, "Last: {0}", id);
    return id;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isFirstId(Object itemId) {
    return Objects.equals(getIdFromItemId(itemId), dao.getFirstId(compiledRestriction, sortOrders));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isLastId(Object itemId) {
    return Objects.equals(getIdFromItemId(itemId), dao.getLastId(compiledRestriction, sortOrders));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object addItemAfter(Object previousItemId) throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Item addItemAfter(Object previousItemId, Object newItemId) throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Item getItem(Object itemId) {
    T persistable = dao.load(getIdFromItemId(itemId));
    if (persistable == null) { return null; }
    return new BeanItem<T>(persistable);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Collection<?> getContainerPropertyIds() {
    return propertyIds.keySet();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Collection<?> getItemIds() {
    List<?> ids = dao.getAllIds(compiledRestriction, 0, Integer.MAX_VALUE, sortOrders);
    ServiceRegistry.getLogProxy().trace(this, "All IDs: {0}", ids);
    return ids;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @SuppressWarnings("rawtypes")
  public Property getContainerProperty(Object itemId, Object propertyId) {
    Object value = dao.getProperty(getIdFromItemId(itemId), (String) propertyId);
    if (Boolean.class.isAssignableFrom(propertyIds.get(propertyId))) {
      if (value != null) {
        value = !value.equals(0);
      }
    }
    return new ObjectProperty<>(value);
  }

  /**
   * Gets the DB ID from the Vaadin item ID.
   * 
   * @param itemId the Vaadin ID
   * @return the DB ID
   */
  protected Integer getIdFromItemId(Object itemId) {
    if (itemId instanceof Integer) {
      return (Integer) itemId;
    } else if (itemId instanceof AbstractDatabasePersistable) { return ((AbstractDatabasePersistable) itemId)
        .getRecordId(); }
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Class<?> getType(Object propertyId) {
    return propertyIds.get(propertyId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int size() {
    int count = dao.count(compiledRestriction).intValue();
    ServiceRegistry.getLogProxy().trace(this, "Count: {0}", count);
    return count;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean containsId(Object itemId) {
    return dao.containsId(compiledRestriction, getIdFromItemId(itemId));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Item addItem(Object itemId) throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object addItem() throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean removeItem(Object itemId) throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean addContainerProperty(Object propertyId, Class<?> type, Object defaultValue)
      throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean removeContainerProperty(Object propertyId) throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean removeAllItems() throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int indexOfId(Object itemId) {
    return dao.getPersistableIndex(compiledRestriction, getIdFromItemId(itemId), sortOrders).intValue();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object getIdByIndex(int index) {
    List<Integer> ids = dao.getAllIds(compiledRestriction, index, 1, sortOrders);
    ServiceRegistry.getLogProxy().trace(this, "ID: {0}", ids);
    return ids.get(0);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<?> getItemIds(int startIndex, int numberOfItems) {
    List<?> ids = dao.getAllIds(compiledRestriction, startIndex, numberOfItems, sortOrders);
    ServiceRegistry.getLogProxy().trace(this, "IDs: {0}", ids);
    return ids;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object addItemAt(int index) throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Item addItemAt(int index, Object newItemId) throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void sort(Object[] propertyId, boolean[] ascending) {
    sortOrders.clear();

    for (int i = 0; i < ascending.length; i++) {
      sortOrders.add(new Pair<String, Boolean>((String) propertyId[i], ascending[i]));
    }
  }

  /**
   * @return the current sort orders
   */
  public List<Pair<String, Boolean>> getSortOrders() {
    return sortOrders;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Collection<?> getSortableContainerPropertyIds() {
    return getContainerPropertyIds();
  }

  @Override
  public void addContainerFilter(Filter filter) throws UnsupportedFilterException {
    filters.add(filter);

    recompileRestriction();
  }

  @Override
  public void removeContainerFilter(Filter filter) {
    filters.remove(filter);

    recompileRestriction();
  }

  @Override
  public void removeAllContainerFilters() {
    filters.clear();

    recompileRestriction();
  }

  @Override
  public Collection<Filter> getContainerFilters() {
    return filters;
  }

  /**
   * @return the base restriction
   */
  public AbstractRestriction getDefaultRestriction() {
    return defaultRestriction;
  }

  /**
   * @param restriction the base restriction
   */
  public void setDefaultRestriction(AbstractRestriction restriction) {
    this.defaultRestriction = restriction;
  }

  private void recompileRestriction() {

    if (filters.isEmpty()) {
      compiledRestriction = defaultRestriction;
      return;
    }

    ConjunctionRestriction topLevel = new ConjunctionRestriction(defaultRestriction, null);
    ConjunctionRestriction currentParent = topLevel;
    ConjunctionRestriction currentLevel = null;

    for (Filter filter : filters) {
      currentLevel = new ConjunctionRestriction();
      AbstractRestriction restriction = RestrictionFilterConverter.convertFilterToRestriction(filter);

      currentParent.setRight(currentLevel);
      currentLevel.setLeft(restriction);

      currentParent = currentLevel;
    }

    if (defaultRestriction == null) {
      compiledRestriction = ((ConjunctionRestriction) topLevel.getRight()).getLeft();
    } else {
      compiledRestriction = topLevel;
    }
  }

}
