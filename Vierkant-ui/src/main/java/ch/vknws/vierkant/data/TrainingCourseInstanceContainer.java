package ch.vknws.vierkant.data;

import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.TrainingCourseInstanceModel;

/**
 * Container for the MaterialModel.
 *
 * @author Benjamin Weber
 *
 */
public class TrainingCourseInstanceContainer extends AbstractDaoContainer<TrainingCourseInstanceModel> {

	private static final long serialVersionUID = 3212402674409382744L;

	/**
	 * Constructor handling initialization of mandatory fields.
	 */
	public TrainingCourseInstanceContainer() {
		super(DaoFactory.getInstance().getTrainingCourseInstanceDao(),
						TrainingCourseInstanceModel.PropertyId.PROPERTY_TYPES);
	}

}
