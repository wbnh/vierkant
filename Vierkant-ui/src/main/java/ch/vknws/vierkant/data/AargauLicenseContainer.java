package ch.vknws.vierkant.data;

import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.AargauLicenseModel;

/**
 * Container for the MaterialModel.
 *
 * @author Benjamin Weber
 *
 */
public class AargauLicenseContainer extends AbstractDaoContainer<AargauLicenseModel> {

	private static final long serialVersionUID = 3212402674409382744L;

	/**
	 * Constructor handling initialization of mandatory fields.
	 */
	public AargauLicenseContainer() {
		super(DaoFactory.getInstance().getAargauLicenseDao(), AargauLicenseModel.PropertyId.PROPERTY_TYPES);
	}

}
