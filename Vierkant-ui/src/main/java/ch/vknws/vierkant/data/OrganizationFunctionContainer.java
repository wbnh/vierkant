package ch.vknws.vierkant.data;

import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.OrganizationFunctionModel;

/**
 * Container for the MaterialModel.
 *
 * @author Benjamin Weber
 *
 */
public class OrganizationFunctionContainer extends AbstractDaoContainer<OrganizationFunctionModel> {

	private static final long serialVersionUID = 3212402674409382744L;

	/**
	 * Constructor handling initialization of mandatory fields.
	 */
	public OrganizationFunctionContainer() {
		super(DaoFactory.getInstance().getOrganizationFunctionDao(), OrganizationFunctionModel.PropertyId.PROPERTY_TYPES);
	}

}
