package ch.vknws.vierkant.data;

import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.FunctionGroupModel;

/**
 * Container for the MaterialModel.
 *
 * @author Benjamin Weber
 *
 */
public class FunctionGroupContainer extends AbstractDaoContainer<FunctionGroupModel> {

	private static final long serialVersionUID = 3212402674409382744L;

	/**
	 * Constructor handling initialization of mandatory fields.
	 */
	public FunctionGroupContainer() {
		super(DaoFactory.getInstance().getFunctionGroupDao(), FunctionGroupModel.PropertyId.PROPERTY_TYPES);
	}

}
