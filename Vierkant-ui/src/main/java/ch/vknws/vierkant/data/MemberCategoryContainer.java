package ch.vknws.vierkant.data;

import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.MemberGroupModel;

/**
 * Container for the MemberCategoryModel.
 *
 * @author Benjamin Weber
 */
public class MemberCategoryContainer extends AbstractDaoContainer<MemberGroupModel> {

  private static final long serialVersionUID = -1831621326783370834L;

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param personRecordId the person whose categories need to be adapted
   */
  public MemberCategoryContainer(Integer personRecordId) {
    super(DaoFactory.getInstance().getMemberCategoryDao(), MemberGroupModel.PropertyId.PROPERTY_TYPES);

    setPersonId(personRecordId);
  }

  /**
   * @param personId a new person ID
   */
  public void setPersonId(Integer personId) {
    if (personId == null) {
      setDefaultRestriction(null);
    } else {
      setDefaultRestriction(new EqualityRestriction("refPerson", personId));
    }
  }

}
