package ch.vknws.vierkant;

import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.CustomizedSystemMessages;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.ui.UI;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.faa.v1.core.util.StringUtils;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.ConfigurationModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.ui.ContentPanel;
import ch.vknws.vierkant.ui.ErrorMessages;
import ch.vknws.vierkant.ui.OverviewView;
import ch.vknws.vierkant.ui.admin.AddressModelTableView;
import ch.vknws.vierkant.ui.admin.AdminOverview;
import ch.vknws.vierkant.ui.admin.ConfigurationModelTableView;
import ch.vknws.vierkant.ui.admin.OrganizationFunctionModelTableView;
import ch.vknws.vierkant.ui.admin.PersonModelTableView;
import ch.vknws.vierkant.ui.admin.RankModelTableView;
import ch.vknws.vierkant.ui.dialog.DialogUtils;
import ch.vknws.vierkant.ui.field.converter.VierkantConverterFactory;
import ch.vknws.vierkant.ui.login.LoginView;
import ch.vknws.vierkant.ui.login.PasswordResetDialog;
import ch.vknws.vierkant.ui.personal.PersonalContactPersonTableView;
import ch.vknws.vierkant.ui.personal.PersonalPersonDataView;
import ch.vknws.vierkant.ui.secretary.MeetingTableView;
import ch.vknws.vierkant.ui.secretary.MeetingTypeTableView;
import ch.vknws.vierkant.ui.secretary.SecretaryAargauLicenseTableView;
import ch.vknws.vierkant.ui.secretary.SecretaryFunctionTableView;
import ch.vknws.vierkant.ui.secretary.SecretaryPersonTableView;
import ch.vknws.vierkant.ui.secretary.SecretaryRankModificationView;
import ch.vknws.vierkant.ui.secretary.SecretaryTrainingCourseTableView;
import ch.vknws.vierkant.ui.training.TrainingCourseInstanceTableView;
import ch.vknws.vierkant.ui.view.ApplicationInformationView;

/**
 * The main entry point to the application.
 * 
 * @author benjamin
 */
@Theme("vierkant_theme")
@Push(value = PushMode.AUTOMATIC)
public class VierkantUI extends UI {

  private static final long serialVersionUID = 5196979089780729162L;
  private static final ErrorMessages errorMessages = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(ErrorMessages.class));
  private ApplicationRegistry registry;

  /**
   * @param navigation the navigation state to examine
   * @return the ID or <code>null</code>.
   */
  public static Integer getIdFromNavigation(String navigation) {
    int lastIndex = navigation.lastIndexOf("/");
    String candidate = navigation.substring(lastIndex + 1);

    if (StringUtils.isInteger(candidate)) { return Integer.valueOf(candidate); }

    return null;
  }

  @Override
  protected void init(VaadinRequest vaadinRequest) {
    getPage().setTitle(ApplicationConstants.APPLICATION_NAME);

    registry = getSession().getAttribute(ApplicationRegistry.class);
    if (registry == null) {
      registry = new ApplicationRegistry();
      getSession().setAttribute(ApplicationRegistry.class, registry);
      getSession().setConverterFactory(new VierkantConverterFactory());
    }

    setContent(registry);

    String resetCode = vaadinRequest.getParameter("passwordResetCode");
    if (resetCode != null && !resetCode.isEmpty()) {
      AbstractRestriction restriction = new EqualityRestriction(ConfigurationModel.PropertyId.VALUE, resetCode);
      List<ConfigurationModel> found = DaoFactory.getInstance().getConfigurationDao().find(restriction, 0, 1);
      if (found.isEmpty()) {
        handleInvalidPasswordResetCode();
      } else {
        ConfigurationModel config = found.get(0);
        if (config.getModificationDateTime().plusHours(1).isBefore(LocalDateTime.now())) {
          handleInvalidPasswordResetCode();
        } else {
          PersonModel persistable = DaoFactory.getInstance().getPersonDao().loadByShorthand(config.getUserName());

          PasswordResetDialog dialog = new PasswordResetDialog(persistable, false);
          dialog.open();
        }
      }
    }
  }

  private void handleInvalidPasswordResetCode() {
    DialogUtils.showErrorDialog(errorMessages.passwordResetCodeInvalidTitle(),
        errorMessages.passwordResetCodeInvalidMessage(), null);
  }

  private void setContent(ApplicationRegistry registry) {
    if (registry.getLoggedInUser() == null) {
      LoginView loginView = new LoginView();
      loginView.addLoginListener(e -> setContent(registry));
      setContent(loginView);
    } else {
      ContentPanel panel = new ContentPanel(this);
      setUpViews(panel);
      setContent(panel);
      panel.refresh();
    }
  }

  private void setUpViews(ContentPanel panel) {

    OverviewView overview = new OverviewView();

    PersonModelTableView personView = new PersonModelTableView();
    RankModelTableView rankView = new RankModelTableView();
    AddressModelTableView addressView = new AddressModelTableView();
    OrganizationFunctionModelTableView functionView = new OrganizationFunctionModelTableView();
    ConfigurationModelTableView configView = new ConfigurationModelTableView();

    TrainingCourseInstanceTableView trainingCourseInstance = new TrainingCourseInstanceTableView();

    MeetingTypeTableView meetingTypes = new MeetingTypeTableView();
    MeetingTableView meetings = new MeetingTableView();

    SecretaryRankModificationView secretaryRank = new SecretaryRankModificationView();
    SecretaryPersonTableView secretaryPerson = new SecretaryPersonTableView();
    SecretaryFunctionTableView secretaryFunction = new SecretaryFunctionTableView();
    SecretaryAargauLicenseTableView secretaryAargauLicense = new SecretaryAargauLicenseTableView();
    SecretaryTrainingCourseTableView secretaryTrainingCourse = new SecretaryTrainingCourseTableView();

    PersonalContactPersonTableView personalContactPersonView = new PersonalContactPersonTableView();
    PersonalPersonDataView personalDataView = new PersonalPersonDataView(registry.getLoggedInUser());

    AdminOverview adminOverview = new AdminOverview();

    ApplicationInformationView metapage = new ApplicationInformationView();

    panel.addNavigation(overview);

    panel.addNavigation(trainingCourseInstance);

    panel.addNavigation(meetingTypes);
    panel.addNavigation(meetings);

    panel.addNavigation(secretaryRank);
    panel.addNavigation(secretaryPerson);
    panel.addNavigation(secretaryFunction);
    panel.addNavigation(secretaryAargauLicense);
    panel.addNavigation(secretaryTrainingCourse);

    panel.addNavigation(personalContactPersonView);
    panel.addNavigation(personalDataView);

    panel.addNavigation(adminOverview);
    panel.addNavigation(functionView);
    panel.addNavigation(addressView);
    panel.addNavigation(personView);
    panel.addNavigation(rankView);
    panel.addNavigation(configView);

    panel.addNavigation(metapage);

    panel.setDefaultView(overview);
  }

  /**
   * The servlet to direct the server to the UI.
   * 
   * @author benjamin
   */
  @WebServlet(urlPatterns = "/*", name = "VierkantUIServlet", asyncSupported = true
  /*
   * , initParams = {
   * @WebInitParam ( name = "UIProvider", value = "com.vaadin.cdi.CDIUIProvider")
   * }
   */)
  @VaadinServletConfiguration(ui = VierkantUI.class, productionMode = false)
  public static class VierkantUIServlet extends VaadinServlet {

    private static final long serialVersionUID = 1418921361544435835L;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void servletInitialized() throws ServletException {
      super.servletInitialized();

      SystemMessages systemMessages = ServiceRegistry.getTypedConfigurationProxy()
          .get(new ConfigurationProperties<>(SystemMessages.class));
      CustomizedSystemMessages messages = new CustomizedSystemMessages();

      messages.setSessionExpiredCaption(systemMessages.sessionExpiredTitle());
      messages.setSessionExpiredMessage(systemMessages.sessionExpiredMessage());

      messages.setAuthenticationErrorCaption(systemMessages.authenticationErrorTitle());
      messages.setAuthenticationErrorMessage(systemMessages.authenticationErrorMessage());

      messages.setCommunicationErrorCaption(systemMessages.communicationErrorTitle());
      messages.setCommunicationErrorMessage(systemMessages.communicationErrorMessage());

      messages.setInternalErrorCaption(systemMessages.internalErrorTitle());
      messages.setInternalErrorMessage(systemMessages.internalErrorMessage());

      messages.setCookiesDisabledCaption(systemMessages.cookiesDisabledTitle());
      messages.setCookiesDisabledMessage(systemMessages.cookiesDisabledMessage());

      getService().setSystemMessagesProvider(e -> messages);
    }
  }
}
