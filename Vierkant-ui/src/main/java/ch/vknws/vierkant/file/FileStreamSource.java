package ch.vknws.vierkant.file;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import com.vaadin.server.StreamResource.StreamSource;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.util.AssertUtils;

/**
 * Acts as a source to a file.
 * 
 * @author benjamin
 */
public class FileStreamSource implements StreamSource {

  private static final long serialVersionUID = 2524485597604776828L;

  private Path path;

  /**
   * @param path the file to open
   */
  public FileStreamSource(Path path) {
    AssertUtils.notNull(path);
    AssertUtils.assertTrue(Files.exists(path));
    this.path = path;
  }

  @Override
  public InputStream getStream() {
    try {
      return Files.newInputStream(path);
    } catch (IOException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not open file {0}", e, path);
      return null;
    }
  }

}
