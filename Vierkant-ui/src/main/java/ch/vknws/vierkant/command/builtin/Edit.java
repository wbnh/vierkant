package ch.vknws.vierkant.command.builtin;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.util.AssertUtils;
import ch.bwe.faa.v1.core.util.StringUtils;
import ch.bwe.fac.v1.database.AbstractDatabaseDAO;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.database.DescriptiveName;
import ch.bwe.fac.v1.database.DescriptiveNames;
import ch.bwe.fac.v1.type.SaveException;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.ConjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.DisjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.command.Command;
import ch.vknws.vierkant.backend.command.CommandContext;
import ch.vknws.vierkant.backend.command.CommandExecutionResult;
import ch.vknws.vierkant.backend.command.CommandExecutionResult.Level;
import ch.vknws.vierkant.backend.command.CommandExecutor;
import ch.vknws.vierkant.backend.command.CommandName;
import ch.vknws.vierkant.backend.command.builtin.Help;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.ConfigurationModel;
import ch.vknws.vierkant.type.ModelUtils;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.RankModel;

/**
 * Enters admin mode.
 * 
 * @author benjamin
 */
@CommandName("edit")
public class Edit implements Command {

  static {
    Class<?> clazz = Edit.class;
    String name = clazz.getAnnotation(CommandName.class).value();
    CommandExecutor.registerCommand(name, clazz);
  }
  private static final String QUERY_INDICATOR = ">";
  private static final char ESCAPE_CHARACTER = '\\';
  private static final Pattern COMMAND_END = Pattern.compile("[\n;]");
  private static final Pattern SET_FIELD = Pattern.compile("(.*)\\s*=\\s*(.*)");
  private static final Pattern ADD_TO_FIELD = Pattern.compile("(.*)\\s*\\+=\\s*(.*)");
  private static final Pattern REMOVE_FROM_FIELD = Pattern.compile("(.*)\\s*\\-=\\s*(.*)");
  private static final Pattern HELP = Pattern.compile("help");
  private static final Pattern EXIT = Pattern.compile("exit|quit");
  private static final Pattern COMMIT = Pattern.compile("save|commit");
  private static final Pattern ROLLBACK = Pattern.compile("rollback|cancel|discard");
  private static final Pattern SHOW = Pattern.compile("show|display");

  @Override
  public List<CommandExecutionResult> execute(Map<String, String> parameters, List<String> arguments,
      CommandContext context) {
    AssertUtils.notNull(parameters);
    AssertUtils.notNull(arguments);
    AssertUtils.notNull(context);
    List<CommandExecutionResult> result = new LinkedList<>();

    if (arguments.isEmpty()) {
      result.add(new CommandExecutionResult(Level.ERROR, "No model to edit was passed."));
    }

    Class<? extends DatabaseModel> clazz = null;
    String identification = null;

    if (arguments.size() > 0) {
      String clazzName = arguments.get(0);
      clazz = ModelUtils.findModelForName(clazzName);
    }
    if (clazz == null) {
      result.add(new CommandExecutionResult(Level.ERROR, "The passed model type does not exist."));
      return result;
    }

    if (arguments.size() > 1) {
      identification = arguments.get(1);
    }
    if (identification == null) {
      result.add(new CommandExecutionResult(Level.ERROR, "null identifier not allowed."));
    }

    DatabaseModel model = loadModel(clazz, identification);
    if (model == null) {
      result.add(new CommandExecutionResult(Level.ERROR, "Could not find the model."));
    }

    boolean continueRunning = true;
    InputStreamReader reader = new InputStreamReader(context.getInputStream());
    while (result.isEmpty() && continueRunning) {
      String action = readAction(context, reader);
      continueRunning = handleAction(result, context, action, clazz, model);
    }

    return result;
  }

  private DatabaseModel loadModel(Class<? extends DatabaseModel> clazz, String identifier) {
    AssertUtils.notNull(clazz);
    AssertUtils.notNull(identifier);

    DatabaseModel model = null;
    AbstractDatabaseDAO<?> dao = DaoFactory.getInstance().findDaoForType(clazz);

    if (StringUtils.isInteger(identifier)) {
      // it's the ID
      model = dao.load(Integer.valueOf(identifier));
    } else {
      // it's the name (or similar, depends on the model)
      AbstractRestriction restriction = null;

      if (clazz.isAnnotationPresent(DescriptiveNames.class) || clazz.isAnnotationPresent(DescriptiveName.class)) {
        DescriptiveName[] names = clazz.getAnnotationsByType(DescriptiveName.class);

        // handle special cases
        if (ConfigurationModel.class.equals(clazz)) {
          String[] split = identifier.split("/");
          if (split.length != 3) { return model; }

          ConjunctionRestriction lower = new ConjunctionRestriction();
          ConjunctionRestriction upper = new ConjunctionRestriction();

          lower.setLeft(new EqualityRestriction(ConfigurationModel.PropertyId.USER_NAME, split[0]));
          lower.setRight(new EqualityRestriction(ConfigurationModel.PropertyId.CONTEXT, split[1]));
          upper.setLeft(lower);
          upper.setRight(new EqualityRestriction(ConfigurationModel.PropertyId.KEY_NAME, split[2]));

          restriction = upper;
        } else if (PersonModel.class.equals(clazz)) {
          String[] split = identifier.split(",");
          if (split.length != 2) {
            restriction = new EqualityRestriction(PersonModel.PropertyId.SHORTHAND, identifier);
          } else {

            restriction = new ConjunctionRestriction(
                new EqualityRestriction(PersonModel.PropertyId.FAMILY_NAME, split[0]),
                new EqualityRestriction(PersonModel.PropertyId.GIVEN_NAME, split[1]));
          }
        } else if (RankModel.class.equals(clazz)) {
          restriction = new DisjunctionRestriction(
              new EqualityRestriction(RankModel.PropertyId.ABBREVIATION, identifier),
              new EqualityRestriction(RankModel.PropertyId.RANK_NAME, identifier));
        } else {
          if (names.length == 1) {
            restriction = new EqualityRestriction(names[0].value(), identifier);
          } else {
            DisjunctionRestriction bottom = new DisjunctionRestriction();
            bottom.setLeft(new EqualityRestriction(names[0].value(), identifier));
            bottom.setRight(new EqualityRestriction(names[1].value(), identifier));
            DisjunctionRestriction current = bottom;

            for (int i = 2; i < names.length; i++) {
              DisjunctionRestriction work = new DisjunctionRestriction();
              work.setLeft(new EqualityRestriction(names[i].value(), identifier));
              work.setRight(current);
              current = work;
            }

            restriction = current;
          }
        }
      }
      if (restriction != null) {
        List<?> found = dao.find(restriction, 0, 2);
        if (found != null && found.size() == 1) {
          model = (DatabaseModel) found.get(0);
        }
      }
    }

    return model;
  }

  private String readAction(CommandContext context, InputStreamReader reader) {
    try {
      context.getOutputStream().write(QUERY_INDICATOR.getBytes());
      context.getOutputStream().write(" ".getBytes());
    } catch (IOException e) {
      context.error("Could not write to stream.", e);
      return null;
    }

    StringBuilder builder = new StringBuilder();
    try {
      while (true) {
        char character = (char) reader.read();
        if (-1 == character) break;
        // String character = scanner.next(".");
        if (Objects.equals(character, ESCAPE_CHARACTER)) {
          // character = scanner.next();
          character = (char) reader.read();
        } else if (COMMAND_END.matcher(String.valueOf(character)).matches()) {
          break;
        }

        builder.append(character);

      }
    } catch (IOException e) {
      context.error("Could not read from stream.", e);
      return null;
    }

    return builder.toString();
  }

  private boolean handleAction(List<CommandExecutionResult> result, CommandContext context, String action,
      Class<? extends DatabaseModel> clazz, DatabaseModel model) {
    if (HELP.matcher(action).matches()) {
      return handleHelpAction(result, context);
    } else if (EXIT.matcher(action).matches()) {
      return handleExitAction(result, context);
    } else if (COMMIT.matcher(action).matches()) {
      return handleCommitAction(result, context, clazz, model);
    } else if (ROLLBACK.matcher(action).matches()) {
      return handleRollbackAction(result, context);
    } else if (SHOW.matcher(action).matches()) {
      return handleShowAction(result, context, clazz, model);
    } else {
      Matcher matcher = ADD_TO_FIELD.matcher(action);
      if (matcher.matches()) { return handleAddToFieldAction(result, context, matcher.group(1).trim(),
          matcher.group(2).trim(), clazz, model); }

      matcher = REMOVE_FROM_FIELD.matcher(action);
      if (matcher.matches()) { return handleRemoveFromFieldAction(result, context, matcher.group(1).trim(),
          matcher.group(2).trim(), clazz, model); }

      matcher = SET_FIELD.matcher(action);
      if (matcher.matches()) { return handleSetFieldAction(result, context, matcher.group(1).trim(),
          matcher.group(2).trim(), clazz, model); }
    }

    return true;
  }

  @SuppressWarnings("unchecked")
  private boolean handleSetFieldAction(List<CommandExecutionResult> result, CommandContext context, String field,
      String value, Class<? extends DatabaseModel> clazz, DatabaseModel model) {

    Map<String, Class<?>> propertyTypes = null;

    for (Class<?> candidate : clazz.getDeclaredClasses()) {
      if ("PropertyId".equals(candidate.getSimpleName())) {
        try {
          Field types = candidate.getField("PROPERTY_TYPES");
          types.setAccessible(true);
          propertyTypes = (Map<String, Class<?>>) types.get(null);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
          result.add(new CommandExecutionResult(Level.ERROR, "Could not verify field existence"));
          return true;
        }
      }
    }

    if (propertyTypes == null) {
      result.add(new CommandExecutionResult(Level.ERROR, "Could not verify field existence"));
      return true;
    }

    Class<?> fieldType = propertyTypes.get(field);
    Object effectiveValue = null;
    if (Collection.class.isAssignableFrom(fieldType)) {
      Class<?> containedType = fieldType.getTypeParameters()[0].getGenericDeclaration();

      List<?> list = null;

      if (DatabaseModel.class.isAssignableFrom(containedType)) {
        List<Object> l = new LinkedList<>();

        for (String identifier : value.split(";")) {
          DatabaseModel loadModel = loadModel((Class<? extends DatabaseModel>) containedType, identifier);
          l.add(loadModel);
        }

        list = l;
      } else {
        list = StringUtils.convertStringToList(containedType, value);
      }

      if (List.class.isAssignableFrom(fieldType)) {
        effectiveValue = list;
      } else if (Set.class.isAssignableFrom(fieldType)) {
        effectiveValue = new HashSet<>(list);
      }

      if (effectiveValue == null) {
        result.add(new CommandExecutionResult(Level.ERROR, "Could not convert value to collection"));
        return true;
      }
    } else if (DatabaseModel.class.isAssignableFrom(fieldType)) {
      DatabaseModel loadModel = loadModel((Class<? extends DatabaseModel>) fieldType, value);
      effectiveValue = loadModel;
    } else {
      effectiveValue = StringUtils.convertStringToType(fieldType, value);
    }

    try {
      model.set(field, effectiveValue);
    } catch (RuntimeException e) {
      result.add(new CommandExecutionResult(Level.ERROR, "Could not convert value to collection"));
    }

    return true;
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  private boolean handleAddToFieldAction(List<CommandExecutionResult> result, CommandContext context, String field,
      String value, Class<? extends DatabaseModel> clazz, DatabaseModel model) {

    Map<String, Class<?>> propertyTypes = null;

    for (Class<?> candidate : clazz.getDeclaredClasses()) {
      if ("PropertyId".equals(candidate.getSimpleName())) {
        try {
          Field types = candidate.getField("PROPERTY_TYPES");
          types.setAccessible(true);
          propertyTypes = (Map<String, Class<?>>) types.get(null);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
          result.add(new CommandExecutionResult(Level.ERROR, "Could not verify field existence"));
          return true;
        }
      }
    }

    if (propertyTypes == null) {
      result.add(new CommandExecutionResult(Level.ERROR, "Could not verify field existence"));
      return true;
    }

    Class<?> fieldType = propertyTypes.get(field);
    Object effectiveValue = null;
    if (Collection.class.isAssignableFrom(fieldType)) {
      Type genericReturnType;
      try {
        genericReturnType = clazz.getMethod("get" + StringUtils.toSentenceCase(field)).getGenericReturnType();
      } catch (NoSuchMethodException | SecurityException e) {
        ServiceRegistry.getLogProxy().error(this, "Could not get type of field {0}", e, field);
        result.add(new CommandExecutionResult(Level.ERROR, "Could not get type of field " + field));
        return true;
      }
      Class<?> containedType = (Class<?>) ((ParameterizedType) genericReturnType).getActualTypeArguments()[0];

      List<?> list = null;

      if (DatabaseModel.class.isAssignableFrom(containedType)) {
        List<Object> l = new LinkedList<>();

        for (String identifier : value.split(";")) {
          DatabaseModel loadModel = loadModel((Class<? extends DatabaseModel>) containedType, identifier);
          l.add(loadModel);
        }

        list = l;
      } else {
        list = StringUtils.convertStringToList(containedType, value);
      }

      if (List.class.isAssignableFrom(fieldType)) {
        effectiveValue = list;
      } else if (Set.class.isAssignableFrom(fieldType)) {
        effectiveValue = new HashSet<>(list);
      }

      if (effectiveValue == null) {
        result.add(new CommandExecutionResult(Level.ERROR, "Could not convert value to collection"));
        return true;
      }
    } else {
      result.add(new CommandExecutionResult(Level.ERROR, "Could not convert value to collection"));
      return true;
    }

    try {
      Collection<?> object = (Collection<?>) model.get(field);
      object.addAll((Collection) effectiveValue);
      model.set(field, object);
    } catch (RuntimeException e) {
      result.add(new CommandExecutionResult(Level.ERROR, "Could not convert value to collection"));
    }

    return true;
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  private boolean handleRemoveFromFieldAction(List<CommandExecutionResult> result, CommandContext context, String field,
      String value, Class<? extends DatabaseModel> clazz, DatabaseModel model) {

    Map<String, Class<?>> propertyTypes = null;

    for (Class<?> candidate : clazz.getDeclaredClasses()) {
      if ("PropertyId".equals(candidate.getSimpleName())) {
        try {
          Field types = candidate.getField("PROPERTY_TYPES");
          types.setAccessible(true);
          propertyTypes = (Map<String, Class<?>>) types.get(null);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
          result.add(new CommandExecutionResult(Level.ERROR, "Could not verify field existence"));
          return true;
        }
      }
    }

    if (propertyTypes == null) {
      result.add(new CommandExecutionResult(Level.ERROR, "Could not verify field existence"));
      return true;
    }

    Class<?> fieldType = propertyTypes.get(field);
    Object effectiveValue = null;
    if (Collection.class.isAssignableFrom(fieldType)) {
      Type genericReturnType;
      try {
        genericReturnType = clazz.getMethod("get" + StringUtils.toSentenceCase(field)).getGenericReturnType();
      } catch (NoSuchMethodException | SecurityException e) {
        ServiceRegistry.getLogProxy().error(this, "Could not get type of field {0}", e, field);
        result.add(new CommandExecutionResult(Level.ERROR, "Could not get type of field " + field));
        return true;
      }
      Class<?> containedType = (Class<?>) ((ParameterizedType) genericReturnType).getActualTypeArguments()[0];

      List<?> list = null;

      if (DatabaseModel.class.isAssignableFrom(containedType)) {
        List<Object> l = new LinkedList<>();

        for (String identifier : value.split(";")) {
          DatabaseModel loadModel = loadModel((Class<? extends DatabaseModel>) containedType, identifier);
          l.add(loadModel);
        }

        list = l;
      } else {
        list = StringUtils.convertStringToList(containedType, value);
      }

      if (List.class.isAssignableFrom(fieldType)) {
        effectiveValue = list;
      } else if (Set.class.isAssignableFrom(fieldType)) {
        effectiveValue = new HashSet<>(list);
      }

      if (effectiveValue == null) {
        result.add(new CommandExecutionResult(Level.ERROR, "Could not convert value to collection"));
        return true;
      }
    } else {
      result.add(new CommandExecutionResult(Level.ERROR, "Could not convert value to collection"));
      return true;
    }

    try {
      Collection<?> object = (Collection<?>) model.get(field);
      object.removeAll((Collection) effectiveValue);
      model.set(field, object);
    } catch (RuntimeException e) {
      result.add(new CommandExecutionResult(Level.ERROR, "Could not convert value to collection"));
    }

    return true;
  }

  private boolean handleHelpAction(List<CommandExecutionResult> result, CommandContext context) {
    Map<String, String> parameters = new HashMap<>();
    List<String> arguments = new LinkedList<>();
    arguments.add(Edit.class.getAnnotation(CommandName.class).value());

    new Help().execute(parameters, arguments, context);

    return true;
  }

  private boolean handleExitAction(List<CommandExecutionResult> result, CommandContext context) {
    handleRollbackAction(result, context);

    try {
      context.getOutputStream().write("Goodbye.".getBytes());
    } catch (IOException e) {
    }

    return false;
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  private boolean handleCommitAction(List<CommandExecutionResult> result, CommandContext context,
      Class<? extends DatabaseModel> clazz, DatabaseModel model) {

    AbstractDatabaseDAO dao = DaoFactory.getInstance().findDaoForType(clazz);
    try {
      dao.save(model, ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
    } catch (SaveException e) {
      result.add(new CommandExecutionResult(Level.ERROR, "Could not save model"));
    }

    return true;
  }

  private boolean handleRollbackAction(List<CommandExecutionResult> result, CommandContext context) {
    // TODO
    return true;
  }

  @SuppressWarnings("unchecked")
  private boolean handleShowAction(List<CommandExecutionResult> result, CommandContext context,
      Class<? extends DatabaseModel> clazz, DatabaseModel model) {

    Map<String, Class<?>> propertyTypes = null;

    for (Class<?> candidate : clazz.getDeclaredClasses()) {
      if ("PropertyId".equals(candidate.getSimpleName())) {
        try {
          Field types = candidate.getField("PROPERTY_TYPES");
          types.setAccessible(true);
          propertyTypes = (Map<String, Class<?>>) types.get(null);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
          result.add(new CommandExecutionResult(Level.ERROR, "Could not verify field existence"));
          return true;
        }
      }
    }

    if (propertyTypes == null) {
      result.add(new CommandExecutionResult(Level.ERROR, "Could not verify field existence"));
      return true;
    }

    for (String fieldName : propertyTypes.keySet()) {
      try {
        Object value = model.get(fieldName);
        context.getOutputStream().write(fieldName.getBytes());
        context.getOutputStream().write(" = ".getBytes());
        if (value != null) {
          context.getOutputStream().write(Objects.toString(value).getBytes());
        } else {
          context.getOutputStream().write("<null>".getBytes());
        }
        context.getOutputStream().write("\n".getBytes());
      } catch (SecurityException | IOException | IllegalArgumentException e) {
        result.add(new CommandExecutionResult(Level.ERROR, "Could not display field: " + fieldName));
      }
    }

    return true;
  }

}
