package ch.vknws.vierkant.command.builtin;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.google.common.base.Strings;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.util.AssertUtils;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.command.Command;
import ch.vknws.vierkant.backend.command.CommandContext;
import ch.vknws.vierkant.backend.command.CommandExecutionResult;
import ch.vknws.vierkant.backend.command.CommandExecutionResult.Level;
import ch.vknws.vierkant.backend.command.CommandExecutor;
import ch.vknws.vierkant.backend.command.CommandName;
import ch.vknws.vierkant.backend.command.CommandParameter;
import ch.vknws.vierkant.backend.ldap.AuthenticationUtil;
import ch.vknws.vierkant.type.PersonModel;

/**
 * Enters admin mode.
 * 
 * @author benjamin
 */
@CommandName("sudo")
@CommandParameter(value = "password", flag = false, optional = false, aliases = { "p" })
public class Sudo implements Command {
  static {
    Class<?> clazz = Sudo.class;
    String name = clazz.getAnnotation(CommandName.class).value();
    CommandExecutor.registerCommand(name, clazz);
  }

  @Override
  public List<CommandExecutionResult> execute(Map<String, String> parameters, List<String> arguments,
      CommandContext context) {
    AssertUtils.notNull(parameters);
    AssertUtils.notNull(arguments);
    AssertUtils.notNull(context);
    List<CommandExecutionResult> result = new LinkedList<>();

    String user = ApplicationRegistry.getInstance().getLoggedInUser().getShorthand();
    String password = parameters.get("password");

    if (Strings.isNullOrEmpty(password)) {
      result.add(new CommandExecutionResult(Level.ERROR, "No password to authenticate given."));
      return result;
    }

    PersonModel authenticated = AuthenticationUtil.authenticate(user, password);

    if (authenticated == null) {
      result.add(new CommandExecutionResult(Level.INFO, "Username/password was incorrect."));
      return result;
    }

    boolean administrator = AuthenticationUtil.isAdministrator(user);

    if (!administrator) {
      ServiceRegistry.getLogProxy().info(this, "User {0} attempted to sudo, but has insufficient privileges.", user);
      result.add(new CommandExecutionResult(Level.INFO, "User has insufficient privileges."));
      return result;
    }

    ApplicationRegistry.getInstance().setAdminModeEntered(LocalDateTime.now());
    ServiceRegistry.getLogProxy().info(this, "User {0} was granted sudo.", user);

    return result;
  }

}
