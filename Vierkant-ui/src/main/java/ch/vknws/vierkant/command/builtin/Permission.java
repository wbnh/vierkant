package ch.vknws.vierkant.command.builtin;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import ch.bwe.faa.v1.core.util.AssertUtils;
import ch.bwe.fac.v1.type.SaveException;
import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.command.Command;
import ch.vknws.vierkant.backend.command.CommandContext;
import ch.vknws.vierkant.backend.command.CommandExecutionResult;
import ch.vknws.vierkant.backend.command.CommandExecutionResult.Level;
import ch.vknws.vierkant.backend.command.CommandExecutor;
import ch.vknws.vierkant.backend.command.CommandName;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.AuthorizationKeyModel;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.impl.AuthorizationKeyPersistable;

/**
 * Enters admin mode.
 * 
 * @author benjamin
 */
@CommandName("permission")
public class Permission implements Command {
  static {
    Class<?> clazz = Permission.class;
    String name = clazz.getAnnotation(CommandName.class).value();
    CommandExecutor.registerCommand(name, clazz);
  }

  private static final String KEYWORD_USER = "user";
  private static final String KEYWORD_ROLE = "role";
  private static final String KEYWORD_GRANT = "grant";
  private static final String KEYWORD_REVOKE = "revoke";

  @Override
  public List<CommandExecutionResult> execute(Map<String, String> parameters, List<String> arguments,
      CommandContext context) {
    AssertUtils.notNull(parameters);
    AssertUtils.notNull(arguments);
    AssertUtils.notNull(context);
    List<CommandExecutionResult> result = new LinkedList<>();

    Iterator<String> iterator = arguments.iterator();
    String target = iterator.next();
    switch (target) {
    case KEYWORD_USER:
      handleUser(iterator, result, context);
      break;
    case KEYWORD_ROLE:
      handleRole(iterator, result, context);
      break;
    default:
      result.add(new CommandExecutionResult(Level.ERROR,
          "Target '" + target + "' not recognised, must be '" + KEYWORD_USER + "' or '" + KEYWORD_ROLE + "'."));
      return result;
    }

    return result;
  }

  private void handleUser(Iterator<String> iterator, List<CommandExecutionResult> result, CommandContext context) {

    String token = iterator.next();
    List<PersonModel> users = new LinkedList<>();

    while (token != null && !Objects.equals(token, KEYWORD_GRANT) && !Objects.equals(token, KEYWORD_REVOKE)) {

      String user = token;

      PersonModel person = DaoFactory.getInstance().getPersonDao().loadByShorthand(user);
      if (person == null) {
        result.add(new CommandExecutionResult(Level.ERROR, "User '" + user + "' not found."));
        return;
      }

      if (person.getRefAuthorizationKey() == null) {
        AuthorizationKeyModel authorizationKey = new AuthorizationKeyPersistable();
        try {
          authorizationKey = DaoFactory.getInstance().getAuthorizationKeyDao().save(authorizationKey, 0);
        } catch (SaveException e) {
          String message = "Could not save new authorization key.";
          context.error(message, e);
          result.add(new CommandExecutionResult(Level.ERROR, message));
          return;
        }
        person.setRefAuthorizationKey(authorizationKey);

        try {
          DaoFactory.getInstance().getPersonDao().save(person,
              ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
        } catch (SaveException e) {
          String message = "Could not save person";
          context.error(message, e);
          result.add(new CommandExecutionResult(Level.ERROR, message));
          return;
        }
      }

      users.add(person);

      token = iterator.next();
    }

    if (token == null) {
      result.add(new CommandExecutionResult(Level.ERROR, "Arguments malformed, no action detected"));
      return;
    }

    String action = token;
    String authorizationKey = iterator.next();

    switch (action) {
    case KEYWORD_GRANT:
      AuthorizationKeyModel authorization = DaoFactory.getInstance().getAuthorizationKeyDao()
          .loadByKeyName(authorizationKey);

      if (authorization == null) {
        result.add(new CommandExecutionResult(Level.ERROR, "Could not find authorization: " + authorizationKey));
        return;
      }
      for (PersonModel user : users) {
        user.setRefAuthorizationKey(grantAuthorization(user.getRefAuthorizationKey(), authorization, result, context));
      }
      break;
    case KEYWORD_REVOKE:
      for (PersonModel user : users) {
        user.setRefAuthorizationKey(
            revokeAuthorization(user.getRefAuthorizationKey(), authorizationKey, result, context));
      }
      break;
    default:
      result.add(new CommandExecutionResult(Level.ERROR,
          "Action '" + action + "' not recognised, must be '" + KEYWORD_GRANT + "' or '" + KEYWORD_REVOKE + "'."));
      return;
    }
  }

  private void handleRole(Iterator<String> iterator, List<CommandExecutionResult> result, CommandContext context) {

    String token = iterator.next();
    List<OrganizationFunctionModel> functions = new LinkedList<>();

    while (token != null && !Objects.equals(token, KEYWORD_GRANT) && !Objects.equals(token, KEYWORD_REVOKE)) {

      String role = token;

      OrganizationFunctionModel function = DaoFactory.getInstance().getOrganizationFunctionDao().loadByName(role);
      if (function == null) {
        result.add(new CommandExecutionResult(Level.ERROR, "Role '" + role + "' not found."));
        return;
      }

      if (function.getRefAuthorizationKey() == null) {
        AuthorizationKeyModel authorizationKey = new AuthorizationKeyPersistable();
        try {
          authorizationKey = DaoFactory.getInstance().getAuthorizationKeyDao().save(authorizationKey, 0);
        } catch (SaveException e) {
          String message = "Could not save new authorization key.";
          context.error(message, e);
          result.add(new CommandExecutionResult(Level.ERROR, message));
          return;
        }
        function.setRefAuthorizationKey(authorizationKey);

        try {
          DaoFactory.getInstance().getOrganizationFunctionDao().save(function,
              ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
        } catch (SaveException e) {
          String message = "Could not save role";
          context.error(message, e);
          result.add(new CommandExecutionResult(Level.ERROR, message));
          return;
        }
      }

      functions.add(function);

      token = iterator.next();
    }

    if (token == null) {
      result.add(new CommandExecutionResult(Level.ERROR, "Arguments malformed, no action detected"));
      return;
    }

    String action = token;
    String authorizationKey = iterator.next();

    switch (action) {
    case KEYWORD_GRANT:
      AuthorizationKeyModel authorization = DaoFactory.getInstance().getAuthorizationKeyDao()
          .loadByKeyName(authorizationKey);

      if (authorization == null) {
        result.add(new CommandExecutionResult(Level.ERROR, "Could not find authorization: " + authorizationKey));
        return;
      }
      for (OrganizationFunctionModel function : functions) {
        function.setRefAuthorizationKey(
            grantAuthorization(function.getRefAuthorizationKey(), authorization, result, context));
      }
      break;
    case KEYWORD_REVOKE:
      for (OrganizationFunctionModel function : functions) {
        function.setRefAuthorizationKey(
            revokeAuthorization(function.getRefAuthorizationKey(), authorizationKey, result, context));
      }
      break;
    default:
      result.add(new CommandExecutionResult(Level.ERROR,
          "Action '" + action + "' not recognised, must be '" + KEYWORD_GRANT + "' or '" + KEYWORD_REVOKE + "'."));
      return;
    }
  }

  private AuthorizationKeyModel grantAuthorization(AuthorizationKeyModel model, AuthorizationKeyModel authorizationKey,
      List<CommandExecutionResult> result, CommandContext context) {
    model = DaoFactory.getInstance().getAuthorizationKeyDao().load(model.getRecordId());
    Set<AuthorizationKeyModel> subordinates = model.getRefSubordinates();
    subordinates.add(authorizationKey);
    model.setRefSubordinates(subordinates);

    try {
      return DaoFactory.getInstance().getAuthorizationKeyDao().save(model,
          ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
    } catch (SaveException e) {
      String message = "Could not save authorization key";
      context.error(message, e);
      result.add(new CommandExecutionResult(Level.ERROR, message));
      return null;
    }
  }

  private AuthorizationKeyModel revokeAuthorization(AuthorizationKeyModel model, String authorizationKey,
      List<CommandExecutionResult> result, CommandContext context) {
    model = DaoFactory.getInstance().getAuthorizationKeyDao().load(model.getRecordId());
    Set<AuthorizationKeyModel> subordinates = model.getRefSubordinates();
    for (Iterator<AuthorizationKeyModel> iterator = subordinates.iterator(); iterator.hasNext();) {
      AuthorizationKeyModel currentKey = iterator.next();
      if (Objects.equals(currentKey.getKeyName(), authorizationKey)) {
        iterator.remove();
      }
    }

    model.setRefSubordinates(subordinates);

    try {
      return DaoFactory.getInstance().getAuthorizationKeyDao().save(model,
          ApplicationRegistry.getInstance().getLoggedInUser().getRecordId());
    } catch (SaveException e) {
      String message = "Could not save authorization key";
      context.error(message, e);
      result.add(new CommandExecutionResult(Level.ERROR, message));
      return null;
    }
  }

}
