package ch.vknws.vierkant;

/**
 * Texts valid for the whole application.
 *
 * @author Benjamin Weber
 */
public interface ApplicationTexts {

  /**
   * @return the text
   */
  String systemUserShortcut();

  /**
   * @return the text
   */
  String applicationNameLabel();

  /**
   * @return the text
   */
  String applicationVersionLabel();

  /**
   * @return the text
   */
  String applicationVersion();

  /**
   * @return the text
   */
  String applicationBuildTimeLabel();

  /**
   * @return the text
   */
  String applicationBuildTime();

  /**
   * @return the text
   */
  String maintainer();

  /**
   * @return the text
   */
  String usernameField();

  /**
   * @return the text
   */
  String passwordField();

  /**
   * @return the text
   */
  String resetPassword();

  /**
   * @return the text
   */
  String login();

  /**
   * @return the text
   */
  String interested();

  /**
   * @return the text
   */
  String currentPasswordField();

  /**
   * @return the text
   */
  String newPasswordField();

  /**
   * @return the text
   */
  String passwordConfirmField();

  /**
   * @return the text
   */
  String resetPasswordMailSent();

  /**
   * @return the text
   */
  String add();

  /**
   * @return the text
   */
  String remove();

  /**
   * @return the text
   */
  String sendTestErrorReport();

  /**
   * @return the text
   */
  String adminMessageTitle();
}
