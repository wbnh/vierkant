package ch.vknws.vierkant;

/**
 * Audit comment modes.
 *
 * @author Benjamin Weber
 */
public enum AuditCommentMode {

  /**
   * Audit comments must be given for all changes.
   */
  MANDATORY,

  /**
   * Audit comments can be given, but are not mandatory.
   */
  OPTIONAL,

  /**
   * No audit comments can be given.
   */
  DISABLED
}
