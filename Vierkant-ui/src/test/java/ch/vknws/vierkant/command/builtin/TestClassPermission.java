package ch.vknws.vierkant.command.builtin;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.command.CommandExecutionResult;
import ch.vknws.vierkant.backend.command.CommandExecutor;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.AuthorizationKeyModel;
import ch.vknws.vierkant.type.PersonModel;

/**
 * Test class for the sudo command.
 * 
 * @author benjamin
 */
public class TestClassPermission {

  private Permission permission;
  private Map<String, String> parameters;
  private List<String> arguments;

  /**
   * Sets the test up.
   */
  @Before
  public void setUp() {
    permission = new Permission();
    parameters = new HashMap<>();
    arguments = new LinkedList<>();

    ApplicationRegistry.setTestInstance(new ApplicationRegistry());

    PersonModel admin = DaoFactory.getInstance().getPersonDao().loadByShorthand("adm");
    ApplicationRegistry.getInstance().setLoggedInUser(admin);
  }

  /**
   * Checks whether the auth key contains another.
   * 
   * @param authKey the auth key to check
   * @param name    the name of the sub auth key
   * @return whether it is contained
   */
  private boolean containsAuthorizationKey(AuthorizationKeyModel authKey, String name) {
    if (authKey == null) { return false; }
    for (AuthorizationKeyModel sub : authKey.getRefSubordinates()) {
      if (sub == null) {
        continue;
      }
      if (Objects.equals(sub.getKeyName(), name)) { return true; }
    }
    return false;
  }

  /**
   * @throws Exception if a problem occurs.
   */
  @Test
  public void testUserSuccessful() throws Exception {
    arguments.add("user");
    arguments.add("adm");
    arguments.add("grant");
    arguments.add("auditor");

    List<CommandExecutionResult> result = CommandExecutor.executeCommand(permission, parameters, arguments);

    Assert.assertEquals(0, result.size());
    Assert.assertTrue(containsAuthorizationKey(
        DaoFactory.getInstance().getPersonDao().loadByShorthand("adm").getRefAuthorizationKey(), "auditor"));

    arguments.clear();
    arguments.add("user");
    arguments.add("adm");
    arguments.add("revoke");
    arguments.add("auditor");

    result = CommandExecutor.executeCommand(permission, parameters, arguments);

    Assert.assertEquals(0, result.size());
    Assert.assertFalse(containsAuthorizationKey(
        DaoFactory.getInstance().getPersonDao().loadByShorthand("adm").getRefAuthorizationKey(), "auditor"));
  }

  /**
   * @throws Exception if a problem occurs.
   */
  @Test
  public void testRoleSuccessful() throws Exception {
    arguments.add("role");
    arguments.add("Sekretär");
    arguments.add("grant");
    arguments.add("auditor");

    List<CommandExecutionResult> result = CommandExecutor.executeCommand(permission, parameters, arguments);

    Assert.assertEquals(0, result.size());
    Assert.assertTrue(containsAuthorizationKey(
        DaoFactory.getInstance().getOrganizationFunctionDao().loadByName("Sekretär").getRefAuthorizationKey(), "auditor"));

    arguments.clear();
    arguments.add("role");
    arguments.add("Sekretär");
    arguments.add("revoke");
    arguments.add("auditor");

    result = CommandExecutor.executeCommand(permission, parameters, arguments);

    Assert.assertEquals(0, result.size());
    Assert.assertFalse(containsAuthorizationKey(
        DaoFactory.getInstance().getOrganizationFunctionDao().loadByName("Sekretär").getRefAuthorizationKey(), "auditor"));
  }

}
