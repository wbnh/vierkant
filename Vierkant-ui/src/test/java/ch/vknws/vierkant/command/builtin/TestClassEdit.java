package ch.vknws.vierkant.command.builtin;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.command.CommandContext;
import ch.vknws.vierkant.backend.command.CommandExecutionResult;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.TrainingCourseModel;
import ch.vknws.vierkant.type.impl.TrainingCoursePersistable;

/**
 * Test class for the sudo command.
 * 
 * @author benjamin
 */
public class TestClassEdit {

  private Edit edit;
  private Map<String, String> parameters;
  private List<String> arguments;

  private static class CustomCommandContext extends CommandContext {
    private InputStream in;
    private OutputStream out;

    public CustomCommandContext(InputStream in, OutputStream out) {
      this.in = in;
      this.out = out;
    }

    @Override
    public void log(Level level, String message, Throwable problem, Object... arguments) {
      // TODO Auto-generated method stub

    }

    @Override
    public void print(String message, Object... arguments) {
      try {
        out.write(MessageFormat.format(message, arguments).getBytes());
      } catch (IOException e) {
      }
    }

    @Override
    public InputStream getInputStream() {
      return in;
    }

    @Override
    public OutputStream getOutputStream() {
      return out;
    }

  }

  /**
   * Sets the test up.
   */
  @Before
  public void setUp() {
    edit = new Edit();
    parameters = new HashMap<>();
    arguments = new LinkedList<>();

    ApplicationRegistry.setTestInstance(new ApplicationRegistry());

    PersonModel admin = DaoFactory.getInstance().getPersonDao().loadByShorthand("adm");
    ApplicationRegistry.getInstance().setLoggedInUser(admin);
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testEditAdminSuccessful() throws Exception {
    arguments.add("Person");
    arguments.add("adm");

    InputStream in = new ByteArrayInputStream("givenName=sali;show;exit\n".getBytes());
    OutputStream out = new ByteArrayOutputStream();

    List<CommandExecutionResult> result = edit.execute(parameters, arguments, new CustomCommandContext(in, out));

    String string = out.toString();
    System.out.println(string);
    System.out.println();

    Assert.assertTrue(string.contains("givenName = sali"));
    Assert.assertTrue(result.isEmpty());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testEditAdminReferenceSuccessful() throws Exception {
    DaoFactory.getInstance().getRankDao();
    arguments.add("Person");
    arguments.add("adm");

    InputStream in = new ByteArrayInputStream("refRank=Asp;show;exit\n".getBytes());
    OutputStream out = new ByteArrayOutputStream();

    List<CommandExecutionResult> result = edit.execute(parameters, arguments, new CustomCommandContext(in, out));

    String string = out.toString();
    System.out.println(string);
    System.out.println();

    Assert.assertTrue(string.contains("refRank = Aspirant"));
    Assert.assertTrue(result.isEmpty());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testEditAdminCollectionAddRemoveSuccessful() throws Exception {
    TrainingCourseModel trainingCourse = new TrainingCoursePersistable();
    trainingCourse.setTrainingCourseName("TrainingCourseTest");
    trainingCourse = DaoFactory.getInstance().getTrainingCourseDao().save(trainingCourse, 0);

    arguments.add("Person");
    arguments.add("adm");

    InputStream in = new ByteArrayInputStream(
        "refCompletedCourses += TrainingCourseTest;show;refCompletedCourses -= TrainingCourseTest;show;exit\n"
            .getBytes());
    OutputStream out = new ByteArrayOutputStream();

    List<CommandExecutionResult> result = edit.execute(parameters, arguments, new CustomCommandContext(in, out));

    DaoFactory.getInstance().getTrainingCourseDao().remove(trainingCourse.getRecordId());

    String string = out.toString();
    System.out.println(string);
    System.out.println();

    Assert.assertTrue(result.isEmpty());
    Assert.assertTrue(string.contains("refCompletedCourses = [TrainingCourseTest]"));
    Assert.assertTrue(string.contains("refCompletedCourses = []"));

  }

}
