package ch.vknws.vierkant.command.builtin;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.ApplicationRegistry;
import ch.vknws.vierkant.backend.command.CommandExecutionResult;
import ch.vknws.vierkant.backend.command.CommandExecutionResult.Level;
import ch.vknws.vierkant.backend.command.CommandExecutor;
import ch.vknws.vierkant.backend.dao.DaoFactory;
import ch.vknws.vierkant.type.PersonModel;

/**
 * Test class for the sudo command.
 * 
 * @author benjamin
 */
public class TestClassSudo {

  private Sudo sudo;
  private Map<String, String> parameters;
  private List<String> arguments;

  /**
   * Sets the test up.
   */
  @Before
  public void setUp() {
    sudo = new Sudo();
    parameters = new HashMap<>();
    arguments = new LinkedList<>();

    ApplicationRegistry.setTestInstance(new ApplicationRegistry());

    PersonModel admin = DaoFactory.getInstance().getPersonDao().loadByShorthand("adm");
    ApplicationRegistry.getInstance().setLoggedInUser(admin);
  }

  /**
   * @throws Exception if a problem occurs.
   */
  @Test
  public void testSuccessful() throws Exception {
    parameters.put("password", "vk-nws");

    List<CommandExecutionResult> result = CommandExecutor.executeCommand(sudo, parameters, arguments);

    Assert.assertEquals(0, result.size());

    Assert.assertTrue(ApplicationRegistry.getInstance().isAdminMode());
  }

  /**
   * @throws Exception if a problem occurs.
   */
  @Test
  public void testNoPassword() throws Exception {

    List<CommandExecutionResult> result = CommandExecutor.executeCommand(sudo, parameters, arguments);

    Assert.assertEquals(1, result.size());
    Assert.assertEquals(Level.ERROR, result.get(0).getLevel());
    Assert.assertEquals("No password to authenticate given.", result.get(0).getMessage());

    Assert.assertFalse(ApplicationRegistry.getInstance().isAdminMode());
  }

  /**
   * @throws Exception if a problem occurs.
   */
  @Test
  public void testEmptyPassword() throws Exception {
    parameters.put("password", "");

    List<CommandExecutionResult> result = CommandExecutor.executeCommand(sudo, parameters, arguments);

    Assert.assertEquals(1, result.size());
    Assert.assertEquals(Level.ERROR, result.get(0).getLevel());
    Assert.assertEquals("No password to authenticate given.", result.get(0).getMessage());

    Assert.assertFalse(ApplicationRegistry.getInstance().isAdminMode());
  }

  /**
   * @throws Exception if a problem occurs.
   */
  @Test
  public void testWrongPassword() throws Exception {
    parameters.put("password", "wrongpassword");

    List<CommandExecutionResult> result = CommandExecutor.executeCommand(sudo, parameters, arguments);

    Assert.assertEquals(1, result.size());
    Assert.assertEquals(Level.INFO, result.get(0).getLevel());
    Assert.assertEquals("Username/password was incorrect.", result.get(0).getMessage());

    Assert.assertFalse(ApplicationRegistry.getInstance().isAdminMode());
  }
}
