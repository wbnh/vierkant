package ch.vknws.vierkant.data;

import java.util.Collection;

import org.junit.Test;

/**
 * Test class for the person container.
 *
 * @author Benjamin Weber
 */
public class TestClassPersonContainer {

  /**
   * @throws Exception if a problem occurs.
   */
  @Test
  public void testProperties() throws Exception {

    PersonContainer container = new PersonContainer();
    Collection<?> containerPropertyIds = container.getContainerPropertyIds();
    System.out.println(containerPropertyIds);
  }
}
