package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.TaskStatusModel;
import ch.vknws.vierkant.type.TaskWorkflowStatusModel;

/**
 * Implementation.
 * 
 * @author benjamin
 */
public class TaskWorkflowStatusPersistable extends AbstractDatabasePersistable implements TaskWorkflowStatusModel {

  private static final long serialVersionUID = 2169111668036357065L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refStatusRecordId")
  private TaskStatusPersistable refStatus;

  @OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
  @Fetch(FetchMode.SELECT)
  @JoinTable(name = "TaskWorkflowNextStatusesList", joinColumns = {
      @JoinColumn(name = "refWorkflowStatusRecordId") }, inverseJoinColumns = {
          @JoinColumn(name = "refNextWorkflowStatusRecordId") })
  private Set<TaskWorkflowStatusPersistable> refNextStatuses;

  @Override
  public void setRefStatus(TaskStatusModel refStatus) {
    if (refStatus == null || refStatus instanceof TaskStatusPersistable) {
      this.refStatus = (TaskStatusPersistable) refStatus;
    } else {
      throw new IllegalArgumentException();
    }
  }

  @Override
  public TaskStatusModel getRefStatus() {
    return refStatus;
  }

  @Override
  public void setRefNextStatuses(Set<TaskWorkflowStatusModel> refNextStatuses) {
    if (refNextStatuses == null) {
      this.refNextStatuses = null;
    } else {
      Set<TaskWorkflowStatusPersistable> persistables = new HashSet<>();
      for (var model : refNextStatuses) {
        persistables.add((TaskWorkflowStatusPersistable) model);
      }
      this.refNextStatuses = persistables;
    }
  }

  @Override
  public Set<TaskWorkflowStatusModel> getRefNextStatuses() {
    if (refNextStatuses == null) {
      refNextStatuses = new HashSet<>();
    }
    Set<TaskWorkflowStatusModel> models = new HashSet<>();
    for (var persistable : refNextStatuses) {
      models.add(persistable);
    }
    return models;
  }

  @Override
  public Integer getRecordId() {
    return recordId;
  }

  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  @Override
  public Integer getCreator() {
    return creator;
  }

  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  @Override
  public Integer getModifier() {
    return modifier;
  }

  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  @Override
  public String getAuditComment() {
    return auditComment;
  }

  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

}
