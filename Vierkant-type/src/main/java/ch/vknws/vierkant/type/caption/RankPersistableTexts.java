package ch.vknws.vierkant.type.caption;

/**
 * Captions for RankModels.
 *
 * @author Benjamin Weber
 */
public interface RankPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return the text
   */
  String abbreviation();

  /**
   * @return the text
   */
  String rankName();

  /**
   * @return the text
   */
  String defaultFunction();

  /**
   * @return the text
   */
  String hierarchyIndex();

  /**
   * @return the text
   */
  String category();

  /**
   * @return the text
   */
  String memberCountGoal();

  /**
   * @return the text
   */
  String shoulderStrapPicturePath();
}
