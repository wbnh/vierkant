package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.TaskBehaviorType;
import ch.vknws.vierkant.type.TaskTypeModel;

/**
 * @author benjamin
 */
public class TaskTypePersistable extends AbstractDatabasePersistable implements TaskTypeModel {

  private static final long serialVersionUID = 405900161531404092L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @Column
  private String taskTypeName;

  @Attribute
  @Column
  private String description;

  @Attribute
  @Column
  private Integer color;

  @Attribute
  @Column
  private String icon;

  @Attribute
  @Column
  private String taskBehaviorType;

  @Override
  public void setTaskTypeName(String taskTypeName) {
    this.taskTypeName = taskTypeName;
  }

  @Override
  public String getTaskTypeName() {
    return taskTypeName;
  }

  @Override
  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public void setColor(Integer color) {
    this.color = color;
  }

  @Override
  public Integer getColor() {
    return color;
  }

  @Override
  public void setIcon(String icon) {
    this.icon = icon;
  }

  @Override
  public String getIcon() {
    return icon;
  }

  @Override
  public void setBehaviorType(TaskBehaviorType behaviorType) {
    if (behaviorType == null) {
      this.taskBehaviorType = null;
    } else {
      this.taskBehaviorType = behaviorType.name();
    }
  }

  @Override
  public TaskBehaviorType getTaskBehaviorType() {
    if (taskBehaviorType == null) { return null; }
    return TaskBehaviorType.valueOf(taskBehaviorType);
  }

  @Override
  public Integer getRecordId() {
    return recordId;
  }

  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  @Override
  public Integer getCreator() {
    return creator;
  }

  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  @Override
  public Integer getModifier() {
    return modifier;
  }

  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  @Override
  public String getAuditComment() {
    return auditComment;
  }

  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

}
