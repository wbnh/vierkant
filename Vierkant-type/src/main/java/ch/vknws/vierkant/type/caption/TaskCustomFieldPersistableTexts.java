package ch.vknws.vierkant.type.caption;

/**
 * Texts.
 * 
 * @author benjamin
 */
public interface TaskCustomFieldPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return text
   */
  String customFieldName();

  /**
   * @return text
   */
  String description();

  /**
   * @return text
   */
  String doublePrecision();

  /**
   * @return text
   */
  String valueFilter();

  /**
   * @return text
   */
  String type();
}
