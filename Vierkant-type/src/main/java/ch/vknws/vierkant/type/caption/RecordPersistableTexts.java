package ch.vknws.vierkant.type.caption;

/**
 * Captions for the meeting record
 * 
 * @author benjamin
 */
public interface RecordPersistableTexts {

  /**
   * @return the text for the classification
   */
  String classification();

  /**
   * @return the text for the planned tractanda
   */
  String refPlannedTractanda();

  /**
   * @return the text for the actual tractanda
   */
  String refActualTractanda();
}
