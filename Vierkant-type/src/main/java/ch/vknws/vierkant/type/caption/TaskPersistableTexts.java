package ch.vknws.vierkant.type.caption;

/**
 * Texts.
 * 
 * @author benjamin
 */
public interface TaskPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return text
   */
  String title();

  /**
   * @return text
   */
  String description();

  /**
   * @return text
   */
  String classification();

  /**
   * @return text
   */
  String assignToCalculation();

  /**
   * @return text
   */
  String refType();

  /**
   * @return text
   */
  String refStatus();

  /**
   * @return text
   */
  String refWorkflow();

  /**
   * @return text
   */
  String refSubtasks();

  /**
   * @return text
   */
  String refCustomFields();
}
