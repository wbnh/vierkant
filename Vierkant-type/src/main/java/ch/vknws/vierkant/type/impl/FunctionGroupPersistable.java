package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.AuthorizationKeyModel;
import ch.vknws.vierkant.type.FunctionGroupModel;
import ch.vknws.vierkant.type.OrganizationFunctionModel;

/**
 * Persistable to the FunctionModel.
 *
 * @author Benjamin Weber
 */
@Entity
@Audited
public class FunctionGroupPersistable extends AbstractDatabasePersistable implements FunctionGroupModel {

  private static final long serialVersionUID = 2144228153470820234L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.REFRESH })
  @JoinColumn(name = "refAuthorizationKeyRecordId")
  private AuthorizationKeyPersistable refAuthorizationKey;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "refDetailsRecordId")
  private OrganizationFunctionPersistable refDetails;

  @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH }, mappedBy = "refGroups")
  private Set<OrganizationFunctionPersistable> refGroupMembers;

  /**
   * Returns the comment for the last modification.
   * 
   * @return the comment. Can be null depending on configuration.
   */
  @Override
  public String getAuditComment() {
    return auditComment;
  }

  /**
   * Returns the time the model was first saved on the DB.
   * 
   * @return the original save time. Null if unsaved.
   */
  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  /**
   * Returns the ID of the person who created the model.
   * 
   * @return the creator. Null if unsaved, special values apply.
   */
  @Override
  public Integer getCreator() {
    return creator;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  /**
   * Returns the time the model was last saved.
   * 
   * @return the time. Null if unsaved.
   */
  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  /**
   * Returns the ID of the person who last saved the model.
   * 
   * @return the ID. Null if unsaved. Special values apply.
   */
  @Override
  public Integer getModifier() {
    return modifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getRecordId() {
    return recordId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AuthorizationKeyModel getRefAuthorizationKey() {
    return refAuthorizationKey;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public OrganizationFunctionModel getRefDetails() {
    return refDetails;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Set<OrganizationFunctionModel> getRefGroupMembers() {
    Set<OrganizationFunctionPersistable> persistables = refGroupMembers;
    if (persistables == null) { return null; }
    Set<OrganizationFunctionModel> models = new HashSet<>();
    for (var persistable : persistables) {
      models.add(persistable);
    }

    return models;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  /**
   * Sets the comment for the last modification.
   * 
   * @param auditComment the comment
   */
  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

  /**
   * Sets the time the model was first saved on the DB.
   * 
   * @param creationDateTime the time
   */
  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  /**
   * Sets the ID of the person who created the model.
   * 
   * @param creator the creator
   */
  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  /**
   * Sets the time the model was last saved.
   * 
   * @param modificationDateTime the time
   */
  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  /**
   * Sets the ID of the person who last saved the model.
   * 
   * @param modifier the ID
   */
  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefAuthorizationKey(AuthorizationKeyModel refAuthorizationKey) {
    if (refAuthorizationKey == null || refAuthorizationKey instanceof AuthorizationKeyPersistable) {
      this.refAuthorizationKey = (AuthorizationKeyPersistable) refAuthorizationKey;
    } else {
      throw new IllegalArgumentException();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefDetails(OrganizationFunctionModel details) {
    if (details == null || details instanceof OrganizationFunctionPersistable) {
      this.refDetails = (OrganizationFunctionPersistable) details;
    } else {
      throw new IllegalArgumentException();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefGroupMembers(Set<OrganizationFunctionModel> refGroupMembers) {
    if (refGroupMembers == null) {
      this.refGroupMembers = null;
    } else {
      Set<OrganizationFunctionPersistable> persistables = new HashSet<>();
      for (var model : refGroupMembers) {
        persistables.add((OrganizationFunctionPersistable) model);
      }
      this.refGroupMembers = persistables;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    if (getRefDetails() == null) return null;
    return getRefDetails().toString();
  }
}
