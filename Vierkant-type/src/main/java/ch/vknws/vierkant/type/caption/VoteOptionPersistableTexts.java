package ch.vknws.vierkant.type.caption;

/**
 * Captions for the vote option.
 * 
 * @author benjamin
 */
public interface VoteOptionPersistableTexts {

  /**
   * @return the name of the option
   */
  String optionName();
}
