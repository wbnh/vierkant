package ch.vknws.vierkant.type.caption;

/**
 * Texts for enum.
 *
 * @author Benjamin Weber
 */
public interface FunctionTypeTexts {

  /**
   * @return the text
   */
  String NORMAL();

  /**
   * @return the text
   */
  String STAFF();

  /**
   * @return the text
   */
  String SUBSTITUTE();
}
