package ch.vknws.vierkant.type.caption;

/**
 * Texts.
 * 
 * @author benjamin
 */
public interface ProcessInstanceStatusTexts {

  /**
   * @return text
   */
  String NOT_STARTED();

  /**
   * @return text
   */
  String IN_PROGRESS();

  /**
   * @return text
   */
  String FINISHED();
}
