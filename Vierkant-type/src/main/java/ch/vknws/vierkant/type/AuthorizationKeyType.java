package ch.vknws.vierkant.type;

/**
 * Different key types.
 *
 * @author Benjamin Weber
 */
public enum AuthorizationKeyType {

  /**
   * No special changes.
   */
  NORMAL,

  /**
   * Explicit allow.
   */
  ALLOW,

  /**
   * Explicit deny.
   */
  DENY;
}
