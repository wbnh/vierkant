package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.database.DescriptiveName;
import ch.vknws.vierkant.type.impl.TrainingCoursePersistable;

/**
 * Model for training courses.
 *
 * @author Benjamin Weber
 */
@DescriptiveName(TrainingCourseModel.PropertyId.TRAINING_COURSE_NAME)
public interface TrainingCourseModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The name.
     */
    public static final String TRAINING_COURSE_NAME = "trainingCourseName";

    /**
     * The person prerequisites.
     */
    public static final String REF_PERSON_PREREQUISITE = "refPersonPrerequisite";

    /**
     * The rank prerequisites.
     */
    public static final String REF_RANK_PREREQUISITE = "refPersonRankPrerequisite";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : TrainingCourseModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(TrainingCoursePersistable.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param trainingCourseName the name
   */
  void setTrainingCourseName(String trainingCourseName);

  /**
   * @return the name
   */
  String getTrainingCourseName();

  /**
   * @param refPersonPrerequisite the person prerequisites
   */
  void setRefPersonPrerequisite(PrerequisiteModel refPersonPrerequisite);

  /**
   * @return the person prerequisites
   */
  PrerequisiteModel getRefPersonPrerequisite();

  /**
   * @param refPersonRankPrerequisite the rank prerequisites
   */
  void setRefPersonRankPrerequisite(PrerequisiteModel refPersonRankPrerequisite);

  /**
   * @return the rank prerequisites.
   */
  PrerequisiteModel getRefPersonRankPrerequisite();
}
