package ch.vknws.vierkant.type.caption;

/**
 * Texts.
 * 
 * @author benjamin
 */
public interface SubtaskPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return text
   */
  String sortIndex();

  /**
   * @return text
   */
  String refTask();
}
