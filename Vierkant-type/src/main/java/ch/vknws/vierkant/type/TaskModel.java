package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * @author benjamin
 */
public interface TaskModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The name of the task.
     */
    public static final String TITLE = "title";
    /**
     * The description of the task.
     */
    public static final String DESCRIPTION = "description";

    /**
     * The classification. Can be overridden by watchers.
     */
    public static final String CLASSIFICATION = "classification";

    /**
     * The calculation for the assignee.
     */
    public static final String ASSIGN_TO_CALCULATION = "assignToCalculation";

    //
    // Referenced Models
    //

    /**
     * The type of the task.
     */
    public static final String REF_TYPE = "refType";

    /**
     * The task status.
     */
    public static final String REF_STATUS = "refStatus";

    /**
     * The workflow.
     */
    public static final String REF_WORKFLOW = "refWorkflow";

    /**
     * The subtasks of the task.
     */
    public static final String REF_SUBTASKS = "refSubtasks";

    /**
     * The custom fields of the task.
     */
    public static final String REF_CUSTOM_FIELDS = "refCustomFields";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {

      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : TaskModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(TaskModel.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param title the title
   */
  void setTitle(String title);

  /**
   * @return the title
   */
  String getTitle();

  /**
   * @param description the description
   */
  void setDescription(String description);

  /**
   * @return the description
   */
  String getDescription();

  /**
   * @param classification the classification
   */
  void setClassification(ClassificationLevel classification);

  /**
   * @return the classification
   */
  ClassificationLevel getClassification();

  /**
   * @param assignToCalculation the calculation for the assignee.
   */
  void setAssignToCalculation(String assignToCalculation);

  /**
   * @return the calculation for the assignee.
   */
  String getAssignToCalculation();

  /**
   * @param refType the type
   */
  void setRefType(TaskTypeModel refType);

  /**
   * @return the type
   */
  TaskTypeModel getRefType();

  /**
   * @param refStatus the status
   */
  void setRefStatus(TaskStatusModel refStatus);

  /**
   * @return the status
   */
  TaskStatusModel getRefStatus();

  /**
   * @param refWorkflow the workflow
   */
  void setRefWorkflow(TaskWorkflowModel refWorkflow);

  /**
   * @return the workflow
   */
  TaskWorkflowModel getRefWorkflow();

  /**
   * @param refSubtasks the subtasks
   */
  void setRefSubtasks(Set<SubtaskModel> refSubtasks);

  /**
   * @return the subtasks
   */
  Set<SubtaskModel> getRefSubtasks();

  /**
   * @param refCustomFields the custom fields on this task.
   */
  void setRefCustomFields(Set<TaskCustomFieldInstanceModel> refCustomFields);

  /**
   * @return the custom fields on this task.
   */
  Set<TaskCustomFieldInstanceModel> getRefCustomFields();

}
