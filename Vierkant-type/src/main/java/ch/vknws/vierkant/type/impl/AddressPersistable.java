package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.AddressModel;

/**
 * Persistable for the AddressModel.
 *
 * @author Benjamin Weber
 */
@Entity
@Audited
public class AddressPersistable extends AbstractDatabasePersistable implements AddressModel {

  private static final long serialVersionUID = 5631971920093591064L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @Column
  private String line1;

  @Attribute
  @Column
  private String line2;

  @Attribute
  @Column
  private String zipCode;

  @Attribute
  @Column
  private String city;

  @Attribute
  @Column
  private String canton;

  @Attribute
  @Column
  private String country;

  /**
   * Returns the comment for the last modification.
   * 
   * @return the comment. Can be null depending on configuration.
   */
  @Override
  public String getAuditComment() {
    return auditComment;
  }

  @Override
  public String getCanton() {
    return canton;
  }

  @Override
  public String getCity() {
    return city;
  }

  @Override
  public String getCountry() {
    return country;
  }

  /**
   * Returns the time the model was first saved on the DB.
   * 
   * @return the original save time. Null if unsaved.
   */
  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  /**
   * Returns the ID of the person who created the model.
   * 
   * @return the creator. Null if unsaved, special values apply.
   */
  @Override
  public Integer getCreator() {
    return creator;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  @Override
  public String getLine1() {
    return line1;
  }

  @Override
  public String getLine2() {
    return line2;
  }

  /**
   * Returns the time the model was last saved.
   * 
   * @return the time. Null if unsaved.
   */
  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  /**
   * Returns the ID of the person who last saved the model.
   * 
   * @return the ID. Null if unsaved. Special values apply.
   */
  @Override
  public Integer getModifier() {
    return modifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getRecordId() {
    return recordId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  @Override
  public String getZipCode() {
    return zipCode;
  }

  /**
   * Sets the comment for the last modification.
   * 
   * @param auditComment the comment
   */
  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

  @Override
  public void setCanton(String canton) {
    this.canton = canton;
  }

  @Override
  public void setCity(String city) {
    this.city = city;
  }

  @Override
  public void setCountry(String country) {
    this.country = country;
  }

  /**
   * Sets the time the model was first saved on the DB.
   * 
   * @param creationDateTime the time
   */
  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  /**
   * Sets the ID of the person who created the model.
   * 
   * @param creator the creator
   */
  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  @Override
  public void setLine1(String line1) {
    this.line1 = line1;
  }

  @Override
  public void setLine2(String line2) {
    this.line2 = line2;
  }

  /**
   * Sets the time the model was last saved.
   * 
   * @param modificationDateTime the time
   */
  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  /**
   * Sets the ID of the person who last saved the model.
   * 
   * @param modifier the ID
   */
  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  @Override
  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();

    if (getLine1() != null) {
      builder.append(getLine1());
      builder.append("\n");
    }
    if (getLine2() != null && !getLine2().isEmpty()) {
      builder.append(getLine2());
      builder.append("\n");
    }
    if (getZipCode() != null) {
      builder.append(getZipCode());
    }
    if (getZipCode() != null && getCity() != null) {
      builder.append(" ");
    }
    if (getCity() != null) {
      builder.append(getCity());
    }
    if (getCity() != null && getCanton() != null) {
      builder.append(" ");
    }
    if (getCanton() != null) {
      builder.append(getCanton());
    }
    if (getZipCode() != null || getCity() != null || getCanton() != null) {
      builder.append("\n");
    }
    if (getCountry() != null && !"CH".equals(getCountry())) {
      builder.append(getCountry());
    }

    return builder.toString();
  }
}
