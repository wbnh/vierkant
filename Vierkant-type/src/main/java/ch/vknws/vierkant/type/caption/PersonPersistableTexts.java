package ch.vknws.vierkant.type.caption;

/**
 * Captions for PersonModel.
 *
 * @author Benjamin Weber
 */
public interface PersonPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return the text
   */
  String givenName();

  /**
   * @return the text
   */
  String familyName();

  /**
   * @return the text
   */
  String title();

  /**
   * @return the text
   */
  String telephone();

  /**
   * @return the text
   */
  String mobile();

  /**
   * @return the text
   */
  String emailPrivate();

  /**
   * @return the text
   */
  String emailOrganization();

  /**
   * @return the text
   */
  String birthdate();

  /**
   * @return the text
   */
  String secretaryComment();

  /**
   * @return the text
   */
  String vkNews();

  /**
   * @return the text
   */
  String lastUpdateCheck();

  /**
   * @return the text
   */
  String shorthand();

  /**
   * @return the text
   */
  String vka();

  /**
   * @return the text
   */
  String memberCategory();

  /**
   * @return the text
   */
  String sex();

  /**
   * @return text
   */
  String status();

  /**
   * @return the text
   */
  String homeAddress();

  /**
   * @return the text
   */
  String contactPersons();

  /**
   * @return the text
   */
  String aargauLicense();

  /**
   * @return the text
   */
  String memberGroup();

  /**
   * @return the text
   */
  String refRank();

  /**
   * @return the text
   */
  String functions();

  /**
   * @return the text
   */
  String primaryFunction();

  /**
   * @return the text
   */
  String completedCourses();

  /**
   * @return the text
   */
  String medicalInformation();

  /**
   * @return text
   */
  String authorizationKey();

  /**
   * @return the text
   */
  String leaveOrganization();

  /**
   * @return the text
   */
  String mister();

  /**
   * @return the text
   */
  String mistress();
}
