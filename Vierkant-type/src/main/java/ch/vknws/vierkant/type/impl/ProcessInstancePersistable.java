package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.ProcessInstanceModel;
import ch.vknws.vierkant.type.ProcessInstanceStatus;
import ch.vknws.vierkant.type.ProcessModel;
import ch.vknws.vierkant.type.TaskInstanceModel;

/**
 * @author benjamin
 */
public class ProcessInstancePersistable extends AbstractDatabasePersistable implements ProcessInstanceModel {

  private static final long serialVersionUID = 1159699191280050274L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @Column
  private String status;

  @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
  @Fetch(FetchMode.SELECT)
  @JoinTable(name = "ProcessInstanceWatcherList", joinColumns = {
      @JoinColumn(name = "refProcessInstanceRecordId") }, inverseJoinColumns = {
          @JoinColumn(name = "refWatcherRecordId") })
  private Set<PersonPersistable> refWatchers;

  @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
  @Fetch(FetchMode.SELECT)
  @JoinTable(name = "ProcessInstanceTaskInstanceList", joinColumns = {
      @JoinColumn(name = "refProcessInstanceRecordId") }, inverseJoinColumns = {
          @JoinColumn(name = "refTaskInstanceRecordId") })
  private Set<TaskInstancePersistable> refTaskInstances;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refTemplateRecordId")
  private ProcessPersistable refTemplate;

  @Override
  public void setStatus(ProcessInstanceStatus status) {
    if (status == null) {
      this.status = null;
    } else {
      this.status = status.name();
    }
  }

  @Override
  public ProcessInstanceStatus getStatus() {
    if (status == null) return null;
    return ProcessInstanceStatus.valueOf(status);
  }

  @Override
  public void setRefWatchers(Set<PersonModel> refWatchers) {
    if (refWatchers == null) {
      this.refWatchers = null;
    } else {
      Set<PersonPersistable> persistables = new HashSet<>();
      for (var model : refWatchers) {
        persistables.add((PersonPersistable) model);
      }
      this.refWatchers = persistables;
    }
  }

  @Override
  public Set<PersonModel> getRefWatchers() {
    if (refWatchers == null) {
      refWatchers = new HashSet<>();
    }
    Set<PersonModel> models = new HashSet<>();
    for (var persistable : refWatchers) {
      models.add(persistable);
    }
    return models;
  }

  @Override
  public void setRefTaskInstances(Set<TaskInstanceModel> refTaskInstances) {
    if (refTaskInstances == null) {
      this.refTaskInstances = null;
    } else {
      Set<TaskInstancePersistable> persistables = new HashSet<>();
      for (var model : refTaskInstances) {
        persistables.add((TaskInstancePersistable) model);
      }
      this.refTaskInstances = persistables;
    }
  }

  @Override
  public Set<TaskInstanceModel> getRefTaskInstances() {
    if (refTaskInstances == null) {
      refTaskInstances = new HashSet<>();
    }
    Set<TaskInstanceModel> models = new HashSet<>();
    for (var persistable : refTaskInstances) {
      models.add(persistable);
    }
    return models;
  }

  @Override
  public void setRefTemplate(ProcessModel refTemplate) {
    if (refTemplate == null || refTemplate instanceof ProcessPersistable) {
      this.refTemplate = (ProcessPersistable) refTemplate;
    } else {
      throw new IllegalArgumentException();
    }
  }

  @Override
  public ProcessModel getRefTemplate() {
    return refTemplate;
  }

  @Override
  public Integer getRecordId() {
    return recordId;
  }

  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  @Override
  public Integer getCreator() {
    return creator;
  }

  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  @Override
  public Integer getModifier() {
    return modifier;
  }

  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  @Override
  public String getAuditComment() {
    return auditComment;
  }

  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

}
