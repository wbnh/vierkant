package ch.vknws.vierkant.type.caption;

/**
 * Captions for the prerequisite.
 *
 * @author Benjamin Weber
 */
public interface PrerequisitePersistableTexts extends AbstractPersistableTexts {

  /**
   * @return the text
   */
  String gt();

  /**
   * @return the text
   */
  String ge();

  /**
   * @return the text
   */
  String lt();

  /**
   * @return the text
   */
  String le();

  /**
   * @return the text
   */
  String eq();

  /**
   * @return the text
   */
  String isNull();

  /**
   * @return the text
   */
  String isNotNull();
}
