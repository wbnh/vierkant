package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.vknws.vierkant.type.impl.TrainingCourseInstancePersistable;

/**
 * Model for specific instances of training courses.
 *
 * @author Benjamin Weber
 */
public interface TrainingCourseInstanceModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The template.
     */
    public static final String REF_TEMPLATE = "refTemplate";

    /**
     * The responsible person.
     */
    public static final String REF_RESPONSIBLE = "refResponsible";

    /**
     * The teachers.
     */
    public static final String REF_TEACHERS = "refTeachers";

    /**
     * The participants.
     */
    public static final String REF_PARTICIPANTS = "refParticipants";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : TrainingCourseInstanceModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(TrainingCourseInstancePersistable.class,
                "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param refTemplate the template
   */
  void setRefTemplate(TrainingCourseModel refTemplate);

  /**
   * @return the template
   */
  TrainingCourseModel getRefTemplate();

  /**
   * @param refResponsible the responsible person
   */
  void setRefResponsible(PersonModel refResponsible);

  /**
   * @return the responsible person
   */
  PersonModel getRefResponsible();

  /**
   * @param refTeachers the teachers
   */
  void setRefTeachers(Set<PersonModel> refTeachers);

  /**
   * @return the teachers
   */
  Set<PersonModel> getRefTeachers();

  /**
   * @param refParticipants the participants
   */
  void setRefParticipants(Set<PersonModel> refParticipants);

  /**
   * @return the participants
   */
  Set<PersonModel> getRefParticipants();
}
