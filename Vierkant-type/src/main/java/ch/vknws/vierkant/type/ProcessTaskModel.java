package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * Task in a workflow.
 * 
 * @author benjamin
 */
public interface ProcessTaskModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The underlying task.
     */
    public static final String REF_TASK = "refTask";

    /**
     * The following tasks.
     */
    public static final String REF_NEXT_TASKS = "refNextTasks";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : ProcessTaskModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(ProcessTaskModel.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param refTask the underlying task.
   */
  void setRefTask(TaskModel refTask);

  /**
   * @return the underlying task.
   */
  TaskModel getRefTask();

  /**
   * @param refNextTasks the following tasks.
   */
  void setRefNextTasks(Set<ProcessTaskModel> refNextTasks);

  /**
   * @return the following tasks.
   */
  Set<ProcessTaskModel> getRefNextTasks();
}
