package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * A process to execute.
 * 
 * @author benjamin
 */
public interface ProcessModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The descriptive name.
     */
    public static final String PROCESS_NAME = "processName";

    /**
     * The description.
     */
    public static final String DESCRIPTION = "description";

    /**
     * Whether multiple instances can run at the same time.
     */
    public static final String ALLOW_PARALLEL_EXECUTION = "allowParallelExecution";

    /**
     * Whether the process is enabled.
     */
    public static final String ENABLED = "enabled";

    /**
     * The classification.
     */
    public static final String CLASSIFICATION = "classification";

    /**
     * The starting tasks.
     */
    public static final String REF_TASKS = "refTasks";

    /**
     * The variables on process level (readable to all tasks within it).
     */
    public static final String REF_PROCESS_VARIABLES = "refProcessVariables";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : ProcessModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(ProcessModel.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param processName the process name.
   */
  void setProcessName(String processName);

  /**
   * @return the process name.
   */
  String getProcessName();

  /**
   * @param description the description.
   */
  void setDescription(String description);

  /**
   * @return the description.
   */
  String getDescription();

  /**
   * @param allowParallelExecution Whether multiple instances can run at the same
   *                               time.
   */
  void setAllowParallelExecution(Boolean allowParallelExecution);

  /**
   * @return Whether multiple instances can run at the same time.
   */
  Boolean getAllowParallelExecution();

  /**
   * @param enabled whether the process is enabled.
   */
  void setEnabled(Boolean enabled);

  /**
   * @return whether the process is enabled.
   */
  Boolean isEnabled();

  /**
   * @param classification the classification.
   */
  void setClassification(ClassificationLevel classification);

  /**
   * @return the classification.
   */
  ClassificationLevel getClassification();

  /**
   * @param refTasks the initial tasks.
   */
  void setRefTasks(Set<ProcessTaskModel> refTasks);

  /**
   * @return the initial tasks.
   */
  Set<ProcessTaskModel> getRefTasks();

  /**
   * @param refProcessVariables the variables on process level (readable to all
   *                            tasks within it).
   */
  void setRefProcessVariables(Set<TaskCustomFieldModel> refProcessVariables);

  /**
   * @return the variables on process level (readable to all tasks within it).
   */
  Set<TaskCustomFieldModel> getRefProcessVariables();

}
