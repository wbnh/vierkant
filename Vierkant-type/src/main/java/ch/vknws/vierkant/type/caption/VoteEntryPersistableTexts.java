package ch.vknws.vierkant.type.caption;

/**
 * Captions for the vote entry.
 * 
 * @author benjamin
 */
public interface VoteEntryPersistableTexts {

  /**
   * @return the text for the vote option
   */
  String refVoteOption();

  /**
   * @return the text for the voter
   */
  String refVoter();

  /**
   * @return the text for the validity timestamp
   */
  String validityTimestamp();

  /**
   * @return the text for the checksum
   */
  String checksum();
}
