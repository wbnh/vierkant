package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.database.DescriptiveName;
import ch.vknws.vierkant.type.impl.ConfigurationPersistable;

/**
 * Model for configurations.
 *
 * @author Benjamin Weber
 */
@DescriptiveName(ConfigurationModel.PropertyId.USER_NAME)
@DescriptiveName(ConfigurationModel.PropertyId.CONTEXT)
@DescriptiveName(ConfigurationModel.PropertyId.KEY_NAME)
public interface ConfigurationModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The user whom it concerns.
     */
    public static final String USER_NAME = "userName";

    /**
     * The context in which it applies.
     */
    public static final String CONTEXT = "context";

    /**
     * The key of the configuration.
     */
    public static final String KEY_NAME = "keyName";

    /**
     * The value.
     */
    public static final String VALUE = "value";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : ConfigurationModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(ConfigurationPersistable.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param userName the user
   */
  void setUserName(String userName);

  /**
   * @return the user
   */
  String getUserName();

  /**
   * @param context the context
   */
  void setContext(String context);

  /**
   * @return the context
   */
  String getContext();

  /**
   * @param keyName the key
   */
  void setKeyName(String keyName);

  /**
   * @return the key
   */
  String getKeyName();

  /**
   * @param value the value
   */
  void setValue(String value);

  /**
   * @return the value
   */
  String getValue();
}
