package ch.vknws.vierkant.type.caption;

/**
 * Captions for the VKA.
 *
 * @author Benjamin Weber
 */
public interface VKATexts {

  /**
   * @return the text
   */
  String A();

  /**
   * @return the text
   */
  String AP();

  /**
   * @return the text
   */
  String AS();

  /**
   * @return the text
   */
  String KKB();

  /**
   * @return the text
   */
  String BE();

  /**
   * @return the text
   */
  String C();

  /**
   * @return the text
   */
  String F();

  /**
   * @return the text
   */
  String GL();

  /**
   * @return the text
   */
  String NWS();

  /**
   * @return the text
   */
  String RJ();

  /**
   * @return the text
   */
  String SG();

  /**
   * @return the text
   */
  String SGO();

  /**
   * @return the text
   */
  String SH();

  /**
   * @return the text
   */
  String SR();

  /**
   * @return the text
   */
  String W();

  /**
   * @return the text
   */
  String ZG();

  /**
   * @return the text
   */
  String ZO();

  /**
   * @return the text
   */
  String ZU();

  /**
   * @return the text
   */
  String ZS();
}
