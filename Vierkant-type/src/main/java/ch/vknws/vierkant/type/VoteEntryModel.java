package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * Type to classify meetings.
 * 
 * @author benjamin
 */
public interface VoteEntryModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The option the voter voted for.
     */
    public static final String REF_VOTE_OPTION = "refVoteOption";

    /**
     * The person who voted.
     */
    public static final String REF_VOTER = "refVoter";

    /**
     * The time from when the vote is valid.
     */
    public static final String VALIDITY_TIMESTAMP = "validityTimestamp";

    /**
     * The checksum.
     */
    public static final String CHECKSUM = "checksum";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : VoteEntryModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(VoteEntryModel.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param refVoteOption the new vote option
   */
  void setRefVoteOption(VoteOptionModel refVoteOption);

  /**
   * @return the option the voter voted for
   */
  VoteOptionModel getRefVoteOption();

  /**
   * @param refVoter the person who voted
   */
  void setRefVoter(PersonModel refVoter);

  /**
   * @return the person who voted
   */
  PersonModel getRefVoter();

  /**
   * @param validityTimestamp the new timestamp
   */
  void setValidityTimestamp(LocalDateTime validityTimestamp);

  /**
   * @return the timestamp from which the vote is valid
   */
  LocalDateTime getValidityTimestamp();

  /**
   * @param checksum the new checksum
   */
  void setChecksum(String checksum);

  /**
   * @return the checksum
   */
  String getChecksum();

}
