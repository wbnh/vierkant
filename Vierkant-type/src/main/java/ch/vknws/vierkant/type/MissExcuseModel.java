package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * Reasons for people not attending.
 * 
 * @author benjamin
 */
public interface MissExcuseModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The description of the reason for missing.
     */
    public static final String DESCRIPTION = "description";

    /**
     * The ID of the person who accepted the reason. Can be 0 for the System.
     * <code>null</code> for unaccepted excuses.
     */
    public static final String ACCEPTOR_ID = "acceptorId";

    /**
     * The person that is excusing themselves.
     */
    public static final String REF_MISSING_PERSON = "refMissingPerson";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : MissExcuseModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(MissExcuseModel.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param description the new description
   */
  void setDescription(String description);

  /**
   * @return The description of the reason for missing.
   */
  String getDescription();

  /**
   * @param acceptorId the new acceptor id
   */
  void setAcceptorId(Integer acceptorId);

  /**
   * @return The ID of the person who accepted the reason. Can be 0 for the
   *         System. <code>null</code> for unaccepted excuses.
   */
  Integer getAcceptorId();

  /**
   * @param refMissingPerson the new missing person
   */
  void setRefMissingPerson(PersonModel refMissingPerson);

  /**
   * @return The person that is excusing themselves.
   */
  PersonModel getRefMissingPerson();

}
