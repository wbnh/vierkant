package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.ClassificationLevel;
import ch.vknws.vierkant.type.RecordModel;
import ch.vknws.vierkant.type.TractandumModel;

/**
 * Implementation of the record.
 * 
 * @author benjamin
 */
@Entity
@Audited
public class RecordPersistable extends AbstractDatabasePersistable implements RecordModel {

  private static final long serialVersionUID = 3410748480725832019L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @Column
  private String classification;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "refRecord")
  private Set<TractandumPersistable> refPlannedTractanda;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "refRecord")
  private Set<TractandumPersistable> refActualTractanda;

  @Override
  public void setClassification(ClassificationLevel classification) {
    if (classification == null) {
      this.classification = null;
    } else {
      this.classification = classification.name();
    }
  }

  @Override
  public ClassificationLevel getClassification() {
    if (classification == null) { return null; }
    return ClassificationLevel.valueOf(classification);
  }

  @Override
  public void setRefPlannedTractanda(Set<TractandumModel> refTractanda) {
    this.refPlannedTractanda = refTractanda == null ? null
        : refTractanda.stream().map(m -> (TractandumPersistable) m).collect(Collectors.toSet());
  }

  @Override
  public Set<TractandumModel> getRefPlannedTractanda() {
    return refPlannedTractanda == null ? null
        : refPlannedTractanda.stream().map(p -> (TractandumModel) p).collect(Collectors.toSet());
  }

  @Override
  public void setRefActualTractanda(Set<TractandumModel> refTractanda) {
    this.refActualTractanda = refTractanda == null ? null
        : refTractanda.stream().map(m -> (TractandumPersistable) m).collect(Collectors.toSet());
  }

  @Override
  public Set<TractandumModel> getRefActualTractanda() {
    return refActualTractanda == null ? null
        : refActualTractanda.stream().map(p -> (TractandumModel) p).collect(Collectors.toSet());
  }

  @Override
  public Integer getRecordId() {
    return recordId;
  }

  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  @Override
  public Integer getCreator() {
    return creator;
  }

  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  @Override
  public Integer getModifier() {
    return modifier;
  }

  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  @Override
  public String getAuditComment() {
    return auditComment;
  }

  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

}
