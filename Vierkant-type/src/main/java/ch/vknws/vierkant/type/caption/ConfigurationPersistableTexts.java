package ch.vknws.vierkant.type.caption;

/**
 * Texts for the configuration model.
 *
 * @author Benjamin Weber
 *
 */
public interface ConfigurationPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return the text
   */
	String user();

  /**
   * @return the text
   */
	String context();

  /**
   * @return the text
   */
	String key();

  /**
   * @return the text
   */
	String value();
}
