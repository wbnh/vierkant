package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.vknws.vierkant.type.impl.AddressPersistable;

/**
 * Model for addresses.
 *
 * @author Benjamin Weber
 */
public interface AddressModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The first address line.
     */
    public static final String LINE1 = "line1";

    /**
     * The second address line.
     */
    public static final String LINE2 = "line2";

    /**
     * The post code.
     */
    public static final String ZIP_CODE = "zipCode";

    /**
     * The city.
     */
    public static final String CITY = "city";

    /**
     * The canton.
     */
    public static final String CANTON = "canton";

    /**
     * The country.
     */
    public static final String COUNTRY = "country";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : AddressModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(AddressPersistable.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param line1 the new first line.
   */
  void setLine1(String line1);

  /**
   * @return the first line.
   */
  String getLine1();

  /**
   * @param line2 the new second line.
   */
  void setLine2(String line2);

  /**
   * @return the second line.
   */
  String getLine2();

  /**
   * @param zipCode the post code.
   */
  void setZipCode(String zipCode);

  /**
   * @return the post code.
   */
  String getZipCode();

  /**
   * @param city the city.
   */
  void setCity(String city);

  /**
   * @return the city.
   */
  String getCity();

  /**
   * @param canton the canton.
   */
  void setCanton(String canton);

  /**
   * @return the canton
   */
  String getCanton();

  /**
   * @param country the country
   */
  void setCountry(String country);

  /**
   * @return the country
   */
  String getCountry();
}
