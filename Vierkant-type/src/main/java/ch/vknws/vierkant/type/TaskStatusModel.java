package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * A status in a task's workflow.
 * 
 * @author benjamin
 */
public interface TaskStatusModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The name.
     */
    public static final String TASK_STATUS_NAME = "taskStatusName";

    /**
     * The description.
     */
    public static final String DESCRIPTION = "description";

    /**
     * The display color.
     */
    public static final String COLOR = "color";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : TaskStatusModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(TaskStatusModel.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param taskStatusName the name
   */
  void setTaskStatusName(String taskStatusName);

  /**
   * @return the name
   */
  String getTaskStatusName();

  /**
   * @param description the description
   */
  void setDescription(String description);

  /**
   * @return the description
   */
  String getDescription();

  /**
   * @param color the display color
   */
  void setColor(Integer color);

  /**
   * @return the display color
   */
  Integer getColor();

}
