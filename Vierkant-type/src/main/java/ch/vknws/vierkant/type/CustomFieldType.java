package ch.vknws.vierkant.type;

/**
 * Types for custom fields.
 * 
 * @author benjamin
 */
public enum CustomFieldType {

  /**
   * String.
   */
  STRING,

  /**
   * Integer.
   */
  INTEGER,

  /**
   * Double.
   */
  DOUBLE,

  /**
   * Boolean.
   */
  BOOLEAN,

  /**
   * Date.
   */
  DATE,

  /**
   * Datetime.
   */
  DATETIME,

  /**
   * Model.
   */
  MODEL,

  /**
   * List (type in configuration).
   */
  LIST,

  /**
   * Map (type in configuration).
   */
  MAP,

  /**
   * Calculated.
   */
  CALCULATED
}
