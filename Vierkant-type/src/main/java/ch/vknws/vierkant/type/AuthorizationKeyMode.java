package ch.vknws.vierkant.type;

/**
 * Modes.
 * 
 * @author Benjamin Weber
 */
public enum AuthorizationKeyMode {

  /**
   * Read permissions.
   */
  READ,

  /**
   * Write permissions.
   */
  WRITE;
}
