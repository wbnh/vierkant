package ch.vknws.vierkant.type.caption;

/**
 * Texts.
 * 
 * @author benjamin
 */
public interface TaskBehaviorTypeTexts {

  /**
   * @return text
   */
  String NONE();

  /**
   * @return text
   */
  String APPVORVAL();

  /**
   * @return text
   */
  String DECISION();

  /**
   * @return text
   */
  String JOIN();

  /**
   * @return text
   */
  String TODO();

  /**
   * @return text
   */
  String AUTO_UPDATE_FIELD();

  /**
   * @return text
   */
  String AUTO_ADD_TO_FIELD();

  /**
   * @return text
   */
  String AUTO_REMOVE_FROM_FIELD();

  /**
   * @return text
   */
  String AUTO_NOTIFY();

  /**
   * @return text
   */
  String AUTO_FIRE_EVENT();

  /**
   * @return text
   */
  String AUTO_LISTEN_FOR_EVENT();

  /**
   * @return text
   */
  String AUTO_REST_CALL();

  /**
   * @return text
   */
  String AUTO_TRIGGER_PROCESS();

  /**
   * @return text
   */
  String AUTO_WAIT();

  /**
   * @return text
   */
  String AUTO_DECISION();
}
