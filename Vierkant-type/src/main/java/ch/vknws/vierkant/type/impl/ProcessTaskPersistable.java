package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.ProcessTaskModel;
import ch.vknws.vierkant.type.TaskModel;

/**
 * @author benjamin
 */
public class ProcessTaskPersistable extends AbstractDatabasePersistable implements ProcessTaskModel {

  private static final long serialVersionUID = -4947600259040351494L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refTaskRecordId")
  private TaskPersistable refTask;

  @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
  @Fetch(FetchMode.SELECT)
  @JoinTable(name = "ProcessTaskNextTaskList", joinColumns = {
      @JoinColumn(name = "refPreviousTaskRecordId") }, inverseJoinColumns = {
          @JoinColumn(name = "refNextTaskRecordId") })
  private Set<ProcessTaskPersistable> refNextTasks;

  @Override
  public void setRefTask(TaskModel refTask) {
    if (refTask == null || refTask instanceof TaskPersistable) {
      this.refTask = (TaskPersistable) refTask;
    } else {
      throw new IllegalArgumentException();
    }
  }

  @Override
  public TaskModel getRefTask() {
    return refTask;
  }

  @Override
  public void setRefNextTasks(Set<ProcessTaskModel> refNextTasks) {
    if (refNextTasks == null) {
      this.refNextTasks = null;
    } else {
      Set<ProcessTaskPersistable> persistables = new HashSet<>();
      for (var model : refNextTasks) {
        persistables.add((ProcessTaskPersistable) model);
      }
      this.refNextTasks = persistables;
    }
  }

  @Override
  public Set<ProcessTaskModel> getRefNextTasks() {
    if (refNextTasks == null) {
      refNextTasks = new HashSet<>();
    }
    Set<ProcessTaskModel> models = new HashSet<>();
    for (var persistable : refNextTasks) {
      models.add(persistable);
    }
    return models;
  }

  @Override
  public Integer getRecordId() {
    return recordId;
  }

  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  @Override
  public Integer getCreator() {
    return creator;
  }

  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  @Override
  public Integer getModifier() {
    return modifier;
  }

  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  @Override
  public String getAuditComment() {
    return auditComment;
  }

  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

}
