package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.AuthorizationKeyMode;
import ch.vknws.vierkant.type.AuthorizationKeyModel;
import ch.vknws.vierkant.type.AuthorizationKeyType;

/**
 * Persistable type for the AuthorizationKey.
 *
 * @author Benjamin Weber
 */
@Entity
@Audited
public class AuthorizationKeyPersistable extends AbstractDatabasePersistable implements AuthorizationKeyModel {

  private static final long serialVersionUID = 8166454512910816226L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @Column
  private String keyName;

  @Attribute
  @Column
  private String mode;

  @Attribute
  @Column
  private String type;

  @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinTable(name = "AuthorizationKeySubordinateList", joinColumns = @JoinColumn(name = "refAuthorizationRecordId"), inverseJoinColumns = @JoinColumn(name = "refSubordinateRecordId"))
  private Set<AuthorizationKeyPersistable> refSubordinates;

  @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "refSubordinates")
  private Set<AuthorizationKeyPersistable> refSuperiors;

  /**
   * Returns the comment for the last modification.
   * 
   * @return the comment. Can be null depending on configuration.
   */
  @Override
  public String getAuditComment() {
    return auditComment;
  }

  /**
   * Returns the time the model was first saved on the DB.
   * 
   * @return the original save time. Null if unsaved.
   */
  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  /**
   * Returns the ID of the person who created the model.
   * 
   * @return the creator. Null if unsaved, special values apply.
   */
  @Override
  public Integer getCreator() {
    return creator;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  @Override
  public String getKeyName() {
    return keyName;
  }

  @Override
  public AuthorizationKeyMode getMode() {
    if (mode == null) { return null; }
    return AuthorizationKeyMode.valueOf(mode);
  }

  /**
   * Returns the time the model was last saved.
   * 
   * @return the time. Null if unsaved.
   */
  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  /**
   * Returns the ID of the person who last saved the model.
   * 
   * @return the ID. Null if unsaved. Special values apply.
   */
  @Override
  public Integer getModifier() {
    return modifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getRecordId() {
    return recordId;
  }

  /**
   * @return the referenced subordinates.
   */
  @Override
  public Set<AuthorizationKeyModel> getRefSubordinates() {
    if (refSubordinates == null) {
      setRefSubordinates(new HashSet<>());
    }
    return refSubordinates.stream().collect(Collectors.toSet());
  }

  /**
   * @return the referenced subordinates.
   */
  public Set<AuthorizationKeyPersistable> getRefSuperiors() {
    return refSuperiors;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  @Override
  public AuthorizationKeyType getType() {
    if (type == null) { return null; }
    return AuthorizationKeyType.valueOf(type);
  }

  /**
   * Sets the comment for the last modification.
   * 
   * @param auditComment the comment
   */
  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

  /**
   * Sets the time the model was first saved on the DB.
   * 
   * @param creationDateTime the time
   */
  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  /**
   * Sets the ID of the person who created the model.
   * 
   * @param creator the creator
   */
  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  @Override
  public void setKeyName(String keyName) {
    this.keyName = keyName;
  }

  @Override
  public void setMode(AuthorizationKeyMode mode) {
    if (mode == null) {
      this.mode = null;
    } else {
      this.mode = mode.name();
    }
  }

  /**
   * Sets the time the model was last saved.
   * 
   * @param modificationDateTime the time
   */
  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  /**
   * Sets the ID of the person who last saved the model.
   * 
   * @param modifier the ID
   */
  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  /**
   * @param refSubordinates the referenced subordinates
   */
  @Override
  public void setRefSubordinates(Set<AuthorizationKeyModel> refSubordinates) {
    if (refSubordinates == null) {
      this.refSubordinates = null;
      return;
    }
    this.refSubordinates = refSubordinates.stream().map(m -> {
      return (AuthorizationKeyPersistable) m;
    }).collect(Collectors.toSet());
    ;
  }

  /**
   * @param refSuperiors the referenced subordinates
   */
  public void setRefSuperiors(Set<AuthorizationKeyPersistable> refSuperiors) {
    this.refSuperiors = refSuperiors;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  @Override
  public void setType(AuthorizationKeyType type) {
    if (type == null) {
      this.type = null;
    } else {
      this.type = type.name();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return getKeyName() != null ? getKeyName() : "";
  }
}
