package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.ApplicationConstants;
import ch.vknws.vierkant.type.ConfigurationModel;

/**
 * Persistable for configurations.
 *
 * @author Benjamin Weber
 */
@Entity
@Audited
public class ConfigurationPersistable extends AbstractDatabasePersistable implements ConfigurationModel {

  private static final long serialVersionUID = -6819510074565720791L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @Column
  private String userName;

  @Attribute
  @Column
  private String context;

  @Attribute
  @Column
  private String keyName;

  @Attribute
  @Column
  private String value;

  @Transient
  private boolean shouldEncrypt = false;

  private String addEncryptionTags(String input) {
    return "ENC(" + input + ")";
  }

  /**
   * Returns the comment for the last modification.
   * 
   * @return the comment. Can be null depending on configuration.
   */
  @Override
  public String getAuditComment() {
    return auditComment;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getContext() {
    return context;
  }

  /**
   * Returns the time the model was first saved on the DB.
   * 
   * @return the original save time. Null if unsaved.
   */
  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  /**
   * Returns the ID of the person who created the model.
   * 
   * @return the creator. Null if unsaved, special values apply.
   */
  @Override
  public Integer getCreator() {
    return creator;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getKeyName() {
    return keyName;
  }

  /**
   * Returns the time the model was last saved.
   * 
   * @return the time. Null if unsaved.
   */
  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  /**
   * Returns the ID of the person who last saved the model.
   * 
   * @return the ID. Null if unsaved. Special values apply.
   */
  @Override
  public Integer getModifier() {
    return modifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getRecordId() {
    return recordId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getUserName() {
    return userName;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getValue() {
    if (isEncrypted()) { return ServiceRegistry.getEncryptionService()
        .decryptString(ApplicationConstants.PASSWORD_ENCRYPTION_PASSWORD, removeEncryptionTags(value)); }
    return value;
  }

  private boolean isEffectivelyEncrypted() {
    return value != null && value.matches("ENC\\(.*\\)");
  }

  /**
   * @return whether the value is encrypted
   */
  public boolean isEncrypted() {
    return shouldEncrypt || isEffectivelyEncrypted();
  }

  private String removeEncryptionTags(String input) {
    return input.substring(4, input.length() - 1);
  }

  /**
   * Sets the comment for the last modification.
   * 
   * @param auditComment the comment
   */
  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setContext(String context) {
    this.context = context;
  }

  /**
   * Sets the time the model was first saved on the DB.
   * 
   * @param creationDateTime the time
   */
  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  /**
   * Sets the ID of the person who created the model.
   * 
   * @param creator the creator
   */
  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setKeyName(String keyName) {
    this.keyName = keyName;
  }

  /**
   * Sets the time the model was last saved.
   * 
   * @param modificationDateTime the time
   */
  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  /**
   * Sets the ID of the person who last saved the model.
   * 
   * @param modifier the ID
   */
  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setUserName(String userName) {
    this.userName = userName;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setValue(String value) {
    if (isEncrypted()) {
      this.value = addEncryptionTags(ServiceRegistry.getEncryptionService()
          .encryptString(ApplicationConstants.PASSWORD_ENCRYPTION_PASSWORD, value));
    } else {
      this.value = value;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append(getUserName());
    builder.append("/");
    builder.append(getContext());
    builder.append("/");
    builder.append(getKeyName());
    builder.append("=");
    builder.append(getValue());

    return builder.toString();
  }
}
