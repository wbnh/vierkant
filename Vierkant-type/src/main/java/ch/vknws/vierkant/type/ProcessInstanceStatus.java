package ch.vknws.vierkant.type;

/**
 * The status of a process instance.
 * 
 * @author benjamin
 */
public enum ProcessInstanceStatus {

  /**
   * The process has not started yet.
   */
  NOT_STARTED,

  /**
   * The process is currently in progress.
   */
  IN_PROGRESS,

  /**
   * The process has finished and cannot be modified further.
   */
  FINISHED
}
