package ch.vknws.vierkant.type.caption;

/**
 * Caption for the tractandum.
 * 
 * @author benjamin
 */
public interface TractandumPersistableTexts {

  /**
   * @return the text for the classification
   */
  String classification();

  /**
   * @return the text for the sort index
   */
  String sortIndex();

  /**
   * @return the text for the header
   */
  String header();

  /**
   * @return the text for the content
   */
  String content();

  /**
   * @return the text for the responsible
   */
  String refResponsible();

  /**
   * @return the text for the vote result
   */
  String refVoteResult();
}
