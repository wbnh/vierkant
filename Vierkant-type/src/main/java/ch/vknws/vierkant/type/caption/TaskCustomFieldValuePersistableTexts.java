package ch.vknws.vierkant.type.caption;

/**
 * Texts.
 * 
 * @author benjamin
 */
public interface TaskCustomFieldValuePersistableTexts extends AbstractPersistableTexts {

  /**
   * @return text
   */
  String value();

  /**
   * @return text
   */
  String calculatedValue();

  /**
   * @return text
   */
  String refTemplate();
}
