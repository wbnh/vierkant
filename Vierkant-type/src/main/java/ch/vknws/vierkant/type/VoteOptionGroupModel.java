package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * Type to classify meetings.
 * 
 * @author benjamin
 */
public interface VoteOptionGroupModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The human-readable name of the option.
     */
    public static final String OPTION_GROUP_NAME = "optionGroupName";

    /**
     * The referenced options in the group.
     */
    public static final String REF_OPTIONS = "refOptions";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : VoteOptionGroupModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(VoteOptionGroupModel.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param optionGroupName the new option group name
   */
  void setOptionGroupName(String optionGroupName);

  /**
   * @return the human-readable name of the option group
   */
  String getOptionGroupName();

  /**
   * @param refOptions the new vote options.
   */
  void setRefOptions(Set<VoteOptionModel> refOptions);

  /**
   * @return the options in this group.
   */
  Set<VoteOptionModel> getRefOptions();

}
