package ch.vknws.vierkant.type.caption;

/**
 * Texts.
 * 
 * @author benjamin
 */
public interface TaskTypePersistableTexts extends AbstractPersistableTexts {

  /**
   * @return text
   */
  String taskTypeName();

  /**
   * @return text
   */
  String description();

  /**
   * @return text
   */
  String color();

  /**
   * @return text
   */
  String icon();

  /**
   * @return text
   */
  String behaviorType();
}
