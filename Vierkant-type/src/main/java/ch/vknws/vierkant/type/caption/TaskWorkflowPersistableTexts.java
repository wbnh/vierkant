package ch.vknws.vierkant.type.caption;

/**
 * Texts.
 * 
 * @author benjamin
 */
public interface TaskWorkflowPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return text.
   */
  String taskWorkflowName();

  /**
   * @return text.
   */
  String description();

  /**
   * @return text.
   */
  String refInitialStatus();
}
