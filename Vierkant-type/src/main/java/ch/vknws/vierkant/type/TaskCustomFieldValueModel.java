package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * The specific value of a custom field for a task instance.
 * 
 * @author benjamin
 */
public interface TaskCustomFieldValueModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The value.
     */
    public static final String VALUE = "value";

    /**
     * The calculated value to save for history.
     */
    public static final String CALCULATED_VALUE = "calculatedValue";

    /**
     * The underlying custom field.
     */
    public static final String REF_TEMPLATE = "refTemplate";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {

      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : TaskCustomFieldValueModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(TaskCustomFieldValueModel.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param value the value.
   */
  void setValue(String value);

  /**
   * @return the value.
   */
  String getValue();

  /**
   * @param calculatedValue the calculated value to save for history.
   */
  void setCalculatedValue(String calculatedValue);

  /**
   * @return the calculated value to save for history.
   */
  String getCalculatedValue();

  /**
   * @param refTemplate the underlying custom field.
   */
  void setRefTemplate(TaskCustomFieldModel refTemplate);

  /**
   * @return the underlying custom field.
   */
  TaskCustomFieldModel getRefTemplate();

}
