package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.database.DescriptiveName;
import ch.vknws.vierkant.type.impl.RankPersistable;

/**
 * Model for Ranks in the organisation.
 *
 * @author Benjamin Weber
 */
@DescriptiveName(RankModel.PropertyId.ABBREVIATION)
@DescriptiveName(RankModel.PropertyId.RANK_NAME)
public interface RankModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The abbreviation.
     */
    public static final String ABBREVIATION = "abbreviation";

    /**
     * The name.
     */
    public static final String RANK_NAME = "rankName";

    /**
     * The default function.
     */
    public static final String DEFAULT_FUNCTION = "defaultFunction";

    /**
     * The hierarchy index.
     */
    public static final String HIERARCHY_INDEX = "hierarchyIndex";

    /**
     * The category.
     */
    public static final String CATEGORY = "rankCategory";

    /**
     * The member count goal.
     */
    public static final String MEMBER_COUNT_GOAL = "memberCountGoal";

    /**
     * The shoulder strap picture.
     */
    public static final String SHOULDER_STRAP_PICTURE_PATH = "shoulderStrapPicturePath";

    /**
     * The authorisations.
     */
    public static final String REF_AUTHORIZATION_KEY = "refAuthorizationKey";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : RankModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(RankPersistable.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param abbreviation the abbreviation
   */
  void setAbbreviation(String abbreviation);

  /**
   * @return the abbreviation
   */
  String getAbbreviation();

  /**
   * @param rankName the name
   */
  void setRankName(String rankName);

  /**
   * @return the name
   */
  String getRankName();

  /**
   * @param defaultFunction the default function
   */
  void setDefaultFunction(String defaultFunction);

  /**
   * @return the default function
   */
  String getDefaultFunction();

  /**
   * @param hierarchyIndex the hierarchy index
   */
  void setHierarchyIndex(Integer hierarchyIndex);

  /**
   * @return the hierarchy index
   */
  Integer getHierarchyIndex();

  /**
   * @param rankCategory the category
   */
  void setRankCategory(RankCategory rankCategory);

  /**
   * @return the category
   */
  RankCategory getRankCategory();

  /**
   * @param memberCountGoal the count goal
   */
  void setMemberCountGoal(Integer memberCountGoal);

  /**
   * @return the count goal
   */
  Integer getMemberCountGoal();

  /**
   * @param shoulderStrapPicture the picture
   */
  void setShoulderStrapPicture(Path shoulderStrapPicture);

  /**
   * @return the picture
   */
  Path getShoulderStrapPicture();

  /**
   * @param refAuthorizationKey the authorisation
   */
  void setRefAuthorizationKey(AuthorizationKeyModel refAuthorizationKey);

  /**
   * @return the authorisation
   */
  AuthorizationKeyModel getRefAuthorizationKey();
}
