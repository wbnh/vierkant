package ch.vknws.vierkant.type.caption;

/**
 * Texts for model.
 *
 * @author Benjamin Weber
 */
public interface TechnicalEventPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return the text
   */
  String eventName();

  /**
   * @return the text
   */
  String description();

  /**
   * @return the text
   */
  String verificationId();

  /**
   * @return the text
   */
  String level();
}
