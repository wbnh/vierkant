package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.AuthorizationKeyModel;
import ch.vknws.vierkant.type.FunctionGroupModel;
import ch.vknws.vierkant.type.FunctionType;
import ch.vknws.vierkant.type.OrganizationFunctionModel;

/**
 * Persistable to the FunctionModel.
 *
 * @author Benjamin Weber
 */
@Entity
@Audited
public class OrganizationFunctionPersistable extends AbstractDatabasePersistable implements OrganizationFunctionModel {

  private static final long serialVersionUID = 2144228153470820234L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @Column
  private String functionName;

  @Attribute
  @Column
  private String email;

  @Attribute
  @Column
  private Integer maxPersonCount;

  @Attribute
  @Column
  private Integer minPersonCount;

  @Attribute
  @Column
  private String type;

  @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.REFRESH })
  @JoinTable(name = "FunctionSuperiorList", joinColumns = @JoinColumn(name = "organizationFunctionRecordId"), inverseJoinColumns = @JoinColumn(name = "superiorFunctionRecordId"))
  private Set<OrganizationFunctionPersistable> refSuperiors;

  @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.REFRESH }, mappedBy = "refSuperiors")
  private Set<OrganizationFunctionPersistable> refSubordinates;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH })
  @JoinColumn(name = "refAuthorizationKeyRecordId")
  private AuthorizationKeyPersistable refAuthorizationKey;

  @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH })
  @JoinTable(name = "FunctionGroupFunctionList", joinColumns = @JoinColumn(name = "memberRecordId"), inverseJoinColumns = @JoinColumn(name = "groupRecordId"))
  private Set<FunctionGroupPersistable> refGroups;

  /**
   * Returns the comment for the last modification.
   * 
   * @return the comment. Can be null depending on configuration.
   */
  @Override
  public String getAuditComment() {
    return auditComment;
  }

  /**
   * Returns the time the model was first saved on the DB.
   * 
   * @return the original save time. Null if unsaved.
   */
  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  /**
   * Returns the ID of the person who created the model.
   * 
   * @return the creator. Null if unsaved, special values apply.
   */
  @Override
  public Integer getCreator() {
    return creator;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getEmail() {
    return email;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getFunctionName() {
    return functionName;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getMaxPersonCount() {
    return maxPersonCount;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getMinPersonCount() {
    return minPersonCount;
  }

  /**
   * Returns the time the model was last saved.
   * 
   * @return the time. Null if unsaved.
   */
  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  /**
   * Returns the ID of the person who last saved the model.
   * 
   * @return the ID. Null if unsaved. Special values apply.
   */
  @Override
  public Integer getModifier() {
    return modifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getRecordId() {
    return recordId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AuthorizationKeyModel getRefAuthorizationKey() {
    return refAuthorizationKey;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Set<FunctionGroupModel> getRefGroups() {
    Set<FunctionGroupPersistable> persistables = refGroups;
    if (persistables == null) { return null; }
    Set<FunctionGroupModel> models = new HashSet<>();
    for (var persistable : persistables) {
      models.add(persistable);
    }
    return models;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Set<OrganizationFunctionModel> getRefSuperiors() {
    Set<OrganizationFunctionPersistable> persistables = refSuperiors;
    if (persistables == null) { return null; }
    Set<OrganizationFunctionModel> models = new HashSet<>();
    for (var persistable : persistables) {
      models.add(persistable);
    }
    return models;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public FunctionType getType() {
    if (type == null) { return null; }
    return FunctionType.valueOf(type);
  }

  /**
   * Sets the comment for the last modification.
   * 
   * @param auditComment the comment
   */
  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

  /**
   * Sets the time the model was first saved on the DB.
   * 
   * @param creationDateTime the time
   */
  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  /**
   * Sets the ID of the person who created the model.
   * 
   * @param creator the creator
   */
  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setFunctionName(String functionName) {
    this.functionName = functionName;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setMaxPersonCount(Integer maxPersonCount) {
    this.maxPersonCount = maxPersonCount;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setMinPersonCount(Integer minPersonCount) {
    this.minPersonCount = minPersonCount;
  }

  /**
   * Sets the time the model was last saved.
   * 
   * @param modificationDateTime the time
   */
  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  /**
   * Sets the ID of the person who last saved the model.
   * 
   * @param modifier the ID
   */
  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefAuthorizationKey(AuthorizationKeyModel refAuthorizationKey) {
    this.refAuthorizationKey = (AuthorizationKeyPersistable) refAuthorizationKey;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefGroups(Set<FunctionGroupModel> refGroups) {
    if (refGroups == null) {
      this.refGroups = null;
    } else {
      Set<FunctionGroupPersistable> persistables = new HashSet<>();
      for (var model : refGroups) {
        persistables.add((FunctionGroupPersistable) model);
      }
      this.refGroups = persistables;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefSuperiors(Set<OrganizationFunctionModel> refSuperiors) {
    if (refSuperiors == null) {
      this.refSuperiors = null;
    } else {
      Set<OrganizationFunctionPersistable> persistables = new HashSet<>();
      for (var model : refSuperiors) {
        persistables.add((OrganizationFunctionPersistable) model);
      }
      this.refSuperiors = persistables;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setType(FunctionType type) {
    if (type == null) {
      this.type = null;
    } else {
      this.type = type.name();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return getFunctionName();
  }
}
