package ch.vknws.vierkant.type;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.Configuration;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.vknws.vierkant.type.caption.PersonPersistableTexts;
import ch.vknws.vierkant.type.caption.SexTypeTexts;

/**
 * Different sexes.
 *
 * @author Benjamin Weber
 */
public enum SexType {

  /**
   * Male.
   */
  MALE,

  /**
   * Female.
   */
  FEMALE,

  /**
   * Non-human entities.
   */
  NON_HUMAN;

  private static final Configuration texts = ServiceRegistry.getConfigurationProxy()
      .getConfiguration(new ConfigurationProperties<>(SexTypeTexts.class));
  private static final PersonPersistableTexts personTexts = ServiceRegistry.getTypedConfigurationProxy()
      .get(new ConfigurationProperties<>(PersonPersistableTexts.class));

  /**
   * @return the defaultTitle
   */
  public String getDefaultTitle() {
    switch (this) {
    case MALE:
      return personTexts.mister();
    case FEMALE:
      return personTexts.mistress();
    default:
      return null;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return texts.getStringValue(name());
  }
}
