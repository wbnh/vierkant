package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.ClassificationLevel;
import ch.vknws.vierkant.type.SubtaskModel;
import ch.vknws.vierkant.type.TaskCustomFieldInstanceModel;
import ch.vknws.vierkant.type.TaskModel;
import ch.vknws.vierkant.type.TaskStatusModel;
import ch.vknws.vierkant.type.TaskTypeModel;
import ch.vknws.vierkant.type.TaskWorkflowModel;

/**
 * @author benjamin
 */
public class TaskPersistable extends AbstractDatabasePersistable implements TaskModel {

  private static final long serialVersionUID = 8958488626673545725L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @Column
  private String title;

  @Attribute
  @Column
  private String description;

  @Attribute
  @Column
  private String classification;

  @Attribute
  @Column
  private String assignToCalculation;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refTypeRecordId")
  private TaskTypePersistable refType;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refStatusRecordId")
  private TaskStatusPersistable refStatus;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refWorkflowRecordId")
  private TaskWorkflowPersistable refWorkflow;

  @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
  @Fetch(FetchMode.SELECT)
  @JoinTable(name = "SubtasksList", joinColumns = { @JoinColumn(name = "refTaskRecordId") }, inverseJoinColumns = {
      @JoinColumn(name = "refSubtaskRecordId") })
  private Set<SubtaskPersistable> refSubtasks;

  @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
  @Fetch(FetchMode.SELECT)
  @JoinTable(name = "TaskCustomFieldInstanceList", joinColumns = {
      @JoinColumn(name = "refTaskRecordId") }, inverseJoinColumns = {
          @JoinColumn(name = "refCustomFieldInstanceRecordId") })
  private Set<TaskCustomFieldInstancePersistable> refCustomFields;

  @Override
  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public void setClassification(ClassificationLevel classification) {
    if (classification == null) {
      this.classification = null;
    } else {
      this.classification = classification.name();
    }
  }

  @Override
  public ClassificationLevel getClassification() {
    if (classification == null) return null;
    return ClassificationLevel.valueOf(classification);
  }

  @Override
  public void setAssignToCalculation(String assignToCalculation) {
    this.assignToCalculation = assignToCalculation;
  }

  @Override
  public String getAssignToCalculation() {
    return assignToCalculation;
  }

  @Override
  public void setRefType(TaskTypeModel refType) {
    if (refType == null || refType instanceof TaskTypePersistable) {
      this.refType = (TaskTypePersistable) refType;
    } else {
      throw new IllegalArgumentException();
    }
  }

  @Override
  public TaskTypeModel getRefType() {
    return refType;
  }

  @Override
  public void setRefStatus(TaskStatusModel refStatus) {
    if (refStatus == null || refStatus instanceof TaskStatusPersistable) {
      this.refStatus = (TaskStatusPersistable) refStatus;
    } else {
      throw new IllegalArgumentException();
    }
  }

  @Override
  public TaskStatusModel getRefStatus() {
    return refStatus;
  }

  @Override
  public void setRefWorkflow(TaskWorkflowModel refWorkflow) {
    if (refWorkflow == null || refWorkflow instanceof TaskWorkflowPersistable) {
      this.refWorkflow = (TaskWorkflowPersistable) refWorkflow;
    } else {
      throw new IllegalArgumentException();
    }
  }

  @Override
  public TaskWorkflowModel getRefWorkflow() {
    return refWorkflow;
  }

  @Override
  public void setRefSubtasks(Set<SubtaskModel> refSubtasks) {
    if (refSubtasks == null) {
      this.refSubtasks = null;
    } else {
      Set<SubtaskPersistable> persistables = new HashSet<>();
      for (var model : refSubtasks) {
        persistables.add((SubtaskPersistable) model);
      }
      this.refSubtasks = persistables;
    }
  }

  @Override
  public Set<SubtaskModel> getRefSubtasks() {
    if (refSubtasks == null) {
      refSubtasks = new HashSet<>();
    }
    Set<SubtaskModel> models = new HashSet<>();
    for (var persistable : refSubtasks) {
      models.add(persistable);
    }
    return models;
  }

  @Override
  public void setRefCustomFields(Set<TaskCustomFieldInstanceModel> refCustomFields) {
    if (refCustomFields == null) {
      this.refCustomFields = null;
    } else {
      Set<TaskCustomFieldInstancePersistable> persistables = new HashSet<>();
      for (var model : refCustomFields) {
        persistables.add((TaskCustomFieldInstancePersistable) model);
      }
      this.refCustomFields = persistables;
    }
  }

  @Override
  public Set<TaskCustomFieldInstanceModel> getRefCustomFields() {
    if (refCustomFields == null) {
      refCustomFields = new HashSet<>();
    }
    Set<TaskCustomFieldInstanceModel> models = new HashSet<>();
    for (var persistable : refCustomFields) {
      models.add(persistable);
    }
    return models;
  }

  @Override
  public Integer getRecordId() {
    return recordId;
  }

  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  @Override
  public Integer getCreator() {
    return creator;
  }

  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  @Override
  public Integer getModifier() {
    return modifier;
  }

  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  @Override
  public String getAuditComment() {
    return auditComment;
  }

  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

}
