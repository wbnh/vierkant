package ch.vknws.vierkant.type.caption;

/**
 * Texts for sex type.
 *
 * @author Benjamin Weber
 */
public interface SexTypeTexts {

  /**
   * @return the text
   */
  String MALE();

  /**
   * @return the text
   */
  String FEMALE();

  /**
   * @return the text
   */
  String NON_HUMAN();
}
