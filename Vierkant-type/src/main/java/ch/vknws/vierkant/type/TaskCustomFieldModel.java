package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * A custom field for a task.
 * 
 * @author benjamin
 */
public interface TaskCustomFieldModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The custom field name.
     */
    public static final String CUSTOM_FIELD_NAME = "customFieldName";

    /**
     * The description.
     */
    public static final String DESCRIPTION = "description";

    /**
     * The precision for double values.
     */
    public static final String DOUBLE_PRECISION = "doublePrecision";

    /**
     * The filter for the potential values.
     */
    public static final String VALUE_FILTER = "valueFilter";

    /**
     * The type of the field.
     */
    public static final String TYPE = "type";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : TaskCustomFieldModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(TaskCustomFieldModel.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param customFieldName the name.
   */
  void setCustomFieldName(String customFieldName);

  /**
   * @return the name.
   */
  String getCustomFieldName();

  /**
   * @param description the description.
   */
  void setDescription(String description);

  /**
   * @return the description.
   */
  String getDescription();

  /**
   * @param doublePrecision the precision for double values.
   */
  void setDoublePrecision(Integer doublePrecision);

  /**
   * @return the precision for double values.
   */
  Integer getDoublePrecision();

  /**
   * @param valueFilter the filter for the potential values.
   */
  void setValueFilter(String valueFilter);

  /**
   * @return the filter for the potential values.
   */
  String getValueFilter();

  /**
   * @param type the type.
   */
  void setType(CustomFieldType type);

  /**
   * @return the type.
   */
  CustomFieldType getType();

}
