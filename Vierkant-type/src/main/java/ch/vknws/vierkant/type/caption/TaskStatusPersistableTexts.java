package ch.vknws.vierkant.type.caption;

/**
 * Texts.
 * 
 * @author benjamin
 */
public interface TaskStatusPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return text
   */
  String taskStatusName();

  /**
   * @return text
   */
  String description();

  /**
   * @return text
   */
  String color();
}
