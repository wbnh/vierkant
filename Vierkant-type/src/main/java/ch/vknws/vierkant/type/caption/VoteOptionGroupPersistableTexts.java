package ch.vknws.vierkant.type.caption;

/**
 * Caption for the vote option group.
 * 
 * @author benjamin
 */
public interface VoteOptionGroupPersistableTexts {

  /**
   * @return the text of the option group name
   */
  String optionGroupName();

  /**
   * @return the text of the ref options
   */
  String refOptions();
}
