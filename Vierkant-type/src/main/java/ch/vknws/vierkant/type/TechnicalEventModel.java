package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.vknws.vierkant.type.impl.TechnicalEventPersistable;

/**
 * Model for technical events.
 *
 * @author Benjamin Weber
 */
public interface TechnicalEventModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The name.
     */
    public static final String EVENT_NAME = "eventName";

    /**
     * The description.
     */
    public static final String DESCRIPTION = "description";

    /**
     * The verifier ID.
     */
    public static final String VERIFICATION_ID = "verificationId";

    /**
     * The level.
     */
    public static final String LEVEL = "level";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : TechnicalEventModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(TechnicalEventPersistable.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param eventName the name
   */
  void setEventName(String eventName);

  /**
   * @return the name
   */
  String getEventName();

  /**
   * @param description the description
   */
  void setDescription(String description);

  /**
   * @return the description
   */
  String getDescription();

  /**
   * @param verificationId the verifier ID
   */
  void setVerificationId(Integer verificationId);

  /**
   * @return the verifier ID
   */
  Integer getVerificationId();

  /**
   * @param level the level
   */
  void setLevel(TechnicalEventLevel level);

  /**
   * @return the level
   */
  TechnicalEventLevel getLevel();

}
