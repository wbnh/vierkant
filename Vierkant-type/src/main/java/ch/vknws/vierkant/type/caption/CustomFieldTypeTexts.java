package ch.vknws.vierkant.type.caption;

/**
 * Texts.
 * 
 * @author benjamin
 */
public interface CustomFieldTypeTexts {

  /**
   * @return text
   */
  String STRING();

  /**
   * @return text
   */
  String INTEGER();

  /**
   * @return text
   */
  String DOUBLE();

  /**
   * @return text
   */
  String BOOLEAN();

  /**
   * @return text
   */
  String DATE();

  /**
   * @return text
   */
  String DATETIME();

  /**
   * @return text
   */
  String MODEL();

  /**
   * @return text
   */
  String CALCULATED();

  /**
   * @return text
   */
  String LIST();

  /**
   * @return text
   */
  String MAP();
}
