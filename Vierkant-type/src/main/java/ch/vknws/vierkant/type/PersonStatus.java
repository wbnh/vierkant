package ch.vknws.vierkant.type;

/**
 * Status of a person entry.
 * 
 * @author benjamin
 */
public enum PersonStatus {

  /**
   * The person has applied (himself or through a superior) to become an aspirant.
   */
  APPLIED_FOR_ASPIRANT,

  /**
   * The person has applied (himself or through a superior) to become a VK.
   */
  APPLIED_FOR_VK,

  /**
   * The person has applied (himself or through a superior) to become a
   * non-commissioned officer.
   */
  APPLIED_FOR_NC_OF,

  /**
   * The person has applied (himself or through a superior) to become an officer.
   */
  APPLIED_FOR_OF,

  /**
   * The person wants to be a passive member.
   */
  APPLIED_FOR_PASSIVE,

  /**
   * No special status.
   */
  NORMAL,

  /**
   * The person wants to leave the organisation, but the process has not been
   * finished yet.
   */
  LEAVING_ORGANIATION
}
