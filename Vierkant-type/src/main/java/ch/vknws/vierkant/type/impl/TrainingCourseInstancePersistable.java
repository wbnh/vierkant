package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.TrainingCourseInstanceModel;
import ch.vknws.vierkant.type.TrainingCourseModel;

/**
 * Persistable for training course instances.
 *
 * @author Benjamin Weber
 */
@Entity
@Audited
public class TrainingCourseInstancePersistable extends AbstractDatabasePersistable
    implements TrainingCourseInstanceModel {

  private static final long serialVersionUID = 6648432148274753306L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refTemplateRecordId")
  private TrainingCoursePersistable refTemplate;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refResponsibleRecordId")
  private PersonPersistable refResponsible;

  @ManyToMany(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SELECT)
  @JoinTable(name = "TrainingCourseInstanceTeacherList", joinColumns = {
      @JoinColumn(name = "refTrainingCourseInstanceRecordId") }, inverseJoinColumns = {
          @JoinColumn(name = "refTeacherRecordId") })
  private Set<PersonPersistable> refTeachers;

  @ManyToMany(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SELECT)
  @JoinTable(name = "TrainingCourseInstanceParticipantList", joinColumns = {
      @JoinColumn(name = "refTrainingCourseInstanceRecordId") }, inverseJoinColumns = {
          @JoinColumn(name = "refParticipantRecordId") })
  private Set<PersonPersistable> refParticipants;

  /**
   * Returns the comment for the last modification.
   * 
   * @return the comment. Can be null depending on configuration.
   */
  @Override
  public String getAuditComment() {
    return auditComment;
  }

  /**
   * Returns the time the model was first saved on the DB.
   * 
   * @return the original save time. Null if unsaved.
   */
  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  /**
   * Returns the ID of the person who created the model.
   * 
   * @return the creator. Null if unsaved, special values apply.
   */
  @Override
  public Integer getCreator() {
    return creator;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  /**
   * Returns the time the model was last saved.
   * 
   * @return the time. Null if unsaved.
   */
  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  /**
   * Returns the ID of the person who last saved the model.
   * 
   * @return the ID. Null if unsaved. Special values apply.
   */
  @Override
  public Integer getModifier() {
    return modifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getRecordId() {
    return recordId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Set<PersonModel> getRefParticipants() {
    Set<PersonPersistable> persistables = refParticipants;
    if (persistables == null) { return null; }
    Set<PersonModel> models = new HashSet<>();
    for (var persistable : persistables) {
      models.add(persistable);
    }
    return models;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PersonModel getRefResponsible() {
    return refResponsible;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Set<PersonModel> getRefTeachers() {
    Set<PersonPersistable> persistables = refTeachers;
    if (persistables == null) { return null; }
    Set<PersonModel> models = new HashSet<>();
    for (var persistable : persistables) {
      models.add(persistable);
    }
    return models;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public TrainingCourseModel getRefTemplate() {
    return refTemplate;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  /**
   * Sets the comment for the last modification.
   * 
   * @param auditComment the comment
   */
  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

  /**
   * Sets the time the model was first saved on the DB.
   * 
   * @param creationDateTime the time
   */
  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  /**
   * Sets the ID of the person who created the model.
   * 
   * @param creator the creator
   */
  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  /**
   * Sets the time the model was last saved.
   * 
   * @param modificationDateTime the time
   */
  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  /**
   * Sets the ID of the person who last saved the model.
   * 
   * @param modifier the ID
   */
  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefParticipants(Set<PersonModel> refParticipants) {
    if (refParticipants == null) {
      this.refParticipants = null;
    } else {
      Set<PersonPersistable> persistables = new HashSet<>();
      for (var model : refParticipants) {
        persistables.add((PersonPersistable) model);
      }
      this.refParticipants = persistables;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefResponsible(PersonModel refResponsible) {
    if (refResponsible == null || refResponsible instanceof PersonPersistable) {
      this.refResponsible = (PersonPersistable) refResponsible;
    } else {
      throw new IllegalArgumentException();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefTeachers(Set<PersonModel> refTeachers) {
    if (refTeachers == null) {
      this.refTeachers = null;
    } else {
      Set<PersonPersistable> persistables = new HashSet<>();
      for (var model : refTeachers) {
        persistables.add((PersonPersistable) model);
      }
      this.refTeachers = persistables;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefTemplate(TrainingCourseModel template) {
    if (template == null || template instanceof TrainingCoursePersistable) {
      this.refTemplate = (TrainingCoursePersistable) template;
    } else {
      throw new IllegalArgumentException();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    if (getRefTemplate() == null) { return ""; }
    return getRefTemplate().toString();
  }

}
