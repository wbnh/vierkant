package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.ClassificationLevel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.TractandumModel;
import ch.vknws.vierkant.type.VoteResultModel;

/**
 * Implementation of the tractandum.
 * 
 * @author benjamin
 */
@Entity
@Audited
public class TractandumPersistable extends AbstractDatabasePersistable implements TractandumModel {

  private static final long serialVersionUID = 3618531497676877363L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @Column
  private String classification;

  @Attribute
  @Column
  private Integer sortIndex;

  @Attribute
  @Column
  private String header;

  @Attribute
  @Column
  private String content;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refResponsibleRecordId")
  private PersonPersistable refResponsible;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refVoteResultRecordId")
  private VoteResultPersistable refVoteResult;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refRecordRecordId")
  private RecordPersistable refRecord;

  @Override
  public void setClassification(ClassificationLevel classification) {
    if (classification == null) {
      this.classification = null;
    } else {
      this.classification = classification.name();
    }
  }

  @Override
  public ClassificationLevel getClassification() {
    if (classification == null) { return null; }
    return ClassificationLevel.valueOf(classification);
  }

  @Override
  public void setSortIndex(Integer sortIndex) {
    this.sortIndex = sortIndex;
  }

  @Override
  public Integer getSortIndex() {
    return sortIndex;
  }

  @Override
  public void setHeader(String header) {
    this.header = header;
  }

  @Override
  public String getHeader() {
    return header;
  }

  @Override
  public void setContent(String content) {
    this.content = content;
  }

  @Override
  public String getContent() {
    return content;
  }

  @Override
  public void setRefResponsible(PersonModel refResponsible) {
    if (refResponsible == null) {
      this.refResponsible = null;
    } else {
      this.refResponsible = (PersonPersistable) refResponsible;
    }
  }

  @Override
  public PersonModel getRefResponsible() {
    return refResponsible;
  }

  @Override
  public void setRefVoteResult(VoteResultModel refVoteResult) {
    if (refVoteResult == null) {
      this.refVoteResult = null;
    } else {
      this.refVoteResult = (VoteResultPersistable) refVoteResult;
    }
  }

  @Override
  public VoteResultModel getRefVoteResult() {
    return refVoteResult;
  }

  @Override
  public Integer getRecordId() {
    return recordId;
  }

  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  @Override
  public Integer getCreator() {
    return creator;
  }

  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  @Override
  public Integer getModifier() {
    return modifier;
  }

  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  @Override
  public String getAuditComment() {
    return auditComment;
  }

  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

  @Override
  public String toString() {
    return header;
  }
}
