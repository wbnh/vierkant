package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * The connector between task and custom field.
 * 
 * @author benjamin
 */
public interface TaskCustomFieldInstanceModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The default value of the field.
     */
    public static final String DEFAULT_VALUE = "defaultValue";

    /**
     * Whether the field can only written to once.
     */
    public static final String READONLY = "readonly";

    /**
     * Whether the field is required.
     */
    public static final String REQUIRED = "required";

    /**
     * The underlying custom field.
     */
    public static final String REF_CUSTOM_FIELD = "refCustomField";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {

      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : TaskCustomFieldInstanceModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(TaskCustomFieldInstanceModel.class, "Could not initialise field names",
                e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param defaultValue the default value.
   */
  void setDefaultValue(String defaultValue);

  /**
   * @return the default value.
   */
  String getDefaultValue();

  /**
   * @param readonly whether the field can only be written to once.
   */
  void setReadonly(Boolean readonly);

  /**
   * @return whether the field can only be written to once.
   */
  Boolean isReadonly();

  /**
   * @param required whether the field cannot be empty.
   */
  void setRequired(Boolean required);

  /**
   * @return whether the field cannot be empty.
   */
  Boolean isRequired();

  /**
   * @param refCustomField the underlying custom field.
   */
  void setRefCustomField(TaskCustomFieldModel refCustomField);

  /**
   * @return the underlying custom field.
   */
  TaskCustomFieldModel getRefCustomField();

}
