package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.PrerequisiteModel;

/**
 * Persistable for prerequisites.
 *
 * @author Benjamin Weber
 */
@Entity
@Audited
public class PrerequisitePersistable extends AbstractDatabasePersistable implements PrerequisiteModel {

  private static final long serialVersionUID = -7877749414626429966L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @ElementCollection(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SELECT)
  @MapKeyColumn(name = "fieldName")
  @Column(name = "fieldValue")
  @CollectionTable(name = "PrerequisiteGt", joinColumns = @JoinColumn(name = "refPrerequisiteRecordId"))
  private Map<String, Integer> gt;

  @ElementCollection(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SELECT)
  @MapKeyColumn(name = "fieldName")
  @Column(name = "fieldValue")
  @CollectionTable(name = "PrerequisiteGe", joinColumns = @JoinColumn(name = "refPrerequisiteRecordId"))
  private Map<String, Integer> ge;

  @ElementCollection(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SELECT)
  @MapKeyColumn(name = "fieldName")
  @Column(name = "fieldValue")
  @CollectionTable(name = "PrerequisiteLt", joinColumns = @JoinColumn(name = "refPrerequisiteRecordId"))
  private Map<String, Integer> lt;

  @ElementCollection(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SELECT)
  @MapKeyColumn(name = "fieldName")
  @Column(name = "fieldValue")
  @CollectionTable(name = "PrerequisiteLe", joinColumns = @JoinColumn(name = "refPrerequisiteRecordId"))
  private Map<String, Integer> le;

  @ElementCollection(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SELECT)
  @MapKeyColumn(name = "fieldName")
  @Column(name = "fieldValue")
  @CollectionTable(name = "PrerequisiteEq", joinColumns = @JoinColumn(name = "refPrerequisiteRecordId"))
  private Map<String, String> eq;

  @ElementCollection(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SELECT)
  @MapKeyColumn(name = "fieldName")
  @Column(name = "fieldValue")
  @CollectionTable(name = "PrerequisiteIsNull", joinColumns = @JoinColumn(name = "refPrerequisiteRecordId"))
  private Map<String, String> isNull;

  @ElementCollection(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SELECT)
  @MapKeyColumn(name = "fieldName")
  @Column(name = "fieldValue")
  @CollectionTable(name = "PrerequisiteNotNull", joinColumns = @JoinColumn(name = "refPrerequisiteRecordId"))
  private Map<String, String> isNotNull;

  /**
   * Returns the comment for the last modification.
   * 
   * @return the comment. Can be null depending on configuration.
   */
  @Override
  public String getAuditComment() {
    return auditComment;
  }

  /**
   * Returns the time the model was first saved on the DB.
   * 
   * @return the original save time. Null if unsaved.
   */
  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  /**
   * Returns the ID of the person who created the model.
   * 
   * @return the creator. Null if unsaved, special values apply.
   */
  @Override
  public Integer getCreator() {
    return creator;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Map<String, String> getEq() {
    return eq;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Map<String, Integer> getGe() {
    return ge;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Map<String, Integer> getGt() {
    return gt;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Map<String, String> getIsNotNull() {
    return isNotNull;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Map<String, String> getIsNull() {
    return isNull;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Map<String, Integer> getLe() {
    return le;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Map<String, Integer> getLt() {
    return lt;
  }

  /**
   * Returns the time the model was last saved.
   * 
   * @return the time. Null if unsaved.
   */
  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  /**
   * Returns the ID of the person who last saved the model.
   * 
   * @return the ID. Null if unsaved. Special values apply.
   */
  @Override
  public Integer getModifier() {
    return modifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getRecordId() {
    return recordId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  /**
   * Sets the comment for the last modification.
   * 
   * @param auditComment the comment
   */
  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

  /**
   * Sets the time the model was first saved on the DB.
   * 
   * @param creationDateTime the time
   */
  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  /**
   * Sets the ID of the person who created the model.
   * 
   * @param creator the creator
   */
  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setEq(Map<String, String> eq) {
    this.eq = eq;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setGe(Map<String, Integer> ge) {
    this.ge = ge;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setGt(Map<String, Integer> gt) {
    this.gt = gt;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setIsNotNull(Map<String, String> isNotNull) {
    this.isNotNull = isNotNull;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setIsNull(Map<String, String> isNull) {
    this.isNull = isNull;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setLe(Map<String, Integer> le) {
    this.le = le;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setLt(Map<String, Integer> lt) {
    this.lt = lt;
  }

  /**
   * Sets the time the model was last saved.
   * 
   * @param modificationDateTime the time
   */
  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  /**
   * Sets the ID of the person who last saved the model.
   * 
   * @param modifier the ID
   */
  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

}
