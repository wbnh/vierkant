package ch.vknws.vierkant.type.caption;

/**
 * Captions for the member group.
 *
 * @author Benjamin Weber
 */
public interface MemberGroupPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return the text
   */
  String category();

  /**
   * @return the text
   */
  String changeAuditComment();
}
