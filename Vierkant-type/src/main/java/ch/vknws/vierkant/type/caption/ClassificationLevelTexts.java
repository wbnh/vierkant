package ch.vknws.vierkant.type.caption;

/**
 * Captions for the classification levels.
 * 
 * @author benjamin
 */
public interface ClassificationLevelTexts {

  /**
   * @return the texts
   */
  String NON_CLASSIFIED();

  /**
   * @return the texts
   */
  String MEMBERS_ONLY();

  /**
   * @return the texts
   */
  String DEFINITIVE_MEMBERS_ONLY();

  /**
   * @return the texts
   */
  String OFFICERS_ONLY();

  /**
   * @return the texts
   */
  String COMMISSIONED_OFFICERS_ONLY();

  /**
   * @return the texts
   */
  String BOARD_OF_DIRECTORS_ONLY();
}
