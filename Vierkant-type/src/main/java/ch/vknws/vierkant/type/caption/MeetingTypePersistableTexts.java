package ch.vknws.vierkant.type.caption;

/**
 * Captions for the meeting type.
 * 
 * @author benjamin
 */
public interface MeetingTypePersistableTexts {

  /**
   * @return the texts for the type name
   */
  String typeName();

  /**
   * @return the texts for the description
   */
  String description();
}
