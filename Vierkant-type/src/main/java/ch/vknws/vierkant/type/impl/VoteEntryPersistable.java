package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.VoteEntryModel;
import ch.vknws.vierkant.type.VoteOptionModel;

/**
 * Implementation of the vote entry.
 * 
 * @author benjamin
 */
@Entity
@Audited
public class VoteEntryPersistable extends AbstractDatabasePersistable implements VoteEntryModel {

  private static final long serialVersionUID = -8605850966818693880L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refVoteOptionRecordId")
  private VoteOptionPersistable refVoteOption;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refVoterRecordId")
  private PersonPersistable refVoter;

  @Attribute
  @Column
  private LocalDateTime validityTimestamp;

  @Attribute
  @Column
  private String checksum;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refVoteResultRecordId")
  private VoteResultPersistable refVoteResult;

  @Override
  public void setRefVoteOption(VoteOptionModel refVoteOption) {
    if (refVoteOption == null) {
      this.refVoteOption = null;
    } else {
      this.refVoteOption = (VoteOptionPersistable) refVoteOption;
    }
  }

  @Override
  public VoteOptionModel getRefVoteOption() {
    return refVoteOption;
  }

  @Override
  public void setRefVoter(PersonModel voter) {
    if (voter == null) {
      refVoter = null;
    } else {
      refVoter = (PersonPersistable) voter;
    }
  }

  @Override
  public PersonModel getRefVoter() {
    return refVoter;
  }

  @Override
  public void setValidityTimestamp(LocalDateTime validityTimestamp) {
    this.validityTimestamp = validityTimestamp;
  }

  @Override
  public LocalDateTime getValidityTimestamp() {
    return validityTimestamp;
  }

  @Override
  public void setChecksum(String checksum) {
    this.checksum = checksum;
  }

  @Override
  public String getChecksum() {
    return checksum;
  }

  @Override
  public Integer getRecordId() {
    return recordId;
  }

  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  @Override
  public Integer getCreator() {
    return creator;
  }

  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  @Override
  public Integer getModifier() {
    return modifier;
  }

  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  @Override
  public String getAuditComment() {
    return auditComment;
  }

  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

  @Override
  public String toString() {
    if (refVoter == null) { return ""; }
    return refVoter.toString();
  }
}
