package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.util.AssertUtils;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * Utility for actions on models.
 *
 * @author Benjamin Weber
 */
public class ModelUtils {

  /**
   * Gets the property types of the passed class.
   * 
   * @param modelClass the class to get the types of
   * @return the properties and their types
   */
  @SuppressWarnings({ "unchecked", "null" })
  public static <T extends DatabaseModel> Map<String, Class<?>> getPropertyTypes(Class<T> modelClass) {
    Class<?>[] declaredClasses = modelClass.getDeclaredClasses();
    Class<?> propertyIdClass = null;

    for (Class<?> currentClass : declaredClasses) {
      if ("PropertyId".equals(currentClass.getSimpleName())) {
        propertyIdClass = currentClass;
        break;
      }
    }

    AssertUtils.notNull(propertyIdClass, "Class does not declare property IDs.");

    try {
      return (Map<String, Class<?>>) propertyIdClass.getDeclaredField("PROPERTY_TYPES").get(null);
    } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
      ServiceRegistry.getLogProxy().error(ModelUtils.class, "Could not get property types for class {0}", e,
          modelClass);
      throw new RuntimeException(e);
    }
  }

  /**
   * Merges the first object into the second.
   * 
   * @param source the object to merge into the other
   * @param target the object to be merged into
   */
  public static <T extends DatabaseModel> void merge(T source, T target) {
    if (source == null || target == null) { return; }

    if (source.getRevisionId() > target.getRevisionId()) {
      merge(target, source);
      return;
    }

    Class<? extends DatabaseModel> clazz = target.getClass();

    for (Field field : clazz.getDeclaredFields()) {
      field.setAccessible(true);
      try {
        field.set(target, field.get(source));
      } catch (IllegalArgumentException | IllegalAccessException e) {
        ServiceRegistry.getLogProxy().error(ModelUtils.class, "Could not merge models", e);
        throw new RuntimeException(e);
      }
    }
  }

  /**
   * Finds the class for the passed model name.
   * 
   * @param name the name of the model
   * @return the class or <code>null</code> if no model with that name was found
   */
  public static Class<? extends DatabaseModel> findModelForName(String name) {

    try {
      return (Class<? extends DatabaseModel>) ModelUtils.class.getClassLoader()
          .loadClass(ModelUtils.class.getPackageName() + "." + name + "Model");
    } catch (ClassNotFoundException e) {
      ServiceRegistry.getLogProxy().debug(ModelUtils.class, "Class {0} does not exist", name);
      return null;
    }
  }

}
