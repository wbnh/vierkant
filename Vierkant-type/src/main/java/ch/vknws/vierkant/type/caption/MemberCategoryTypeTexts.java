package ch.vknws.vierkant.type.caption;

/**
 * Captions for MemberCategoryTypes.
 *
 * @author Benjamin Weber
 */
public interface MemberCategoryTypeTexts {

  /**
   * @return the text
   */
  String INTERESTED();

  /**
   * @return the text
   */
  String ASPIRANT();

  /**
   * @return the text
   */
  String ACTIVE();

  /**
   * @return the text
   */
  String PASSIVE_SINGLE();

  /**
   * @return the text
   */
  String PASSIVE_COLLECTIVE();

  /**
   * @return the text
   */
  String PASSIVE_FAMILY();

  /**
   * @return the text
   */
  String HONORARY_ACTIVE();

  /**
   * @return the text
   */
  String HONORARY_PASSIVE();
}
