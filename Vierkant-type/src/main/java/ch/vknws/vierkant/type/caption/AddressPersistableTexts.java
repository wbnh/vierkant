package ch.vknws.vierkant.type.caption;

/**
 * Captions for the AddressModel.
 *
 * @author Benjamin Weber
 */
public interface AddressPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return the text
   */
  String line1();

  /**
   * @return the text
   */
  String line2();

  /**
   * @return the text
   */
  String zipCode();

  /**
   * @return the text
   */
  String city();

  /**
   * @return the text
   */
  String canton();

  /**
   * @return the text
   */
  String country();
}
