package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * A workflow of a task.
 * 
 * @author benjamin
 */
public interface TaskWorkflowModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The name of the workflow.
     */
    public static final String TASK_WORKFLOW_NAME = "taskWorkflowName";

    /**
     * The description of the workflow.
     */
    public static final String DESCRIPTION = "description";

    /**
     * The initial status of the workflow.
     */
    public static final String REF_INITIAL_STATUS = "refInitialStatus";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : TaskWorkflowModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(TaskWorkflowModel.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param taskWorkflowName the workflow name.
   */
  void setTaskWorkflowName(String taskWorkflowName);

  /**
   * @return the workflow name.
   */
  String getTaskWorkflowName();

  /**
   * @param description the description.
   */
  void setDescription(String description);

  /**
   * @return the description.
   */
  String getDescription();

  /**
   * @param refInitialStatus the initial status.
   */
  void setRefInitialStatus(TaskWorkflowStatusModel refInitialStatus);

  /**
   * @return the initial status.
   */
  TaskWorkflowStatusModel getRefInitialStatus();

}
