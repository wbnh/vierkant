package ch.vknws.vierkant.type.caption;

/**
 * Captions for the vote result.
 * 
 * @author benjamin
 */
public interface VoteResultPersistableTexts {

  /**
   * @return the text for the poll start
   */
  String pollStart();

  /**
   * @return the text for the poll end
   */
  String pollEnd();

  /**
   * @return the text for the vote option group
   */
  String refVoteOptionGroup();

  /**
   * @return the text for the voters
   */
  String refVoters();

  /**
   * @return the text for the vote entries
   */
  String refVoteEntries();

  /**
   * @return the text for the checksum
   */
  String checksum();
}
