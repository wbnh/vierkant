package ch.vknws.vierkant.type.caption;

/**
 * Captions for all models.
 *
 * @author Benjamin Weber
 */
public interface AbstractPersistableTexts {

  /**
   * @return the text
   */
  String recordId();

  /**
   * @return the text
   */
  String revisionId();

  /**
   * @return the text
   */
  String creator();

  /**
   * @return the text
   */
  String creationDateTime();

  /**
   * @return the text
   */
  String modifier();

  /**
   * @return the text
   */
  String modificationDateTime();

  /**
   * @return the text
   */
  String deletedFlag();

  /**
   * @return the text
   */
  String auditComment();
}
