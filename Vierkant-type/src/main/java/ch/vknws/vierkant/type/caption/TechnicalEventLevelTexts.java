package ch.vknws.vierkant.type.caption;

/**
 * Texts for enum.
 *
 * @author Benjamin Weber
 */
public interface TechnicalEventLevelTexts {

  /**
   * @return the text
   */
  String DEBUG();

  /**
   * @return the text
   */
  String INFO();

  /**
   * @return the text
   */
  String ERROR();

  /**
   * @return the text
   */
  String SEVERE();
}
