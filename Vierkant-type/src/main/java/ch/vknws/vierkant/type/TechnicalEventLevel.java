package ch.vknws.vierkant.type;

/**
 * Severity level for technical events.
 *
 * @author Benjamin Weber
 */
public enum TechnicalEventLevel {

  /**
   * Debug level.
   */
  DEBUG,

  /**
   * Info level.
   */
  INFO,

  /**
   * Error level.
   */
  ERROR,

  /**
   * Severe level.
   */
  SEVERE;
}
