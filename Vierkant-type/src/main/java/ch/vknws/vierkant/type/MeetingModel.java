package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.database.DescriptiveName;

/**
 * Type to classify meetings.
 * 
 * @author benjamin
 */
@DescriptiveName(MeetingModel.PropertyId.MEETING_NAME)
public interface MeetingModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The type of the meeting.
     */
    public static final String REF_MEETING_TYPE = "refMeetingType";

    /**
     * The descriptive non-unique name of the meeting.
     */
    public static final String MEETING_NAME = "meetingName";

    /**
     * The in-depth description of the meeting.
     */
    public static final String DESCRIPTION = "description";

    /**
     * The start date time of the meeting.
     */
    public static final String START = "start";

    /**
     * The end date time of the meeting.
     */
    public static final String END = "end";

    /**
     * The person responsible for the meeting.
     */
    public static final String REF_RESPONSIBLE = "refResponsible";

    /**
     * The invited persons.
     */
    public static final String REF_PLANNED_ATTENDEES = "refPlannedAttendees";

    /**
     * The persons actually present at the meeting. Persons planned, but not
     * actually attending and without excuse are treated as missing without excuse.
     * Persons not planned, but present are treated as guests.
     */
    public static final String REF_ACTUAL_ATTENDEES = "refActualAttendees";

    /**
     * The excuses of missing persons.
     */
    public static final String REF_MISS_EXCUSES = "refMissExcuses";

    /**
     * The record of the meeting.
     */
    public static final String REF_RECORD = "refRecord";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : MeetingModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(MeetingModel.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param refMeetingType the new meeting type
   */
  void setRefMeetingType(MeetingTypeModel refMeetingType);

  /**
   * @return the type of the meeting
   */
  MeetingTypeModel getRefMeetingType();

  /**
   * @param meetingName the new meeting name
   */
  void setMeetingName(String meetingName);

  /**
   * @return the human-readable name of the meeting
   */
  String getMeetingName();

  /**
   * @param description the new description
   */
  void setDescription(String description);

  /**
   * @return the description of the meeting
   */
  String getDescription();

  /**
   * @param start the new start time
   */
  void setStart(LocalDateTime start);

  /**
   * @return the start time of the meeting
   */
  LocalDateTime getStart();

  /**
   * @param end the new end time
   */
  void setEnd(LocalDateTime end);

  /**
   * @return the end time of the meeting
   */
  LocalDateTime getEnd();

  /**
   * @param refResponsible the new responsible
   */
  void setRefResponsible(PersonModel refResponsible);

  /**
   * @return the person responsible for the meeting
   */
  PersonModel getRefResponsible();

  /**
   * @param refPlannedAttendees the new invited persons
   */
  void setRefPlannedAttendees(Set<PersonModel> refPlannedAttendees);

  /**
   * @return the invited persons
   */
  Set<PersonModel> getRefPlannedAttendees();

  /**
   * @param refActualAttendees the new attendees
   */
  void setRefActualAttendees(Set<PersonModel> refActualAttendees);

  /**
   * @return the persons actually present
   */
  Set<PersonModel> getRefActualAttendees();

  /**
   * @param refMissExcuses the new excuses
   */
  void setRefMissExcuses(Set<MissExcuseModel> refMissExcuses);

  /**
   * @return the excuses of the missing persons.
   */
  Set<MissExcuseModel> getRefMissExcuses();

  /**
   * @param refRecord the new record
   */
  void setRefRecord(RecordModel refRecord);

  /**
   * @return the record of the meeting
   */
  RecordModel getRefRecord();

}
