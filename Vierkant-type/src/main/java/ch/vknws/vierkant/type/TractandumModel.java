package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * Type to classify meetings.
 * 
 * @author benjamin
 */
public interface TractandumModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The classification of the tractandum.
     */
    public static final String CLASSIFICATION = "classification";

    /**
     * The index according to which it should be sorted.
     */
    public static final String SORT_INDEX = "sortIndex";

    /**
     * The header of the item.
     */
    public static final String HEADER = "header";

    /**
     * The content of the item.
     */
    public static final String CONTENT = "content";

    /**
     * The main speaker of the tractandum.
     */
    public static final String REF_RESPONSIBLE = "refResponsible";

    /**
     * If there was a vote it is accessible here.
     */
    public static final String REF_VOTE_RESULT = "refVoteResult";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : TractandumModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(TractandumModel.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param classification the classification
   */
  void setClassification(ClassificationLevel classification);

  /**
   * @return the classification
   */
  ClassificationLevel getClassification();

  /**
   * @param sortIndex the new sort index
   */
  void setSortIndex(Integer sortIndex);

  /**
   * @return the index according to which a sort is necessary
   */
  Integer getSortIndex();

  /**
   * @param header the new header
   */
  void setHeader(String header);

  /**
   * @return the header of the item
   */
  String getHeader();

  /**
   * @param content the new content
   */
  void setContent(String content);

  /**
   * @return the content of the item
   */
  String getContent();

  /**
   * @param refResponsible the new responsible
   */
  void setRefResponsible(PersonModel refResponsible);

  /**
   * @return the main speaker
   */
  PersonModel getRefResponsible();

  /**
   * @param refVoteResult the new vote result
   */
  void setRefVoteResult(VoteResultModel refVoteResult);

  /**
   * @return the result of the vote if any
   */
  VoteResultModel getRefVoteResult();

}
