package ch.vknws.vierkant.type.caption;

/**
 * Texts.
 * 
 * @author benjamin
 */
public interface ProcessPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return text
   */
  String processName();

  /**
   * @return text
   */
  String description();

  /**
   * @return text
   */
  String allowParallelExecution();

  /**
   * @return text
   */
  String enabled();

  /**
   * @return text
   */
  String classification();

  /**
   * @return text
   */
  String refTasks();

  /**
   * @return text
   */
  String refProcessVariables();
}
