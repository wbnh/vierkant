package ch.vknws.vierkant.type;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.Configuration;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.vknws.vierkant.type.caption.VKATexts;

/**
 * The VKAs.
 *
 * @author Benjamin Weber
 */
public enum VKA {

  /**
   * Albis.
   */
  A,

  /**
   * Appenzellerland.
   */
  AP,

  /**
   * Ausserschwyz.
   */
  AS,

  /**
   * Kadetten-Korps Basel.
   */
  KKB,

  /**
   * Bern.
   */
  BE,

  /**
   * Chur.
   */
  C,

  /**
   * Fürstenland.
   */
  F,

  /**
   * Glarus.
   */
  GL,

  /**
   * Nordwestschweiz.
   */
  NWS,

  /**
   * Rapperswil-Jona.
   */
  RJ,

  /**
   * Sankt Gallen.
   */
  SG,

  /**
   * Sankt Galler Oberland.
   */
  SGO,

  /**
   * Schaffhausen.
   */
  SH,

  /**
   * Samstagern-Richterswil.
   */
  SR,

  /**
   * Winterthur.
   */
  W,

  /**
   * Zug.
   */
  ZG,

  /**
   * Zürcher Oberland.
   */
  ZO,

  /**
   * Zürcher Unterland.
   */
  ZU,

  /**
   * Zürichsee.
   */
  ZS;

  private static Configuration texts = ServiceRegistry.getConfigurationProxy()
      .getConfiguration(new ConfigurationProperties<>(VKATexts.class));

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return texts.getStringValue(name());
  }
}
