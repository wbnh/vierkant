package ch.vknws.vierkant.type.caption;

/**
 * Texts for model.
 *
 * @author Benjamin Weber
 */
public interface AuthorizationKeyPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return the text
   */
  String keyName();

  /**
   * @return the text
   */
  String subjectId();

  /**
   * @return the text
   */
  String subjectType();

  /**
   * @return the text
   */
  String mode();

  /**
   * @return the text
   */
  String type();

  /**
   * @return the text
   */
  String subordinate();
}
