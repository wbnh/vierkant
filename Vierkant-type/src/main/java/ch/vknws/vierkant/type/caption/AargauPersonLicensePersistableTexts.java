package ch.vknws.vierkant.type.caption;

/**
 * Captions for the AargauPersonLicenseModel.
 *
 * @author Benjamin Weber
 */
public interface AargauPersonLicensePersistableTexts extends AbstractPersistableTexts {

  /**
   * @return the text
   */
  String invoiceNumber();

  /**
   * @return the text
   */
  String validityEnd();

  /**
   * @return the text
   */
  String registrationDate();

  /**
   * @return the text
   */
  String refLicense();
}
