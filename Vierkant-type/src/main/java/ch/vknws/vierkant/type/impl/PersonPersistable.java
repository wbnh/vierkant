package ch.vknws.vierkant.type.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.AargauPersonLicenseModel;
import ch.vknws.vierkant.type.AddressModel;
import ch.vknws.vierkant.type.AuthorizationKeyModel;
import ch.vknws.vierkant.type.ContactPersonModel;
import ch.vknws.vierkant.type.MedicalInformationModel;
import ch.vknws.vierkant.type.MemberCategoryType;
import ch.vknws.vierkant.type.MemberGroupModel;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.PersonFunctionModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.PersonStatus;
import ch.vknws.vierkant.type.RankModel;
import ch.vknws.vierkant.type.SexType;
import ch.vknws.vierkant.type.TrainingCourseModel;
import ch.vknws.vierkant.type.VKA;

/**
 * Persistable for PersonModel.
 *
 * @author Benjamin Weber
 */
@Entity
@Audited
public class PersonPersistable extends AbstractDatabasePersistable implements PersonModel {

  private static final long serialVersionUID = -3903388367508139527L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @Column
  private String givenName;

  @Attribute
  @Column
  private String familyName;

  @Attribute
  @Column
  private String title;

  @Attribute
  @Column
  private String telephone;

  @Attribute
  @Column
  private String mobile;

  @Attribute
  @Column
  private String emailPrivate;

  @Attribute
  @Column
  private String emailOrganization;

  @Attribute
  @Column
  private LocalDate birthdate;

  @Attribute
  @Column
  private String secretaryComment;

  @Attribute
  @Column
  private Integer vkNews;

  @Attribute
  @Column
  private LocalDate lastUpdateCheck;

  @Attribute
  @Column
  private String shorthand;

  @Attribute
  @Column
  private String vka;

  @Attribute
  @Column
  private String memberCategory;

  @Attribute
  @Column
  private String sex;

  @Attribute
  @Column
  private String status = PersonStatus.NORMAL.name();

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refHomeAddressRecordId")
  private AddressPersistable refHomeAddress;

  @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
  @Fetch(FetchMode.SELECT)
  @JoinTable(name = "PersonContactPersonList", joinColumns = {
      @JoinColumn(name = "refPersonRecordId") }, inverseJoinColumns = {
          @JoinColumn(name = "refContactPersonRecordId") })
  private Set<ContactPersonPersistable> refContactPersons;

  @Attribute
  @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refAargauLicenseRecordId")
  private AargauPersonLicensePersistable refAargauLicense;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refMemberGroupRecordId")
  private MemberGroupPersistable refMemberGroup;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH })
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refRankRecordId")
  private RankPersistable refRank;

  @OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
  @Fetch(FetchMode.SELECT)
  @JoinTable(name = "PersonFunctionList", joinColumns = {
      @JoinColumn(name = "refPersonRecordId") }, inverseJoinColumns = { @JoinColumn(name = "refFunctionRecordId") })
  private Set<PersonFunctionPersistable> refFunctions;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH })
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refPrimaryFunctionRecordId")
  private OrganizationFunctionPersistable refPrimaryFunction;

  @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @Fetch(FetchMode.SELECT)
  @JoinTable(name = "PersonTrainingCourseList", joinColumns = {
      @JoinColumn(name = "refPersonRecordId") }, inverseJoinColumns = {
          @JoinColumn(name = "refTrainingCourseRecordId") })
  private Set<TrainingCoursePersistable> refCompletedCourses;

  @Attribute
  @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refMedicalInformationRecordId")
  private MedicalInformationPersistable refMedicalInformation;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH })
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refAuthorizationKeyRecordId")
  private AuthorizationKeyPersistable refAuthorizationKey;

  /**
   * Returns the comment for the last modification.
   * 
   * @return the comment. Can be null depending on configuration.
   */
  @Override
  public String getAuditComment() {
    return auditComment;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public LocalDate getBirthdate() {
    return birthdate;
  }

  /**
   * Returns the time the model was first saved on the DB.
   * 
   * @return the original save time. Null if unsaved.
   */
  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  /**
   * Returns the ID of the person who created the model.
   * 
   * @return the creator. Null if unsaved, special values apply.
   */
  @Override
  public Integer getCreator() {
    return creator;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getEmailOrganization() {
    return emailOrganization;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getEmailPrivate() {
    return emailPrivate;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getFamilyName() {
    return familyName;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getGivenName() {
    return givenName;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public LocalDate getLastUpdateCheck() {
    return lastUpdateCheck;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public MemberCategoryType getMemberCategory() {
    if (memberCategory == null) { return null; }
    return MemberCategoryType.valueOf(memberCategory);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getMobile() {
    return mobile;
  }

  /**
   * Returns the time the model was last saved.
   * 
   * @return the time. Null if unsaved.
   */
  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  /**
   * Returns the ID of the person who last saved the model.
   * 
   * @return the ID. Null if unsaved. Special values apply.
   */
  @Override
  public Integer getModifier() {
    return modifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getRecordId() {
    return recordId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AargauPersonLicenseModel getRefAargauLicense() {
    return refAargauLicense;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AuthorizationKeyModel getRefAuthorizationKey() {
    return refAuthorizationKey;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Set<TrainingCourseModel> getRefCompletedCourses() {
    if (refCompletedCourses == null) {
      refCompletedCourses = new HashSet<>();
    }
    Set<TrainingCourseModel> models = new HashSet<>();
    for (var persistable : refCompletedCourses) {
      models.add(persistable);
    }
    return models;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Set<ContactPersonModel> getRefContactPersons() {
    if (refContactPersons == null) {
      refContactPersons = new HashSet<>();
    }
    Set<ContactPersonModel> models = new HashSet<>();
    for (var persistable : refContactPersons) {
      models.add(persistable);
    }
    return models;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Set<PersonFunctionModel> getRefFunctions() {
    if (refFunctions == null) {
      refFunctions = new HashSet<>();
    }
    Set<PersonFunctionModel> models = new HashSet<>();
    for (var persistable : refFunctions) {
      models.add(persistable);
    }
    return models;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AddressModel getRefHomeAddress() {
    return refHomeAddress;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public MedicalInformationModel getRefMedicalInformation() {
    return refMedicalInformation;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public MemberGroupModel getRefMemberGroup() {
    return refMemberGroup;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public OrganizationFunctionModel getRefPrimaryFunction() {
    return refPrimaryFunction;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public RankModel getRefRank() {
    return refRank;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getSecretaryComment() {
    return secretaryComment;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public SexType getSex() {
    if (sex == null) { return null; }
    return SexType.valueOf(sex);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getShorthand() {
    return shorthand;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PersonStatus getStatus() {
    if (status == null) { return null; }
    return PersonStatus.valueOf(status);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getTelephone() {
    return telephone;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getTitle() {
    return title;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public VKA getVka() {
    if (vka == null) { return null; }
    return VKA.valueOf(vka);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean getVkNews() {
    return integerToBoolean(vkNews);
  }

  /**
   * Sets the comment for the last modification.
   * 
   * @param auditComment the comment
   */
  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setBirthdate(LocalDate birthdate) {
    this.birthdate = birthdate;
  }

  /**
   * Sets the time the model was first saved on the DB.
   * 
   * @param creationDateTime the time
   */
  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  /**
   * Sets the ID of the person who created the model.
   * 
   * @param creator the creator
   */
  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setEmailOrganization(String emailOrganization) {
    this.emailOrganization = emailOrganization;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setEmailPrivate(String emailPrivate) {
    this.emailPrivate = emailPrivate;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setFamilyName(String familyName) {
    this.familyName = familyName;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setGivenName(String givenName) {
    this.givenName = givenName;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setLastUpdateCheck(LocalDate lastUpdateCheck) {
    this.lastUpdateCheck = lastUpdateCheck;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setMemberCategory(MemberCategoryType memberCategory) {
    if (memberCategory == null) {
      this.memberCategory = null;
    } else {
      this.memberCategory = memberCategory.name();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  /**
   * Sets the time the model was last saved.
   * 
   * @param modificationDateTime the time
   */
  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  /**
   * Sets the ID of the person who last saved the model.
   * 
   * @param modifier the ID
   */
  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefAargauLicense(AargauPersonLicenseModel refAargauLicense) {
    if (refAargauLicense == null || refAargauLicense instanceof AargauPersonLicensePersistable) {
      this.refAargauLicense = (AargauPersonLicensePersistable) refAargauLicense;
    } else {
      throw new IllegalArgumentException();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefAuthorizationKey(AuthorizationKeyModel refAuthorizationKey) {
    if (refAuthorizationKey == null || refAuthorizationKey instanceof AuthorizationKeyPersistable) {
      this.refAuthorizationKey = (AuthorizationKeyPersistable) refAuthorizationKey;
    } else {
      throw new IllegalArgumentException();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefCompletedCourses(Set<TrainingCourseModel> refCompletedCourse) {
    if (refCompletedCourse == null) {
      this.refCompletedCourses = null;
    } else {
      Set<TrainingCoursePersistable> persistables = new HashSet<>();
      for (var model : refCompletedCourse) {
        persistables.add((TrainingCoursePersistable) model);
      }
      this.refCompletedCourses = persistables;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefContactPersons(Set<ContactPersonModel> refContactPersons) {
    if (refContactPersons == null) {
      this.refContactPersons = null;
    } else {
      Set<ContactPersonPersistable> persistables = new HashSet<>();
      for (var model : refContactPersons) {
        persistables.add((ContactPersonPersistable) model);
      }
      this.refContactPersons = persistables;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefFunctions(Set<PersonFunctionModel> refFunctions) {
    if (refFunctions == null) {
      this.refFunctions = null;
    } else {
      Set<PersonFunctionPersistable> persistables = new HashSet<>();
      for (var model : refFunctions) {
        persistables.add((PersonFunctionPersistable) model);
      }
      this.refFunctions = persistables;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefHomeAddress(AddressModel refHomeAddress) {
    if (refHomeAddress == null || refHomeAddress instanceof AddressPersistable) {
      this.refHomeAddress = (AddressPersistable) refHomeAddress;
    } else {
      throw new IllegalArgumentException();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefMedicalInformation(MedicalInformationModel refMedicalInformation) {
    if (refMedicalInformation == null || refMedicalInformation instanceof MedicalInformationPersistable) {
      this.refMedicalInformation = (MedicalInformationPersistable) refMedicalInformation;
    } else {
      throw new IllegalArgumentException();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefMemberGroup(MemberGroupModel refMemberGroup) {
    if (refMemberGroup == null || refMemberGroup instanceof MemberGroupPersistable) {
      this.refMemberGroup = (MemberGroupPersistable) refMemberGroup;
    } else {
      throw new IllegalArgumentException();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefPrimaryFunction(OrganizationFunctionModel refPrimaryFunction) {
    if (refPrimaryFunction == null || refPrimaryFunction instanceof OrganizationFunctionPersistable) {
      this.refPrimaryFunction = (OrganizationFunctionPersistable) refPrimaryFunction;
    } else {
      throw new IllegalArgumentException();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRefRank(RankModel refRank) {
    if (refRank == null || refRank instanceof RankPersistable) {
      this.refRank = (RankPersistable) refRank;
    } else {
      throw new IllegalArgumentException();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setSecretaryComment(String secretaryComment) {
    this.secretaryComment = secretaryComment;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setSex(SexType sex) {
    if (sex == null) {
      this.sex = null;
    } else {
      this.sex = sex.name();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setShorthand(String shorthand) {
    this.shorthand = shorthand;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setStatus(PersonStatus status) {
    if (status == null) {
      this.status = null;
    } else {
      this.status = status.name();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setVka(VKA vka) {
    if (vka == null) {
      this.vka = null;
    } else {
      this.vka = vka.name();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setVkNews(boolean vkNews) {
    this.vkNews = booleanToInteger(vkNews);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();

    if (getRefRank() != null) {
      builder.append(getRefRank().getAbbreviation());
      builder.append(" ");
    }
    builder.append(getGivenName());
    builder.append(" ");
    builder.append(getFamilyName());

    return builder.toString();
  }
}
