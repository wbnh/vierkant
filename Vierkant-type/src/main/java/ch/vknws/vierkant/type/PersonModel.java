package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.database.DescriptiveName;
import ch.vknws.vierkant.type.impl.PersonPersistable;

/**
 * Model for a person.
 *
 * @author Benjamin Weber
 */
@DescriptiveName(PersonModel.PropertyId.GIVEN_NAME)
@DescriptiveName(PersonModel.PropertyId.FAMILY_NAME)
public interface PersonModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The first name.
     */
    public static final String GIVEN_NAME = "givenName";

    /**
     * The last name.
     */
    public static final String FAMILY_NAME = "familyName";

    /**
     * The title.
     */
    public static final String TITLE = "title";

    /**
     * The telephone number.
     */
    public static final String TELEPHONE = "telephone";

    /**
     * The mobile phone number.
     */
    public static final String MOBILE = "mobile";

    /**
     * The private email.
     */
    public static final String EMAIL_PRIVATE = "emailPrivate";

    /**
     * The organisation email.
     */
    public static final String EMAIL_ORGANISATION = "emailOrganization";

    /**
     * The birthdate.
     */
    public static final String BIRTHDATE = "birthdate";

    /**
     * The comment by the secretary.
     */
    public static final String SECRETARY_COMMENT = "secretaryComment";

    /**
     * Whether to send the newsletter.
     */
    public static final String VK_NEWS = "vkNews";

    /**
     * The last time the data was checked.
     */
    public static final String LAST_UPDATE_CHECK = "lastUpdateCheck";

    /**
     * The shorthand name.
     */
    public static final String SHORTHAND = "shorthand";

    /**
     * The VKA.
     */
    public static final String VKA = "vka";

    /**
     * The category.
     */
    public static final String MEMBER_CATEGORY = "memberCategory";

    /**
     * The person's sex.
     */
    public static final String SEX = "sex";

    /**
     * The person status.
     */
    public static final String STATUS = "status";

    //
    // Referenced models
    //

    /**
     * The home address.
     */
    public static final String REF_HOME_ADDRESS = "refHomeAddress";

    /**
     * The contact persons.
     */
    public static final String REF_CONTACT_PERSONS = "refContactPersons";

    /**
     * The aargau license.
     */
    public static final String REF_AARGAU_LICENSE = "refAargauLicense";

    /**
     * The member group.
     */
    public static final String REF_MEMBER_GROUP = "refMemberGroup";

    /**
     * The rank.
     */
    public static final String REF_RANK = "refRank";

    /**
     * The functions.
     */
    public static final String REF_FUNCTIONS = "refFunctions";

    /**
     * The primary function.
     */
    public static final String REF_PRIMARY_FUNCTION = "refPrimaryFunction";

    /**
     * The completed courses.
     */
    public static final String REF_COMPLETED_COURSES = "refCompletedCourses";

    /**
     * The medical information.
     */
    public static final String REF_MEDICAL_INFORMATION = "refMedicalInformation";

    /**
     * The authorisations.
     */
    public static final String REF_AUTHORIZATION_KEY = "refAuthorizationKey";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : PersonModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(PersonPersistable.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param givenName the first name
   */
  void setGivenName(String givenName);

  /**
   * @return the first name
   */
  String getGivenName();

  /**
   * @param familyName the last name
   */
  void setFamilyName(String familyName);

  /**
   * @return the last name
   */
  String getFamilyName();

  /**
   * @param title the title
   */
  void setTitle(String title);

  /**
   * @return the title
   */
  String getTitle();

  /**
   * @param telephone the telephone number
   */
  void setTelephone(String telephone);

  /**
   * @return the telephone number
   */
  String getTelephone();

  /**
   * @param mobile the mobile number
   */
  void setMobile(String mobile);

  /**
   * @return the mobile number
   */
  String getMobile();

  /**
   * @param emailPrivate the private email
   */
  void setEmailPrivate(String emailPrivate);

  /**
   * @return the private email
   */
  String getEmailPrivate();

  /**
   * @param emailOrganization the organisation email
   */
  void setEmailOrganization(String emailOrganization);

  /**
   * @return the organisation email
   */
  String getEmailOrganization();

  /**
   * @param birthdate the birthdate
   */
  void setBirthdate(LocalDate birthdate);

  /**
   * @return the birthdate
   */
  LocalDate getBirthdate();

  /**
   * @param secretaryComment the secretary comment
   */
  void setSecretaryComment(String secretaryComment);

  /**
   * @return the secretary comment
   */
  String getSecretaryComment();

  /**
   * @param vkNews whether to send the newsletter
   */
  void setVkNews(boolean vkNews);

  /**
   * @return whether to send the newsletter
   */
  boolean getVkNews();

  /**
   * @param lastUpdateCheck the last update check
   */
  void setLastUpdateCheck(LocalDate lastUpdateCheck);

  /**
   * @return the last update check
   */
  LocalDate getLastUpdateCheck();

  /**
   * @param shorthand the shorthand
   */
  void setShorthand(String shorthand);

  /**
   * @return the shorthand
   */
  String getShorthand();

  /**
   * @param vka the VKA
   */
  void setVka(VKA vka);

  /**
   * @return the VKA
   */
  VKA getVka();

  /**
   * @param memberCategory the member category
   */
  void setMemberCategory(MemberCategoryType memberCategory);

  /**
   * @return the member category
   */
  MemberCategoryType getMemberCategory();

  /**
   * @param sex the sex
   */
  void setSex(SexType sex);

  /**
   * @return the sex
   */
  SexType getSex();

  /**
   * @param status the new person status
   */
  void setStatus(PersonStatus status);

  /**
   * @return the person status
   */
  PersonStatus getStatus();

  /**
   * @param refHomeAddress the home address
   */
  void setRefHomeAddress(AddressModel refHomeAddress);

  /**
   * @return the home address
   */
  AddressModel getRefHomeAddress();

  /**
   * @param refContactPersons the contact persons
   */
  void setRefContactPersons(Set<ContactPersonModel> refContactPersons);

  /**
   * @return the contact persons
   */
  Set<ContactPersonModel> getRefContactPersons();

  /**
   * @param refAargauLicense the aargau license
   */
  void setRefAargauLicense(AargauPersonLicenseModel refAargauLicense);

  /**
   * @return the aargau license
   */
  AargauPersonLicenseModel getRefAargauLicense();

  /**
   * @param refMemberGroup the member group
   */
  void setRefMemberGroup(MemberGroupModel refMemberGroup);

  /**
   * @return the member group
   */
  MemberGroupModel getRefMemberGroup();

  /**
   * @param refRank the rank
   */
  void setRefRank(RankModel refRank);

  /**
   * @return the rank
   */
  RankModel getRefRank();

  /**
   * @param refFunctions the functions
   */
  void setRefFunctions(Set<PersonFunctionModel> refFunctions);

  /**
   * @return the functions
   */
  Set<PersonFunctionModel> getRefFunctions();

  /**
   * @param refPrimaryFunction the primary function
   */
  void setRefPrimaryFunction(OrganizationFunctionModel refPrimaryFunction);

  /**
   * @return the primary function
   */
  OrganizationFunctionModel getRefPrimaryFunction();

  /**
   * @param refCompletedCourses the completed courses
   */
  void setRefCompletedCourses(Set<TrainingCourseModel> refCompletedCourses);

  /**
   * @return the completed courses
   */
  Set<TrainingCourseModel> getRefCompletedCourses();

  /**
   * @param refMedicalInformation the medical information
   */
  void setRefMedicalInformation(MedicalInformationModel refMedicalInformation);

  /**
   * @return the medical information
   */
  MedicalInformationModel getRefMedicalInformation();

  /**
   * @param refAuthorizationKey the authorisations
   */
  void setRefAuthorizationKey(AuthorizationKeyModel refAuthorizationKey);

  /**
   * @return the authorsations
   */
  AuthorizationKeyModel getRefAuthorizationKey();

}
