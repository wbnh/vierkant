package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.vknws.vierkant.type.impl.FunctionGroupPersistable;

/**
 * Model for groups of functions.
 *
 * @author Benjamin Weber
 */
public interface FunctionGroupModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The members of the group.
     */
    public static final String REF_GROUP_MEMBERS = "refGroupMembers";

    /**
     * The group data.
     */
    public static final String REF_DETAILS = "refDetails";

    /**
     * The authorisations of the group.
     */
    public static final String REF_AUTHORIZATION_KEY = "refAuthorizationKey";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : FunctionGroupModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(FunctionGroupPersistable.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param refGroupMembers the members
   */
  void setRefGroupMembers(Set<OrganizationFunctionModel> refGroupMembers);

  /**
   * @return the members
   */
  Set<OrganizationFunctionModel> getRefGroupMembers();

  /**
   * @param refDetails the details
   */
  void setRefDetails(OrganizationFunctionModel refDetails);

  /**
   * @return the details
   */
  OrganizationFunctionModel getRefDetails();

  /**
   * @param refAuthorizationKey the authorisation
   */
  void setRefAuthorizationKey(AuthorizationKeyModel refAuthorizationKey);

  /**
   * @return the authorisation
   */
  AuthorizationKeyModel getRefAuthorizationKey();

}
