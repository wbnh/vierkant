package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.vknws.vierkant.type.impl.PrerequisitePersistable;

/**
 * Model for prerequisites.
 *
 * @author Benjamin Weber
 */
public interface PrerequisiteModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The > relation.
     */
    public static final String GT = "gt";

    /**
     * The >= relation.
     */
    public static final String GE = "ge";

    /**
     * The < relation.
     */
    public static final String LT = "lt";

    /**
     * The <= relation.
     */
    public static final String LE = "le";

    /**
     * The == relation.
     */
    public static final String EQ = "eq";

    /**
     * The == null check.
     */
    public static final String IS_NULL = "isNull";

    /**
     * The != null check.
     */
    public static final String IS_NOT_NULL = "isNotNull";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : PrerequisiteModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(PrerequisitePersistable.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param gt greater than
   */
  void setGt(Map<String, Integer> gt);

  /**
   * @return greater than
   */
  Map<String, Integer> getGt();

  /**
   * @param ge greater or equals
   */
  void setGe(Map<String, Integer> ge);

  /**
   * @return greater or equals
   */
  Map<String, Integer> getGe();

  /**
   * @param lt less than
   */
  void setLt(Map<String, Integer> lt);

  /**
   * @return less than
   */
  Map<String, Integer> getLt();

  /**
   * @param le less or equal
   */
  void setLe(Map<String, Integer> le);

  /**
   * @return less or equal
   */
  Map<String, Integer> getLe();

  /**
   * @param eq equals
   */
  void setEq(Map<String, String> eq);

  /**
   * @return equals
   */
  Map<String, String> getEq();

  /**
   * @param isNull is null
   */
  void setIsNull(Map<String, String> isNull);

  /**
   * @return is null
   */
  Map<String, String> getIsNull();

  /**
   * @param isNotNull is not null
   */
  void setIsNotNull(Map<String, String> isNotNull);

  /**
   * @return is not null
   */
  Map<String, String> getIsNotNull();
}
