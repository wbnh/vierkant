package ch.vknws.vierkant.type.caption;

/**
 * Texts.
 * 
 * @author benjamin
 */
public interface ProcessTaskPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return text
   */
  String refTask();

  /**
   * @return text
   */
  String refNextTasks();
}
