package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.MeetingModel;
import ch.vknws.vierkant.type.MeetingTypeModel;
import ch.vknws.vierkant.type.MissExcuseModel;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.RecordModel;

/**
 * Implementation of the meeting.
 * 
 * @author benjamin
 */
@Entity
@Audited
public class MeetingPersistable extends AbstractDatabasePersistable implements MeetingModel {

  private static final long serialVersionUID = 1404856569744204797L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
  @Fetch(FetchMode.JOIN)
  @JoinColumn(name = "refMeetingTypeRecordId")
  private MeetingTypePersistable refMeetingType;

  @Attribute
  @Column
  private String meetingName;

  @Attribute
  @Column
  private String description;

  @Attribute
  @Column
  private LocalDateTime start;

  @Attribute
  @Column
  private LocalDateTime end;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refResponsibleRecordId")
  private PersonPersistable refResponsible;

  @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinTable(name = "MeetingPlannedAttendeeList", joinColumns = @JoinColumn(name = "refMeetingRecordId"), inverseJoinColumns = @JoinColumn(name = "refPlannedAttendeeRecordId"))
  private Set<PersonPersistable> refPlannedAttendees;

  @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinTable(name = "MeetingActualAttendeeList", joinColumns = @JoinColumn(name = "refMeetingRecordId"), inverseJoinColumns = @JoinColumn(name = "refActualAttendeeRecordId"))
  private Set<PersonPersistable> refActualAttendees;

  @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinTable(name = "MeetingMissExcuseList", joinColumns = @JoinColumn(name = "refMeetingRecordId"), inverseJoinColumns = @JoinColumn(name = "refMissExcuseRecordId"))
  private Set<MissExcusePersistable> refMissExcuses;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refRecordRecordId")
  private RecordPersistable refRecord;

  @Override
  public void setRefMeetingType(MeetingTypeModel refMeetingType) {
    this.refMeetingType = refMeetingType == null ? null : (MeetingTypePersistable) refMeetingType;
  }

  @Override
  public MeetingTypeModel getRefMeetingType() {
    return refMeetingType;
  }

  @Override
  public void setMeetingName(String meetingName) {
    this.meetingName = meetingName;
  }

  @Override
  public String getMeetingName() {
    return meetingName;
  }

  @Override
  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public void setStart(LocalDateTime start) {
    this.start = start;
  }

  @Override
  public LocalDateTime getStart() {
    return start;
  }

  @Override
  public void setEnd(LocalDateTime end) {
    this.end = end;
  }

  @Override
  public LocalDateTime getEnd() {
    return end;
  }

  @Override
  public void setRefResponsible(PersonModel refResponsible) {
    this.refResponsible = refResponsible == null ? null : (PersonPersistable) refResponsible;
  }

  @Override
  public PersonModel getRefResponsible() {
    return refResponsible;
  }

  @Override
  public void setRefPlannedAttendees(Set<PersonModel> refPlannedAttendees) {
    this.refPlannedAttendees = refPlannedAttendees == null ? null
        : refPlannedAttendees.stream().map(m -> (PersonPersistable) m).collect(Collectors.toSet());
  }

  @Override
  public Set<PersonModel> getRefPlannedAttendees() {
    return refPlannedAttendees == null ? null
        : refPlannedAttendees.stream().map(p -> (PersonModel) p).collect(Collectors.toSet());
  }

  @Override
  public void setRefActualAttendees(Set<PersonModel> refActualAttendees) {
    this.refActualAttendees = refActualAttendees == null ? null
        : refActualAttendees.stream().map(m -> (PersonPersistable) m).collect(Collectors.toSet());
  }

  @Override
  public Set<PersonModel> getRefActualAttendees() {
    return refActualAttendees == null ? null
        : refActualAttendees.stream().map(p -> (PersonModel) p).collect(Collectors.toSet());
  }

  @Override
  public void setRefMissExcuses(Set<MissExcuseModel> refMissExcuses) {
    this.refMissExcuses = refMissExcuses == null ? null
        : refMissExcuses.stream().map(m -> (MissExcusePersistable) m).collect(Collectors.toSet());
  }

  @Override
  public Set<MissExcuseModel> getRefMissExcuses() {
    return refMissExcuses == null ? null
        : refMissExcuses.stream().map(p -> (MissExcuseModel) p).collect(Collectors.toSet());
  }

  @Override
  public void setRefRecord(RecordModel refRecord) {
    this.refRecord = refRecord == null ? null : (RecordPersistable) refRecord;
  }

  @Override
  public RecordModel getRefRecord() {
    return refRecord;
  }

  @Override
  public Integer getRecordId() {
    return recordId;
  }

  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  @Override
  public Integer getCreator() {
    return creator;
  }

  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  @Override
  public Integer getModifier() {
    return modifier;
  }

  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  @Override
  public String getAuditComment() {
    return auditComment;
  }

  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

  @Override
  public String toString() {
    return meetingName;
  }

}
