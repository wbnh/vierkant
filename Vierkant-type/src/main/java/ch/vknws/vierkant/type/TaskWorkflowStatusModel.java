package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * A status in a workflow.
 * 
 * @author benjamin
 */
public interface TaskWorkflowStatusModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The underlying status.
     */
    public static final String REF_STATUS = "refStatus";

    /**
     * The next statuses in the workflow.
     */
    public static final String REF_NEXT_STATUSES = "refNextStatuses";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : TaskWorkflowStatusModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(TaskWorkflowStatusModel.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param refStatus the underlying status.
   */
  void setRefStatus(TaskStatusModel refStatus);

  /**
   * @return the underlying status.
   */
  TaskStatusModel getRefStatus();

  /**
   * @param refNextStatuses the next statuses in the workflow.
   */
  void setRefNextStatuses(Set<TaskWorkflowStatusModel> refNextStatuses);

  /**
   * @return the next statuses in the workflow.
   */
  Set<TaskWorkflowStatusModel> getRefNextStatuses();

}
