package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.PersonModel;
import ch.vknws.vierkant.type.VoteEntryModel;
import ch.vknws.vierkant.type.VoteOptionGroupModel;
import ch.vknws.vierkant.type.VoteResultModel;

/**
 * Captions for the vote result.
 * 
 * @author benjamin
 */
@Entity
@Audited
public class VoteResultPersistable extends AbstractDatabasePersistable implements VoteResultModel {

  private static final long serialVersionUID = -4570510071124344539L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @Column
  private LocalDateTime pollStart;

  @Attribute
  @Column
  private LocalDateTime pollEnd;

  @Attribute
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "refVoteOptionGroupRecordId")
  private VoteOptionGroupPersistable refVoteOptionGroup;

  @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinTable(name = "VoteResultVoterList", joinColumns = @JoinColumn(name = "refVoteResultRecordId"), inverseJoinColumns = @JoinColumn(name = "refVoterRecordId"))
  private Set<PersonPersistable> refVoters;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "refVoteResult")
  private Set<VoteEntryPersistable> refVoteEntries;

  @Attribute
  @Column
  private String checksum;

  @Override
  public void setPollStart(LocalDateTime pollStart) {
    this.pollStart = pollStart;
  }

  @Override
  public LocalDateTime getPollStart() {
    return pollStart;
  }

  @Override
  public void setPollEnd(LocalDateTime pollEnd) {
    this.pollEnd = pollEnd;
  }

  @Override
  public LocalDateTime getPollEnd() {
    return pollEnd;
  }

  @Override
  public void setRefVoteOptionGroup(VoteOptionGroupModel refVoteOptionGroup) {
    if (refVoteOptionGroup == null) {
      this.refVoteOptionGroup = null;
    } else {
      this.refVoteOptionGroup = (VoteOptionGroupPersistable) refVoteOptionGroup;
    }
  }

  @Override
  public VoteOptionGroupModel getRefVoteOptionGroup() {
    return refVoteOptionGroup;
  }

  @Override
  public void setRefVoters(Set<PersonModel> refVoters) {
    this.refVoters = refVoters == null ? null
        : refVoters.stream().map(m -> (PersonPersistable) m).collect(Collectors.toSet());
  }

  @Override
  public Set<PersonModel> getRefVoters() {
    return refVoters == null ? null : refVoters.stream().map(p -> (PersonModel) p).collect(Collectors.toSet());
  }

  @Override
  public void setRefVoteEntries(Set<VoteEntryModel> refVoteEntries) {
    this.refVoteEntries = refVoteEntries == null ? null
        : refVoteEntries.stream().map(m -> (VoteEntryPersistable) m).collect(Collectors.toSet());
  }

  @Override
  public Set<VoteEntryModel> getRefVoteEntries() {
    return refVoteEntries == null ? null
        : refVoteEntries.stream().map(p -> (VoteEntryModel) p).collect(Collectors.toSet());
  }

  @Override
  public void setChecksum(String checksum) {
    this.checksum = checksum;
  }

  @Override
  public String getChecksum() {
    return checksum;
  }

  @Override
  public Integer getRecordId() {
    return recordId;
  }

  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  @Override
  public Integer getCreator() {
    return creator;
  }

  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  @Override
  public Integer getModifier() {
    return modifier;
  }

  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  @Override
  public String getAuditComment() {
    return auditComment;
  }

  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

}
