package ch.vknws.vierkant.type.caption;

/**
 * Captions for the medical information.
 *
 * @author Benjamin Weber
 *
 */
public interface MedicalInformationPersistableTexts {

  /**
   * @return the text
   */
	String allergies();

  /**
   * @return the text
   */
	String classifiedConditions();

  /**
   * @return the text
   */
	String comment();

  /**
   * @return the text
   */
	String classifiedComment();
}
