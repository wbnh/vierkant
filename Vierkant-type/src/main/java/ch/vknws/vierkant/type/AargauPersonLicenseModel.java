package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.database.DescriptiveName;
import ch.vknws.vierkant.type.impl.AargauPersonLicensePersistable;

/**
 * Reference model for persons to an aargau license.
 *
 * @author Benjamin Weber
 */
@DescriptiveName(AargauPersonLicenseModel.PropertyId.INVOICE_NUMBER)
public interface AargauPersonLicenseModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The number of the invoice for the associated license.
     */
    public static final String INVOICE_NUMBER = "invoiceNumber";

    /**
     * The date until which the association is valid.
     */
    public static final String VALIDITY_END_DATE = "validityEndDate";

    /**
     * The date when the person was registered.
     */
    public static final String REGISTRATION_DATE = "registrationDate";

    /**
     * The associated license.
     */
    public static final String REF_LICENSE = "refLicense";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {

      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : AargauPersonLicenseModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(AargauPersonLicensePersistable.class,
                "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param invoiceNumber the new invoice number
   */
  void setInvoiceNumber(String invoiceNumber);

  /**
   * @return the current invoice number
   */
  String getInvoiceNumber();

  /**
   * @param validityEndDate the new date
   */
  void setValidityEndDate(LocalDate validityEndDate);

  /**
   * @return the date
   */
  LocalDate getValidityEndDate();

  /**
   * @param registrationDate the new date
   */
  void setRegistrationDate(LocalDate registrationDate);

  /**
   * @return the registration date
   */
  LocalDate getRegistrationDate();

  /**
   * @param refLicense the new license
   */
  void setRefLicense(AargauLicenseModel refLicense);

  /**
   * @return the license
   */
  AargauLicenseModel getRefLicense();

}
