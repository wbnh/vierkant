package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import ch.bwe.fac.v1.type.Attribute;

/**
 * The audit fields to be embedded in persistables.
 *
 * @author Benjamin Weber
 */
@Embeddable
public class AuditFields {

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Column
  private Integer revtype;

  /**
   * Returns the ID of the person who created the model.
   * 
   * @return the creator. Null if unsaved, special values apply.
   */
  public Integer getCreator() {
    return creator;
  }

  /**
   * Sets the ID of the person who created the model.
   * 
   * @param creator the creator
   */
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  /**
   * Returns the time the model was first saved on the DB.
   * 
   * @return the original save time. Null if unsaved.
   */
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  /**
   * Sets the time the model was first saved on the DB.
   * 
   * @param creationDateTime the time
   */
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  /**
   * Returns the ID of the person who last saved the model.
   * 
   * @return the ID. Null if unsaved. Special values apply.
   */
  public Integer getModifier() {
    return modifier;
  }

  /**
   * Sets the ID of the person who last saved the model.
   * 
   * @param modifier the ID
   */
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  /**
   * Returns the time the model was last saved.
   * 
   * @return the time. Null if unsaved.
   */
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  /**
   * Sets the time the model was last saved.
   * 
   * @param modificationDateTime the time
   */
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  /**
   * Returns the comment for the last modification.
   * 
   * @return the comment. Can be null depending on configuration.
   */
  public String getAuditComment() {
    return auditComment;
  }

  /**
   * Sets the comment for the last modification.
   * 
   * @param auditComment the comment
   */
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

}
