package ch.vknws.vierkant.type.caption;

/**
 * Captions for the training course instance.
 *
 * @author Benjamin Weber
 */
public interface TrainingCourseInstancePersistableTexts extends AbstractPersistableTexts {

  /**
   * @return the text
   */
  String template();

  /**
   * @return the text
   */
  String responsible();

  /**
   * @return the text
   */
  String teachers();

  /**
   * @return the text
   */
  String participants();
}
