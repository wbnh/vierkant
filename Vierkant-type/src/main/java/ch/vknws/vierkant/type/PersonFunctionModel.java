package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.vknws.vierkant.type.impl.PersonFunctionPersistable;

/**
 * Model to link persons with their functions.
 *
 * @author Benjamin Weber
 */
public interface PersonFunctionModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The comment of the superior person.
     */
    public static final String SUPERVISOR_COMMENT = "supervisorComment";

    /**
     * The referenced function.
     */
    public static final String REF_FUNCTION = "refFunction";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : PersonFunctionModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(PersonFunctionPersistable.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param supervisorComment the comment
   */
  void setSupervisorComment(String supervisorComment);

  /**
   * @return the comment
   */
  String getSupervisorComment();

  /**
   * @param refFunction the function
   */
  void setRefFunction(OrganizationFunctionModel refFunction);

  /**
   * @return the function
   */
  OrganizationFunctionModel getRefFunction();

}
