package ch.vknws.vierkant.type.caption;

/**
 * Captions for the FunctionModel.
 *
 * @author Benjamin Weber
 */
public interface OrganizationFunctionPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return the text
   */
  String functionName();

  /**
   * @return the text
   */
  String email();

  /**
   * @return the text
   */
  String maxPersonCount();

  /**
   * @return the text
   */
  String minPersonCount();

  /**
   * @return the text
   */
  String type();

  /**
   * @return the text
   */
  String superiors();

  /**
   * @return the text
   */
  String authorizationKey();

  /**
   * @return the text
   */
  String functionGroup();
}
