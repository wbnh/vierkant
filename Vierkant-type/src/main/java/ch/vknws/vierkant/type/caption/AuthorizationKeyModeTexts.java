package ch.vknws.vierkant.type.caption;

/**
 * Texts for enum.
 *
 * @author Benjamin Weber
 */
public interface AuthorizationKeyModeTexts {

  /**
   * @return the text
   */
  String READ();

  /**
   * @return the text
   */
  String WRITE();
}
