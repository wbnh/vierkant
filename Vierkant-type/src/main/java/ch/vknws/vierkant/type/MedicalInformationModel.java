package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.vknws.vierkant.type.impl.MedicalInformationPersistable;

/**
 * Model for a person's medical information.
 *
 * @author Benjamin Weber
 */
public interface MedicalInformationModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * Allergy information.
     */
    public static final String ALLERGIES = "allergies";

    /**
     * Conditions which must be treated with special discretion.
     */
    public static final String CLASSIFIED_CONDITIONS = "classifiedConditions";

    /**
     * Comment to allergies etc.
     */
    public static final String COMMENT = "comment";

    /**
     * Comment to be treated with special discretion.
     */
    public static final String CLASSIFIED_COMMENT = "classifiedComment";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : MedicalInformationModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(MedicalInformationPersistable.class, "Could not initialise field names",
                e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param allergies the allergies
   */
  void setAllergies(String allergies);

  /**
   * @return the allergies
   */
  String getAllergies();

  /**
   * @param classifiedConditions the classified conditions
   */
  void setClassifiedConditions(String classifiedConditions);

  /**
   * @return the classified conditions
   */
  String getClassifiedConditions();

  /**
   * @param comment the comment
   */
  void setComment(String comment);

  /**
   * @return the comment
   */
  String getComment();

  /**
   * @param classifiedComment the classified comment
   */
  void setClassifiedComment(String classifiedComment);

  /**
   * @return the classified comment
   */
  String getClassifiedComment();

}
