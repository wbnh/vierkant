package ch.vknws.vierkant.type.caption;

/**
 * Captions for RankCategory.
 *
 * @author Benjamin Weber
 */
public interface RankCategoryTexts {

  /**
   * @return the text
   */
  String ASPIRANT();

  /**
   * @return the text
   */
  String ENLISTED();

  /**
   * @return the text
   */
  String NC_OFFICERS();

  /**
   * @return the text
   */
  String LOWER_NC_OFFICER();

  /**
   * @return the text
   */
  String UPPER_NC_OFFICER();

  /**
   * @return the text
   */
  String OFFICERS();

  /**
   * @return the text
   */
  String SUBALTERN_OFFICER();

  /**
   * @return the text
   */
  String STAFF_OFFICER();
}
