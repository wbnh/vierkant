package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * Written for events.
 * 
 * @author benjamin
 */
public interface RecordModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The classification for this record.
     */
    public static final String CLASSIFICATION = "classification";

    /**
     * The tractanda that were planned to be discussed.
     */
    public static final String REF_PLANNED_TRACTANDA = "refPlannedTractanda";

    /**
     * The tractanda that were actually discussed.
     */
    public static final String REF_ACTUAL_TRACTANDA = "refActualTractanda";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : RecordModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(RecordModel.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param classification the classification
   */
  void setClassification(ClassificationLevel classification);

  /**
   * @return the classification
   */
  ClassificationLevel getClassification();

  /**
   * @param refPlannedTractanda the new tractanda
   */
  void setRefPlannedTractanda(Set<TractandumModel> refPlannedTractanda);

  /**
   * @return the tractanda to discuss
   */
  Set<TractandumModel> getRefPlannedTractanda();

  /**
   * @param refActualTractanda the new tractanda
   */
  void setRefActualTractanda(Set<TractandumModel> refActualTractanda);

  /**
   * @return the tractanda that were discussed
   */
  Set<TractandumModel> getRefActualTractanda();

}
