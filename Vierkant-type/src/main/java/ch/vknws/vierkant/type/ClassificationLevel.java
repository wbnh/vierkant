package ch.vknws.vierkant.type;

/**
 * The different levels of classification.
 * 
 * @author benjamin
 */
public enum ClassificationLevel {

  /**
   * Everyone can see this.
   */
  NON_CLASSIFIED,

  /**
   * Only members can see this.
   */
  MEMBERS_ONLY,

  /**
   * Only non-aspirant members can see this.
   */
  DEFINITIVE_MEMBERS_ONLY,

  /**
   * Only non-commissioned and commissioned officers and the board of directors
   * can see this.
   */
  OFFICERS_ONLY,

  /**
   * Only commissioned officers and the board of directors can see this.
   */
  COMMISSIONED_OFFICERS_ONLY,

  /**
   * Only the board of directors can see this.
   */
  BOARD_OF_DIRECTORS_ONLY;
}
