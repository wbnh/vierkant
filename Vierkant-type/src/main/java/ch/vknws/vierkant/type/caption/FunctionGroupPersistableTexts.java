package ch.vknws.vierkant.type.caption;

/**
 * Texts for model.
 *
 * @author Benjamin Weber
 */
public interface FunctionGroupPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return the text
   */
  String groupMembers();

  /**
   * @return the text
   */
  String details();

  /**
   * @return text
   */
  String authorizationKey();
}
