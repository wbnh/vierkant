package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.database.DescriptiveName;
import ch.vknws.vierkant.type.impl.AuthorizationKeyPersistable;

/**
 * Model for authorisation in the application.
 *
 * @author Benjamin Weber
 */
@DescriptiveName(AuthorizationKeyModel.PropertyId.KEY_NAME)
public interface AuthorizationKeyModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The name of the authorization.
     */
    public static final String KEY_NAME = "keyName";

    /**
     * The mode.
     */
    public static final String MODE = "mode";

    /**
     * The type.
     */
    public static final String TYPE = "type";

    /**
     * The sub-authorizations.
     */
    public static final String REF_SUBORDINATES = "refSubordinates";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : AuthorizationKeyModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(AuthorizationKeyPersistable.class, "Could not initialise field names",
                e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param keyName the key name
   */
  void setKeyName(String keyName);

  /**
   * @return the key name
   */
  String getKeyName();

  /**
   * @param mode the mode
   */
  void setMode(AuthorizationKeyMode mode);

  /**
   * @return the mode
   */
  AuthorizationKeyMode getMode();

  /**
   * @param type the type
   */
  void setType(AuthorizationKeyType type);

  /**
   * @return the type
   */
  AuthorizationKeyType getType();

  /**
   * @param refSubordinates the sub-authorisations
   */
  void setRefSubordinates(Set<AuthorizationKeyModel> refSubordinates);

  /**
   * @return the sub-authorisations
   */
  Set<AuthorizationKeyModel> getRefSubordinates();
}
