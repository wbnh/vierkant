package ch.vknws.vierkant.type.caption;

/**
 * Texts for model.
 *
 * @author Benjamin Weber
 */
public interface PersonFunctionPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return the text
   */
  String supervisorComment();

  /**
   * @return the text
   */
  String refFunction();
}
