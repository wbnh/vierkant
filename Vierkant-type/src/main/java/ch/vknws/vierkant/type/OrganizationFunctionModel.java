package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.database.DescriptiveName;
import ch.vknws.vierkant.type.impl.OrganizationFunctionPersistable;

/**
 * Model for functions within the organization.
 *
 * @author Benjamin Weber
 */
@DescriptiveName(OrganizationFunctionModel.PropertyId.FUNCTION_NAME)
public interface OrganizationFunctionModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The name of the function.
     */
    public static final String FUNCTION_NAME = "functionName";

    /**
     * The function email address.
     */
    public static final String EMAIL = "email";

    /**
     * The max number of people who can have this function.
     */
    public static final String MAX_PERSON_COUNT = "maxPersonCount";

    /**
     * The min number of people who need to have this function.
     */
    public static final String MIN_PERSON_COUNT = "minPersonCount";

    /**
     * The function type.
     */
    public static final String TYPE = "type";

    /**
     * The function's superiors.
     */
    public static final String REF_SUPERIORS = "refSuperiors";

    /**
     * The authorisations of this function.
     */
    public static final String REF_AUTHORIZATION_KEY = "refAuthorizationKey";

    /**
     * The groups this function is in.
     */
    public static final String REF_GROUPS = "refGroups";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : OrganizationFunctionModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(OrganizationFunctionPersistable.class,
                "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param functionName the name
   */
  void setFunctionName(String functionName);

  /**
   * @return the name
   */
  String getFunctionName();

  /**
   * @param email the email
   */
  void setEmail(String email);

  /**
   * @return the email
   */
  String getEmail();

  /**
   * @param maxPersonCount the max person count
   */
  void setMaxPersonCount(Integer maxPersonCount);

  /**
   * @return the max person count
   */
  Integer getMaxPersonCount();

  /**
   * @param minPersonCount the min person count
   */
  void setMinPersonCount(Integer minPersonCount);

  /**
   * @return the min person count
   */
  Integer getMinPersonCount();

  /**
   * @param type the type
   */
  void setType(FunctionType type);

  /**
   * @return the type
   */
  FunctionType getType();

  /**
   * @param refSuperiors the superiors
   */
  void setRefSuperiors(Set<OrganizationFunctionModel> refSuperiors);

  /**
   * @return the superiors
   */
  Set<OrganizationFunctionModel> getRefSuperiors();

  /**
   * @param refAuthorizationKey the authorisations
   */
  void setRefAuthorizationKey(AuthorizationKeyModel refAuthorizationKey);

  /**
   * @return the authorisations
   */
  AuthorizationKeyModel getRefAuthorizationKey();

  /**
   * @param refGroups the groups
   */
  void setRefGroups(Set<FunctionGroupModel> refGroups);

  /**
   * @return the groups
   */
  Set<FunctionGroupModel> getRefGroups();

}
