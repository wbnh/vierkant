package ch.vknws.vierkant.type;

/**
 * Different types of a function.
 *
 * @author Benjamin Weber
 */
public enum FunctionType {

  /**
   * A normal function.
   */
  NORMAL,

  /**
   * Staff to another function.
   */
  STAFF,

  /**
   * Substitute of another function.
   */
  SUBSTITUTE;
}
