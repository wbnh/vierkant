package ch.vknws.vierkant.type.caption;

/**
 * Texts.
 * 
 * @author benjamin
 */
public interface TaskCustomFieldInstancePersistableTexts extends AbstractPersistableTexts {

  /**
   * @return text
   */
  String defaultValue();

  /**
   * @return text
   */
  String readonly();

  /**
   * @return text
   */
  String required();

  /**
   * @return text
   */
  String refCustomField();
}
