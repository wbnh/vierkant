package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * Specific instance/execution of a task.
 * 
 * @author benjamin
 */
public interface TaskInstanceModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The person this task is assigned to.
     */
    public static final String REF_ASSIGNEE = "refAssignee";

    /**
     * The persons watching this task. They are notified of changes.
     */
    public static final String REF_WATCHERS = "refWatchers";

    /**
     * The comments on this task.
     */
    public static final String REF_COMMENTS = "refComments";

    /**
     * The template for the task.
     */
    public static final String REF_TEMPLATE = "refTemplate";

    /**
     * The values of the custom fields.
     */
    public static final String REF_CUSTOM_FIELD_VALUES = "refCustomFieldValues";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : TaskInstanceModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(TaskInstanceModel.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param refAssignee the person assigned to this task.
   */
  void setRefAssignee(PersonModel refAssignee);

  /**
   * @return the person assigned to this task.
   */
  PersonModel getRefAssignee();

  /**
   * @param refWatchers the persons watching this task.
   */
  void setRefWatchers(Set<PersonModel> refWatchers);

  /**
   * @return the persons watching this task.
   */
  Set<PersonModel> getRefWatchers();

  /**
   * @param refComments the comments on this task.
   */
  void setRefComments(Set<TaskInstanceCommentModel> refComments);

  /**
   * @return the comments on this task.
   */
  Set<TaskInstanceCommentModel> getRefComments();

  /**
   * @param refTemplate the underlying task.
   */
  void setRefTemplate(TaskModel refTemplate);

  /**
   * @return the underlying task.
   */
  TaskModel getRefTemplate();

  /**
   * @param refCustomFieldValues the custom field values.
   */
  void setRefCustomFieldValues(Set<TaskCustomFieldValueModel> refCustomFieldValues);

  /**
   * @return the custom field values.
   */
  Set<TaskCustomFieldValueModel> getRefCustomFieldValues();

}
