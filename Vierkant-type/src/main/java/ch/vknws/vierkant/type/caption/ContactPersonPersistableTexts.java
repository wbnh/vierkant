package ch.vknws.vierkant.type.caption;

/**
 * Captions for the ContactPersonModel.
 *
 * @author Benjamin Weber
 */
public interface ContactPersonPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return the text
   */
  String relationship();

  /**
   * @return the text
   */
  String refPerson();
}
