package ch.vknws.vierkant.type;

/**
 * Different behaviours of a task.
 * @author benjamin
 *
 */
public enum TaskBehaviorType {

  /**
   * No special behaviour. The user needs to do everything by himself.<br>
   * Required Custom fields: none
   */
  NONE,
  
  /**
   * Specified people need to approve the continuation of the process.<br>
   * Required Custom fields:
   * <ul>
   * <li>Approvers (PersonModel to Boolean)</li>
   * <li>ApprovalQuestion (String)</li>
   * <li>MinApprovalCount (Integer)</li>
   * <li>RequiredApprovals (Integer to PersonModel/OrganizationFunctionModel/FunctionGroupModel)</li>
   * <li>RejectionBehavior (String to String: [FinishProcess, null], [ContinueWithTasks, TaskIds])</li>
   * <li>ApprovalBehavior (String to String: [ContinueWithTasks, TaskIds])</li>
   * </ul>
   */
  APPROVAL,

  /**
   * The assignee needs to decide which path we should continue on.<br>
   * Required Custom fields:
   * <ul>
   * <li>DecisionOptions (String to String: [&lt;option name&gt;, TaskIds])</li>
   * </ul>
   */
  DECISION,
  
  /**
   * Wait for all the listed tasks need to be finished.<br>
   * Required Custom fields:
   * <ul>
   * <li>JoinTasks (Integer to Integer: [&lt;index&gt;, TaskId])</li>
   * </ul>
   */
  JOIN,
  
  /**
   * Special task without a process to function as a todo list.<br>
   * Required Custom fields: none
   * 
   */
  TODO,
  
  /**
   * The system automatically updates fields on models.<br>
   * Required Custom fields:
   * <ul>
   * <li>UpdateFields (Model to Any)</li>
   * </ul>
   */
  AUTO_UPDATE_FIELD,
  
  /**
   * The system automatically adds to fields on models.<br>
   * Required Custom fields:
   * <ul>
   * <li>UpdateFields (Model to Any)</li>
   * </ul>
   */
  AUTO_ADD_TO_FIELD,

  
  /**
   * The system automatically removes from fields on models.<br>
   * Required Custom fields:
   * <ul>
   * <li>UpdateFields (Model to Any)</li>
   * </ul>
   */
  AUTO_REMOVE_FROM_FIELD,
  
  /**
   * The system automatically notifies persons.<br>
   * Required Custom fields:
   * <ul>
   * <li>NotifyProcessWatchers (Boolean)</li>
   * <li>Notified (Integer to PersonModel/OrganizationFunctionModel/FunctionGroupModel)</li>
   * </ul>
   */
  AUTO_NOTIFY,
  
  /**
   * The system fires an event to be processed.<br>
   * Required Custom fields:
   * <ul>
   * <li>EventName (String)</li>
   * <li>EventParameters (String to Any)</li>
   * </ul>
   */
  AUTO_FIRE_EVENT,
  
  /**
   * The system waits for an event to be thrown and potentially saves its parameters to process variables.<br>
   * Required Custom fields:
   * <ul>
   * <li>EventNames (Integer to String)</li>
   * <li>SaveParametersToVariable (String to Process Variable)</li>
   * </ul>
   */
  AUTO_LISTEN_FOR_EVENT,
  
  /**
   * The system performs a REST call.<br>
   * Required Custom fields:
   * <ul>
   * <li>RestMethod (String: GET/POST/PUT/DELETE)</li>
   * <li>RestUrl (String)</li>
   * <li>RestAuthorization (String)</li>
   * <li>RestBody (Calculated)</li>
   * </ul>
   */
  AUTO_REST_CALL,
  
  /**
   * The system triggers another process.<br>
   * Required Custom fields:
   * <ul>
   * <li>ProcessToExecute (ProcessModel)</li>
   * <li>ProcessVariables (ProcessVariable to Any)</li>
   * </ul>
   */
  AUTO_TRIGGER_PROCESS,
  
  /**
   * The system waits until a time or for a duration and continues.<br>
   * Required Custom fields:
   * <ul>
   * <li>WaitUntil (Datetime)</li>
   * <li>WaitFor (String)</li>
   * </ul>
   */
  AUTO_WAIT,
  
  /**
   * The system decides based on a calculation.<br>
   * Required Custom fields:
   * <ul>
   * <li>DecisionCalculation (Calculated)</li>
   * </ul>
   */
  AUTO_DECISION
}
