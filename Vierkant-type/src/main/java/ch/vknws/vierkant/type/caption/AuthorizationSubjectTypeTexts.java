package ch.vknws.vierkant.type.caption;

/**
 * Texts for enum.
 *
 * @author Benjamin Weber
 */
public interface AuthorizationSubjectTypeTexts {

  /**
   * @return the text
   */
  String FUNCTION();

  /**
   * @return the text
   */
  String FUNCTION_GROUP();

  /**
   * @return the text
   */
  String RANK();

  /**
   * @return the text
   */
  String PERSON();
}
