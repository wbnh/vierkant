package ch.vknws.vierkant.type.impl;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;
import ch.vknws.vierkant.type.ClassificationLevel;
import ch.vknws.vierkant.type.ProcessModel;
import ch.vknws.vierkant.type.ProcessTaskModel;
import ch.vknws.vierkant.type.TaskCustomFieldModel;

/**
 * @author benjamin
 */
public class ProcessPersistable extends AbstractDatabasePersistable implements ProcessModel {

  private static final long serialVersionUID = 4492092447522678320L;

  @Attribute
  @Identifier
  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer recordId;

  @Attribute
  @HistoryIdentifier
  @Column
  @Version
  private Integer revisionId;

  @Attribute
  @Column
  private Integer deletedFlag;

  @Attribute
  @Column
  private Integer creator;

  @Attribute
  @Column
  @CreationTimestamp
  private LocalDateTime creationDateTime;

  @Attribute
  @Column
  private Integer modifier;

  @Attribute
  @Column
  @UpdateTimestamp
  private LocalDateTime modificationDateTime;

  @Attribute
  @Column
  private String auditComment;

  @Attribute
  @Transient
  private Integer revtype;

  @Attribute
  @Column
  private String processName;

  @Attribute
  @Column
  private String description;

  @Attribute
  @Column
  private Integer allowParallelExecution;

  @Attribute
  @Column
  private Integer enabled;

  @Attribute
  @Column
  private String classification;

  @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
  @Fetch(FetchMode.SELECT)
  @JoinTable(name = "ProcessTaskList", joinColumns = {
      @JoinColumn(name = "refProcessRecordId") }, inverseJoinColumns = { @JoinColumn(name = "refTaskRecordId") })
  private Set<ProcessTaskPersistable> refTasks;

  @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
  @Fetch(FetchMode.SELECT)
  @JoinTable(name = "ProcessVariableList", joinColumns = {
      @JoinColumn(name = "refProcessRecordId") }, inverseJoinColumns = { @JoinColumn(name = "refVariableRecordId") })
  private Set<TaskCustomFieldPersistable> refProcessVariables;

  @Override
  public void setProcessName(String processName) {
    this.processName = processName;
  }

  @Override
  public String getProcessName() {
    return processName;
  }

  @Override
  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public void setAllowParallelExecution(Boolean allowParallelExecution) {
    this.allowParallelExecution = booleanToInteger(allowParallelExecution);
  }

  @Override
  public Boolean getAllowParallelExecution() {
    return integerToBoolean(allowParallelExecution);
  }

  @Override
  public void setEnabled(Boolean enabled) {
    this.enabled = booleanToInteger(enabled);
  }

  @Override
  public Boolean isEnabled() {
    return integerToBoolean(enabled);
  }

  @Override
  public void setClassification(ClassificationLevel classification) {
    if (classification == null) {
      this.classification = null;
    } else {
      this.classification = classification.name();
    }
  }

  @Override
  public ClassificationLevel getClassification() {
    if (classification == null) { return null; }
    return ClassificationLevel.valueOf(classification);
  }

  @Override
  public void setRefTasks(Set<ProcessTaskModel> refTasks) {
    if (refTasks == null) {
      this.refTasks = null;
    } else {
      Set<ProcessTaskPersistable> persistables = new HashSet<>();
      for (var model : refTasks) {
        persistables.add((ProcessTaskPersistable) model);
      }
      this.refTasks = persistables;
    }
  }

  @Override
  public Set<ProcessTaskModel> getRefTasks() {
    if (refTasks == null) {
      refTasks = new HashSet<>();
    }
    Set<ProcessTaskModel> models = new HashSet<>();
    for (var persistable : refTasks) {
      models.add(persistable);
    }
    return models;
  }

  @Override
  public void setRefProcessVariables(Set<TaskCustomFieldModel> refProcessVariables) {
    if (refProcessVariables == null) {
      this.refProcessVariables = null;
    } else {
      Set<TaskCustomFieldPersistable> persistables = new HashSet<>();
      for (var model : refProcessVariables) {
        persistables.add((TaskCustomFieldPersistable) model);
      }
      this.refProcessVariables = persistables;
    }
  }

  @Override
  public Set<TaskCustomFieldModel> getRefProcessVariables() {
    if (refProcessVariables == null) {
      refProcessVariables = new HashSet<>();
    }
    Set<TaskCustomFieldModel> models = new HashSet<>();
    for (var persistable : refProcessVariables) {
      models.add(persistable);
    }
    return models;
  }

  @Override
  public Integer getRecordId() {
    return recordId;
  }

  @Override
  public void setRecordId(Integer recordId) {
    this.recordId = recordId;
  }

  @Override
  public Integer getRevisionId() {
    return revisionId;
  }

  @Override
  public void setRevisionId(Integer revisionId) {
    this.revisionId = revisionId;
  }

  @Override
  public Integer getCreator() {
    return creator;
  }

  @Override
  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  @Override
  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  @Override
  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  @Override
  public Integer getModifier() {
    return modifier;
  }

  @Override
  public void setModifier(Integer modifier) {
    this.modifier = modifier;
  }

  @Override
  public LocalDateTime getModificationDateTime() {
    return modificationDateTime;
  }

  @Override
  public void setModificationDateTime(LocalDateTime modificationDateTime) {
    this.modificationDateTime = modificationDateTime;
  }

  @Override
  public boolean getDeletedFlag() {
    return integerToBoolean(deletedFlag);
  }

  @Override
  public void setDeletedFlag(boolean deletedFlag) {
    this.deletedFlag = booleanToInteger(deletedFlag);
  }

  @Override
  public String getAuditComment() {
    return auditComment;
  }

  @Override
  public void setAuditComment(String auditComment) {
    this.auditComment = auditComment;
  }

}
