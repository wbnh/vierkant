package ch.vknws.vierkant.type.caption;

/**
 * Captions for the AargauLicenseModel.
 *
 * @author Benjamin Weber
 */
public interface AargauLicensePersistableTexts extends AbstractPersistableTexts {

  /**
   * @return the text
   */
  String licenseName();

  /**
   * @return the text
   */
  String licenseNumber();
}
