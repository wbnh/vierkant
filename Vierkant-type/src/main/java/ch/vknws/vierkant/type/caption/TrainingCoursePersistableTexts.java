package ch.vknws.vierkant.type.caption;

/**
 * Captions for the training course.
 *
 * @author Benjamin Weber
 */
public interface TrainingCoursePersistableTexts extends AbstractPersistableTexts {

  /**
   * @return the text
   */
  String name();

  /**
   * @return the text
   */
  String personPrerequisite();

  /**
   * @return the text
   */
  String rankPrerequisite();
}
