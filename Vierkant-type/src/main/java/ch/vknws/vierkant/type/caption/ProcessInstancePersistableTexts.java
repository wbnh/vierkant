package ch.vknws.vierkant.type.caption;

/**
 * Texts.
 * 
 * @author benjamin
 */
public interface ProcessInstancePersistableTexts extends AbstractPersistableTexts {

  /**
   * @return text
   */
  String status();

  /**
   * @return text
   */
  String refWatchers();

  /**
   * @return text
   */
  String refTaskInstances();

  /**
   * @return text
   */
  String refTemplate();
}
