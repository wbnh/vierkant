package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * Type to classify meetings.
 * 
 * @author benjamin
 */
public interface VoteResultModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The time the poll started.
     */
    public static final String POLL_START = "pollStart";

    /**
     * The time the poll ended.
     */
    public static final String POLL_END = "pollEnd";

    /**
     * The used option group.
     */
    public static final String REF_VOTE_OPTION_GROUP = "refVoteOptionGroup";

    /**
     * The persons authorised to vote.
     */
    public static final String REF_VOTERS = "refVoters";

    /**
     * The vote result entries.
     */
    public static final String REF_VOTE_ENTRIES = "refVoteEntries";

    /**
     * The checksum.
     */
    public static final String CHECKSUM = "checksum";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : VoteResultModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(VoteResultModel.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param pollStart the new poll start
   */
  void setPollStart(LocalDateTime pollStart);

  /**
   * @return the time the poll opened
   */
  LocalDateTime getPollStart();

  /**
   * @param pollEnd the new poll end
   */
  void setPollEnd(LocalDateTime pollEnd);

  /**
   * @return the time the poll closed
   */
  LocalDateTime getPollEnd();

  /**
   * @param refVoteOptionGroup the new option group
   */
  void setRefVoteOptionGroup(VoteOptionGroupModel refVoteOptionGroup);

  /**
   * @return the options to vote on
   */
  VoteOptionGroupModel getRefVoteOptionGroup();

  /**
   * @param refVoters the new voters
   */
  void setRefVoters(Set<PersonModel> refVoters);

  /**
   * @return the persons voting on the poll
   */
  Set<PersonModel> getRefVoters();

  /**
   * @param refVoteEntries the new vote entries
   */
  void setRefVoteEntries(Set<VoteEntryModel> refVoteEntries);

  /**
   * @return the entries with the results
   */
  Set<VoteEntryModel> getRefVoteEntries();

  /**
   * @param checksum the checksum
   */
  void setChecksum(String checksum);

  /**
   * @return the checksum
   */
  String getChecksum();

}
