package ch.vknws.vierkant.type.caption;

/**
 * Texts for enum.
 *
 * @author Benjamin Weber
 */
public interface AuthorizationKeyTypeTexts {

  /**
   * @return the text
   */
  String NORMAL();

  /**
   * @return the text
   */
  String ALLOW();

  /**
   * @return the text
   */
  String DENY();
}
