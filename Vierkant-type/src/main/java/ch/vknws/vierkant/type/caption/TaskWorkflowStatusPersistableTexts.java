package ch.vknws.vierkant.type.caption;

/**
 * Texts.
 * 
 * @author benjamin
 */
public interface TaskWorkflowStatusPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return the text.
   */
  String refStatus();

  /**
   * @return the text.
   */
  String refNextStatuses();
}
