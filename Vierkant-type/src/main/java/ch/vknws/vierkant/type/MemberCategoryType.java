package ch.vknws.vierkant.type;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.Configuration;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.vknws.vierkant.type.caption.MemberCategoryTypeTexts;

/**
 * The types of members we can have.
 *
 * @author Benjamin Weber
 */
public enum MemberCategoryType {

  /**
   * Non-members interested in joining.
   */
  INTERESTED,

  /**
   * Aspirants.
   */
  ASPIRANT,

  /**
   * Active members.
   */
  ACTIVE,

  /**
   * Single passive members.
   */
  PASSIVE_SINGLE,

  /**
   * Passive member organisations.
   */
  PASSIVE_COLLECTIVE,

  /**
   * Passive family members.
   */
  PASSIVE_FAMILY(true),

  /**
   * Honorary active members.
   */
  HONORARY_ACTIVE,

  /**
   * Honorary passive members.
   */
  HONORARY_PASSIVE;

  private static Configuration texts = ServiceRegistry.getConfigurationProxy()
      .getConfiguration(new ConfigurationProperties<>(MemberCategoryTypeTexts.class));

  private boolean composite;

  /**
   * Constructor handling initialization of mandatory fields.
   */
  private MemberCategoryType(boolean composite) {
    this.composite = composite;
  }

  /**
   * Constructor handling initialization of mandatory fields.
   */
  private MemberCategoryType() {
    this(false);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return texts.getStringValue(name());
  }

  /**
   * @return the composite
   */
  public boolean isComposite() {
    return composite;
  }
}
