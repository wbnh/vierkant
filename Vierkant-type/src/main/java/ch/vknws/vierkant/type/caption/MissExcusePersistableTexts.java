package ch.vknws.vierkant.type.caption;

/**
 * Captions for the miss excuses
 * 
 * @author benjamin
 */
public interface MissExcusePersistableTexts {

  /**
   * @return the text for the description
   */
  String description();

  /**
   * @return the text for the acceptor ID
   */
  String acceptorId();

  /**
   * @return the text for the missing person
   */
  String refMissingPerson();
}
