package ch.vknws.vierkant.type;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.Configuration;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.vknws.vierkant.type.caption.RankCategoryTexts;

/**
 * A category to which a rank can belong.
 *
 * @author Benjamin Weber
 */
public enum RankCategory {

  /**
   * Asp.
   */
  ASPIRANT,

  /**
   * VK, Gfr, Obgfr.
   */
  ENLISTED,

  /**
   * Kpl, Wm.
   */
  LOWER_NC_OFFICER,

  /**
   * Fw, Hptfw, Adj Uof, Stabsadj, Hptadj.
   */
  UPPER_NC_OFFICER,

  /**
   * Lt, Oblt.
   */
  SUBALTERN_OFFICER,

  /**
   * Hptm, Maj, Oberstlt, Oberst.
   */
  STAFF_OFFICER;

  /**
   * Non-commissioned officers.
   */
  public static final RankCategory[] NC_OFFICERS = { RankCategory.LOWER_NC_OFFICER, RankCategory.UPPER_NC_OFFICER };

  /**
   * Officers.
   */
  public static final RankCategory[] OFFICERS = { RankCategory.SUBALTERN_OFFICER, RankCategory.STAFF_OFFICER };

  private static Configuration texts = ServiceRegistry.getConfigurationProxy()
      .getConfiguration(new ConfigurationProperties<>(RankCategoryTexts.class));

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return texts.getStringValue(name());
  }
}
