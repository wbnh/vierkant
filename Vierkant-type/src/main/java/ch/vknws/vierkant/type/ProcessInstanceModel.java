package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;

/**
 * A specific instance of the process.
 * 
 * @author benjamin
 */
public interface ProcessInstanceModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The status of the process.
     */
    public static final String STATUS = "status";

    /**
     * The persons watching this process. They are notified of changes.
     */
    public static final String REF_WATCHERS = "refWatchers";

    /**
     * The specific task instances in the process instance.
     */
    public static final String REF_TASK_INSTANCES = "refTaskInstances";

    /**
     * The underlying process.
     */
    public static final String REF_TEMPLATE = "refTemplate";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {
      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : ProcessInstanceModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(ProcessInstanceModel.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param status the status.
   */
  void setStatus(ProcessInstanceStatus status);

  /**
   * @return the status.
   */
  ProcessInstanceStatus getStatus();

  /**
   * @param refWatchers the watchers.
   */
  void setRefWatchers(Set<PersonModel> refWatchers);

  /**
   * @return the watchers.
   */
  Set<PersonModel> getRefWatchers();

  /**
   * @param refTaskInstances the specific instances associated with this instance.
   */
  void setRefTaskInstances(Set<TaskInstanceModel> refTaskInstances);

  /**
   * @return the specific instances associated with this instance.
   */
  Set<TaskInstanceModel> getRefTaskInstances();

  /**
   * @param refTemplate the underlying process.
   */
  void setRefTemplate(ProcessModel refTemplate);

  /**
   * @return the underlying process.
   */
  ProcessModel getRefTemplate();

}
