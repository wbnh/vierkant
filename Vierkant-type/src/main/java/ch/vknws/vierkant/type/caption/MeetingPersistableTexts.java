package ch.vknws.vierkant.type.caption;

/**
 * Captions for the meeting.
 * 
 * @author benjamin
 */
public interface MeetingPersistableTexts {

  /**
   * @return the texts for the meeting type
   */
  String refMeetingType();

  /**
   * @return the texts for the meeting name
   */
  String meetingName();

  /**
   * @return the texts for the description
   */
  String description();

  /**
   * @return the texts for the start
   */
  String start();

  /**
   * @return the texts for the end
   */
  String end();

  /**
   * @return the texts for the responsible
   */
  String refResponsible();

  /**
   * @return the texts for the planned attendees
   */
  String refPlannedAttendees();

  /**
   * @return the texts for the actual attendees
   */
  String refActualAttendees();

  /**
   * @return the texts for the miss excuses
   */
  String refMissExcuses();

  /**
   * @return the texts for the record
   */
  String refRecord();
}
