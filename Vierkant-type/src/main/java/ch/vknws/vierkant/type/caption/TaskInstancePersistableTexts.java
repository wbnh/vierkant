package ch.vknws.vierkant.type.caption;

/**
 * Texts.
 * 
 * @author benjamin
 */
public interface TaskInstancePersistableTexts extends AbstractPersistableTexts {

  /**
   * @return text
   */
  String refAssignee();

  /**
   * @return text
   */
  String refWatchers();

  /**
   * @return text
   */
  String refComments();

  /**
   * @return text
   */
  String refTemplate();

  /**
   * @return text
   */
  String refCustomFieldValues();
}
