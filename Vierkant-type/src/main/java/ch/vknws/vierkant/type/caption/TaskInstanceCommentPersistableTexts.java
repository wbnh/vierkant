package ch.vknws.vierkant.type.caption;

/**
 * Texts.
 * 
 * @author benjamin
 */
public interface TaskInstanceCommentPersistableTexts extends AbstractPersistableTexts {

  /**
   * @return text
   */
  String content();

  /**
   * @return text
   */
  String refReplies();
}
