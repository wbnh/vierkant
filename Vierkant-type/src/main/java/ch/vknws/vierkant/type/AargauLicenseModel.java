package ch.vknws.vierkant.type;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.database.DescriptiveName;
import ch.vknws.vierkant.type.impl.AargauLicensePersistable;

/**
 * Model for regulation licenses in the canton of aargau.
 *
 * @author Benjamin Weber
 */
@DescriptiveName(AargauLicenseModel.PropertyId.LICENSE_NAME)
public interface AargauLicenseModel extends DatabaseModel {

  /**
   * Contains the properties of this model.
   * 
   * @author benjamin
   */
  public static class PropertyId extends DatabaseModel.PropertyId {

    /**
     * The name of the license.
     */
    public static final String LICENSE_NAME = "licenseName";
    /**
     * The number of the license.
     */
    public static final String LICENSE_NUMBER = "licenseNumber";

    /**
     * The types of the fields on this model.
     */
    @SuppressWarnings("hiding")
    public static final Map<String, Class<?>> PROPERTY_TYPES = DatabaseModel.PropertyId.PROPERTY_TYPES;

    static {

      Field[] fields = PropertyId.class.getFields();

      for (Field field : fields) {
        if (String.class.isAssignableFrom(field.getType())) {
          try {
            String fieldName = (String) field.get(null);
            for (Method method : AargauLicenseModel.class.getMethods()) {
              if (fieldName.equalsIgnoreCase(method.getName()) || ("get" + fieldName).equalsIgnoreCase(method.getName())
                  || ("is" + fieldName).equalsIgnoreCase(method.getName())
                  || ("has" + fieldName).equalsIgnoreCase(method.getName())) {
                PROPERTY_TYPES.put(fieldName, method.getReturnType());
              }
            }
          } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            ServiceRegistry.getLogProxy().error(AargauLicensePersistable.class, "Could not initialise field names", e);
            throw new RuntimeException(e);
          }
        }
      }
    }
  }

  /**
   * @param licenseName the new name
   */
  void setLicenseName(String licenseName);

  /**
   * @return the name
   */
  String getLicenseName();

  /**
   * @param licenseNumber the new number
   */
  void setLicenseNumber(String licenseNumber);

  /**
   * @return the number
   */
  String getLicenseNumber();
}
