package ch.vknws.vierkant;

/**
 * Constants to be used in the whole application.
 *
 * @author Benjamin Weber
 */
public class ApplicationConstants {

  /**
   * The name of the application.
   */
  public static final String APPLICATION_NAME = "Vierkant";
  /**
   * The key phrase to system-readable encrypted values.
   */
  public static final String PASSWORD_ENCRYPTION_PASSWORD = APPLICATION_NAME + "PasswordEncryption";

  /**
   * The max length of comments.
   */
  public static final int COMMENT_LENGTH = 1000;
  /**
   * The max length of names.
   */
  public static final int NAME_LENGTH = 100;
  /**
   * The max length of phone numbers.
   */
  public static final int PHONE_LENGTH = 12;
  /**
   * The max length of shorthands.
   */
  public static final int SHORTHAND_LENGTH = 4;
  /**
   * The max length of rank abbreviations.
   */
  public static final int RANK_ABBREVIATION_LENGTH = 8;
  /**
   * The max length of an address line.
   */
  public static final int ADDRESS_LINE_LENGTH = 50;
  /**
   * The max length of zip codes.
   */
  public static final int ZIP_CODE_LENGTH = 8;
  /**
   * The max length of swiss zip codes.
   */
  public static final int SWISS_ZIP_CODE_LENGTH = 4;
  /**
   * The max length of canton codes.
   */
  public static final int CANTON_LENGTH = 2;
  /**
   * The max length of country codes.
   */
  public static final int COUNTRY_LENGTH = 3;// ISO-3
  /**
   * The max length of email addresses.
   */
  public static final int EMAIL_LENGTH = 70;
  /**
   * The max length of size values.
   */
  public static final int SIZE_LENGTH = 10;
  /**
   * The max length of paths.
   */
  public static final int PATH_LENGTH = 300;
}
