package ch.vknws.vierkant.type;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.impl.AuthorizationKeyPersistable;
import ch.vknws.vierkant.type.impl.RankPersistable;

/**
 * Test for the RankModel.
 *
 * @author Benjamin Weber
 */
public class TestClassRankModel {

  private RankModel model;

  /**
   * Sets up the test.
   */
  @Before
  public void setUpTest() {
    model = new RankPersistable();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRecordId() throws Exception {
    model.setRecordId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRecordId());

    model.setRecordId(null);
    Assert.assertNull(model.getRecordId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRevisionId() throws Exception {
    model.setRevisionId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRevisionId());

    model.setRevisionId(null);
    Assert.assertNull(model.getRevisionId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreator() throws Exception {
    model.setCreator(0);
    Assert.assertEquals(Integer.valueOf(0), model.getCreator());

    model.setCreator(null);
    Assert.assertNull(model.getCreator());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setCreationDateTime(now);
    Assert.assertEquals(now, model.getCreationDateTime());

    model.setCreationDateTime(null);
    Assert.assertNull(model.getCreationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModifier() throws Exception {
    model.setModifier(0);
    Assert.assertEquals(Integer.valueOf(0), model.getModifier());

    model.setModifier(null);
    Assert.assertNull(model.getModifier());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModificationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setModificationDateTime(now);
    Assert.assertEquals(now, model.getModificationDateTime());

    model.setModificationDateTime(null);
    Assert.assertNull(model.getModificationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testDeletedFlag() throws Exception {
    model.setDeletedFlag(true);
    Assert.assertTrue(model.getDeletedFlag());

    model.setDeletedFlag(false);
    Assert.assertFalse(model.getDeletedFlag());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAuditComment() throws Exception {
    model.setAuditComment("AuditComment");
    Assert.assertEquals("AuditComment", model.getAuditComment());

    model.setAuditComment(null);
    Assert.assertNull(model.getAuditComment());

    model.setAuditComment("");
    Assert.assertEquals("", model.getAuditComment());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAbbreviation() throws Exception {
    model.setAbbreviation("abbreviation");
    Assert.assertEquals("abbreviation", model.getAbbreviation());

    model.setAbbreviation(null);
    Assert.assertNull(model.getAbbreviation());

    model.setAbbreviation("");
    Assert.assertEquals("", model.getAbbreviation());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRankName() throws Exception {
    model.setRankName("rankName");
    Assert.assertEquals("rankName", model.getRankName());

    model.setRankName(null);
    Assert.assertNull(model.getRankName());

    model.setRankName("");
    Assert.assertEquals("", model.getRankName());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testDefaultFunction() throws Exception {
    model.setDefaultFunction("defaultFunction");
    Assert.assertEquals("defaultFunction", model.getDefaultFunction());

    model.setDefaultFunction(null);
    Assert.assertNull(model.getDefaultFunction());

    model.setDefaultFunction("");
    Assert.assertEquals("", model.getDefaultFunction());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testHierarchyIndex() throws Exception {
    model.setHierarchyIndex(0);
    Assert.assertEquals(Integer.valueOf(0), model.getHierarchyIndex());

    model.setHierarchyIndex(null);
    Assert.assertNull(model.getHierarchyIndex());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCategory() throws Exception {
    model.setRankCategory(RankCategory.ASPIRANT);
    Assert.assertEquals(RankCategory.ASPIRANT, model.getRankCategory());

    model.setRankCategory(null);
    Assert.assertNull(model.getRankCategory());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMemberCountGoal() throws Exception {
    model.setMemberCountGoal(0);
    Assert.assertEquals(Integer.valueOf(0), model.getMemberCountGoal());

    model.setMemberCountGoal(null);
    Assert.assertNull(model.getMemberCountGoal());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testShoulderStrapPicturePath() throws Exception {
    Path path = Paths.get("/");
    model.setShoulderStrapPicture(path);
    Assert.assertEquals(path, model.getShoulderStrapPicture());

    model.setShoulderStrapPicture(null);
    Assert.assertNull(model.getShoulderStrapPicture());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAuthorizationKey() throws Exception {
    AuthorizationKeyModel key = new AuthorizationKeyPersistable();
    model.setRefAuthorizationKey(key);
    Assert.assertEquals(key, model.getRefAuthorizationKey());

    model.setRefAuthorizationKey(null);
    Assert.assertNull(model.getRefAuthorizationKey());
  }
}
