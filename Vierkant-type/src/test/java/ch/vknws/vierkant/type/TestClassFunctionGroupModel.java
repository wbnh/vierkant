package ch.vknws.vierkant.type;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.impl.AuthorizationKeyPersistable;
import ch.vknws.vierkant.type.impl.FunctionGroupPersistable;
import ch.vknws.vierkant.type.impl.OrganizationFunctionPersistable;

/**
 * Test for the FunctionGroupModel.
 *
 * @author Benjamin Weber
 */
public class TestClassFunctionGroupModel {

  private FunctionGroupModel model;

  /**
   * Sets up the test.
   */
  @Before
  public void setUpTest() {
    model = new FunctionGroupPersistable();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRecordId() throws Exception {
    model.setRecordId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRecordId());

    model.setRecordId(null);
    Assert.assertNull(model.getRecordId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRevisionId() throws Exception {
    model.setRevisionId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRevisionId());

    model.setRevisionId(null);
    Assert.assertNull(model.getRevisionId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreator() throws Exception {
    model.setCreator(0);
    Assert.assertEquals(Integer.valueOf(0), model.getCreator());

    model.setCreator(null);
    Assert.assertNull(model.getCreator());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setCreationDateTime(now);
    Assert.assertEquals(now, model.getCreationDateTime());

    model.setCreationDateTime(null);
    Assert.assertNull(model.getCreationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModifier() throws Exception {
    model.setModifier(0);
    Assert.assertEquals(Integer.valueOf(0), model.getModifier());

    model.setModifier(null);
    Assert.assertNull(model.getModifier());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModificationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setModificationDateTime(now);
    Assert.assertEquals(now, model.getModificationDateTime());

    model.setModificationDateTime(null);
    Assert.assertNull(model.getModificationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testDeletedFlag() throws Exception {
    model.setDeletedFlag(true);
    Assert.assertTrue(model.getDeletedFlag());

    model.setDeletedFlag(false);
    Assert.assertFalse(model.getDeletedFlag());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAuditComment() throws Exception {
    model.setAuditComment("AuditComment");
    Assert.assertEquals("AuditComment", model.getAuditComment());

    model.setAuditComment(null);
    Assert.assertNull(model.getAuditComment());

    model.setAuditComment("");
    Assert.assertEquals("", model.getAuditComment());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testGroupMembers() throws Exception {
    Set<OrganizationFunctionModel> members = new HashSet<>();
    members.add(new OrganizationFunctionPersistable());

    model.setRefGroupMembers(members);
    Assert.assertEquals(members, model.getRefGroupMembers());

    members = new HashSet<>();
    model.setRefGroupMembers(members);
    Assert.assertEquals(members, model.getRefGroupMembers());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testDetails() throws Exception {
    OrganizationFunctionModel details = new OrganizationFunctionPersistable();
    model.setRefDetails(details);
    Assert.assertEquals(details, model.getRefDetails());

    model.setRefDetails(null);
    Assert.assertNull(model.getRefDetails());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAuthorizationKey() throws Exception {
    AuthorizationKeyModel key = new AuthorizationKeyPersistable();
    model.setRefAuthorizationKey(key);
    Assert.assertEquals(key, model.getRefAuthorizationKey());

    model.setRefAuthorizationKey(null);
    Assert.assertNull(model.getRefAuthorizationKey());
  }
}
