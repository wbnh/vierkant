package ch.vknws.vierkant.type;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.impl.AargauLicensePersistable;
import ch.vknws.vierkant.type.impl.AargauPersonLicensePersistable;

/**
 * Test for the AargauPersonLicenseModel.
 *
 * @author Benjamin Weber
 */
public class TestClassAargauPersonLicenseModel {

  private AargauPersonLicenseModel model;

  /**
   * Sets up the test.
   */
  @Before
  public void setUpTest() {
    model = new AargauPersonLicensePersistable();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRecordId() throws Exception {
    model.setRecordId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRecordId());

    model.setRecordId(null);
    Assert.assertNull(model.getRecordId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRevisionId() throws Exception {
    model.setRevisionId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRevisionId());

    model.setRevisionId(null);
    Assert.assertNull(model.getRevisionId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreator() throws Exception {
    model.setCreator(0);
    Assert.assertEquals(Integer.valueOf(0), model.getCreator());

    model.setCreator(null);
    Assert.assertNull(model.getCreator());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setCreationDateTime(now);
    Assert.assertEquals(now, model.getCreationDateTime());

    model.setCreationDateTime(null);
    Assert.assertNull(model.getCreationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModifier() throws Exception {
    model.setModifier(0);
    Assert.assertEquals(Integer.valueOf(0), model.getModifier());

    model.setModifier(null);
    Assert.assertNull(model.getModifier());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModificationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setModificationDateTime(now);
    Assert.assertEquals(now, model.getModificationDateTime());

    model.setModificationDateTime(null);
    Assert.assertNull(model.getModificationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testDeletedFlag() throws Exception {
    model.setDeletedFlag(true);
    Assert.assertTrue(model.getDeletedFlag());

    model.setDeletedFlag(false);
    Assert.assertFalse(model.getDeletedFlag());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAuditComment() throws Exception {
    model.setAuditComment("AuditComment");
    Assert.assertEquals("AuditComment", model.getAuditComment());

    model.setAuditComment(null);
    Assert.assertNull(model.getAuditComment());

    model.setAuditComment("");
    Assert.assertEquals("", model.getAuditComment());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testInvoiceNumber() throws Exception {
    model.setInvoiceNumber("invoiceNumber");
    Assert.assertEquals("invoiceNumber", model.getInvoiceNumber());

    model.setInvoiceNumber(null);
    Assert.assertNull(model.getInvoiceNumber());

    model.setInvoiceNumber("");
    Assert.assertEquals("", model.getInvoiceNumber());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testValidityEndDate() throws Exception {
    LocalDate now = LocalDate.now();
    model.setValidityEndDate(now);
    Assert.assertEquals(now, model.getValidityEndDate());

    model.setValidityEndDate(null);
    Assert.assertNull(model.getValidityEndDate());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRegistrationDate() throws Exception {
    LocalDate now = LocalDate.now();
    model.setRegistrationDate(now);
    Assert.assertEquals(now, model.getRegistrationDate());

    model.setRegistrationDate(null);
    Assert.assertNull(model.getRegistrationDate());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRefLicense() throws Exception {
    AargauLicenseModel refLicense = new AargauLicensePersistable();
    model.setRefLicense(refLicense);
    Assert.assertEquals(refLicense, model.getRefLicense());

    model.setRefLicense(null);
    Assert.assertNull(model.getRefLicense());
  }
}
