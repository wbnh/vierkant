package ch.vknws.vierkant.type;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.impl.AuthorizationKeyPersistable;

/**
 * Test for the AuthorizationKeyModel.
 *
 * @author Benjamin Weber
 */
public class TestClassAuthorizationKeyModel {

  private AuthorizationKeyModel model;

  /**
   * Sets up the test.
   */
  @Before
  public void setUpTest() {
    model = new AuthorizationKeyPersistable();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRecordId() throws Exception {
    model.setRecordId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRecordId());

    model.setRecordId(null);
    Assert.assertNull(model.getRecordId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRevisionId() throws Exception {
    model.setRevisionId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRevisionId());

    model.setRevisionId(null);
    Assert.assertNull(model.getRevisionId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreator() throws Exception {
    model.setCreator(0);
    Assert.assertEquals(Integer.valueOf(0), model.getCreator());

    model.setCreator(null);
    Assert.assertNull(model.getCreator());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setCreationDateTime(now);
    Assert.assertEquals(now, model.getCreationDateTime());

    model.setCreationDateTime(null);
    Assert.assertNull(model.getCreationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModifier() throws Exception {
    model.setModifier(0);
    Assert.assertEquals(Integer.valueOf(0), model.getModifier());

    model.setModifier(null);
    Assert.assertNull(model.getModifier());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModificationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setModificationDateTime(now);
    Assert.assertEquals(now, model.getModificationDateTime());

    model.setModificationDateTime(null);
    Assert.assertNull(model.getModificationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testDeletedFlag() throws Exception {
    model.setDeletedFlag(true);
    Assert.assertTrue(model.getDeletedFlag());

    model.setDeletedFlag(false);
    Assert.assertFalse(model.getDeletedFlag());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAuditComment() throws Exception {
    model.setAuditComment("AuditComment");
    Assert.assertEquals("AuditComment", model.getAuditComment());

    model.setAuditComment(null);
    Assert.assertNull(model.getAuditComment());

    model.setAuditComment("");
    Assert.assertEquals("", model.getAuditComment());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testKeyName() throws Exception {
    model.setKeyName("keyName");
    Assert.assertEquals("keyName", model.getKeyName());

    model.setKeyName(null);
    Assert.assertNull(model.getKeyName());

    model.setKeyName("");
    Assert.assertEquals("", model.getKeyName());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMode() throws Exception {
    model.setMode(AuthorizationKeyMode.READ);
    Assert.assertEquals(AuthorizationKeyMode.READ, model.getMode());

    model.setMode(null);
    Assert.assertNull(model.getMode());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testType() throws Exception {
    model.setType(AuthorizationKeyType.ALLOW);
    Assert.assertEquals(AuthorizationKeyType.ALLOW, model.getType());

    model.setType(null);
    Assert.assertNull(model.getType());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testSubordinates() throws Exception {
    Set<AuthorizationKeyModel> set = new HashSet<>();
    set.add(new AuthorizationKeyPersistable());

    model.setRefSubordinates(set);
    Assert.assertEquals(set, model.getRefSubordinates());

    set = new HashSet<>();
    model.setRefSubordinates(set);
    Assert.assertEquals(set, model.getRefSubordinates());

    model.setRefSubordinates(null);
    Assert.assertEquals(0, model.getRefSubordinates().size());
  }
}
