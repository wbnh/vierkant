package ch.vknws.vierkant.type;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.impl.PrerequisitePersistable;

/**
 * Test for the PrerequisiteModel.
 *
 * @author Benjamin Weber
 */
public class TestClassPrerequisiteModel {

  private PrerequisiteModel model;

  /**
   * Sets up the test.
   */
  @Before
  public void setUpTest() {
    model = new PrerequisitePersistable();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRecordId() throws Exception {
    model.setRecordId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRecordId());

    model.setRecordId(null);
    Assert.assertNull(model.getRecordId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRevisionId() throws Exception {
    model.setRevisionId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRevisionId());

    model.setRevisionId(null);
    Assert.assertNull(model.getRevisionId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreator() throws Exception {
    model.setCreator(0);
    Assert.assertEquals(Integer.valueOf(0), model.getCreator());

    model.setCreator(null);
    Assert.assertNull(model.getCreator());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setCreationDateTime(now);
    Assert.assertEquals(now, model.getCreationDateTime());

    model.setCreationDateTime(null);
    Assert.assertNull(model.getCreationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModifier() throws Exception {
    model.setModifier(0);
    Assert.assertEquals(Integer.valueOf(0), model.getModifier());

    model.setModifier(null);
    Assert.assertNull(model.getModifier());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModificationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setModificationDateTime(now);
    Assert.assertEquals(now, model.getModificationDateTime());

    model.setModificationDateTime(null);
    Assert.assertNull(model.getModificationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testDeletedFlag() throws Exception {
    model.setDeletedFlag(true);
    Assert.assertTrue(model.getDeletedFlag());

    model.setDeletedFlag(false);
    Assert.assertFalse(model.getDeletedFlag());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAuditComment() throws Exception {
    model.setAuditComment("AuditComment");
    Assert.assertEquals("AuditComment", model.getAuditComment());

    model.setAuditComment(null);
    Assert.assertNull(model.getAuditComment());

    model.setAuditComment("");
    Assert.assertEquals("", model.getAuditComment());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testGt() throws Exception {
    Map<String, Integer> map = new HashMap<>();
    model.setGt(map);
    Assert.assertEquals(map, model.getGt());

    model.setGt(null);
    Assert.assertNull(model.getGt());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testGe() throws Exception {
    Map<String, Integer> map = new HashMap<>();
    model.setGe(map);
    Assert.assertEquals(map, model.getGe());

    model.setGe(null);
    Assert.assertNull(model.getGe());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testLt() throws Exception {
    Map<String, Integer> map = new HashMap<>();
    model.setLt(map);
    Assert.assertEquals(map, model.getLt());

    model.setLt(null);
    Assert.assertNull(model.getLt());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testLe() throws Exception {
    Map<String, Integer> map = new HashMap<>();
    model.setLe(map);
    Assert.assertEquals(map, model.getLe());

    model.setLe(null);
    Assert.assertNull(model.getLe());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testEq() throws Exception {
    Map<String, String> map = new HashMap<>();
    model.setEq(map);
    Assert.assertEquals(map, model.getEq());

    model.setEq(null);
    Assert.assertNull(model.getEq());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testIsNull() throws Exception {
    Map<String, String> map = new HashMap<>();
    model.setIsNull(map);
    Assert.assertEquals(map, model.getIsNull());

    model.setIsNull(null);
    Assert.assertNull(model.getIsNull());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testIsNotNull() throws Exception {
    Map<String, String> map = new HashMap<>();
    model.setIsNotNull(map);
    Assert.assertEquals(map, model.getIsNotNull());

    model.setIsNotNull(null);
    Assert.assertNull(model.getIsNotNull());
  }
}
