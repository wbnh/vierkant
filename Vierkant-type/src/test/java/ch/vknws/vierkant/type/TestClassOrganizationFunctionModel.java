package ch.vknws.vierkant.type;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.impl.AuthorizationKeyPersistable;
import ch.vknws.vierkant.type.impl.FunctionGroupPersistable;
import ch.vknws.vierkant.type.impl.OrganizationFunctionPersistable;

/**
 * Test for the OrganizationFunctionModel.
 *
 * @author Benjamin Weber
 */
public class TestClassOrganizationFunctionModel {

  private OrganizationFunctionModel model;

  /**
   * Sets up the test.
   */
  @Before
  public void setUpTest() {
    model = new OrganizationFunctionPersistable();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRecordId() throws Exception {
    model.setRecordId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRecordId());

    model.setRecordId(null);
    Assert.assertNull(model.getRecordId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRevisionId() throws Exception {
    model.setRevisionId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRevisionId());

    model.setRevisionId(null);
    Assert.assertNull(model.getRevisionId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreator() throws Exception {
    model.setCreator(0);
    Assert.assertEquals(Integer.valueOf(0), model.getCreator());

    model.setCreator(null);
    Assert.assertNull(model.getCreator());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setCreationDateTime(now);
    Assert.assertEquals(now, model.getCreationDateTime());

    model.setCreationDateTime(null);
    Assert.assertNull(model.getCreationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModifier() throws Exception {
    model.setModifier(0);
    Assert.assertEquals(Integer.valueOf(0), model.getModifier());

    model.setModifier(null);
    Assert.assertNull(model.getModifier());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModificationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setModificationDateTime(now);
    Assert.assertEquals(now, model.getModificationDateTime());

    model.setModificationDateTime(null);
    Assert.assertNull(model.getModificationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testDeletedFlag() throws Exception {
    model.setDeletedFlag(true);
    Assert.assertTrue(model.getDeletedFlag());

    model.setDeletedFlag(false);
    Assert.assertFalse(model.getDeletedFlag());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAuditComment() throws Exception {
    model.setAuditComment("AuditComment");
    Assert.assertEquals("AuditComment", model.getAuditComment());

    model.setAuditComment(null);
    Assert.assertNull(model.getAuditComment());

    model.setAuditComment("");
    Assert.assertEquals("", model.getAuditComment());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testFunctionName() throws Exception {
    model.setFunctionName("functionName");
    Assert.assertEquals("functionName", model.getFunctionName());

    model.setFunctionName(null);
    Assert.assertNull(model.getFunctionName());

    model.setFunctionName("");
    Assert.assertEquals("", model.getFunctionName());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testEmail() throws Exception {
    model.setEmail("email");
    Assert.assertEquals("email", model.getEmail());

    model.setEmail(null);
    Assert.assertNull(model.getEmail());

    model.setEmail("");
    Assert.assertEquals("", model.getEmail());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMaxPersonCount() throws Exception {
    model.setMaxPersonCount(0);
    Assert.assertEquals(Integer.valueOf(0), model.getMaxPersonCount());

    model.setMaxPersonCount(null);
    Assert.assertNull(model.getMaxPersonCount());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMinPersonCount() throws Exception {
    model.setMinPersonCount(0);
    Assert.assertEquals(Integer.valueOf(0), model.getMinPersonCount());

    model.setMinPersonCount(null);
    Assert.assertNull(model.getMinPersonCount());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testType() throws Exception {
    model.setType(FunctionType.NORMAL);
    Assert.assertEquals(FunctionType.NORMAL, model.getType());

    model.setType(null);
    Assert.assertNull(model.getType());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testSuperiors() throws Exception {
    Set<OrganizationFunctionModel> superiors = new HashSet<>();
    superiors.add(new OrganizationFunctionPersistable());

    model.setRefSuperiors(superiors);
    Assert.assertEquals(superiors, model.getRefSuperiors());

    superiors = new HashSet<>();
    model.setRefSuperiors(superiors);
    Assert.assertEquals(superiors, model.getRefSuperiors());

    model.setRefSuperiors(null);
    Assert.assertNull(model.getRefSuperiors());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAuthorizationKey() throws Exception {
    AuthorizationKeyModel key = new AuthorizationKeyPersistable();
    model.setRefAuthorizationKey(key);
    Assert.assertEquals(key, model.getRefAuthorizationKey());

    model.setRefAuthorizationKey(null);
    Assert.assertNull(model.getRefAuthorizationKey());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testGroups() throws Exception {
    Set<FunctionGroupModel> groups = new HashSet<>();
    groups.add(new FunctionGroupPersistable());

    model.setRefGroups(groups);
    Assert.assertEquals(groups, model.getRefGroups());

    groups = new HashSet<>();
    model.setRefGroups(groups);
    Assert.assertEquals(groups, model.getRefGroups());

    model.setRefGroups(null);
    Assert.assertNull(model.getRefGroups());
  }
}
