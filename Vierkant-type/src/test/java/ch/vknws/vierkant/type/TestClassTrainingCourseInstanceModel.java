package ch.vknws.vierkant.type;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.impl.PersonPersistable;
import ch.vknws.vierkant.type.impl.TrainingCourseInstancePersistable;
import ch.vknws.vierkant.type.impl.TrainingCoursePersistable;

/**
 * Test for the TrainingCourseInstanceModel.
 *
 * @author Benjamin Weber
 */
public class TestClassTrainingCourseInstanceModel {

  private TrainingCourseInstanceModel model;

  /**
   * Sets up the test.
   */
  @Before
  public void setUpTest() {
    model = new TrainingCourseInstancePersistable();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRecordId() throws Exception {
    model.setRecordId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRecordId());

    model.setRecordId(null);
    Assert.assertNull(model.getRecordId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRevisionId() throws Exception {
    model.setRevisionId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRevisionId());

    model.setRevisionId(null);
    Assert.assertNull(model.getRevisionId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreator() throws Exception {
    model.setCreator(0);
    Assert.assertEquals(Integer.valueOf(0), model.getCreator());

    model.setCreator(null);
    Assert.assertNull(model.getCreator());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setCreationDateTime(now);
    Assert.assertEquals(now, model.getCreationDateTime());

    model.setCreationDateTime(null);
    Assert.assertNull(model.getCreationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModifier() throws Exception {
    model.setModifier(0);
    Assert.assertEquals(Integer.valueOf(0), model.getModifier());

    model.setModifier(null);
    Assert.assertNull(model.getModifier());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModificationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setModificationDateTime(now);
    Assert.assertEquals(now, model.getModificationDateTime());

    model.setModificationDateTime(null);
    Assert.assertNull(model.getModificationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testDeletedFlag() throws Exception {
    model.setDeletedFlag(true);
    Assert.assertTrue(model.getDeletedFlag());

    model.setDeletedFlag(false);
    Assert.assertFalse(model.getDeletedFlag());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAuditComment() throws Exception {
    model.setAuditComment("AuditComment");
    Assert.assertEquals("AuditComment", model.getAuditComment());

    model.setAuditComment(null);
    Assert.assertNull(model.getAuditComment());

    model.setAuditComment("");
    Assert.assertEquals("", model.getAuditComment());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testTemplate() throws Exception {
    TrainingCourseModel tc = new TrainingCoursePersistable();
    model.setRefTemplate(tc);
    Assert.assertEquals(tc, model.getRefTemplate());

    model.setRefTemplate(null);
    Assert.assertNull(model.getRefTemplate());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testResponsible() throws Exception {
    PersonModel p = new PersonPersistable();
    model.setRefResponsible(p);
    Assert.assertEquals(p, model.getRefResponsible());

    model.setRefResponsible(null);
    Assert.assertNull(model.getRefResponsible());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testTeachers() throws Exception {
    Set<PersonModel> set = new HashSet<>();
    set.add(new PersonPersistable());

    model.setRefTeachers(set);
    Assert.assertEquals(set, model.getRefTeachers());

    set = new HashSet<>();
    model.setRefTeachers(set);
    Assert.assertEquals(set, model.getRefTeachers());

    model.setRefTeachers(null);
    Assert.assertNull(model.getRefTeachers());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testParticipants() throws Exception {
    Set<PersonModel> set = new HashSet<>();
    set.add(new PersonPersistable());

    model.setRefParticipants(set);
    Assert.assertEquals(set, model.getRefParticipants());

    set = new HashSet<>();
    model.setRefParticipants(set);
    Assert.assertEquals(set, model.getRefParticipants());

    model.setRefParticipants(null);
    Assert.assertNull(model.getRefParticipants());
  }
}
