package ch.vknws.vierkant.type;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.impl.AargauPersonLicensePersistable;
import ch.vknws.vierkant.type.impl.AddressPersistable;
import ch.vknws.vierkant.type.impl.AuthorizationKeyPersistable;
import ch.vknws.vierkant.type.impl.ContactPersonPersistable;
import ch.vknws.vierkant.type.impl.MedicalInformationPersistable;
import ch.vknws.vierkant.type.impl.MemberGroupPersistable;
import ch.vknws.vierkant.type.impl.OrganizationFunctionPersistable;
import ch.vknws.vierkant.type.impl.PersonFunctionPersistable;
import ch.vknws.vierkant.type.impl.PersonPersistable;
import ch.vknws.vierkant.type.impl.RankPersistable;
import ch.vknws.vierkant.type.impl.TrainingCoursePersistable;

/**
 * Test for the PersonModel.
 *
 * @author Benjamin Weber
 */
public class TestClassPersonModel {

  private PersonModel model;

  /**
   * Sets up the test.
   */
  @Before
  public void setUpTest() {
    model = new PersonPersistable();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRecordId() throws Exception {
    model.setRecordId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRecordId());

    model.setRecordId(null);
    Assert.assertNull(model.getRecordId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRevisionId() throws Exception {
    model.setRevisionId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRevisionId());

    model.setRevisionId(null);
    Assert.assertNull(model.getRevisionId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreator() throws Exception {
    model.setCreator(0);
    Assert.assertEquals(Integer.valueOf(0), model.getCreator());

    model.setCreator(null);
    Assert.assertNull(model.getCreator());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setCreationDateTime(now);
    Assert.assertEquals(now, model.getCreationDateTime());

    model.setCreationDateTime(null);
    Assert.assertNull(model.getCreationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModifier() throws Exception {
    model.setModifier(0);
    Assert.assertEquals(Integer.valueOf(0), model.getModifier());

    model.setModifier(null);
    Assert.assertNull(model.getModifier());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModificationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setModificationDateTime(now);
    Assert.assertEquals(now, model.getModificationDateTime());

    model.setModificationDateTime(null);
    Assert.assertNull(model.getModificationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testDeletedFlag() throws Exception {
    model.setDeletedFlag(true);
    Assert.assertTrue(model.getDeletedFlag());

    model.setDeletedFlag(false);
    Assert.assertFalse(model.getDeletedFlag());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAuditComment() throws Exception {
    model.setAuditComment("AuditComment");
    Assert.assertEquals("AuditComment", model.getAuditComment());

    model.setAuditComment(null);
    Assert.assertNull(model.getAuditComment());

    model.setAuditComment("");
    Assert.assertEquals("", model.getAuditComment());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testGivenName() throws Exception {
    model.setGivenName("givenName");
    Assert.assertEquals("givenName", model.getGivenName());

    model.setGivenName(null);
    Assert.assertNull(model.getGivenName());

    model.setGivenName("");
    Assert.assertEquals("", model.getGivenName());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testFamilyName() throws Exception {
    model.setFamilyName("familyName");
    Assert.assertEquals("familyName", model.getFamilyName());

    model.setFamilyName(null);
    Assert.assertNull(model.getFamilyName());

    model.setFamilyName("");
    Assert.assertEquals("", model.getFamilyName());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testTitle() throws Exception {
    model.setTitle("title");
    Assert.assertEquals("title", model.getTitle());

    model.setTitle(null);
    Assert.assertNull(model.getTitle());

    model.setTitle("");
    Assert.assertEquals("", model.getTitle());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testTelephone() throws Exception {
    model.setTelephone("telephone");
    Assert.assertEquals("telephone", model.getTelephone());

    model.setTelephone(null);
    Assert.assertNull(model.getTelephone());

    model.setTelephone("");
    Assert.assertEquals("", model.getTelephone());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMobile() throws Exception {
    model.setMobile("mobile");
    Assert.assertEquals("mobile", model.getMobile());

    model.setMobile(null);
    Assert.assertNull(model.getMobile());

    model.setMobile("");
    Assert.assertEquals("", model.getMobile());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testEmailPrivate() throws Exception {
    model.setEmailPrivate("emailPrivate");
    Assert.assertEquals("emailPrivate", model.getEmailPrivate());

    model.setEmailPrivate(null);
    Assert.assertNull(model.getEmailPrivate());

    model.setEmailPrivate("");
    Assert.assertEquals("", model.getEmailPrivate());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testEmailOrganization() throws Exception {
    model.setEmailOrganization("emailOrganization");
    Assert.assertEquals("emailOrganization", model.getEmailOrganization());

    model.setEmailOrganization(null);
    Assert.assertNull(model.getEmailOrganization());

    model.setEmailOrganization("");
    Assert.assertEquals("", model.getEmailOrganization());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testBirthdate() throws Exception {
    LocalDate now = LocalDate.now();
    model.setBirthdate(now);
    Assert.assertEquals(now, model.getBirthdate());

    model.setBirthdate(null);
    Assert.assertNull(model.getBirthdate());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testSecretaryComment() throws Exception {
    model.setSecretaryComment("secretaryComment");
    Assert.assertEquals("secretaryComment", model.getSecretaryComment());

    model.setSecretaryComment(null);
    Assert.assertNull(model.getSecretaryComment());

    model.setSecretaryComment("");
    Assert.assertEquals("", model.getSecretaryComment());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testVkNews() throws Exception {
    model.setVkNews(true);
    Assert.assertTrue(model.getVkNews());

    model.setVkNews(false);
    Assert.assertFalse(model.getVkNews());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testLastUpdateCheck() throws Exception {
    LocalDate now = LocalDate.now();
    model.setLastUpdateCheck(now);
    Assert.assertEquals(now, model.getLastUpdateCheck());

    model.setLastUpdateCheck(null);
    Assert.assertNull(model.getLastUpdateCheck());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testShorthand() throws Exception {
    model.setShorthand("shorthand");
    Assert.assertEquals("shorthand", model.getShorthand());

    model.setShorthand(null);
    Assert.assertNull(model.getShorthand());

    model.setShorthand("");
    Assert.assertEquals("", model.getShorthand());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testVka() throws Exception {
    model.setVka(VKA.A);
    Assert.assertEquals(VKA.A, model.getVka());

    model.setVka(null);
    Assert.assertNull(model.getVka());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testmemberCategory() throws Exception {
    model.setMemberCategory(MemberCategoryType.ACTIVE);
    Assert.assertEquals(MemberCategoryType.ACTIVE, model.getMemberCategory());

    model.setMemberCategory(null);
    Assert.assertNull(model.getMemberCategory());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testSex() throws Exception {
    model.setSex(SexType.FEMALE);
    Assert.assertEquals(SexType.FEMALE, model.getSex());

    model.setSex(null);
    Assert.assertNull(model.getSex());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRefHomeAddress() throws Exception {
    AddressModel address = new AddressPersistable();
    model.setRefHomeAddress(address);
    Assert.assertEquals(address, model.getRefHomeAddress());

    model.setRefHomeAddress(null);
    Assert.assertNull(model.getRefHomeAddress());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testContactPersons() throws Exception {
    Set<ContactPersonModel> set = new HashSet<>();
    set.add(new ContactPersonPersistable());

    model.setRefContactPersons(set);
    Assert.assertEquals(set, model.getRefContactPersons());

    set = new HashSet<>();
    model.setRefContactPersons(set);
    Assert.assertEquals(set, model.getRefContactPersons());

    model.setRefContactPersons(null);
    Assert.assertEquals(0, model.getRefContactPersons().size());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAargauLicense() throws Exception {
    AargauPersonLicenseModel license = new AargauPersonLicensePersistable();
    model.setRefAargauLicense(license);
    Assert.assertEquals(license, model.getRefAargauLicense());

    model.setRefAargauLicense(null);
    Assert.assertNull(model.getRefAargauLicense());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMemberGroup() throws Exception {
    MemberGroupModel group = new MemberGroupPersistable();
    model.setRefMemberGroup(group);
    Assert.assertEquals(group, model.getRefMemberGroup());

    model.setRefMemberGroup(null);
    Assert.assertNull(model.getRefMemberGroup());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRank() throws Exception {
    RankModel rank = new RankPersistable();
    model.setRefRank(rank);
    Assert.assertEquals(rank, model.getRefRank());

    model.setRefRank(null);
    Assert.assertNull(model.getRefRank());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testFunctions() throws Exception {
    Set<PersonFunctionModel> functions = new HashSet<>();
    functions.add(new PersonFunctionPersistable());

    model.setRefFunctions(functions);
    Assert.assertEquals(functions, model.getRefFunctions());

    functions = new HashSet<>();
    model.setRefFunctions(functions);
    Assert.assertEquals(functions, model.getRefFunctions());

    model.setRefFunctions(null);
    Assert.assertEquals(0, model.getRefFunctions().size());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testPrimaryFunction() throws Exception {
    OrganizationFunctionModel function = new OrganizationFunctionPersistable();
    model.setRefPrimaryFunction(function);
    Assert.assertEquals(function, model.getRefPrimaryFunction());

    model.setRefPrimaryFunction(null);
    Assert.assertNull(model.getRefPrimaryFunction());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCompletedCourses() throws Exception {
    Set<TrainingCourseModel> set = new HashSet<>();
    set.add(new TrainingCoursePersistable());

    model.setRefCompletedCourses(set);
    Assert.assertEquals(set, model.getRefCompletedCourses());

    set = new HashSet<>();
    model.setRefCompletedCourses(set);
    Assert.assertEquals(set, model.getRefCompletedCourses());

    model.setRefCompletedCourses(null);
    Assert.assertEquals(0, model.getRefCompletedCourses().size());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testMedicalInformation() throws Exception {
    MedicalInformationModel info = new MedicalInformationPersistable();
    model.setRefMedicalInformation(info);
    Assert.assertEquals(info, model.getRefMedicalInformation());

    model.setRefMedicalInformation(null);
    Assert.assertNull(model.getRefMedicalInformation());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAuthorizationKey() throws Exception {
    AuthorizationKeyModel key = new AuthorizationKeyPersistable();
    model.setRefAuthorizationKey(key);
    Assert.assertEquals(key, model.getRefAuthorizationKey());

    model.setRefAuthorizationKey(null);
    Assert.assertNull(model.getRefAuthorizationKey());
  }
}
