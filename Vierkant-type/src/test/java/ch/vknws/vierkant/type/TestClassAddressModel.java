package ch.vknws.vierkant.type;

import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.vknws.vierkant.type.impl.AddressPersistable;

/**
 * Test for the AddressModel.
 *
 * @author Benjamin Weber
 */
public class TestClassAddressModel {

  private AddressModel model;

  /**
   * Sets up the test.
   */
  @Before
  public void setUpTest() {
    model = new AddressPersistable();
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRecordId() throws Exception {
    model.setRecordId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRecordId());

    model.setRecordId(null);
    Assert.assertNull(model.getRecordId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testRevisionId() throws Exception {
    model.setRevisionId(0);
    Assert.assertEquals(Integer.valueOf(0), model.getRevisionId());

    model.setRevisionId(null);
    Assert.assertNull(model.getRevisionId());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreator() throws Exception {
    model.setCreator(0);
    Assert.assertEquals(Integer.valueOf(0), model.getCreator());

    model.setCreator(null);
    Assert.assertNull(model.getCreator());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCreationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setCreationDateTime(now);
    Assert.assertEquals(now, model.getCreationDateTime());

    model.setCreationDateTime(null);
    Assert.assertNull(model.getCreationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModifier() throws Exception {
    model.setModifier(0);
    Assert.assertEquals(Integer.valueOf(0), model.getModifier());

    model.setModifier(null);
    Assert.assertNull(model.getModifier());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testModificationDateTime() throws Exception {
    LocalDateTime now = LocalDateTime.now();
    model.setModificationDateTime(now);
    Assert.assertEquals(now, model.getModificationDateTime());

    model.setModificationDateTime(null);
    Assert.assertNull(model.getModificationDateTime());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testDeletedFlag() throws Exception {
    model.setDeletedFlag(true);
    Assert.assertTrue(model.getDeletedFlag());

    model.setDeletedFlag(false);
    Assert.assertFalse(model.getDeletedFlag());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testAuditComment() throws Exception {
    model.setAuditComment("AuditComment");
    Assert.assertEquals("AuditComment", model.getAuditComment());

    model.setAuditComment(null);
    Assert.assertNull(model.getAuditComment());

    model.setAuditComment("");
    Assert.assertEquals("", model.getAuditComment());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testLine1() throws Exception {
    model.setLine1("line 1");
    Assert.assertEquals("line 1", model.getLine1());

    model.setLine1(null);
    Assert.assertNull(model.getLine1());

    model.setLine1("");
    Assert.assertEquals("", model.getLine1());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testLine2() throws Exception {
    model.setLine2("line 2");
    Assert.assertEquals("line 2", model.getLine2());

    model.setLine2(null);
    Assert.assertNull(model.getLine2());

    model.setLine2("");
    Assert.assertEquals("", model.getLine2());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testZipCode() throws Exception {
    model.setZipCode("zipcode");
    Assert.assertEquals("zipcode", model.getZipCode());

    model.setZipCode(null);
    Assert.assertNull(model.getZipCode());

    model.setZipCode("");
    Assert.assertEquals("", model.getZipCode());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCity() throws Exception {
    model.setCity("city");
    Assert.assertEquals("city", model.getCity());

    model.setCity(null);
    Assert.assertNull(model.getCity());

    model.setCity("");
    Assert.assertEquals("", model.getCity());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCanton() throws Exception {
    model.setCanton("canton");
    Assert.assertEquals("canton", model.getCanton());

    model.setCanton(null);
    Assert.assertNull(model.getCanton());

    model.setCanton("");
    Assert.assertEquals("", model.getCanton());
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testCountry() throws Exception {
    model.setCountry("country");
    Assert.assertEquals("country", model.getCountry());

    model.setCountry(null);
    Assert.assertNull(model.getCountry());

    model.setCountry("");
    Assert.assertEquals("", model.getCountry());
  }
}
