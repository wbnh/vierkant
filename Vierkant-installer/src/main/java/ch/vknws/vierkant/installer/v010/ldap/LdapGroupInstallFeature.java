package ch.vknws.vierkant.installer.v010.ldap;

import java.io.IOException;

import org.apache.directory.api.ldap.model.exception.LdapException;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.installer.feature.AbstractFeature;
import ch.bwe.fac.v1.installer.feature.FeatureResult;
import ch.bwe.fac.v1.installer.feature.FeatureResult.Problem;
import ch.bwe.fae.type.LdapConnection;
import ch.vknws.vierkant.ApplicationConstants;
import ch.vknws.vierkant.backend.ldap.AuthenticationUtil;
import ch.vknws.vierkant.backend.ldap.LdapGroup;
import ch.vknws.vierkant.installer.Versions;

/**
 * Feature to install the necessary LDAP groups.
 *
 * @author Benjamin Weber
 */
public class LdapGroupInstallFeature extends AbstractFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 LDAP groups";

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public LdapGroupInstallFeature() {
    super(Versions.v0_1_0, ACTION, Environment.ALL);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FeatureResult preInstallCheck() {
    FeatureResult result = new FeatureResult("Check group nonexistence");

    try (LdapConnection ldapConnection = new LdapConnection(AuthenticationUtil.getLdapHost(),
        AuthenticationUtil.getLdapPort(), AuthenticationUtil.getLdapUsername(), AuthenticationUtil.getLdapPassword(),
        ApplicationConstants.APPLICATION_NAME)) {

      boolean exists = false;

      exists |= ldapConnection.doesGroupExist(LdapGroup.VK_NOLOGIN.toString());
      exists |= ldapConnection.doesGroupExist(LdapGroup.VK_LOGIN.toString());
      exists |= ldapConnection.doesGroupExist(LdapGroup.VK_EXTERNAL.toString());
      exists |= ldapConnection.doesGroupExist(LdapGroup.VK_INTERESTED.toString());
      exists |= ldapConnection.doesGroupExist(LdapGroup.VK_INTERNAL.toString());
      exists |= ldapConnection.doesGroupExist(LdapGroup.VK_ADMIN.toString());

      if (exists) {
        result.addProblem(new Problem(this).setAction("Check nonexistence").setDescription("Groups exist"));
      }

    } catch (LdapException | IOException e) {
      result.addProblem(
          new Problem(this).setAction("LDAP Connection").setDescription(e.getLocalizedMessage()).setThrowable(e));
    }

    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FeatureResult performInstall() {
    FeatureResult result = new FeatureResult("Add groups");

    try (LdapConnection ldapConnection = new LdapConnection(AuthenticationUtil.getLdapHost(),
        AuthenticationUtil.getLdapPort(), AuthenticationUtil.getLdapUsername(), AuthenticationUtil.getLdapPassword(),
        ApplicationConstants.APPLICATION_NAME)) {

      ldapConnection.addGroup(LdapGroup.VK_NOLOGIN.toString());
      ldapConnection.addGroup(LdapGroup.VK_LOGIN.toString());
      ldapConnection.addGroup(LdapGroup.VK_EXTERNAL.toString());
      ldapConnection.addGroup(LdapGroup.VK_INTERESTED.toString());
      ldapConnection.addGroup(LdapGroup.VK_INTERNAL.toString());
      ldapConnection.addGroup(LdapGroup.VK_ADMIN.toString());

      ldapConnection.addGroupToGroup(LdapGroup.VK_EXTERNAL.toString(), LdapGroup.VK_LOGIN.toString());
      ldapConnection.addGroupToGroup(LdapGroup.VK_INTERESTED.toString(), LdapGroup.VK_LOGIN.toString());
      ldapConnection.addGroupToGroup(LdapGroup.VK_INTERNAL.toString(), LdapGroup.VK_LOGIN.toString());
      ldapConnection.addGroupToGroup(LdapGroup.VK_ADMIN.toString(), LdapGroup.VK_LOGIN.toString());

    } catch (LdapException | IOException e) {
      result.addProblem(
          new Problem(this).setAction("LDAP Connection").setDescription(e.getLocalizedMessage()).setThrowable(e));
    }

    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FeatureResult postInstallCheck() {
    FeatureResult result = new FeatureResult("Check group existence");

    try (LdapConnection ldapConnection = new LdapConnection(AuthenticationUtil.getLdapHost(),
        AuthenticationUtil.getLdapPort(), AuthenticationUtil.getLdapUsername(), AuthenticationUtil.getLdapPassword(),
        ApplicationConstants.APPLICATION_NAME)) {

      boolean exists = true;

      exists &= ldapConnection.doesGroupExist(LdapGroup.VK_NOLOGIN.toString());
      exists &= ldapConnection.doesGroupExist(LdapGroup.VK_LOGIN.toString());
      exists &= ldapConnection.doesGroupExist(LdapGroup.VK_EXTERNAL.toString());
      exists &= ldapConnection.doesGroupExist(LdapGroup.VK_INTERESTED.toString());
      exists &= ldapConnection.doesGroupExist(LdapGroup.VK_INTERNAL.toString());
      exists &= ldapConnection.doesGroupExist(LdapGroup.VK_ADMIN.toString());

      if (!exists) {
        result.addProblem(new Problem(this).setAction("Check existence").setDescription("Groups do not exist"));
      }

    } catch (LdapException | IOException e) {
      result.addProblem(
          new Problem(this).setAction("LDAP Connection").setDescription(e.getLocalizedMessage()).setThrowable(e));
    }

    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FeatureResult performUninstall() {
    FeatureResult result = new FeatureResult("Remove groups");

    try (LdapConnection ldapConnection = new LdapConnection(AuthenticationUtil.getLdapHost(),
        AuthenticationUtil.getLdapPort(), AuthenticationUtil.getLdapUsername(), AuthenticationUtil.getLdapPassword(),
        ApplicationConstants.APPLICATION_NAME)) {

      ldapConnection.removeGroup(LdapGroup.VK_NOLOGIN.toString());
      ldapConnection.removeGroup(LdapGroup.VK_LOGIN.toString());
      ldapConnection.removeGroup(LdapGroup.VK_EXTERNAL.toString());
      ldapConnection.removeGroup(LdapGroup.VK_INTERESTED.toString());
      ldapConnection.removeGroup(LdapGroup.VK_INTERNAL.toString());
      ldapConnection.removeGroup(LdapGroup.VK_ADMIN.toString());

    } catch (LdapException | IOException e) {
      result.addProblem(
          new Problem(this).setAction("LDAP Connection").setDescription(e.getLocalizedMessage()).setThrowable(e));
    }

    return result;
  }

}
