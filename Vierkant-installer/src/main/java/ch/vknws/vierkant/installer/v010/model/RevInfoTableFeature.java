package ch.vknws.vierkant.installer.v010.model;

import java.sql.Connection;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.installer.feature.database.AbstractTableFeature;
import ch.bwe.fac.v1.installer.feature.database.ColumnFeatureImpl;
import ch.vknws.vierkant.installer.Versions;

/**
 * List for training course instance teachers.
 *
 * @author Benjamin Weber
 */
public class RevInfoTableFeature extends AbstractTableFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 RevInfo";
  private static final String TABLE_NAME = "REVINFO";

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param connection the open DB connection.
   */
  public RevInfoTableFeature(Connection connection) {
    super(Versions.v0_1_0, ACTION, connection, Environment.ALL);

    name(TABLE_NAME);

    column(new ColumnFeatureImpl(getVersion(), ACTION + " rev", connection, getAllowedEnvironments()).name("rev")
        .type(Long.class).primaryKey().tableName(TABLE_NAME));
    column(new ColumnFeatureImpl(getVersion(), ACTION + " revtstmp", connection, getAllowedEnvironments())
        .name("revtstmp").type(Long.class).tableName(TABLE_NAME));
  }
}
