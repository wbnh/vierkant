package ch.vknws.vierkant.installer.v010.data;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.installer.feature.database.AbstractDatabaseDataFeature;
import ch.vknws.vierkant.installer.Versions;

/**
 * Installs rank data on the db.
 * 
 * @author Benjamin Weber
 */
public class MeetingTypeData010DataFeature extends AbstractDatabaseDataFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 MeetingTypeData";

  private static List<AbstractDatabasePersistable> data = new LinkedList<>();

  static {
    data.add(DefaultMeetingTypeData.generalAssembly);
    data.add(DefaultMeetingTypeData.boardOfDirectorsMeeting);
  }

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param connection the manager for the DB entries
   */
  public MeetingTypeData010DataFeature(EntityManager connection) {
    super(Versions.v0_1_0, ACTION, connection, data, Environment.ALL);
  }
}
