package ch.vknws.vierkant.installer.v010.data;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.installer.feature.database.AbstractDatabaseDataFeature;
import ch.vknws.vierkant.installer.Versions;
import ch.vknws.vierkant.type.AuthorizationKeyModel;
import ch.vknws.vierkant.type.SexType;
import ch.vknws.vierkant.type.impl.AddressPersistable;
import ch.vknws.vierkant.type.impl.AuthorizationKeyPersistable;
import ch.vknws.vierkant.type.impl.PersonPersistable;

/**
 * Feature to install the administrator person.
 *
 * @author Benjamin Weber
 */
public class AdminPersonDataFeature extends AbstractDatabaseDataFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 Admin";

  private static final List<AbstractDatabasePersistable> persistables = new LinkedList<>();

  static {
    AuthorizationKeyPersistable authKey = new AuthorizationKeyPersistable();
    Set<AuthorizationKeyModel> refSubordinates = authKey.getRefSubordinates();
    refSubordinates.add(DefaultAuthorizationKeys.admin);
    authKey.setRefSubordinates(refSubordinates);
    authKey.setCreator(0);
    authKey.setModifier(0);
    authKey.setDeletedFlag(false);
    authKey.setAuditComment("Added during installation");
    persistables.add(authKey);

    PersonPersistable admin = new PersonPersistable();
    persistables.add(admin);
    admin.setShorthand("adm");
    admin.setGivenName("Administrator");
    admin.setFamilyName("Vierkant");
    admin.setEmailOrganization("benjamin.n.weber@gmail.com");
    admin.setRefAuthorizationKey(authKey);
    admin.setSex(SexType.NON_HUMAN);
    admin.setCreator(0);
    admin.setModifier(0);
    admin.setDeletedFlag(false);
    admin.setAuditComment("Added during installation");

    AddressPersistable address = new AddressPersistable();
    admin.setRefHomeAddress(address);
    address.setLine1("Wassergrabenstrasse 19");
    address.setZipCode("4102");
    address.setCity("Binningen");
    address.setCanton("BL");
    address.setCountry("CH");
  }

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param connection the manager of the DB entries
   */
  public AdminPersonDataFeature(EntityManager connection) {
    super(Versions.v0_1_0, ACTION, connection, persistables, Environment.ALL);
  }
}
