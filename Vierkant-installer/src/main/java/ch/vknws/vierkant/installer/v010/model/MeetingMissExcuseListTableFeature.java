package ch.vknws.vierkant.installer.v010.model;

import java.sql.Connection;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.installer.feature.database.AbstractTableFeature;
import ch.bwe.fac.v1.installer.feature.database.ColumnFeatureImpl;
import ch.vknws.vierkant.installer.Versions;

/**
 * List for training course instance teachers.
 *
 * @author Benjamin Weber
 */
public class MeetingMissExcuseListTableFeature extends AbstractTableFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 MeetingMissExcuseList";
  private static final String TABLE_NAME = "MeetingMissExcuseList";

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param connection the DB connection
   */
  public MeetingMissExcuseListTableFeature(Connection connection) {
    super(Versions.v0_1_0, ACTION, connection, Environment.ALL);

    createHistory();
    name(TABLE_NAME);

    addAuditFields(true);
    column(new ColumnFeatureImpl(getVersion(), ACTION + " refMeetingRecordId", connection, getAllowedEnvironments())
        .name("refMeetingRecordId").type(Integer.class).references("MeetingPersistable", "recordId").primaryKey()
        .tableName(TABLE_NAME));
    historyOnlyColumn(
        new ColumnFeatureImpl(getVersion(), ACTION + " refMeetingRevisionId", connection, getAllowedEnvironments())
            .name("refMeetingRevisionId").type(Integer.class).references("MeetingPersistable", "revisionId")
            .tableName(TABLE_NAME));
    column(new ColumnFeatureImpl(getVersion(), ACTION + " refMissExcuseRecordId", connection, getAllowedEnvironments())
        .name("refMissExcuseRecordId").type(Integer.class).primaryKey().references("MissExcusePersistable", "recordId")
        .tableName(TABLE_NAME));
    historyOnlyColumn(
        new ColumnFeatureImpl(getVersion(), ACTION + " refMissExcuseRevisionId", connection, getAllowedEnvironments())
            .name("refMissExcuseRevisionId").type(Integer.class).references("MissExcusePersistable", "revisionId")
            .tableName(TABLE_NAME));
  }
}
