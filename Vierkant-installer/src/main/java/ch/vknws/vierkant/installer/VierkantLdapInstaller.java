package ch.vknws.vierkant.installer;

import ch.bwe.faa.cli.parse.CommandLineParser;
import ch.bwe.faa.v1.core.util.Version;
import ch.vknws.vierkant.installer.v010.ldap.LdapGroupInstallFeature;
import ch.vknws.vierkant.installer.v010.ldap.LdapUserInstallFeature;

/**
 * Installer for LDAP.
 * 
 * @author Benjamin Weber
 */
public class VierkantLdapInstaller {

  /**
   * Modifies the environment.
   * 
   * @param args the arguments
   */
  public static void main(String[] args) {
    InstallerParameters parameters = new InstallerParameters();
    CommandLineParser.parse(args, parameters);

    LdapGroupInstallFeature group = new LdapGroupInstallFeature();
    LdapUserInstallFeature user = new LdapUserInstallFeature();

    Version version = null;
    if (parameters.version != null) {
      version = new Version(parameters.version);
    }

    switch (parameters.method) {
    case "install":
      group.install(version);
      user.install(version);
      break;
    case "uninstall":
      group.uninstall(version);
      user.uninstall(version);
      break;
    case "reinstall":
      user.uninstall(version);
      group.uninstall(version);
      group.install(version);
      user.install(version);
      break;
    default:
      break;
    }
  }
}
