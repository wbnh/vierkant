package ch.vknws.vierkant.installer.v010.model;

import java.sql.Connection;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.installer.feature.database.AbstractTableFeature;
import ch.bwe.fac.v1.installer.feature.database.ColumnFeatureImpl;
import ch.vknws.vierkant.installer.Versions;

/**
 * List for training course instance teachers.
 *
 * @author Benjamin Weber
 */
public class TrainingCourseInstanceParticipantList extends AbstractTableFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 TrainingCourseInstanceParticipantList";
  private static final String TABLE_NAME = "TrainingCourseInstanceParticipantList";

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param connection the DB connection
   */
  public TrainingCourseInstanceParticipantList(Connection connection) {
    super(Versions.v0_1_0, ACTION, connection, Environment.ALL);

    createHistory();
    name(TABLE_NAME);

    addAuditFields(true);
    column(new ColumnFeatureImpl(getVersion(), ACTION + " refPerticipantRecordId", connection, getAllowedEnvironments())
        .name("refParticipantRecordId").type(Integer.class).references("PersonPersistable", "recordId").primaryKey()
        .tableName(TABLE_NAME));
    historyOnlyColumn(
        new ColumnFeatureImpl(getVersion(), ACTION + " refParticipantRevisionId", connection, getAllowedEnvironments())
            .name("refParticipantRevisionId").type(Integer.class).references("PersonPersistable", "revisionId")
            .tableName(TABLE_NAME));
    column(new ColumnFeatureImpl(getVersion(), ACTION + " refTrainingCourseInstanceRecordId", connection,
        getAllowedEnvironments()).name("refTrainingCourseInstanceRecordId").type(Integer.class).primaryKey()
            .references("TrainingCourseInstancePersistable", "recordId").tableName(TABLE_NAME));
    historyOnlyColumn(new ColumnFeatureImpl(getVersion(), ACTION + " refTrainingCourseInstanceRevisionId", connection,
        getAllowedEnvironments()).name("refTrainingCoursInstanceRevisionId").type(Integer.class)
            .references("TrainingCourseInstance", "revisionId").tableName(TABLE_NAME));
  }
}
