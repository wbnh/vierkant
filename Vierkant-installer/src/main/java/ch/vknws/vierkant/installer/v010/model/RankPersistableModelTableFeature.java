package ch.vknws.vierkant.installer.v010.model;

import java.sql.Connection;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.installer.feature.database.AbstractModelTableFeature;
import ch.vknws.vierkant.installer.Versions;
import ch.vknws.vierkant.type.impl.RankPersistable;

/**
 * @author Benjamin Weber
 */
public class RankPersistableModelTableFeature extends AbstractModelTableFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 RankPersistable";

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param connection the DB connection
   */
  public RankPersistableModelTableFeature(Connection connection) {
    super(Versions.v0_1_0, ACTION, RankPersistable.class, connection, Environment.ALL);
  }

}
