package ch.vknws.vierkant.installer.v010.model;

import java.sql.Connection;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.installer.feature.database.AbstractTableFeature;
import ch.bwe.fac.v1.installer.feature.database.ColumnFeatureImpl;
import ch.vknws.vierkant.installer.Versions;

/**
 * Table for previous process tasks.
 *
 * @author Benjamin Weber
 */
public class ProcessTaskNextTableFeature extends AbstractTableFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 ProcessTaskNext";
  private static final String TABLE_NAME = "ProcessTaskNext";

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param connection the DB connection
   */
  public ProcessTaskNextTableFeature(Connection connection) {
    super(Versions.v0_1_0, ACTION, connection, Environment.ALL);

    createHistory();
    name(TABLE_NAME);

    addAuditFields();
    column(new ColumnFeatureImpl(getVersion(), ACTION + " refProcessTaskRecordId", connection, getAllowedEnvironments())
        .name("refProcessTaskRecordId").type(Integer.class).references("ProcessTaskPersistable", "recordId")
        .tableName(TABLE_NAME));
    historyOnlyColumn(
        new ColumnFeatureImpl(getVersion(), ACTION + "refProcessTaskRevisionId", connection, getAllowedEnvironments())
            .name("refProcessTaskRevisionId").type(Integer.class).references("ProcessTaskPersistable", "revisionId")
            .tableName(TABLE_NAME));
    column(new ColumnFeatureImpl(getVersion(), ACTION + " recordIdValue", connection, getAllowedEnvironments())
        .name("recordIdValue").type(Integer.class).tableName(TABLE_NAME));
    historyOnlyColumn(
        new ColumnFeatureImpl(getVersion(), ACTION + " revisionIdValue", connection, getAllowedEnvironments())
            .name("revisionIdValue").type(Integer.class).tableName(TABLE_NAME));
  }

}
