package ch.vknws.vierkant.installer.v010.data;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.installer.feature.database.AbstractDatabaseDataFeature;
import ch.vknws.vierkant.installer.Versions;
import ch.vknws.vierkant.type.RankCategory;
import ch.vknws.vierkant.type.impl.RankPersistable;

/**
 * Installs rank data on the db.
 * 
 * @author Benjamin Weber
 */
public class RankData010DataFeature extends AbstractDatabaseDataFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 RankData";
  private static final String AUDIT_COMMENT = "Bei der Installation der Version 0.1.0 erstellt.";

  private static List<AbstractDatabasePersistable> data = new LinkedList<>();

  static {
    int hierarchyIndex = 10;
    RankPersistable persistable;

    persistable = new RankPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setAbbreviation("Asp");
    persistable.setRankName("Aspirant");
    persistable.setDefaultFunction("VK-Anwärter");
    persistable.setHierarchyIndex(hierarchyIndex);
    persistable.setRankCategory(RankCategory.ASPIRANT);
    persistable.setMemberCountGoal(null);
    persistable.setShoulderStrapPicture(null);
    data.add(persistable);
    hierarchyIndex += 10;

    persistable = new RankPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setAbbreviation("VK");
    persistable.setRankName("Verkehrskadett");
    persistable.setDefaultFunction("Mannschaftsmitglied");
    persistable.setHierarchyIndex(hierarchyIndex);
    persistable.setRankCategory(RankCategory.ENLISTED);
    persistable.setMemberCountGoal(null);
    persistable.setShoulderStrapPicture(null);
    data.add(persistable);
    hierarchyIndex += 10;

    persistable = new RankPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setAbbreviation("Gfr");
    persistable.setRankName("Gefreiter");
    persistable.setDefaultFunction("bewährtes Mannschaftsmitglied");
    persistable.setHierarchyIndex(hierarchyIndex);
    persistable.setRankCategory(RankCategory.ENLISTED);
    persistable.setMemberCountGoal(null);
    persistable.setShoulderStrapPicture(null);
    data.add(persistable);
    hierarchyIndex += 10;

    persistable = new RankPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setAbbreviation("Obgfr");
    persistable.setRankName("Obergefreiter");
    persistable.setDefaultFunction("sehr bewährtes Mannschaftsmitglied");
    persistable.setHierarchyIndex(hierarchyIndex);
    persistable.setRankCategory(RankCategory.ENLISTED);
    persistable.setMemberCountGoal(null);
    persistable.setShoulderStrapPicture(null);
    data.add(persistable);
    hierarchyIndex += 10;

    persistable = new RankPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setAbbreviation("Kpl");
    persistable.setRankName("Korporal");
    persistable.setDefaultFunction("Gruppenführer");
    persistable.setHierarchyIndex(hierarchyIndex);
    persistable.setRankCategory(RankCategory.LOWER_NC_OFFICER);
    persistable.setMemberCountGoal(null);
    persistable.setShoulderStrapPicture(null);
    data.add(persistable);
    hierarchyIndex += 10;

    persistable = new RankPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setAbbreviation("Wm");
    persistable.setRankName("Wachtmeister");
    persistable.setDefaultFunction("bewährter Gruppenführer");
    persistable.setHierarchyIndex(hierarchyIndex);
    persistable.setRankCategory(RankCategory.LOWER_NC_OFFICER);
    persistable.setMemberCountGoal(null);
    persistable.setShoulderStrapPicture(null);
    data.add(persistable);
    hierarchyIndex += 10;

    persistable = new RankPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setAbbreviation("Fw");
    persistable.setRankName("Feldweibel");
    persistable.setDefaultFunction("Zugführer-Stellvertreter");
    persistable.setHierarchyIndex(hierarchyIndex);
    persistable.setRankCategory(RankCategory.UPPER_NC_OFFICER);
    persistable.setMemberCountGoal(null);
    persistable.setShoulderStrapPicture(null);
    data.add(persistable);
    hierarchyIndex += 10;

    persistable = new RankPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setAbbreviation("Hptfw");
    persistable.setRankName("Hauptfeldweibel");
    persistable.setDefaultFunction("bewährter Zugführer-Stellvertreter");
    persistable.setHierarchyIndex(hierarchyIndex);
    persistable.setRankCategory(RankCategory.UPPER_NC_OFFICER);
    persistable.setMemberCountGoal(null);
    persistable.setShoulderStrapPicture(null);
    data.add(persistable);
    hierarchyIndex += 10;

    persistable = new RankPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setAbbreviation("Adj Uof");
    persistable.setRankName("Adjutant Unteroffizier");
    persistable.setDefaultFunction("Sonderfunktion gemäss speziellen Befehls");
    persistable.setHierarchyIndex(hierarchyIndex);
    persistable.setRankCategory(RankCategory.UPPER_NC_OFFICER);
    persistable.setMemberCountGoal(null);
    persistable.setShoulderStrapPicture(null);
    data.add(persistable);
    hierarchyIndex += 10;

    persistable = new RankPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setAbbreviation("Stabsadj");
    persistable.setRankName("Stabsadjutant");
    persistable.setDefaultFunction("Dienstchef-Stellvertreter");
    persistable.setHierarchyIndex(hierarchyIndex);
    persistable.setRankCategory(RankCategory.UPPER_NC_OFFICER);
    persistable.setMemberCountGoal(null);
    persistable.setShoulderStrapPicture(null);
    data.add(persistable);
    hierarchyIndex += 10;

    persistable = new RankPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setAbbreviation("Hptadj");
    persistable.setRankName("Hauptadjutant");
    persistable.setDefaultFunction("bewährter Dienstchef-Stellvertreter");
    persistable.setHierarchyIndex(hierarchyIndex);
    persistable.setRankCategory(RankCategory.UPPER_NC_OFFICER);
    persistable.setMemberCountGoal(null);
    persistable.setShoulderStrapPicture(null);
    data.add(persistable);
    hierarchyIndex += 10;

    persistable = new RankPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setAbbreviation("Lt");
    persistable.setRankName("Leutnant");
    persistable.setDefaultFunction("Zugführer");
    persistable.setHierarchyIndex(hierarchyIndex);
    persistable.setRankCategory(RankCategory.SUBALTERN_OFFICER);
    persistable.setMemberCountGoal(null);
    persistable.setShoulderStrapPicture(null);
    data.add(persistable);
    hierarchyIndex += 10;

    persistable = new RankPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setAbbreviation("Oblt");
    persistable.setRankName("Oberleutnant");
    persistable.setDefaultFunction("bewährter Zugführer");
    persistable.setHierarchyIndex(hierarchyIndex);
    persistable.setRankCategory(RankCategory.SUBALTERN_OFFICER);
    persistable.setMemberCountGoal(null);
    persistable.setShoulderStrapPicture(null);
    data.add(persistable);
    hierarchyIndex += 10;

    persistable = new RankPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setAbbreviation("Hptm");
    persistable.setRankName("Hauptmann");
    persistable.setDefaultFunction("Vorstandsmitglied");
    persistable.setHierarchyIndex(hierarchyIndex);
    persistable.setRankCategory(RankCategory.STAFF_OFFICER);
    persistable.setMemberCountGoal(null);
    persistable.setShoulderStrapPicture(null);
    data.add(persistable);
    hierarchyIndex += 10;

    persistable = new RankPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setAbbreviation("Maj");
    persistable.setRankName("Major");
    persistable.setDefaultFunction("bewährtes Vorstandsmitglied");
    persistable.setHierarchyIndex(hierarchyIndex);
    persistable.setRankCategory(RankCategory.STAFF_OFFICER);
    persistable.setMemberCountGoal(null);
    persistable.setShoulderStrapPicture(null);
    data.add(persistable);
    hierarchyIndex += 10;

    persistable = new RankPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setAbbreviation("Oberstlt");
    persistable.setRankName("Oberstleutnant");
    persistable.setDefaultFunction("sehr bewährtes Vorstandsmitglied");
    persistable.setHierarchyIndex(hierarchyIndex);
    persistable.setRankCategory(RankCategory.STAFF_OFFICER);
    persistable.setMemberCountGoal(null);
    persistable.setShoulderStrapPicture(null);
    data.add(persistable);
    hierarchyIndex += 10;

    persistable = new RankPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setAbbreviation("Oberst");
    persistable.setRankName("Oberst");
    persistable.setDefaultFunction("Präsident");
    persistable.setHierarchyIndex(hierarchyIndex);
    persistable.setRankCategory(RankCategory.STAFF_OFFICER);
    persistable.setMemberCountGoal(null);
    persistable.setShoulderStrapPicture(null);
    data.add(persistable);
    hierarchyIndex += 10;
  }

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param connection the manager for the DB entries
   */
  public RankData010DataFeature(EntityManager connection) {
    super(Versions.v0_1_0, ACTION, connection, data, Environment.ALL);
  }
}
