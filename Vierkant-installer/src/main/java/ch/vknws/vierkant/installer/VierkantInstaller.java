package ch.vknws.vierkant.installer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import ch.bwe.faa.cli.parse.CommandLineParser;
import ch.bwe.faa.v1.core.util.Version;
import ch.vknws.vierkant.backend.configuration.EnvironmentConfigurations;
import ch.vknws.vierkant.installer.v010.Vierkant010DataFeatureList;
import ch.vknws.vierkant.installer.v010.Vierkant010StructureFeatureList;

/**
 * Runner for the installer for Vierkant.
 *
 * @author Benjamin Weber
 */
public class VierkantInstaller {

  /**
   * Modifies the environment.
   * 
   * @param args the arguments
   * @throws SQLException if we cannot connect to the database
   */
  public static void main(String[] args) throws SQLException {
    InstallerParameters parameters = new InstallerParameters();
    CommandLineParser.parse(args, parameters);

    String persistenceUnit = EnvironmentConfigurations.getPersistenceUnit();

    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(persistenceUnit);

    String url = (String) entityManagerFactory.getProperties().get("javax.persistence.jdbc.url");
    String user = parameters.user;
    String password = parameters.password;

    try (Connection connection = DriverManager.getConnection(url, user, password);) {
      connection.setAutoCommit(false);

      EntityManager entityManager = entityManagerFactory.createEntityManager();

      Vierkant010StructureFeatureList structureList = new Vierkant010StructureFeatureList(connection);
      Vierkant010DataFeatureList dataList = new Vierkant010DataFeatureList(entityManager);

      Version version = null;
      if (parameters.version != null) {
        version = new Version(parameters.version);
      }

      switch (parameters.method) {
      case InstallerParameters.METHOD_INSTALL:
        structureList.install(version);
        dataList.install(version);
        break;
      case InstallerParameters.METHOD_UNINSTALL:
        structureList.uninstall(version);
        dataList.uninstall(version);
        break;
      case InstallerParameters.METHOD_REINSTALL:
        dataList.uninstall(version);
        structureList.uninstall(version);
        structureList.install(version);
        dataList.install(version);
        break;
      default:
        break;
      }

      entityManager.close();
      connection.commit();
    }
  }
}
