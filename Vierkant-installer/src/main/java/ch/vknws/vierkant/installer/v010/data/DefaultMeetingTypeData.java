package ch.vknws.vierkant.installer.v010.data;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.vknws.vierkant.type.impl.MeetingTypePersistable;

/**
 * Default data for functions.
 *
 * @author Benjamin Weber
 */
class DefaultMeetingTypeData {
  private static final String AUDIT_COMMENT = "Bei der Installation der Version 0.1.0 erstellt.";

  static final MeetingTypePersistable generalAssembly = new MeetingTypePersistable();
  static final MeetingTypePersistable boardOfDirectorsMeeting = new MeetingTypePersistable();

  static {
    setAuditFields(generalAssembly);
    generalAssembly.setTypeName("Generalversammlung");
    generalAssembly.setDescription("An dieser Versammlung werden die Geschäfte des Vereins bestimmt.");

    setAuditFields(boardOfDirectorsMeeting);
    boardOfDirectorsMeeting.setTypeName("Vorstandssitzung");
    boardOfDirectorsMeeting.setDescription("An dieser Sitzung werden die Geschäfte während des Jahres besprochen.");
  }

  private static void setAuditFields(AbstractDatabasePersistable persistable) {
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
  }

}
