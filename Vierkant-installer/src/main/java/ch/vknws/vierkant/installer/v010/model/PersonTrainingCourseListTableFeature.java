package ch.vknws.vierkant.installer.v010.model;

import java.sql.Connection;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.installer.feature.database.AbstractTableFeature;
import ch.bwe.fac.v1.installer.feature.database.ColumnFeatureImpl;
import ch.vknws.vierkant.installer.Versions;

/**
 * Feature for list of training courses a person has completed.
 *
 * @author Benjamin Weber
 */
public class PersonTrainingCourseListTableFeature extends AbstractTableFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 PersonTrainingCourseList";
  private static final String TABLE_NAME = "PersonTrainingCourseList";

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param connection the DB connection
   */
  public PersonTrainingCourseListTableFeature(Connection connection) {
    super(Versions.v0_1_0, ACTION, connection, Environment.ALL);

    createHistory();
    name(TABLE_NAME);

    addAuditFields(true);
    column(new ColumnFeatureImpl(getVersion(), ACTION + " refPersonRecordId", connection, getAllowedEnvironments())
        .name("refPersonRecordId").type(Integer.class).references("PersonPersistable", "recordId").primaryKey()
        .tableName(TABLE_NAME));
    historyOnlyColumn(
        new ColumnFeatureImpl(getVersion(), ACTION + " refPersonRevisionId", connection, getAllowedEnvironments())
            .name("refPersonRevisionId").type(Integer.class).references("PersonPersistable", "revisionId")
            .tableName(TABLE_NAME));
    column(
        new ColumnFeatureImpl(getVersion(), ACTION + " refTrainingCourseRecordId", connection, getAllowedEnvironments())
            .name("refTrainingCourseRecordId").type(Integer.class).primaryKey()
            .references("TrainingCoursePersistable", "recordId").tableName(TABLE_NAME));
    historyOnlyColumn(new ColumnFeatureImpl(getVersion(), ACTION + " refTrainingCourseRevisionId", connection,
        getAllowedEnvironments()).name("refTrainingCourseRevisionId").type(Integer.class)
            .references("TrainingCoursePersistable", "revisionId").tableName(TABLE_NAME));
  }
}
