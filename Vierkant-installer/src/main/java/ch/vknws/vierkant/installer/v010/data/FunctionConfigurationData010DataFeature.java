package ch.vknws.vierkant.installer.v010.data;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.installer.feature.FeatureResult;
import ch.bwe.fac.v1.installer.feature.database.AbstractDatabaseDataFeature;
import ch.vknws.vierkant.installer.Versions;
import ch.vknws.vierkant.type.impl.ConfigurationPersistable;

/**
 * Feature to install configuration data.
 *
 * @author Benjamin Weber
 */
public class FunctionConfigurationData010DataFeature extends AbstractDatabaseDataFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 functionConfiguration";
  private static final String AUDIT_COMMENT = "Bei der Installation der Version 0.1.0 erstellt.";

  private List<AbstractDatabasePersistable> data = new LinkedList<>();

  private void initialiseData() {
    ConfigurationPersistable persistable = new ConfigurationPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setUserName("sys");
    persistable.setContext("functionGroups");
    persistable.setKeyName("boardOfDirectorsGroup");
    persistable.setValue(DefaultFunctionData.boardOfDirectorsGroup.getRecordId().toString());
    data.add(persistable);

    persistable = new ConfigurationPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setUserName("sys");
    persistable.setContext("functionGroups");
    persistable.setKeyName("aspPlatoonGroup");
    persistable.setValue(DefaultFunctionData.aspPlatoonGroup.getRecordId().toString());
    data.add(persistable);

    persistable = new ConfigurationPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setUserName("sys");
    persistable.setContext("functionGroups");
    persistable.setKeyName("vkPlatoonGroup");
    persistable.setValue(DefaultFunctionData.vkPlatoonGroup.getRecordId().toString());
    data.add(persistable);

    persistable = new ConfigurationPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setUserName("sys");
    persistable.setContext("functionGroups");
    persistable.setKeyName("fourGGroup");
    persistable.setValue(DefaultFunctionData.fourGGroup.getRecordId().toString());
    data.add(persistable);

    persistable = new ConfigurationPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setUserName("sys");
    persistable.setContext("functions");
    persistable.setKeyName("secretary");
    persistable.setValue(DefaultFunctionData.secretary.getRecordId().toString());
    data.add(persistable);
  }

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param connection the manager of the DB entries
   */
  public FunctionConfigurationData010DataFeature(EntityManager connection) {
    super(Versions.v0_1_0, ACTION, connection, null, Environment.ALL);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FeatureResult doPerformInstall() {
    initialiseData();
    setPersistables(data);
    return super.doPerformInstall();
  }
}
