package ch.vknws.vierkant.installer.v010.model;

import java.sql.Connection;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.installer.feature.database.AbstractTableFeature;
import ch.bwe.fac.v1.installer.feature.database.ColumnFeatureImpl;
import ch.vknws.vierkant.ApplicationConstants;
import ch.vknws.vierkant.installer.Versions;

/**
 * Feature for a prerequisite table for gt comparison.
 *
 * @author Benjamin Weber
 */
public class PrerequisiteNotNullTableFeature extends AbstractTableFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 PrerequisiteNotNull";
  private static final String TABLE_NAME = "PrerequisiteNotNull";

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param connection the DB connection
   */
  public PrerequisiteNotNullTableFeature(Connection connection) {
    super(Versions.v0_1_0, ACTION, connection, Environment.ALL);

    createHistory();
    name(TABLE_NAME);

    addAuditFields();
    column(new ColumnFeatureImpl(getVersion(), ACTION + " fieldName", connection, Environment.ALL).name("fieldName")
        .type(String.class).nonNull().size(ApplicationConstants.COMMENT_LENGTH).tableName(TABLE_NAME));
    column(new ColumnFeatureImpl(getVersion(), ACTION + " fieldValue", connection, Environment.ALL).name("fieldValue")
        .type(String.class).size(ApplicationConstants.COMMENT_LENGTH).tableName(TABLE_NAME));
    column(new ColumnFeatureImpl(getVersion(), ACTION + " refPrerequisiteRecordId", connection, Environment.ALL)
        .name("refPrerequisiteRecordId").type(Integer.class).tableName(TABLE_NAME)
        .references("PrerequisitePersistable", "recordId"));
    historyOnlyColumn(
        new ColumnFeatureImpl(getVersion(), ACTION + " refPrerequisiteRevisionId", connection, Environment.ALL)
            .name("refPrerequisiteRevisionId").type(Integer.class).tableName(TABLE_NAME)
            .references("PrerequisitePersistable", "revisionId"));
  }
}
