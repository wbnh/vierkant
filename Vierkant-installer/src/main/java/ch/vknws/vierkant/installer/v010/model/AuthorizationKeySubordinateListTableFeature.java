package ch.vknws.vierkant.installer.v010.model;

import java.sql.Connection;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.installer.feature.database.AbstractTableFeature;
import ch.bwe.fac.v1.installer.feature.database.ColumnFeatureImpl;
import ch.vknws.vierkant.installer.Versions;

/**
 * List for training course instance teachers.
 *
 * @author Benjamin Weber
 */
public class AuthorizationKeySubordinateListTableFeature extends AbstractTableFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 AuthorizationKeySubordinateList";
  private static final String TABLE_NAME = "AuthorizationKeySubordinateList";

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param connection the DB connection
   */
  public AuthorizationKeySubordinateListTableFeature(Connection connection) {
    super(Versions.v0_1_0, ACTION, connection, Environment.ALL);

    createHistory();
    name(TABLE_NAME);

    addAuditFields(true);
    column(
        new ColumnFeatureImpl(getVersion(), ACTION + " refAuthorizationRecordId", connection, getAllowedEnvironments())
            .name("refAuthorizationRecordId").primaryKey().type(Integer.class)
            .references("AuthorizationKeyPersistable", "recordId").tableName(TABLE_NAME));
    historyOnlyColumn(new ColumnFeatureImpl(getVersion(), ACTION + " refAuthorizationRevisionId", connection,
        getAllowedEnvironments()).name("refAuthorizationRevisionId").type(Integer.class)
            .references("AuthorizationKeyPersistable", "revisionId").tableName(TABLE_NAME));
    column(new ColumnFeatureImpl(getVersion(), ACTION + " refSubordinateRecordId", connection, getAllowedEnvironments())
        .name("refSubordinateRecordId").primaryKey().type(Integer.class)
        .references("AuthorizationKeyPersistable", "recordId").tableName(TABLE_NAME));
    historyOnlyColumn(
        new ColumnFeatureImpl(getVersion(), ACTION + " refSubordinateRevisionId", connection, getAllowedEnvironments())
            .name("refSubordinateRevisionId").type(Integer.class)
            .references("AuthorizationKeyPersistable", "revisionId").tableName(TABLE_NAME));
  }
}
