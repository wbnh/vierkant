package ch.vknws.vierkant.installer;

import ch.bwe.faa.cli.parse.Method;
import ch.bwe.faa.cli.parse.Parameter;

/**
 * Struct to hold the parameters passed to the installer.
 *
 * @author Benjamin Weber
 */
public class InstallerParameters {

  /**
   * Install the selected features.
   */
  public static final String METHOD_INSTALL = "install";

  /**
   * Uninstall the selected features.
   */
  public static final String METHOD_UNINSTALL = "uninstall";

  /**
   * Uninstall and install the selected features.
   */
  public static final String METHOD_REINSTALL = "reinstall";

  /**
   * The provided method.
   */
  @Method
  public String method;

  /**
   * The provided version.
   */
  @Parameter
  public String version;

  /**
   * The user to use for the DB connection.
   */
  @Parameter
  public String user;

  /**
   * The password to use for the DB connection.
   */
  @Parameter
  public String password;
}
