package ch.vknws.vierkant.installer.v010;

import javax.persistence.EntityManager;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.installer.feature.database.AbstractDatabaseFeatureList;
import ch.vknws.vierkant.installer.Versions;
import ch.vknws.vierkant.installer.v010.data.AdminPersonDataFeature;
import ch.vknws.vierkant.installer.v010.data.AuthorizationKey010DataFeature;
import ch.vknws.vierkant.installer.v010.data.ConfigurationData010DataFeature;
import ch.vknws.vierkant.installer.v010.data.FunctionConfigurationData010DataFeature;
import ch.vknws.vierkant.installer.v010.data.MeetingTypeConfigurationData010DataFeature;
import ch.vknws.vierkant.installer.v010.data.MeetingTypeData010DataFeature;
import ch.vknws.vierkant.installer.v010.data.OrganizationFunctionData010DataFeature;
import ch.vknws.vierkant.installer.v010.data.RankData010DataFeature;

/**
 * List for DB structure for version 0.1.0.
 *
 * @author Benjamin Weber
 */
public class Vierkant010DataFeatureList extends AbstractDatabaseFeatureList {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 Data";

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param entityManager the manager for the database entries
   */
  public Vierkant010DataFeatureList(EntityManager entityManager) {
    super(Versions.v0_1_0, ACTION, entityManager, Environment.ALL);

    init();
  }

  private void init() {
    addFeature(new ConfigurationData010DataFeature(entitymanager));
    addFeature(new AuthorizationKey010DataFeature(entitymanager));

    addFeature(new RankData010DataFeature(entitymanager));

    addFeature(new OrganizationFunctionData010DataFeature(entitymanager));
    addFeature(new FunctionConfigurationData010DataFeature(entitymanager));

    addFeature(new AdminPersonDataFeature(entitymanager));

    addFeature(new MeetingTypeData010DataFeature(entitymanager));
    addFeature(new MeetingTypeConfigurationData010DataFeature(entitymanager));
  }
}
