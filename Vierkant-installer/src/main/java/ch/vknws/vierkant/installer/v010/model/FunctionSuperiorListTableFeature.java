package ch.vknws.vierkant.installer.v010.model;

import java.sql.Connection;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.installer.feature.database.AbstractTableFeature;
import ch.bwe.fac.v1.installer.feature.database.ColumnFeatureImpl;
import ch.vknws.vierkant.installer.Versions;

/**
 * Feature for list of training courses a person has completed.
 *
 * @author Benjamin Weber
 */
public class FunctionSuperiorListTableFeature extends AbstractTableFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 FunctionSuperiorList";
  private static final String TABLE_NAME = "FunctionSuperiorList";

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param connection the DB connection
   */
  public FunctionSuperiorListTableFeature(Connection connection) {
    super(Versions.v0_1_0, ACTION, connection, Environment.ALL);

    createHistory();
    name(TABLE_NAME);

    addAuditFields(true);
    column(new ColumnFeatureImpl(getVersion(), ACTION + " organizationFunctionRecordId", connection,
        getAllowedEnvironments()).name("organizationFunctionRecordId").type(Integer.class).primaryKey()
            .references("OrganizationFunctionPersistable", "recordId").tableName(TABLE_NAME));
    historyOnlyColumn(new ColumnFeatureImpl(getVersion(), ACTION + " organizationFunctionRevisionId", connection,
        getAllowedEnvironments()).name("organizationFunctionRevisionId").type(Integer.class)
            .references("OrganizationFunctionPersistable", "revisionId").tableName(TABLE_NAME));
    column(
        new ColumnFeatureImpl(getVersion(), ACTION + " superiorFunctionRecordId", connection, getAllowedEnvironments())
            .name("superiorFunctionRecordId").type(Integer.class).primaryKey()
            .references("OrganizationFunctionPersistable", "recordId").tableName(TABLE_NAME));
    historyOnlyColumn(new ColumnFeatureImpl(getVersion(), ACTION + " superiorFunctionRevisionId", connection,
        getAllowedEnvironments()).name("superiorFunctionRevisionId").type(Integer.class)
            .references("OrganizationFunctionPersistable", "revisionId").tableName(TABLE_NAME));
  }
}
