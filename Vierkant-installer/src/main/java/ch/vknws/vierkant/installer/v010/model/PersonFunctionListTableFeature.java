package ch.vknws.vierkant.installer.v010.model;

import java.sql.Connection;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.installer.feature.database.AbstractTableFeature;
import ch.bwe.fac.v1.installer.feature.database.ColumnFeatureImpl;
import ch.vknws.vierkant.installer.Versions;

/**
 * Feature for list of training courses a person has completed.
 *
 * @author Benjamin Weber
 */
public class PersonFunctionListTableFeature extends AbstractTableFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 PersonFunctionList";
  private static final String TABLE_NAME = "PersonFunctionList";

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param connection the DB connection
   */
  public PersonFunctionListTableFeature(Connection connection) {
    super(Versions.v0_1_0, ACTION, connection, Environment.ALL);

    createHistory();
    name(TABLE_NAME);

    addAuditFields(true);
    column(new ColumnFeatureImpl(getVersion(), ACTION + " refPersonRecordId", connection, getAllowedEnvironments())
        .name("refPersonRecordId").type(Integer.class).references("PersonPersistable", "recordId").primaryKey()
        .tableName(TABLE_NAME));
    historyOnlyColumn(
        new ColumnFeatureImpl(getVersion(), ACTION + " refPersonRevisionId", connection, getAllowedEnvironments())
            .name("refPersonRevisionId").type(Integer.class).references("PersonPersistable", "revisionId")
            .tableName(TABLE_NAME));
    column(new ColumnFeatureImpl(getVersion(), ACTION + " refFunctionRecordId", connection, getAllowedEnvironments())
        .name("refFunctionRecordId").type(Integer.class).references("PersonFunctionPersistable", "recordId")
        .primaryKey().tableName(TABLE_NAME));
    historyOnlyColumn(
        new ColumnFeatureImpl(getVersion(), ACTION + " refFunctionRevisionId", connection, getAllowedEnvironments())
            .name("refFunctionRevisionId").type(Integer.class).references("PersonFunctionPersistable", "revisionId")
            .tableName(TABLE_NAME));
  }
}
