package ch.vknws.vierkant.installer.v010;

import java.sql.Connection;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.installer.feature.database.AbstractDatabaseFeatureList;
import ch.vknws.vierkant.installer.Versions;
import ch.vknws.vierkant.installer.v010.model.AargauLicensePersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.AargauPersonLicensePersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.AddressPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.AuthorizationKeyPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.AuthorizationKeySubordinateListTableFeature;
import ch.vknws.vierkant.installer.v010.model.ConfigurationPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.ContactPersonPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.FunctionGroupFunctionListTableFeature;
import ch.vknws.vierkant.installer.v010.model.FunctionGroupPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.FunctionPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.FunctionSuperiorListTableFeature;
import ch.vknws.vierkant.installer.v010.model.MedicalInformationPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.MeetingActualAttendeeListTableFeature;
import ch.vknws.vierkant.installer.v010.model.MeetingMissExcuseListTableFeature;
import ch.vknws.vierkant.installer.v010.model.MeetingPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.MeetingPlannedAttendeeListTableFeature;
import ch.vknws.vierkant.installer.v010.model.MeetingTypePersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.MemberGroupPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.MissExcusePersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.PersonContactPersonList;
import ch.vknws.vierkant.installer.v010.model.PersonFunctionListTableFeature;
import ch.vknws.vierkant.installer.v010.model.PersonFunctionPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.PersonPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.PersonTrainingCourseListTableFeature;
import ch.vknws.vierkant.installer.v010.model.PrerequisiteEqTableFeature;
import ch.vknws.vierkant.installer.v010.model.PrerequisiteGeTableFeature;
import ch.vknws.vierkant.installer.v010.model.PrerequisiteGtTableFeature;
import ch.vknws.vierkant.installer.v010.model.PrerequisiteIsNullTableFeature;
import ch.vknws.vierkant.installer.v010.model.PrerequisiteLeTableFeature;
import ch.vknws.vierkant.installer.v010.model.PrerequisiteLtTableFeature;
import ch.vknws.vierkant.installer.v010.model.PrerequisiteNotNullTableFeature;
import ch.vknws.vierkant.installer.v010.model.PrerequisitePersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.RankPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.RecordPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.RevInfoTableFeature;
import ch.vknws.vierkant.installer.v010.model.TechnicalEventPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.TractandumPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.TrainingCourseInstanceParticipantList;
import ch.vknws.vierkant.installer.v010.model.TrainingCourseInstancePersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.TrainingCourseInstanceTeacherList;
import ch.vknws.vierkant.installer.v010.model.TrainingCoursePersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.VoteEntryPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.VoteOptionGroupPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.VoteOptionPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.VoteResultPersistableModelTableFeature;
import ch.vknws.vierkant.installer.v010.model.VoteResultVoterListTableFeature;

/**
 * List for DB structure for version 0.1.0.
 *
 * @author Benjamin Weber
 */
public class Vierkant010StructureFeatureList extends AbstractDatabaseFeatureList {

  /**
   * The action we are performing.
   */
  public static final String ACTION = "0.1.0 Structure";

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param connection the DB connection to use
   */
  public Vierkant010StructureFeatureList(Connection connection) {
    super(Versions.v0_1_0, ACTION, connection, Environment.ALL);

    init();
  }

  private void init() {
    addFeature(new RevInfoTableFeature(getCurrentConnection()));

    addFeature(new ConfigurationPersistableModelTableFeature(getCurrentConnection()));
    addFeature(new AuthorizationKeyPersistableModelTableFeature(getCurrentConnection()));
    addFeature(new AuthorizationKeySubordinateListTableFeature(getCurrentConnection()));

    addFeature(new TechnicalEventPersistableModelTableFeature(getCurrentConnection()));

    addFeature(new AddressPersistableModelTableFeature(getCurrentConnection()));

    addFeature(new AargauLicensePersistableModelTableFeature(getCurrentConnection()));
    addFeature(new AargauPersonLicensePersistableModelTableFeature(getCurrentConnection()));

    addFeature(new PrerequisitePersistableModelTableFeature(getCurrentConnection()));
    addFeature(new PrerequisiteGtTableFeature(getCurrentConnection()));
    addFeature(new PrerequisiteGeTableFeature(getCurrentConnection()));
    addFeature(new PrerequisiteLeTableFeature(getCurrentConnection()));
    addFeature(new PrerequisiteLtTableFeature(getCurrentConnection()));
    addFeature(new PrerequisiteEqTableFeature(getCurrentConnection()));
    addFeature(new PrerequisiteIsNullTableFeature(getCurrentConnection()));
    addFeature(new PrerequisiteNotNullTableFeature(getCurrentConnection()));
    addFeature(new TrainingCoursePersistableModelTableFeature(getCurrentConnection()));

    addFeature(new RankPersistableModelTableFeature(getCurrentConnection()));

    addFeature(new MemberGroupPersistableModelTableFeature(getCurrentConnection()));
    addFeature(new MedicalInformationPersistableModelTableFeature(getCurrentConnection()));

    addFeature(new FunctionPersistableModelTableFeature(getCurrentConnection()));
    addFeature(new FunctionGroupPersistableModelTableFeature(getCurrentConnection()));
    addFeature(new FunctionGroupFunctionListTableFeature(getCurrentConnection()));
    addFeature(new FunctionSuperiorListTableFeature(getCurrentConnection()));
    addFeature(new PersonPersistableModelTableFeature(getCurrentConnection()));
    addFeature(new PersonFunctionPersistableModelTableFeature(getCurrentConnection()));
    addFeature(new PersonFunctionListTableFeature(getCurrentConnection()));
    addFeature(new ContactPersonPersistableModelTableFeature(getCurrentConnection()));
    addFeature(new PersonContactPersonList(getCurrentConnection()));

    addFeature(new PersonTrainingCourseListTableFeature(getCurrentConnection()));
    addFeature(new TrainingCourseInstancePersistableModelTableFeature(getCurrentConnection()));
    addFeature(new TrainingCourseInstanceTeacherList(getCurrentConnection()));
    addFeature(new TrainingCourseInstanceParticipantList(getCurrentConnection()));

    addFeature(new VoteOptionPersistableModelTableFeature(getCurrentConnection()));
    addFeature(new VoteOptionGroupPersistableModelTableFeature(getCurrentConnection()));
    addFeature(new VoteResultPersistableModelTableFeature(getCurrentConnection()));
    addFeature(new VoteEntryPersistableModelTableFeature(getCurrentConnection()));
    addFeature(new RecordPersistableModelTableFeature(getCurrentConnection()));
    addFeature(new TractandumPersistableModelTableFeature(getCurrentConnection()));
    addFeature(new MissExcusePersistableModelTableFeature(getCurrentConnection()));
    addFeature(new MeetingTypePersistableModelTableFeature(getCurrentConnection()));
    addFeature(new MeetingPersistableModelTableFeature(getCurrentConnection()));

    addFeature(new VoteResultVoterListTableFeature(getCurrentConnection()));
    addFeature(new MeetingPlannedAttendeeListTableFeature(getCurrentConnection()));
    addFeature(new MeetingActualAttendeeListTableFeature(getCurrentConnection()));
    addFeature(new MeetingMissExcuseListTableFeature(getCurrentConnection()));
  }
}
