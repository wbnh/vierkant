package ch.vknws.vierkant.installer.v010.model;

import java.sql.Connection;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.installer.feature.database.AbstractModelTableFeature;
import ch.vknws.vierkant.installer.Versions;
import ch.vknws.vierkant.type.impl.RecordPersistable;

/**
 * @author Benjamin Weber
 */
public class RecordPersistableModelTableFeature extends AbstractModelTableFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 RecordPersistable";

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param connection the DB connection
   */
  public RecordPersistableModelTableFeature(Connection connection) {
    super(Versions.v0_1_0, ACTION, RecordPersistable.class, connection, Environment.ALL);
  }

}
