package ch.vknws.vierkant.installer.v010.model;

import java.sql.Connection;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.installer.feature.database.AbstractTableFeature;
import ch.bwe.fac.v1.installer.feature.database.ColumnFeatureImpl;
import ch.vknws.vierkant.installer.Versions;

/**
 * List for training course instance teachers.
 *
 * @author Benjamin Weber
 */
public class VoteResultVoterListTableFeature extends AbstractTableFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 VoteResultVoterList";
  private static final String TABLE_NAME = "VoteResultVoterList";

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param connection the DB connection
   */
  public VoteResultVoterListTableFeature(Connection connection) {
    super(Versions.v0_1_0, ACTION, connection, Environment.ALL);

    createHistory();
    name(TABLE_NAME);

    addAuditFields(true);
    column(new ColumnFeatureImpl(getVersion(), ACTION + " refVoteResultRecordId", connection, getAllowedEnvironments())
        .name("refVoteResultRecordId").type(Integer.class).references("VoteResultPersistable", "recordId").primaryKey()
        .tableName(TABLE_NAME));
    historyOnlyColumn(
        new ColumnFeatureImpl(getVersion(), ACTION + " refVoteResultRevisionId", connection, getAllowedEnvironments())
            .name("refVoteResultRevisionId").type(Integer.class).references("VoteResultPersistable", "revisionId")
            .tableName(TABLE_NAME));
    column(new ColumnFeatureImpl(getVersion(), ACTION + " refVoterRecordId", connection, getAllowedEnvironments())
        .name("refVoterRecordId").type(Integer.class).primaryKey().references("PersonPersistable", "recordId")
        .tableName(TABLE_NAME));
    historyOnlyColumn(
        new ColumnFeatureImpl(getVersion(), ACTION + " refVoterRevisionId", connection, getAllowedEnvironments())
            .name("refVoterRevisionId").type(Integer.class).references("PersonPersistable", "revisionId")
            .tableName(TABLE_NAME));
  }
}
