package ch.vknws.vierkant.installer.v010.data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.Configuration;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.faa.v1.core.util.AssertUtils;
import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.vknws.vierkant.type.AuthorizationKeyMode;
import ch.vknws.vierkant.type.AuthorizationKeyModel;
import ch.vknws.vierkant.type.AuthorizationKeyType;
import ch.vknws.vierkant.type.impl.AuthorizationKeyPersistable;

/**
 * Default data for functions.
 *
 * @author Benjamin Weber
 */
class DefaultAuthorizationKeys {
  private static final String AUDIT_COMMENT = "Bei der Installation der Version 0.1.0 erstellt.";

  static Map<String, AuthorizationKeyPersistable> basicRights = new HashMap<>();
  static Set<AuthorizationKeyPersistable> functionRights = new HashSet<>();

  static final AuthorizationKeyPersistable boardOfDirectors = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable fourG = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable president = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable archiveManager = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable chiefAccountant = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable secretary = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable trainingChief = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable personnelChief = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable missionChief = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable materialChief = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable officerSpokesman = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable accountant = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable redactor = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable admin = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable driverChief = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable missionRecruiter = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable vehicleMaintainer = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable lieutenant = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable sergeant = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable enlisted = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable auditor = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable councillor = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable driver = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable instructor = new AuthorizationKeyPersistable();
  static final AuthorizationKeyPersistable commissionary = new AuthorizationKeyPersistable();

  static {
    loadAuthorizationKeys();

    AuthorizationKeyPersistable all = new AuthorizationKeyPersistable();
    setAuditFields(all);
    all.setKeyName("all");
    all.setRefSubordinates(getBasicRightsByKey("+write_ownData", "+write_meetingOwn"));

    AuthorizationKeyPersistable member = new AuthorizationKeyPersistable();
    setAuditFields(member);
    member.setKeyName("member");
    Set<AuthorizationKeyModel> memberSubordinates = getBasicRightsByKey("+read_trainingCourseAny", "+read_functionAny",
        "+read_rankAny");
    memberSubordinates.add(all);
    member.setRefSubordinates(memberSubordinates);

    AuthorizationKeyPersistable noFunction = new AuthorizationKeyPersistable();
    functionRights.add(noFunction);
    setAuditFields(noFunction);
    noFunction.setKeyName("noFunction");
    Set<AuthorizationKeyModel> noFunctionSubordinates = new HashSet<>();
    noFunctionSubordinates.add(member);
    noFunction.setRefSubordinates(noFunctionSubordinates);

    functionRights.add(enlisted);
    setAuditFields(enlisted);
    enlisted.setKeyName("enlisted");
    Set<AuthorizationKeyModel> enlistedSubordinates = new HashSet<>();
    enlistedSubordinates.add(member);
    enlisted.setRefSubordinates(enlistedSubordinates);

    functionRights.add(lieutenant);
    setAuditFields(lieutenant);
    lieutenant.setKeyName("lieutenant");
    Set<AuthorizationKeyModel> lieutenantSubordinates = getBasicRightsByKey("+write_personSub",
        "+write_trainingCourseInstanceOwn", "+write_classifiedMedicalInformationSub");
    lieutenantSubordinates.add(member);
    lieutenant.setRefSubordinates(lieutenantSubordinates);

    functionRights.add(sergeant);
    setAuditFields(sergeant);
    sergeant.setKeyName("sergeant");
    sergeant.setRefSubordinates(getBasicRightsByKey("-write_medicalInformationSub"));

    functionRights.add(instructor);
    setAuditFields(instructor);
    instructor.setKeyName("instructor");
    Set<AuthorizationKeyModel> instructorSubordinates = getBasicRightsByKey("+write_personSub",
        "+write_trainingCourseInstanceOwn", "+write_classifiedMedicalInformationSub");
    instructorSubordinates.add(member);
    instructor.setRefSubordinates(instructorSubordinates);

    functionRights.add(driver);
    setAuditFields(driver);
    driver.setKeyName("driver");
    Set<AuthorizationKeyModel> driverSubordinates = new HashSet<>();
    driverSubordinates.add(member);
    driver.setRefSubordinates(driverSubordinates);

    functionRights.add(driverChief);
    setAuditFields(driverChief);
    driverChief.setKeyName("driverChief");
    Set<AuthorizationKeyModel> driverChiefSubordinates = new HashSet<>();
    driverChiefSubordinates.add(member);
    driverChief.setRefSubordinates(driverChiefSubordinates);

    functionRights.add(redactor);
    setAuditFields(redactor);
    redactor.setKeyName("redactor");
    Set<AuthorizationKeyModel> redactorSubordinates = new HashSet<>();
    redactorSubordinates.add(member);
    driverChief.setRefSubordinates(redactorSubordinates);

    functionRights.add(admin);
    setAuditFields(admin);
    admin.setKeyName("admin");
    Set<AuthorizationKeyModel> adminSubordinates = getBasicRightsByKey("+write_admin", "+read_technicalEventsAny",
        "+write_personAny");
    adminSubordinates.add(member);
    admin.setRefSubordinates(adminSubordinates);

    functionRights.add(accountant);
    setAuditFields(accountant);
    accountant.setKeyName("accountant");
    Set<AuthorizationKeyModel> accountantSubordinates = new HashSet<>();
    accountantSubordinates.add(member);
    accountant.setRefSubordinates(accountantSubordinates);

    functionRights.add(missionRecruiter);
    setAuditFields(missionRecruiter);
    missionRecruiter.setKeyName("missionRecruiter");
    Set<AuthorizationKeyModel> missionRecruiterSubordinates = new HashSet<>();
    missionRecruiterSubordinates.add(member);
    missionRecruiter.setRefSubordinates(missionRecruiterSubordinates);

    functionRights.add(vehicleMaintainer);
    setAuditFields(vehicleMaintainer);
    vehicleMaintainer.setKeyName("vehicleMaintainer");
    Set<AuthorizationKeyModel> vehicleMaintainerSubordinates = new HashSet<>();
    vehicleMaintainerSubordinates.add(member);
    vehicleMaintainer.setRefSubordinates(vehicleMaintainerSubordinates);

    functionRights.add(archiveManager);
    setAuditFields(archiveManager);
    archiveManager.setKeyName("archiveManager");
    Set<AuthorizationKeyModel> archiveManagerSubordinates = new HashSet<>();
    archiveManagerSubordinates.add(member);
    archiveManager.setRefSubordinates(archiveManagerSubordinates);

    functionRights.add(chiefAccountant);
    setAuditFields(chiefAccountant);
    chiefAccountant.setKeyName("chiefAccountant");
    Set<AuthorizationKeyModel> chiefAccountantSubordinates = new HashSet<>();
    chiefAccountantSubordinates.add(member);
    chiefAccountant.setRefSubordinates(chiefAccountantSubordinates);

    functionRights.add(secretary);
    setAuditFields(secretary);
    secretary.setKeyName("secretary");
    Set<AuthorizationKeyModel> secretarySubordinates = getBasicRightsByKey("+write_personAny",
        "+write_trainingCourseAny", "+write_functionAny", "+write_rankAny", "+write_aargauLicenseAny",
        "+read_historyAny", "+write_classifiedMedicalInformationAny", "+write_meetingTypeAny", "+write_meetingAny");
    secretarySubordinates.add(member);
    secretary.setRefSubordinates(secretarySubordinates);

    functionRights.add(trainingChief);
    setAuditFields(trainingChief);
    trainingChief.setKeyName("trainingChief");
    Set<AuthorizationKeyModel> trainingChiefSubordinates = getBasicRightsByKey("+write_trainingCourseAny",
        "+write_trainingCourseInstanceAny");
    trainingChiefSubordinates.add(member);
    trainingChief.setRefSubordinates(trainingChiefSubordinates);

    functionRights.add(personnelChief);
    setAuditFields(personnelChief);
    personnelChief.setKeyName("personnelChief");
    Set<AuthorizationKeyModel> personnelChiefSubordinates = getBasicRightsByKey("+write_personSub");
    personnelChiefSubordinates.add(member);
    personnelChief.setRefSubordinates(personnelChiefSubordinates);

    functionRights.add(missionChief);
    setAuditFields(missionChief);
    missionChief.setKeyName("missionChief");
    Set<AuthorizationKeyModel> missionChiefSubordinates = new HashSet<>();
    missionChiefSubordinates.add(member);
    missionChief.setRefSubordinates(missionChiefSubordinates);

    functionRights.add(materialChief);
    setAuditFields(materialChief);
    materialChief.setKeyName("materialChief");
    Set<AuthorizationKeyModel> materialChiefSubordinates = new HashSet<>();
    materialChiefSubordinates.add(member);
    materialChief.setRefSubordinates(materialChiefSubordinates);

    functionRights.add(officerSpokesman);
    setAuditFields(officerSpokesman);
    officerSpokesman.setKeyName("officerSpokesman");
    Set<AuthorizationKeyModel> officerSpokesmanSubordinates = new HashSet<>();
    officerSpokesmanSubordinates.add(member);
    officerSpokesman.setRefSubordinates(officerSpokesmanSubordinates);

    functionRights.add(president);
    setAuditFields(president);
    president.setKeyName("president");
    Set<AuthorizationKeyModel> presidentSubordinates = new HashSet<>();
    presidentSubordinates.add(member);
    president.setRefSubordinates(presidentSubordinates);

    functionRights.add(boardOfDirectors);
    setAuditFields(boardOfDirectors);
    boardOfDirectors.setKeyName("boardOfDirectors");
    Set<AuthorizationKeyModel> boardOfDirectorsSubordinates = getBasicRightsByKey("+read_personAny",
        "+read_trainingCourseAny", "+read_trainingCourseInstanceAny", "+read_functionAny",
        "+read_classifiedMedicalInformationAny", "+read_rankAny", "+read_aargauLicenseAny");
    boardOfDirectorsSubordinates.add(member);
    boardOfDirectors.setRefSubordinates(boardOfDirectorsSubordinates);

    functionRights.add(fourG);
    setAuditFields(fourG);
    fourG.setKeyName("fourG");
    Set<AuthorizationKeyModel> fourGSubordinates = new HashSet<>();
    fourGSubordinates.add(member);
    fourG.setRefSubordinates(fourGSubordinates);

    functionRights.add(commissionary);
    setAuditFields(commissionary);
    commissionary.setKeyName("commissionary");
    Set<AuthorizationKeyModel> commissionarySubordinates = new HashSet<>();
    commissionarySubordinates.add(member);
    commissionary.setRefSubordinates(commissionarySubordinates);

    functionRights.add(councillor);
    setAuditFields(councillor);
    councillor.setKeyName("councillor");
    Set<AuthorizationKeyModel> councillorSubordinates = getBasicRightsByKey("+read_personSub");
    councillorSubordinates.add(member);
    councillor.setRefSubordinates(councillorSubordinates);

    functionRights.add(auditor);
    setAuditFields(auditor);
    auditor.setKeyName("auditor");
    Set<AuthorizationKeyModel> auditorSubordinates = new HashSet<>();
    auditorSubordinates.add(member);
    auditor.setRefSubordinates(auditorSubordinates);
  }

  private static Set<AuthorizationKeyModel> getBasicRightsByKey(String... keys) {
    Set<AuthorizationKeyModel> persistables = new HashSet<>();

    for (String key : keys) {
      persistables.add(basicRights.get(key));
    }

    return persistables;
  }

  private static void loadAuthorizationKeys() {
    Configuration configuration = ServiceRegistry.getConfigurationProxy()
        .getConfiguration(new ConfigurationProperties<>(DefaultAuthorizationKeys.class));

    Set<String> toInitialise = new HashSet<>(configuration.getKeys());
    int subordinateCount = 0;
    while (!toInitialise.isEmpty()) {
      Set<String> toHandle = new HashSet<>();
      for (Iterator<String> iterator = toInitialise.iterator(); iterator.hasNext();) {
        String string = iterator.next();
        List<String> stringList = configuration.getStringList(string);
        if ((subordinateCount == 0 && stringList == null)
            || (stringList != null && stringList.size() == subordinateCount)) {
          toHandle.add(string);
          iterator.remove();
        }
      }

      for (String key : toHandle) {

        initializeAuthorizationKey(key, configuration);
      }

      subordinateCount++;
    }
  }

  private static void initializeAuthorizationKey(String key, Configuration configuration) {
    if (isInitialized(key)) { return; }
    AuthorizationKeyPersistable persistable = createPersistableFromKey(key);
    basicRights.put(key, persistable);

    List<String> subordinateKeys = configuration.getStringList(key);
    if (subordinateKeys != null) {
      for (String subordinate : subordinateKeys) {
        if (!isInitialized(subordinate)) {
          initializeAuthorizationKey(subordinate, configuration);
        }
        Set<AuthorizationKeyModel> subordinates = persistable.getRefSubordinates();
        subordinates.add(basicRights.get(subordinate));
        persistable.setRefSubordinates(subordinates);
      }
    }
  }

  private static boolean isInitialized(String key) {
    return basicRights.containsKey(key);
  }

  private static AuthorizationKeyPersistable createPersistableFromKey(String key) {
    validateKey(key);

    AuthorizationKeyPersistable persistable = new AuthorizationKeyPersistable();
    setAuditFields(persistable);
    persistable.setKeyName(getNameFromKey(key));
    persistable.setMode(getModeFromKey(key));
    persistable.setType(getTypeFromKey(key));

    return persistable;
  }

  private static void validateKey(String key) {
    AssertUtils.notNull(key);
    AssertUtils.assertFalse(key.isEmpty(), "key must not be empty string");
    char type = key.charAt(0);
    String[] split = key.substring(1).split("_");
    AssertUtils.assertTrue(split.length == 2, "key must have a mode and a name separated by one _");
    String mode = split[0];
    String name = split[1];

    AssertUtils.assertTrue('+' == type || '-' == type || '0' == type,
        "type must be +, -, or 0, but was " + type + "for key " + key);
    AuthorizationKeyMode.valueOf(mode.toUpperCase());
    AssertUtils.assertFalse(name.isEmpty(), "name must not be empty string");
  }

  private static String getNameFromKey(String key) {
    return key.split("_")[1];
  }

  private static AuthorizationKeyMode getModeFromKey(String key) {
    return AuthorizationKeyMode.valueOf(key.substring(1).split("_")[0].toUpperCase());
  }

  private static AuthorizationKeyType getTypeFromKey(String key) {
    switch (key.charAt(0)) {
    case '+':
      return AuthorizationKeyType.ALLOW;
    case '-':
      return AuthorizationKeyType.DENY;
    case '0':
      return AuthorizationKeyType.NORMAL;
    }
    throw new IllegalArgumentException("invalid type");
  }

  private static void setAuditFields(AbstractDatabasePersistable persistable) {
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
  }

}
