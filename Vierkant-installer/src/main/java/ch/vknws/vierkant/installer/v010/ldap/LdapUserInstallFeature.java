package ch.vknws.vierkant.installer.v010.ldap;

import java.io.IOException;

import org.apache.directory.api.ldap.model.exception.LdapException;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.installer.feature.AbstractFeature;
import ch.bwe.fac.v1.installer.feature.FeatureResult;
import ch.bwe.fac.v1.installer.feature.FeatureResult.Problem;
import ch.bwe.fae.type.LdapConnection;
import ch.vknws.vierkant.ApplicationConstants;
import ch.vknws.vierkant.backend.ldap.AuthenticationUtil;
import ch.vknws.vierkant.installer.Versions;

/**
 * Feature to install the necessary LDAP groups.
 *
 * @author Benjamin Weber
 */
public class LdapUserInstallFeature extends AbstractFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 LDAP user";

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public LdapUserInstallFeature() {
    super(Versions.v0_1_0, ACTION, Environment.ALL);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FeatureResult preInstallCheck() {
    FeatureResult result = new FeatureResult("Check group nonexistence");

    try (LdapConnection ldapConnection = new LdapConnection(AuthenticationUtil.getLdapHost(),
        AuthenticationUtil.getLdapPort(), AuthenticationUtil.getLdapUsername(), AuthenticationUtil.getLdapPassword(),
        ApplicationConstants.APPLICATION_NAME)) {

      boolean exists = false;

      exists |= ldapConnection.doesUserExist("adm");

      if (exists) {
        result.addProblem(new Problem(this).setAction("Check nonexistence").setDescription("Person exists"));
      }

    } catch (LdapException | IOException e) {
      result.addProblem(
          new Problem(this).setAction("LDAP Connection").setDescription(e.getLocalizedMessage()).setThrowable(e));
    }

    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FeatureResult performInstall() {
    FeatureResult result = new FeatureResult("Add groups");

    try (LdapConnection ldapConnection = new LdapConnection(AuthenticationUtil.getLdapHost(),
        AuthenticationUtil.getLdapPort(), AuthenticationUtil.getLdapUsername(), AuthenticationUtil.getLdapPassword(),
        ApplicationConstants.APPLICATION_NAME)) {

      ldapConnection.addPerson("adm", "vk-nws", "vk_admin", "vk_internal");

    } catch (LdapException | IOException e) {
      result.addProblem(
          new Problem(this).setAction("LDAP Connection").setDescription(e.getLocalizedMessage()).setThrowable(e));
    }

    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FeatureResult postInstallCheck() {
    FeatureResult result = new FeatureResult("Check group existence");

    try (LdapConnection ldapConnection = new LdapConnection(AuthenticationUtil.getLdapHost(),
        AuthenticationUtil.getLdapPort(), AuthenticationUtil.getLdapUsername(), AuthenticationUtil.getLdapPassword(),
        ApplicationConstants.APPLICATION_NAME)) {

      boolean exists = true;

      exists &= ldapConnection.doesUserExist("adm");

      if (!exists) {
        result.addProblem(new Problem(this).setAction("Check existence").setDescription("Person does not exist"));
      }

    } catch (LdapException | IOException e) {
      result.addProblem(
          new Problem(this).setAction("LDAP Connection").setDescription(e.getLocalizedMessage()).setThrowable(e));
    }

    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected FeatureResult performUninstall() {
    FeatureResult result = new FeatureResult("Remove groups");

    try (LdapConnection ldapConnection = new LdapConnection(AuthenticationUtil.getLdapHost(),
        AuthenticationUtil.getLdapPort(), AuthenticationUtil.getLdapUsername(), AuthenticationUtil.getLdapPassword(),
        ApplicationConstants.APPLICATION_NAME)) {

      ldapConnection.removePerson("adm");

    } catch (LdapException | IOException e) {
      result.addProblem(
          new Problem(this).setAction("LDAP Connection").setDescription(e.getLocalizedMessage()).setThrowable(e));
    }

    return result;
  }

}
