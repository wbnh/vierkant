package ch.vknws.vierkant.installer;

import ch.bwe.faa.v1.core.util.Version;

/**
 * Different versions for the installer.
 *
 * @author Benjamin Weber
 */
public interface Versions {

  /**
   * Development version of the Secretary module.
   */
  public static final Version v0_1_0 = new Version("0.1.0");
}
