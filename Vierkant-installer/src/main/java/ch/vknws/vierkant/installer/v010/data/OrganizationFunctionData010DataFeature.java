package ch.vknws.vierkant.installer.v010.data;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.installer.feature.database.AbstractDatabaseDataFeature;
import ch.vknws.vierkant.installer.Versions;

/**
 * Installs rank data on the db.
 * 
 * @author Benjamin Weber
 */
public class OrganizationFunctionData010DataFeature extends AbstractDatabaseDataFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 FunctionData";

  private static List<AbstractDatabasePersistable> data = new LinkedList<>();

  static {
    // Groups
    data.add(DefaultFunctionData.boardOfDirectors);
    data.add(DefaultFunctionData.boardOfDirectorsGroup);
    data.add(DefaultFunctionData.aspPlatoon);
    data.add(DefaultFunctionData.aspPlatoonGroup);
    data.add(DefaultFunctionData.vkPlatoon);
    data.add(DefaultFunctionData.vkPlatoonGroup);
    data.add(DefaultFunctionData.fourG);
    data.add(DefaultFunctionData.fourGGroup);

    // Functions
    data.add(DefaultFunctionData.president);
    data.add(DefaultFunctionData.archiveManager);
    data.add(DefaultFunctionData.chiefAccountant);
    data.add(DefaultFunctionData.secretary);
    data.add(DefaultFunctionData.trainingChief);
    data.add(DefaultFunctionData.personnelChief);
    data.add(DefaultFunctionData.missionChief);
    data.add(DefaultFunctionData.materialChief);
    data.add(DefaultFunctionData.officerSpokesman);
    data.add(DefaultFunctionData.accountant);
    data.add(DefaultFunctionData.admin);
    data.add(DefaultFunctionData.redactor);
    data.add(DefaultFunctionData.trainingChiefSub);
    data.add(DefaultFunctionData.personnelChiefSub);
    data.add(DefaultFunctionData.missionChiefSub);
    data.add(DefaultFunctionData.materialChiefSub);
    data.add(DefaultFunctionData.driverChief);
    data.add(DefaultFunctionData.missionRecruiter);
    data.add(DefaultFunctionData.vehicleMaintainer);
    data.add(DefaultFunctionData.lieutenantVk);
    data.add(DefaultFunctionData.lieutenantAsp);
    data.add(DefaultFunctionData.lieutenantVkSub);
    data.add(DefaultFunctionData.lieutenantAspSub);
    data.add(DefaultFunctionData.sergeantVk);
    data.add(DefaultFunctionData.sergeantAsp);
    data.add(DefaultFunctionData.enlistedVk);
    data.add(DefaultFunctionData.enlistedAsp);
    data.add(DefaultFunctionData.auditor);
    data.add(DefaultFunctionData.substituteAuditor);
    data.add(DefaultFunctionData.councillor);
    data.add(DefaultFunctionData.driver);
    data.add(DefaultFunctionData.instructor);
    data.add(DefaultFunctionData.commissionary);

  }

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param connection the manager of the DB entries
   */
  public OrganizationFunctionData010DataFeature(EntityManager connection) {
    super(Versions.v0_1_0, ACTION, connection, data, Environment.ALL);
  }
}
