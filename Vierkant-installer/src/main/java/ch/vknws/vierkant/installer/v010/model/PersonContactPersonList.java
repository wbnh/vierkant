package ch.vknws.vierkant.installer.v010.model;

import java.sql.Connection;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.fac.v1.installer.feature.database.AbstractTableFeature;
import ch.bwe.fac.v1.installer.feature.database.ColumnFeatureImpl;
import ch.vknws.vierkant.installer.Versions;

/**
 * List for training course instance teachers.
 *
 * @author Benjamin Weber
 */
public class PersonContactPersonList extends AbstractTableFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 PersonContactPersonList";
  private static final String TABLE_NAME = "PersonContactPersonList";

  /**
   * Constructor handling initialization of mandatory fields.
   * 
   * @param connection the DB connection
   */
  public PersonContactPersonList(Connection connection) {
    super(Versions.v0_1_0, ACTION, connection, Environment.ALL);

    createHistory();
    name(TABLE_NAME);

    addAuditFields(true);
    column(new ColumnFeatureImpl(getVersion(), ACTION + " refPersonRecordId", connection, getAllowedEnvironments())
        .name("refPersonRecordId").type(Integer.class).references("PersonPersistable", "recordId").primaryKey()
        .tableName(TABLE_NAME));
    historyOnlyColumn(
        new ColumnFeatureImpl(getVersion(), ACTION + " refPersonRevisionId", connection, getAllowedEnvironments())
            .name("refPersonRevisionId").type(Integer.class).references("PersonPersistable", "revisionId")
            .tableName(TABLE_NAME));
    column(
        new ColumnFeatureImpl(getVersion(), ACTION + " refContactPersonRecordId", connection, getAllowedEnvironments())
            .name("refContactPersonRecordId").type(Integer.class).primaryKey()
            .references("ContactPersonPersistable", "recordId").tableName(TABLE_NAME));
    historyOnlyColumn(new ColumnFeatureImpl(getVersion(), ACTION + " refContactPersonRevisionId", connection,
        getAllowedEnvironments()).name("refContactPersonRevisionId").type(Integer.class)
            .references("ContactPersonPersistable", "revisionId").tableName(TABLE_NAME));
  }
}
