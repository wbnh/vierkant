package ch.vknws.vierkant.installer.v010.data;

import java.util.HashSet;
import java.util.Set;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.vknws.vierkant.type.FunctionGroupModel;
import ch.vknws.vierkant.type.FunctionType;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.impl.FunctionGroupPersistable;
import ch.vknws.vierkant.type.impl.OrganizationFunctionPersistable;

/**
 * Default data for functions.
 *
 * @author Benjamin Weber
 *
 */
class DefaultFunctionData {
	private static final String AUDIT_COMMENT = "Bei der Installation der Version 0.1.0 erstellt.";

	static final FunctionGroupPersistable boardOfDirectorsGroup = new FunctionGroupPersistable();
	static final FunctionGroupPersistable aspPlatoonGroup = new FunctionGroupPersistable();
	static final FunctionGroupPersistable vkPlatoonGroup = new FunctionGroupPersistable();
	static final FunctionGroupPersistable fourGGroup = new FunctionGroupPersistable();

	static final OrganizationFunctionPersistable boardOfDirectors = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable aspPlatoon = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable vkPlatoon = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable fourG = new OrganizationFunctionPersistable();

	static final OrganizationFunctionPersistable president = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable archiveManager = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable chiefAccountant = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable secretary = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable trainingChief = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable personnelChief = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable missionChief = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable materialChief = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable officerSpokesman = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable accountant = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable redactor = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable admin = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable trainingChiefSub = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable personnelChiefSub = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable missionChiefSub = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable materialChiefSub = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable driverChief = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable missionRecruiter = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable vehicleMaintainer = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable lieutenantVk = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable lieutenantAsp = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable lieutenantVkSub = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable lieutenantAspSub = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable sergeantVk = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable sergeantAsp = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable enlistedVk = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable enlistedAsp = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable auditor = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable substituteAuditor = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable councillor = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable driver = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable instructor = new OrganizationFunctionPersistable();
	static final OrganizationFunctionPersistable commissionary = new OrganizationFunctionPersistable();

	static {
		setAuditFields(boardOfDirectorsGroup);
		boardOfDirectorsGroup.setRefGroupMembers(new HashSet<>());
		boardOfDirectorsGroup.setRefDetails(boardOfDirectors);
		boardOfDirectorsGroup.setRefAuthorizationKey(DefaultAuthorizationKeys.boardOfDirectors);

		setAuditFields(aspPlatoonGroup);
		aspPlatoonGroup.setRefGroupMembers(new HashSet<>());
		aspPlatoonGroup.setRefDetails(aspPlatoon);

		setAuditFields(vkPlatoonGroup);
		vkPlatoonGroup.setRefGroupMembers(new HashSet<>());
		vkPlatoonGroup.setRefDetails(vkPlatoon);

		setAuditFields(fourGGroup);
		fourGGroup.setRefGroupMembers(new HashSet<>());
		fourGGroup.setRefDetails(fourG);
		fourGGroup.setRefAuthorizationKey(DefaultAuthorizationKeys.fourG);

		setAuditFields(boardOfDirectors);
		boardOfDirectors.setFunctionName("Vorstand");
		boardOfDirectors.setEmail("vorstand@vk-nws.ch");
		boardOfDirectors.setType(FunctionType.NORMAL);

		setAuditFields(aspPlatoon);
		aspPlatoon.setFunctionName("Asp-Zug");
		aspPlatoon.setEmail(null);
		aspPlatoon.setType(FunctionType.NORMAL);

		setAuditFields(vkPlatoon);
		vkPlatoon.setFunctionName("VK-Zug");
		vkPlatoon.setEmail(null);
		vkPlatoon.setType(FunctionType.NORMAL);

		setAuditFields(fourG);
		fourG.setFunctionName("Vierergremium");
		fourG.setEmail("4g@vk-nws.ch");
		fourG.setType(FunctionType.STAFF);

		setAuditFields(president);
		president.setFunctionName("Präsident");
		president.setEmail("praesident@vk-nws.ch");
		president.setType(FunctionType.STAFF);
		president.setMaxPersonCount(1);
		president.setMinPersonCount(1);
		Set<FunctionGroupModel> presidentGroups = new HashSet<>();
		presidentGroups.add(boardOfDirectorsGroup);
		president.setRefGroups(presidentGroups);
		Set<OrganizationFunctionModel> presidentSuperiors = new HashSet<>();
		presidentSuperiors.add(boardOfDirectors);
		president.setRefSuperiors(presidentSuperiors);
		president.setRefAuthorizationKey(DefaultAuthorizationKeys.president);

		setAuditFields(archiveManager);
		archiveManager.setFunctionName("Archivar");
		archiveManager.setEmail("archivar@vk-nws.ch");
		archiveManager.setType(FunctionType.NORMAL);
		archiveManager.setMaxPersonCount(1);
		archiveManager.setMinPersonCount(0);
		Set<FunctionGroupModel> archiveManagerGroups = new HashSet<>();
		archiveManagerGroups.add(boardOfDirectorsGroup);
		archiveManager.setRefGroups(archiveManagerGroups);
		Set<OrganizationFunctionModel> archiveManagerSuperiors = new HashSet<>();
		archiveManagerSuperiors.add(boardOfDirectors);
		archiveManager.setRefSuperiors(archiveManagerSuperiors);
		archiveManager.setRefAuthorizationKey(DefaultAuthorizationKeys.archiveManager);

		setAuditFields(chiefAccountant);
		chiefAccountant.setFunctionName("Kassier");
		chiefAccountant.setEmail("kassier@vk-nws.ch");
		chiefAccountant.setType(FunctionType.NORMAL);
		chiefAccountant.setMaxPersonCount(1);
		chiefAccountant.setMinPersonCount(1);
		Set<FunctionGroupModel> chiefAccountantGroups = new HashSet<>();
		chiefAccountantGroups.add(boardOfDirectorsGroup);
		chiefAccountant.setRefGroups(chiefAccountantGroups);
		Set<OrganizationFunctionModel> chiefAccountantSuperiors = new HashSet<>();
		chiefAccountantSuperiors.add(boardOfDirectors);
		chiefAccountant.setRefSuperiors(chiefAccountantSuperiors);
		chiefAccountant.setRefAuthorizationKey(DefaultAuthorizationKeys.chiefAccountant);

		setAuditFields(secretary);
		secretary.setFunctionName("Sekretär");
		secretary.setEmail("sekretaer@vk-nws.ch");
		secretary.setType(FunctionType.NORMAL);
		secretary.setMaxPersonCount(1);
		secretary.setMinPersonCount(1);
		Set<FunctionGroupModel> secretaryGroups = new HashSet<>();
		secretaryGroups.add(boardOfDirectorsGroup);
		secretary.setRefGroups(secretaryGroups);
		Set<OrganizationFunctionModel> secretarySuperiors = new HashSet<>();
		secretarySuperiors.add(boardOfDirectors);
		secretary.setRefSuperiors(secretarySuperiors);
		secretary.setRefAuthorizationKey(DefaultAuthorizationKeys.secretary);

		setAuditFields(trainingChief);
		trainingChief.setFunctionName("DC Ausbildung");
		trainingChief.setEmail("ausbildung@vk-nws.ch");
		trainingChief.setType(FunctionType.NORMAL);
		trainingChief.setMaxPersonCount(1);
		trainingChief.setMinPersonCount(0);
		Set<FunctionGroupModel> trainingChiefGroups = new HashSet<>();
		trainingChiefGroups.add(boardOfDirectorsGroup);
		trainingChiefGroups.add(fourGGroup);
		trainingChief.setRefGroups(trainingChiefGroups);
		Set<OrganizationFunctionModel> trainingChiefSuperiors = new HashSet<>();
		trainingChiefSuperiors.add(boardOfDirectors);
		trainingChief.setRefSuperiors(trainingChiefSuperiors);
		trainingChief.setRefAuthorizationKey(DefaultAuthorizationKeys.trainingChief);

		setAuditFields(personnelChief);
		personnelChief.setFunctionName("DC Abteilung");
		personnelChief.setEmail("abteilung@vk-nws.ch");
		personnelChief.setType(FunctionType.NORMAL);
		personnelChief.setMaxPersonCount(1);
		personnelChief.setMinPersonCount(0);
		Set<FunctionGroupModel> personnelChiefGroups = new HashSet<>();
		personnelChiefGroups.add(boardOfDirectorsGroup);
		personnelChiefGroups.add(fourGGroup);
		personnelChief.setRefGroups(personnelChiefGroups);
		Set<OrganizationFunctionModel> personnelChiefSuperiors = new HashSet<>();
		personnelChiefSuperiors.add(boardOfDirectors);
		personnelChief.setRefSuperiors(personnelChiefSuperiors);
		personnelChief.setRefAuthorizationKey(DefaultAuthorizationKeys.personnelChief);

		setAuditFields(missionChief);
		missionChief.setFunctionName("DC Einsatz");
		missionChief.setEmail("einstatz@vk-nws.ch");
		missionChief.setType(FunctionType.NORMAL);
		missionChief.setMaxPersonCount(1);
		missionChief.setMinPersonCount(0);
		Set<FunctionGroupModel> missionChiefGroups = new HashSet<>();
		missionChiefGroups.add(boardOfDirectorsGroup);
		missionChiefGroups.add(fourGGroup);
		missionChief.setRefGroups(missionChiefGroups);
		Set<OrganizationFunctionModel> missionChiefSuperiors = new HashSet<>();
		missionChiefSuperiors.add(boardOfDirectors);
		missionChief.setRefSuperiors(missionChiefSuperiors);
		missionChief.setRefAuthorizationKey(DefaultAuthorizationKeys.missionChief);

		setAuditFields(materialChief);
		materialChief.setFunctionName("DC Material");
		materialChief.setEmail("material@vk-nws.ch");
		materialChief.setType(FunctionType.NORMAL);
		materialChief.setMaxPersonCount(1);
		materialChief.setMinPersonCount(0);
		Set<FunctionGroupModel> materialChiefGroups = new HashSet<>();
		materialChiefGroups.add(boardOfDirectorsGroup);
		materialChiefGroups.add(fourGGroup);
		materialChief.setRefGroups(materialChiefGroups);
		Set<OrganizationFunctionModel> materialChiefSuperiors = new HashSet<>();
		materialChiefSuperiors.add(boardOfDirectors);
		materialChief.setRefSuperiors(materialChiefSuperiors);
		materialChief.setRefAuthorizationKey(DefaultAuthorizationKeys.materialChief);

		setAuditFields(officerSpokesman);
		officerSpokesman.setFunctionName("Kadervertreter");
		officerSpokesman.setEmail("kadervertreter@vk-nws.ch");
		officerSpokesman.setType(FunctionType.NORMAL);
		officerSpokesman.setMaxPersonCount(1);
		officerSpokesman.setMinPersonCount(1);
		Set<FunctionGroupModel> officerSpokesmanGroups = new HashSet<>();
		officerSpokesmanGroups.add(boardOfDirectorsGroup);
		officerSpokesman.setRefGroups(officerSpokesmanGroups);
		Set<OrganizationFunctionModel> officerSpokesmanSuperiors = new HashSet<>();
		officerSpokesmanSuperiors.add(boardOfDirectors);
		officerSpokesman.setRefSuperiors(officerSpokesmanSuperiors);
		officerSpokesman.setRefAuthorizationKey(DefaultAuthorizationKeys.officerSpokesman);

		setAuditFields(accountant);
		accountant.setFunctionName("Soldbuchhalter");
		accountant.setEmail(null);
		accountant.setType(FunctionType.NORMAL);
		accountant.setMaxPersonCount(null);
		accountant.setMinPersonCount(null);
		Set<OrganizationFunctionModel> accountantSuperiors = new HashSet<>();
		accountantSuperiors.add(chiefAccountant);
		accountant.setRefSuperiors(accountantSuperiors);
		accountant.setRefAuthorizationKey(DefaultAuthorizationKeys.accountant);

		setAuditFields(redactor);
		redactor.setFunctionName("Redaktor");
		redactor.setEmail("redaktion@vk-nws.ch");
		redactor.setType(FunctionType.NORMAL);
		redactor.setMaxPersonCount(null);
		redactor.setMinPersonCount(null);
		Set<OrganizationFunctionModel> redactorSuperiors = new HashSet<>();
		redactorSuperiors.add(secretary);
		redactor.setRefSuperiors(redactorSuperiors);
		redactor.setRefAuthorizationKey(DefaultAuthorizationKeys.redactor);

		setAuditFields(admin);
		admin.setFunctionName("Administrator");
		admin.setEmail("administrator@vk-nws.ch");
		admin.setType(FunctionType.NORMAL);
		admin.setMaxPersonCount(null);
		admin.setMinPersonCount(null);
		Set<OrganizationFunctionModel> adminSuperiors = new HashSet<>();
		adminSuperiors.add(secretary);
		admin.setRefSuperiors(adminSuperiors);
		admin.setRefAuthorizationKey(DefaultAuthorizationKeys.admin);

		setAuditFields(trainingChiefSub);
		trainingChiefSub.setFunctionName("DC Ausbildung Stv.");
		trainingChiefSub.setType(FunctionType.SUBSTITUTE);
		trainingChiefSub.setMaxPersonCount(1);
		trainingChiefSub.setMinPersonCount(0);
		Set<OrganizationFunctionModel> trainingChiefSubSuperiors = new HashSet<>();
		trainingChiefSubSuperiors.add(trainingChief);
		trainingChiefSub.setRefSuperiors(trainingChiefSubSuperiors);

		setAuditFields(personnelChiefSub);
		personnelChiefSub.setAuditComment(AUDIT_COMMENT);
		personnelChiefSub.setFunctionName("DC Abteilung Stv.");
		personnelChiefSub.setType(FunctionType.SUBSTITUTE);
		personnelChiefSub.setMaxPersonCount(1);
		personnelChiefSub.setMinPersonCount(0);
		Set<OrganizationFunctionModel> personnelChiefSubSuperiors = new HashSet<>();
		personnelChiefSubSuperiors.add(personnelChief);
		personnelChiefSub.setRefSuperiors(personnelChiefSubSuperiors);

		setAuditFields(missionChiefSub);
		missionChiefSub.setAuditComment(AUDIT_COMMENT);
		missionChiefSub.setFunctionName("DC Einsatz Stv.");
		missionChiefSub.setType(FunctionType.SUBSTITUTE);
		missionChiefSub.setMaxPersonCount(1);
		missionChiefSub.setMinPersonCount(0);
		Set<OrganizationFunctionModel> missionChiefSubSuperiors = new HashSet<>();
		missionChiefSubSuperiors.add(missionChief);
		missionChiefSub.setRefSuperiors(missionChiefSubSuperiors);

		setAuditFields(materialChiefSub);
		materialChiefSub.setAuditComment(AUDIT_COMMENT);
		materialChiefSub.setFunctionName("DC Material Stv.");
		materialChiefSub.setType(FunctionType.SUBSTITUTE);
		materialChiefSub.setMaxPersonCount(1);
		materialChiefSub.setMinPersonCount(0);
		Set<OrganizationFunctionModel> materialChiefSubSuperiors = new HashSet<>();
		materialChiefSubSuperiors.add(materialChief);
		materialChiefSub.setRefSuperiors(materialChiefSubSuperiors);

		setAuditFields(driverChief);
		driverChief.setFunctionName("Fahrerchef");
		driverChief.setEmail(null);
		driverChief.setType(FunctionType.NORMAL);
		driverChief.setMaxPersonCount(1);
		driverChief.setMinPersonCount(0);
		Set<OrganizationFunctionModel> driverChiefSuperiors = new HashSet<>();
		driverChiefSuperiors.add(personnelChief);
		driverChief.setRefSuperiors(driverChiefSuperiors);
		driverChief.setRefAuthorizationKey(DefaultAuthorizationKeys.driverChief);

		setAuditFields(missionRecruiter);
		missionRecruiter.setFunctionName("Aufgebotsstelle");
		missionRecruiter.setEmail("aufgebotsstelle@vk-nws.ch");
		missionRecruiter.setType(FunctionType.NORMAL);
		missionRecruiter.setMaxPersonCount(null);
		missionRecruiter.setMinPersonCount(null);
		Set<OrganizationFunctionModel> missionRecruiterSuperiors = new HashSet<>();
		missionRecruiterSuperiors.add(missionChief);
		missionRecruiter.setRefSuperiors(missionRecruiterSuperiors);
		missionRecruiter.setRefAuthorizationKey(DefaultAuthorizationKeys.missionRecruiter);

		setAuditFields(vehicleMaintainer);
		vehicleMaintainer.setFunctionName("Fahrzeugwart");
		vehicleMaintainer.setEmail(null);
		vehicleMaintainer.setType(FunctionType.NORMAL);
		vehicleMaintainer.setMaxPersonCount(null);
		vehicleMaintainer.setMinPersonCount(null);
		Set<OrganizationFunctionModel> vehicleMaintainerSuperiors = new HashSet<>();
		vehicleMaintainerSuperiors.add(materialChief);
		vehicleMaintainer.setRefSuperiors(vehicleMaintainerSuperiors);
		vehicleMaintainer.setRefAuthorizationKey(DefaultAuthorizationKeys.vehicleMaintainer);

		setAuditFields(lieutenantVk);
		lieutenantVk.setFunctionName("Zugführer VK-Zug");
		lieutenantVk.setRefGroups(new HashSet<>());
		lieutenantVk.getRefGroups().add(vkPlatoonGroup);
		lieutenantVk.setEmail(null);
		lieutenantVk.setType(FunctionType.NORMAL);
		lieutenantVk.setMaxPersonCount(1);
		lieutenantVk.setMinPersonCount(0);
		Set<OrganizationFunctionModel> lieutenantVkSuperiors = new HashSet<>();
		lieutenantVkSuperiors.add(personnelChief);
		lieutenantVkSuperiors.add(trainingChief);
		lieutenantVk.setRefSuperiors(lieutenantVkSuperiors);
		lieutenantVk.setRefAuthorizationKey(DefaultAuthorizationKeys.lieutenant);

		setAuditFields(lieutenantAsp);
		lieutenantAsp.setFunctionName("Zugführer Asp-Zug");
		lieutenantAsp.setRefGroups(new HashSet<>());
		lieutenantAsp.getRefGroups().add(aspPlatoonGroup);
		lieutenantAsp.setEmail(null);
		lieutenantAsp.setType(FunctionType.NORMAL);
		lieutenantAsp.setMaxPersonCount(1);
		lieutenantAsp.setMinPersonCount(0);
		Set<OrganizationFunctionModel> lieutenantAspSuperiors = new HashSet<>();
		lieutenantAspSuperiors.add(personnelChief);
		lieutenantAspSuperiors.add(trainingChief);
		lieutenantAsp.setRefSuperiors(lieutenantAspSuperiors);
		lieutenantAsp.setRefAuthorizationKey(DefaultAuthorizationKeys.lieutenant);

		setAuditFields(lieutenantVkSub);
		lieutenantVkSub.setFunctionName("Zugführer VK-Zug Stv.");
		lieutenantVkSub.setEmail(null);
		lieutenantVkSub.setType(FunctionType.SUBSTITUTE);
		lieutenantVkSub.setMaxPersonCount(1);
		lieutenantVkSub.setMinPersonCount(0);
		Set<OrganizationFunctionModel> lieutenantVkSubSuperiors = new HashSet<>();
		lieutenantVkSubSuperiors.add(lieutenantVk);
		lieutenantVkSub.setRefSuperiors(lieutenantVkSubSuperiors);

		setAuditFields(lieutenantAspSub);
		lieutenantAspSub.setFunctionName("Zugführer Asp-Zug Stv.");
		lieutenantAspSub.setEmail(null);
		lieutenantAspSub.setType(FunctionType.SUBSTITUTE);
		lieutenantAspSub.setMaxPersonCount(1);
		lieutenantAspSub.setMinPersonCount(0);
		Set<OrganizationFunctionModel> lieutenantAspSubSuperiors = new HashSet<>();
		lieutenantAspSubSuperiors.add(lieutenantAsp);
		lieutenantAspSub.setRefSuperiors(lieutenantAspSubSuperiors);

		setAuditFields(sergeantVk);
		sergeantVk.setFunctionName("Gruppenführer VK-Zug");
		sergeantVk.setEmail(null);
		sergeantVk.setType(FunctionType.STAFF);
		sergeantVk.setMaxPersonCount(null);
		sergeantVk.setMinPersonCount(null);
		Set<OrganizationFunctionModel> sergeantVkSuperiors = new HashSet<>();
		sergeantVkSuperiors.add(lieutenantVk);
		sergeantVk.setRefSuperiors(sergeantVkSuperiors);
		sergeantVk.setRefAuthorizationKey(DefaultAuthorizationKeys.sergeant);

		setAuditFields(sergeantAsp);
		sergeantAsp.setFunctionName("Gruppenführer Asp-Zug");
		sergeantAsp.setEmail(null);
		sergeantAsp.setType(FunctionType.STAFF);
		sergeantAsp.setMaxPersonCount(null);
		sergeantAsp.setMinPersonCount(null);
		Set<OrganizationFunctionModel> sergeantAspSuperiors = new HashSet<>();
		sergeantAspSuperiors.add(lieutenantAsp);
		sergeantAsp.setRefSuperiors(sergeantAspSuperiors);
		sergeantAsp.setRefAuthorizationKey(DefaultAuthorizationKeys.sergeant);

		setAuditFields(enlistedVk);
		enlistedVk.setFunctionName("Mannschaft VK-Zug");
		enlistedVk.setRefGroups(new HashSet<>());
		enlistedVk.getRefGroups().add(vkPlatoonGroup);
		enlistedVk.setEmail(null);
		enlistedVk.setType(FunctionType.NORMAL);
		enlistedVk.setMaxPersonCount(null);
		enlistedVk.setMinPersonCount(null);
		Set<OrganizationFunctionModel> enlistedVkSuperiors = new HashSet<>();
		enlistedVkSuperiors.add(lieutenantVk);
		enlistedVk.setRefSuperiors(enlistedVkSuperiors);
		enlistedVk.setRefAuthorizationKey(DefaultAuthorizationKeys.enlisted);

		setAuditFields(enlistedAsp);
		enlistedAsp.setFunctionName("Mannschaft Asp-Zug");
		enlistedAsp.setRefGroups(new HashSet<>());
		enlistedAsp.getRefGroups().add(aspPlatoonGroup);
		enlistedAsp.setEmail(null);
		enlistedAsp.setType(FunctionType.NORMAL);
		enlistedAsp.setMaxPersonCount(null);
		enlistedAsp.setMinPersonCount(null);
		Set<OrganizationFunctionModel> enlistedAspSuperiors = new HashSet<>();
		enlistedAspSuperiors.add(lieutenantAsp);
		enlistedAsp.setRefSuperiors(enlistedAspSuperiors);
		enlistedAsp.setRefAuthorizationKey(DefaultAuthorizationKeys.enlisted);

		setAuditFields(auditor);
		auditor.setFunctionName("Revisor");
		auditor.setEmail(null);
		auditor.setType(FunctionType.STAFF);
		auditor.setMaxPersonCount(2);
		auditor.setMinPersonCount(1);
		auditor.setRefAuthorizationKey(DefaultAuthorizationKeys.auditor);

		setAuditFields(substituteAuditor);
		substituteAuditor.setFunctionName("Ersatzrevisor");
		substituteAuditor.setEmail(null);
		substituteAuditor.setType(FunctionType.SUBSTITUTE);
		substituteAuditor.setMaxPersonCount(1);
		substituteAuditor.setMinPersonCount(1);
		Set<OrganizationFunctionModel> substituteAuditorSuperiors = new HashSet<>();
		substituteAuditorSuperiors.add(auditor);
		substituteAuditor.setRefSuperiors(substituteAuditorSuperiors);

		setAuditFields(councillor);
		councillor.setFunctionName("Beirat");
		councillor.setEmail("beirat@vk-nws.ch");
		councillor.setType(FunctionType.STAFF);
		councillor.setMaxPersonCount(3);
		councillor.setMinPersonCount(0);
		councillor.setRefAuthorizationKey(DefaultAuthorizationKeys.councillor);

		setAuditFields(driver);
		driver.setFunctionName("Fahrer");
		driver.setEmail(null);
		driver.setType(FunctionType.NORMAL);
		driver.setMaxPersonCount(null);
		driver.setMinPersonCount(null);
		Set<OrganizationFunctionModel> driverSuperiors = new HashSet<>();
		driverSuperiors.add(personnelChief);
		driverSuperiors.add(trainingChief);
		driver.setRefSuperiors(driverSuperiors);
		driver.setRefAuthorizationKey(DefaultAuthorizationKeys.driver);

		setAuditFields(instructor);
		instructor.setFunctionName("Instruktor");
		instructor.setEmail(null);
		instructor.setType(FunctionType.NORMAL);
		instructor.setMaxPersonCount(null);
		instructor.setMinPersonCount(null);
		HashSet<OrganizationFunctionModel> instructorSuperiors = new HashSet<>();
		instructorSuperiors.add(personnelChief);
		instructorSuperiors.add(trainingChief);
		instructor.setRefSuperiors(instructorSuperiors);
		instructor.setRefAuthorizationKey(DefaultAuthorizationKeys.instructor);

		setAuditFields(commissionary);
		commissionary.setFunctionName("Kommissionär");
		commissionary.setEmail(null);
		commissionary.setType(FunctionType.STAFF);
		commissionary.setMaxPersonCount(null);
		commissionary.setMinPersonCount(null);
		commissionary.setRefAuthorizationKey(DefaultAuthorizationKeys.commissionary);
	}

	private static void setAuditFields(AbstractDatabasePersistable persistable) {
		persistable.setCreator(0);
		persistable.setModifier(0);
		persistable.setDeletedFlag(false);
		persistable.setAuditComment(AUDIT_COMMENT);
	}

}
