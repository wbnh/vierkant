package ch.vknws.vierkant.installer.v010.data;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.Configuration;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.faa.v1.core.util.AssertUtils;
import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.installer.feature.database.AbstractDatabaseDataFeature;
import ch.vknws.vierkant.installer.Versions;
import ch.vknws.vierkant.type.impl.ConfigurationPersistable;

/**
 * Feature to install configuration data.
 *
 * @author Benjamin Weber
 */
public class ConfigurationData010DataFeature extends AbstractDatabaseDataFeature {

  /**
   * The human-readable action we perform in this feature.
   */
  public static final String ACTION = "0.1.0 Configuration";
  private static final String AUDIT_COMMENT = "Bei der Installation der Version 0.1.0 erstellt.";

  private static List<AbstractDatabasePersistable> data = new LinkedList<>();

  static {
    parseConfigurations();
  }

  private static void parseConfigurations() {
    data.clear();
    Configuration configuration = ServiceRegistry.getConfigurationProxy()
        .getConfiguration(new ConfigurationProperties<>(ConfigurationData010DataFeature.class));

    for (String key : configuration.getKeys()) {
      data.add(parseConfigLine(key, configuration.getStringValue(key)));
    }
  }

  private static ConfigurationPersistable parseConfigLine(String key, String value) {
    String[] split = key.split("/");
    AssertUtils.assertEquals(3, split.length);

    ConfigurationPersistable persistable = new ConfigurationPersistable();
    persistable.setCreator(0);
    persistable.setModifier(0);
    persistable.setDeletedFlag(false);
    persistable.setAuditComment(AUDIT_COMMENT);
    persistable.setUserName(split[0]);
    persistable.setContext(split[1]);
    persistable.setKeyName(split[2]);
    persistable.setValue(value);

    return persistable;
  }

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param connection the manager of the DB entries
   */
  public ConfigurationData010DataFeature(EntityManager connection) {
    super(Versions.v0_1_0, ACTION, connection, data, Environment.ALL);
  }
}
