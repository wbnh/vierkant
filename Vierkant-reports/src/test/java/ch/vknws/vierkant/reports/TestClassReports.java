package ch.vknws.vierkant.reports;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import ch.vknws.vierkant.type.PersonFunctionModel;
import ch.vknws.vierkant.type.impl.AddressPersistable;
import ch.vknws.vierkant.type.impl.OrganizationFunctionPersistable;
import ch.vknws.vierkant.type.impl.PersonFunctionPersistable;
import ch.vknws.vierkant.type.impl.PersonPersistable;

/**
 * TestClass for reports.
 *
 * @author Benjamin Weber
 */
public class TestClassReports {

  /**
   * Exports a letter
   * 
   * @throws Exception if a problem occurs
   */
  @Test
  public void testLetter() throws Exception {
    Path output = Paths.get("generated-reports/LetterTest.pdf");
    Files.deleteIfExists(output);

    ReportFactory.createLetter(createLetterSender(), createLetterRecipient(), "Mutation zum Aktivmitglied",
        "Besten Dank für Ihr Interesse an den Verkehrskadetten Nordwestschweiz. "
            + "Sie sind nun als InteressentIn registriert. Als InteressentIn haben Sie folgende Möglichkeiten:"
            + "<ul><li>Durch Mitteilung an den Sekretär den regelmässigen Erhalt der Vereinszeitschrift VK-NeWS beantragen.</li>"
            + "<li>Wenn Sie ein/e Jugendliche/r im Alter von 13 oder mehr Jahren und mit einer Körpergrösse von mind. 1.50m sind, können Sie sich für die nächste Grundausbildung anmelden.</li>"
            + "<li>Wenn Sie bereits eine erfolgreich absolvierte Grundausbildung vorweisen können, können Sie beim Vorstand die Aufnahme als Aktivmitglied beantragen.</li>"
            + "<li>Sie können beim Vorstand die Aufnahme als Einzelpassivmitglied beantragen.</li>"
            + "<li>Wenn Sie ein im gleichen Haushalt lebendes Paar sind, können Sie (inkl. Ihrer Kinder bis 16 Jahre \u2013 ausgenommen Aktivmitglieder) beim Vorstand die Aufnahme als Familienpassivmitglied beantragen.</li>"
            + "<li>Wenn Sie eine Gesellschaft, ein Verein, ein regionaler oder nationaler Verband, eine Behörde oder eine andere Institution sind, können Sie beim Vorstand die Aufnahme als Kollektivmitglied beantragen.</li></ul>"
            + "Weitere Informationen zu Verein und Mitgliedschaft finden Sie auf unsere Homepage oder auf Anfrage von mir."
            + "Bei Fragen stehe ich Ihnen gerne zur Verfügung.",
        "Lieber Max", "Liebe Grüsse", output);
  }

  private PersonPersistable createLetterSender() {
    PersonPersistable person = new PersonPersistable();

    person.setGivenName("Matthijs");
    person.setFamilyName("de Leeuw");
    person.setShorthand("MaLe");
    person.setMobile("079 111 11 11");

    PersonFunctionPersistable personFunction = new PersonFunctionPersistable();
    personFunction.setRefFunction(createFunction("Sekretär", "sekretaer@vk-nws.ch"));
    personFunction.getRefFunction().setRecordId(1);
    Set<PersonFunctionModel> functions = new HashSet<>();
    functions.add(personFunction);
    person.setRefFunctions(functions);
    person.setRefPrimaryFunction(personFunction.getRefFunction());

    return person;
  }

  private PersonPersistable createLetterRecipient() {
    PersonPersistable person = new PersonPersistable();

    person.setTitle("Herr");
    person.setGivenName("Max");
    person.setFamilyName("Mustermann");

    AddressPersistable address = createAddress("Musterstrasse 1", null, "1234", "Musterdorf", "BL", "CH");
    person.setRefHomeAddress(address);

    return person;
  }

  private AddressPersistable createAddress(String line1, String line2, String zip, String city, String canton,
      String country) {
    AddressPersistable address = new AddressPersistable();

    address.setLine1(line1);
    address.setLine2(line2);
    address.setZipCode(zip);
    address.setCity(city);
    address.setCanton(canton);
    address.setCountry(country);

    return address;
  }

  private OrganizationFunctionPersistable createFunction(String name, String email) {
    OrganizationFunctionPersistable function = new OrganizationFunctionPersistable();

    function.setFunctionName(name);
    function.setEmail(email);

    return function;
  }
}
