package ch.vknws.vierkant.reports;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.vknws.vierkant.type.OrganizationFunctionModel;
import ch.vknws.vierkant.type.PersonFunctionModel;
import ch.vknws.vierkant.type.PersonModel;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 * Factory to create reports.
 *
 * @author Benjamin Weber
 */
public class ReportFactory {

  /**
   * The internal path to the template of a letter.
   */
  public static final String LETTER_TEMPLATE = "/VKA_NWS/Letter.jasper";

  /**
   * Creates a new PDF containing a letter.
   * 
   * @param sender       the sender
   * @param recipient    the recipient
   * @param contentTitle the title of the letter
   * @param contentText  the content of the letter
   * @param greeting     the greeting line
   * @param signature    the signature and final line
   * @param destination  the destination where the PDF should be saved
   * @throws JRException if we could not create the letter
   * @throws IOException if we could not save the letter
   */
  public static void createLetter(PersonModel sender, PersonModel recipient, String contentTitle, String contentText,
      String greeting, String signature, Path destination) throws JRException, IOException {

    Map<String, Object> parameters = new HashMap<>();
    parameters.put("senderPrimaryFunction", getPersonPrimaryFunction(sender));
    parameters.put("senderName", getPersonName(sender));
    parameters.put("senderEmail", getPersonEmail(sender));
    parameters.put("senderMobileNumber", getPersonMobileNumber(sender));
    parameters.put("recipientTitle", getPersonTitle(recipient));
    parameters.put("recipientName", getPersonName(recipient));
    parameters.put("recipientAddress", getPersonAddress(recipient));
    parameters.put("contentTitle", contentTitle);
    parameters.put("contentText", contentText);
    parameters.put("recipientGreeting", greeting);
    parameters.put("signature", signature);
    parameters.put("senderShorthand", getPersonShorthand(sender));

    try (InputStream template = ReportFactory.class.getResourceAsStream(LETTER_TEMPLATE)) {

      JasperPrint report = JasperFillManager.fillReport(template, parameters, new JREmptyDataSource());

      if (Files.exists(destination)) {
        ServiceRegistry.getLogProxy().warn(ReportFactory.class, "File {0} already exists, overwriting...", null,
            destination);
        Files.delete(destination);
      }
      Files.createDirectories(destination.getParent());
      Files.createFile(destination);

      if (!Files.isWritable(destination)) {
        Files.setPosixFilePermissions(destination, PosixFilePermissions.fromString("rw"));
      }

      try (OutputStream outputStream = Files.newOutputStream(destination)) {
        JasperExportManager.exportReportToPdfStream(report, outputStream);
      }
    }
  }

  private static String getPersonPrimaryFunction(PersonModel person) {
    if (person == null || person.getRefPrimaryFunction() == null || person.getRefFunctions() == null
        || person.getRefFunctions().isEmpty()) { return ""; }

    Optional<PersonFunctionModel> primaryFunction = findPersonPrimaryFunction(person);

    if (!primaryFunction.isPresent()) { return ""; }

    return primaryFunction.get().getRefFunction().getFunctionName();
  }

  private static Optional<PersonFunctionModel> findPersonPrimaryFunction(PersonModel person) {
    if (person == null || person.getRefPrimaryFunction() == null || person.getRefFunctions() == null
        || person.getRefFunctions().isEmpty()) { return Optional.empty(); }

    OrganizationFunctionModel primaryFunction = person.getRefPrimaryFunction();
    Optional<PersonFunctionModel> primaryFunctionReference = person.getRefFunctions().stream()
        .filter(pfp -> pfp.getRefFunction() != null
            && Objects.equals(primaryFunction.getRecordId(), pfp.getRefFunction().getRecordId()))
        .findFirst();
    return primaryFunctionReference;
  }

  private static String getPersonName(PersonModel person) {
    if (person == null) { return ""; }

    return person.toString();
  }

  private static String getPersonEmail(PersonModel person) {
    if (person == null) { return ""; }

    Optional<PersonFunctionModel> primaryFunction = findPersonPrimaryFunction(person);

    String email;
    if (primaryFunction.isPresent()) {
      email = primaryFunction.get().getRefFunction().getEmail();
    } else {
      email = null;
    }

    if (email == null || email.isEmpty()) {
      person.getEmailOrganization();
    }

    if (email == null || email.isEmpty()) {
      email = person.getEmailPrivate();
    }

    if (email == null || email.isEmpty()) {
      email = "";
    }

    return email;
  }

  private static String getPersonMobileNumber(PersonModel person) {
    if (person == null) { return ""; }

    return person.getMobile();
  }

  private static String getPersonTitle(PersonModel person) {
    if (person == null) { return ""; }

    return person.getTitle();
  }

  private static String getPersonAddress(PersonModel person) {
    if (person == null || person.getRefHomeAddress() == null) { return ""; }

    return person.getRefHomeAddress().toString();
  }

  private static String getPersonShorthand(PersonModel person) {
    if (person == null) { return ""; }

    return person.getShorthand();
  }
}
